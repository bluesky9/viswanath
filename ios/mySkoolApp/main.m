/**
 * Copyright (c) 2015-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[]) {
  @autoreleasepool {
    NSArray *families = [[UIFont familyNames] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    NSMutableString *fonts = [NSMutableString string];
    for (int i = 0; i < [families count]; i++) {
      [fonts appendString:[NSString stringWithFormat:@"\n%@:\n", families[i]]];
      NSArray *names = [UIFont fontNamesForFamilyName:families[i]];
      for (int j = 0; j < [names count]; j++) {
        [fonts appendString:[NSString stringWithFormat:@"\t%@\n", names[j]]];
      }
    }
    NSLog(@"%@", fonts);
    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
  }
}
