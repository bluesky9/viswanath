## Initial Setup
1. Clone the repo.
2. Do npm install.
5. run command `npm run start:ios` (For iOS).
5. run command `npm run start:android` (For android).

## Release Android on App store
1. Create Android Build
    a) npm run build
    b) react-native run-android --variant=release
    c) apk would be created under project location project_path/android/app/build/outputs/apk/app-release
2. Upload on App store
    a) Login to Google play console
    b) You'll see two apps listed. Select com.uple
    c) Click on Release Management from the list and select App releases
    d) From there on you can select the release you want and just upload the apk and provide the release docs

## If using the latest version of pods > 1.4.0
pod _1.4.0_ install
