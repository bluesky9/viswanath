import React, { Component } from 'react';
import { View } from 'react-native';
import Footer from './components/layouts/footer/footer';
import { NavigationActions } from 'react-navigation';
import LoginRouter from './components/login/login-router';
import { setNavigator } from './NavigationService';
import MSALoader from './components/layouts/loader/loader';
import MSAModalError from './components/layouts/modal/modal-error';
import MSAModalAction from './components/layouts/modal/modal-action';
import MSAModalReport from './components/layouts/modal/modal-report';
import MSAModalConnect from './components/layouts/modal/modal-connection-request';
import MSAModalImage from './components/layouts/modal/modal-image';
import MSAModalFollowDepartment from './components/layouts/modal/modal-follow-department';
import Drawer from './components/layouts/drawer/drawer';
import { setUserId } from './store/actions/Profile';
import { DrawerActions } from './store/actions';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

let navigator;

interface ILoginScreenProps {
  getNavigator?: Function;
  token?: any;
  dialogType?: string;
  userId;
  setUserId;
  drawerActions;
  message: any;
}
interface ILoginScreenState {
  selected: number;
  footerVisible: boolean;
  footArrat: Array<string>;
}
class AppWithFooter extends Component<
  ILoginScreenProps,
  ILoginScreenState
> {
  constructor(props) {
    super(props);
    this.state = {
      selected: 0,
      footerVisible: false,
      footArrat: [
        'HomeFeed',
        'PeopleScreen',
        'Messaging',
        'Notifications',
        'University'
      ]
    };
  }
  componentDidMount() {
    setNavigator(navigator);
    if (this.props.userId) {
      this.props.setUserId(parseInt(this.props.userId, 10));
    }
    if (this.props.token) {
      console.log(this.props.token, 'Check Token')
      this.resetLoginStack(0);
    }
  }
  resetLoginStack(i) {
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName: this.state.footArrat[i]
        })
      ]
    });
    navigator.dispatch(resetAction);
  }
  handleFooterPress(index) {
    this.props.drawerActions.drawerClosed()
    this.resetLoginStack(index);
    this.setState({
      selected: index
    });
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <LoginRouter
          ref={nav => {
            navigator = nav;
            // this.props.getNavigator(navigator);
          }}
          screenProps={navigator}
          onNavigationStateChange={(prevState, nextState) => {
            let found = false;
            if (prevState) {
              // Ignore PrevState use for now
            }
            if (nextState.routes) {
              console.log(nextState.routes, 'Next State Routes')
              this.state.footArrat.map(item => {
                if (
                  nextState.routes[nextState.routes.length - 1].routeName ===
                  item
                ) {
                  if (item === 'PeopleScreen') {
                    this.setState({ selected:  1})   // Need to reset stack too
                  } else if (item === 'Messaging') {
                    this.setState({ selected:  2}) 
                  } else if (item === 'Notifications') {
                    this.setState({ selected:  3}) 
                  }
                  found = true;
                }
              });
              this.setState({
                footerVisible: found ? true : false
              });
            }
          }}
        />
        <Footer
          visible={this.state.footerVisible}
          index={this.state.selected}
          pressCall={ind => this.handleFooterPress(ind)}
        />
        <Drawer navigation={navigator ? navigator._navigation : undefined }/>
        { this.props.dialogType === 'inform' && <MSAModalError />}
        { this.props.dialogType === 'action' && <MSAModalAction />}
        { this.props.dialogType === 'report' && <MSAModalReport />}
        { this.props.dialogType === 'connect' && <MSAModalConnect peopleId={this.props.message}/>}
        { this.props.dialogType === 'image' && <MSAModalImage />}
        { this.props.dialogType === 'followDepartment' && <MSAModalFollowDepartment />}
        <MSALoader />
      </View>
    );
  }
}

const mapStateToProps = ({ errorModal }) => {
  // Use message as peopleId for connect modal
  return {
    dialogType: errorModal.dialogType,
    message: errorModal.message
  };
}
const mapDispatchToProps = dispatch => ({
  setUserId: bindActionCreators(setUserId, dispatch),
  drawerActions: bindActionCreators(DrawerActions, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(AppWithFooter);
