import * as types from '../constants/ActionTypes'
//
// export const addTodo = text => ({ type: types.ADD_TODO, text })
// export const deleteTodo = id => ({ type: types.DELETE_TODO, id })
// export const editTodo = (id, text) => ({ type: types.EDIT_TODO, id, text })
// export const completeTodo = id => ({ type: types.COMPLETE_TODO, id })
// export const completeAll = () => ({ type: types.COMPLETE_ALL })
// export const clearCompleted = () => ({ type: types.CLEAR_COMPLETED })
import * as Login from './Login'
import * as Signup from './Signup'
import * as ForgotPassword from './ForgotPassword'
import * as ResetPassword from './ResetPassword'
import * as People from './People'
import * as Countries from './Countries'
import * as Universities from './Universities'
import * as Feed from './Feed'
import * as Loader from './Loader'
import * as Profile from './Profile'
import * as Dialog from './ErrorModal'
import * as Drawer from './Drawer'
import * as Invite from './Invite'
import * as Notifications from './Notifications'
export const LoginApi = Login;
export const SignupApi = Signup;
export const ForgotPasswordApi = ForgotPassword;
export const ResetPasswordApi = ResetPassword;
export const PeopleApi = People;
export const CountriesApi = Countries;
export const UniversitiesApi = Universities;
export const FeedApi = Feed;
export const LoaderApi = Loader;
export const ProfileApi = Profile;
export const DialogModal = Dialog;
export const DrawerActions = Drawer;
export const InviteActions = Invite;
export const NotificationsActions = Notifications;
