import { ErrorModalConstants } from '../constants'

export const dialogShows = (title,message,dialogType,closable,onConfirm,onCancel,instructionMessage,button,images, index) => { return { type: ErrorModalConstants.DIALOG_SHOWS,title,message,dialogType,closable,onConfirm,onCancel,instructionMessage,button,images, index} }
export const dialogHides = () => { return { type: ErrorModalConstants.DIALOG_HIDES} }
