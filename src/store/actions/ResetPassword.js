import {request} from '../request'
import {ResetPasswordConstants} from '../constants'
import { dialogShows } from './ErrorModal';
import * as NavigationActions from '../../NavigationService'

export const resetPassword = (oldPassword, password) => {
  return dispatch => {
    dispatch(resetPasswordRequest())
    const data = {
      oldPassword,
      password
    }
    console.log(data, 'Reset Data')
    return request('user/password', 'PUT', data)
      .then((userData)=> {
        dispatch(resetPasswordSuccess(userData))
        dispatch(dialogShows('Password updated!','Your Password have been successfully updated','inform',true, null, null, null, 'Close' ))
        NavigationActions.goBack();
      })
      .catch((e)=> {
        console.log(e, 'Error')
        e.response.then((error)=>{
          dispatch(resetPasswordFailure(error.error,error.message,e.status))
          dispatch(dialogShows('Update password Error',error.message,'inform',true, null, null, 'Please try again', 'Try again' ))
        })
      })
  }
}

const resetPasswordRequest = () => ({
  type: ResetPasswordConstants.RESET_PASSWORD_REQUEST,
})
const resetPasswordSuccess = (userInfo) => ({
  type: ResetPasswordConstants.RESET_PASSWORD_SUCCESS,
  userInfo
})
const resetPasswordFailure = (errorType,errorMessage,errorCode) => ({
  type: ResetPasswordConstants.RESET_PASSWORD_FAILURE,
  errorType,
  errorMessage,
  errorCode
})
