import { LoaderConstants } from '../constants'

export const loaderHides = () =>  { return { type: LoaderConstants.LOADER_HIDES} }
export const loaderShows = () =>  { return { type: LoaderConstants.LOADER_SHOWS} }
