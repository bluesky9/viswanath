import {request} from '../request'
import {SignupConstants} from '../constants'
import { loaderShows, loaderHides } from './Loader';
import { dialogShows, dialogHides } from './ErrorModal';
import { uploadProfilePic, getProfileDetails, setUserId, setUserToken } from './Profile';
import * as NavigationActions from '../../NavigationService'
import { AsyncStorage, Platform } from 'react-native'
import { getNotifToken } from '../../App';
import DeviceInfo from 'react-native-device-info';

let INVITATION_MSG1 = 'Invitation successfully sent.'
let INVITATION_MSG2 = 'Your friend shall receive the invitation, now'


export const signup = (data) => {
  return dispatch => {
    dispatch(loaderShows())
    dispatch(signupRequest())
    return request('register/authMsg','POST',data)
      .then((userData)=> {
        dispatch(loaderHides())
        console.log('User Data', userData)
        dispatch(signupSuccess(userData))
        NavigationActions.navigate('SignUpSuccess', {email : data.email})
      })
      .catch((e)=> {
        dispatch(loaderHides())
        let errorMsg = ''
        e.response.then((error)=>{
          if (error.errors) {
            error.errors.map((item) => {
                errorMsg = errorMsg + (item.field ? item.field : '') + ' ' + (item.message ? item.message : '') + '\n'
            })
          }
          dispatch(dialogShows('Sign up error',errorMsg ,'inform',true, null, null, 'Please refresh', 'Refresh'))
          dispatch(signupFailure(error.exception,error.errors,e.status))
        })
      })
  }
}
export const checkAuthCode = (authCode) => {
  return dispatch => {
    dispatch(loaderShows())
    dispatch(signupRequest())
    return request('user/authCode','GET', undefined, undefined, {authCode})
      .then((userData)=> {
        dispatch(loaderHides())
        console.log('User Data', userData)
        if (userData.activateFlag) {
          NavigationActions.navigate('LoginScreen')
        }
      })
      .catch((e)=> {
        console.log(e);
        dispatch(loaderHides())
        e.response.then((error)=>{
          NavigationActions.replace('LoginScreen', {})
          dispatch(dialogShows('Invalid AuthCode',error.errors.length > 0 ? error.errors[0].message : error.message ,'inform',true, null, null, 'Please refresh', 'Refresh'))
        })
      })
  }
}

export const signupUserDetails = (data, profilePic) => {
  console.log(data,'$$$$');
  return dispatch => {
    dispatch(loaderShows())
    dispatch(signupUserDetailsRequest())
    var signupUserDetailsData = `file={undefined}&ext={undefined}&firstName=${encodeURIComponent(data.firstName)}&lastName=${encodeURIComponent(data.lastName)}&country=${encodeURIComponent(data.country)}&authCode=${encodeURIComponent(data.authCode)}`
    // return request('profPic/Login','POST',data)
    return request('profPic/Login','POST',signupUserDetailsData,'application/x-www-form-urlencoded')
      .then((userData)=> {
        let token = userData.headers.get("My-Authorization")
        console.log('User Data', userData)
        if (!profilePic) {
          dispatch(loaderHides());
          dispatch(getProfileDetails(userData.body.id))
            if (getNotifToken()) {
              dispatch(setUserToken({
                userId: userData.body.id,
                token: getNotifToken(),
                deviceId: getNotifToken(),
                deviceType: Platform.OS === 'ios' ? 'IOS' : 'ANDROID'
              }))
            }
            if (userData.body.id) {
              dispatch(setUserId(userData.body.id))
            }
            try{
              AsyncStorage.setItem('token', token)
              AsyncStorage.setItem('userId', userData.body.id.toString())
              NavigationActions.navigate('HomeFeed')
            }
            catch (e) {
              console.log(e, 'Async Storage Error')
            }
        } else {
          let filePath = 'user/'+ userData.body.id + '/post/' + '.jpg'
            var formData = new FormData();
            formData.append('file', profilePic)
            formData.append('filePath', filePath)
            AsyncStorage.setItem('token', token).then(() => {
              request('/uploadfile', 'POST', formData, 'multipart/form-data')
              .then((response) =>  {
                console.log(response, 'Image Response')
                let http = 'https://'
                let file = {
                    mediumProfilePicPath: http + response.body.object['fileS3URL200x200'],
                    originalProfilePicPath: http + response.body.object['fileS3URL'],
                    smallProfilePicPath: http + response.body.object['fileS3URL40x40'],
                }
                let query = `originalProfilePicPath=${file.originalProfilePicPath}&mediumProfilePicPath=${file.mediumProfilePicPath}&smallProfilePicPath=${file.smallProfilePicPath}`
                request('profPic/picture?' + query, 'POST')
                .then((profileResponse) => {
                  dispatch(loaderHides());
                  dispatch(getProfileDetails(userData.body.id))
                    if (getNotifToken()) {
                      dispatch(setUserToken({
                        userId: userData.body.id,
                        token: getNotifToken(),
                        deviceId: getNotifToken(),
                        deviceType: Platform.OS === 'ios' ? 'IOS' : 'ANDROID'
                      }))
                    }
                    if (userData.body.id) {
                      dispatch(setUserId(userData.body.id))
                    }
                    try{
                      AsyncStorage.setItem('userId', userData.body.id.toString())
                      NavigationActions.navigate('HomeFeed')
                    }
                    catch (e) {
                      console.log(e, 'Async Storage Error')
                    }
                  })
                })
                .catch((e)=> {
                  dispatch(loaderHides())
                  e.response.then((error)=>{
                    dispatch(dialogShows('Sign up error', null ,'inform',true, null, null, 'Please try again', 'Try again'))
                    dispatch(signupUserDetailsFailure(error.exception,error.errors,e.status))
                  })
                }) 
            })
        }
  }).catch((e) => {
    dispatch(loaderHides())
      e.response.then((error)=>{
        dispatch(dialogShows('Sign up error',error.errors.length > 0 ? error.errors[0].message : error.message ,'inform',true, null, null, 'Please try again', 'Try again'))
        dispatch(signupUserDetailsFailure(error.exception,error.errors,e.status))
      })
  })
  }
}

export const resendActivation = (email) => {
  console.log(email, '!2331')
  return dispatch => {
    dispatch(loaderShows())
    dispatch(resendActivationRequest())
    return request('register/resend/email/username','POST', undefined, undefined, {email})
      .then(()=> {
        dispatch(loaderHides())
        dispatch(dialogShows('Instructions have been resent to ' + email, null,'inform',true, null, null, null, 'Close'))
      })
      .catch((e)=> {
        dispatch(loaderHides())
        e.response.then((error)=>{
          let errorMsg = ''
          if (error.errors.length > 0) {
            error.errors.map((errorObj) => {
              errorMsg = errorMsg + errorObj.field + ' ' + errorObj.message
            })
          }
          dispatch(dialogShows('Resend mail error',errorMsg ,'inform',true, null, null, 'Please try again', 'Try again'))
          dispatch(resendActivationFailure())
        })
      })
  }
}

const signupRequest = () => ({
  type: SignupConstants.SIGNUP_REQUEST,
})
const signupSuccess = (userInfo) => ({
  type: SignupConstants.SIGNUP_SUCCESS,
  userInfo
})
const signupFailure = (errorType,errorList,errorCode) => ({
  type: SignupConstants.SIGNUP_FAILURE,
  errorType,
  errorList,
  errorCode
})
const signupUserDetailsRequest = () => ({
  type: SignupConstants.SIGNUP_USER_DETAILS_REQUEST,
})
const signupUserDetailsSuccess = (userInfo) => ({
  type: SignupConstants.SIGNUP_USER_DETAILS_SUCCESS,
  userInfo
})
const signupUserDetailsFailure = (errorType,errorList,errorCode) => ({
  type: SignupConstants.SIGNUP_USER_DETAILS_FAILURE,
  errorType,
  errorList,
  errorCode
})
const resendActivationRequest = () => ({
  type: SignupConstants.RESEND_ACTIVATION_REQUEST,
})
const resendActivationSuccess = (userInfo) => ({
  type: SignupConstants.RESEND_ACTIVATION_SUCCESS,
  userInfo
})
const resendActivationFailure = (errorType,errorList,errorCode) => ({
  type: SignupConstants.RESEND_ACTIVATION_FAILURE
})
