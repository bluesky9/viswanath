import { request } from '../request';
import { CountriesConstants } from '../constants';
import { loaderShows, loaderHides } from './Loader';

// getCountries START
/**
 * Get people list for network page, university people list, department people list.
 *
 * @param  {object} data to be sent to the API. Should be different for each list type. Check swagger for more info
 *
 * @return {content:Array, totalElements:Number}
 */
export const getCountries = () => {
  return dispatch => {
    dispatch(getCountriesRequest());
    dispatch(loaderShows());

    return request('country', 'GET')
      .then(response => {
        dispatch(loaderHides());
        dispatch(getCountriesSuccess(response.body));
      })
      .catch(e => {
        dispatch(loaderHides());
        e.response.then(error => {
          dispatch(getCountriesFailure(error.error, error.message, e.status));
        });
      });
  };
};
const getCountriesRequest = () => ({
  type: CountriesConstants.COUNTRIES_REQUEST
});
const getCountriesSuccess = countries => ({
  type: CountriesConstants.COUNTRIES_SUCCESS,
  countries
});
const getCountriesFailure = (errorType, errorMessage, errorCode) => ({
  type: CountriesConstants.COUNTRIES_FAILURE,
  errorType,
  errorMessage,
  errorCode
});
// getCountries END
