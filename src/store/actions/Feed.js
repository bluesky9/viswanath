import { request } from '../request';
import { FeedConstants } from '../constants';
import { loaderShows, loaderHides } from './Loader';
import * as NavigationActions from '../../NavigationService';
import { dialogShows } from './ErrorModal';
import {
  getUniversityFeedsSuccess,
  getDepartmentFeedsSuccess
} from './Universities';
import _ from 'lodash';

export const getPosts = (feedType, size, page, univId, deptId) => {
  var url = `post/${size}/${page}`;
  switch (feedType) {
    case 1:
      url = `post/${size}/${page}`;
      break;
    case 2:
      url = `post/university/${univId}/${size}/${page}`;
      break;
    case 3:
      url = `post/university/${univId}/department/${deptId}/${size}/${page}`;
      break;
  }
  return dispatch => {
    dispatch(loaderShows());
    dispatch(getPostsRequest());
    return request(url, 'GET')
      .then(data => {
        dispatch(loaderHides());
        dispatch(getPostsSuccess(data.body));
      })
      .catch(e => {
        e.response.then(error => {
          dispatch(loaderHides());
          dispatch(getPostsFailure(error.exception, error.errors, e.status));
        });
      });
  };
};

export const getUserConnections = userId => {
  return dispatch => {
    dispatch(getUserConnectionsRequest());
    return request('userConnection?userId=' + userId, 'GET')
      .then(data => {
        console.log(data);
        dispatch(getUserConnectionsSuccess(data.body));
      })
      .catch(e => {
        e.response.then(error => {
          dispatch(
            getUserConnectionsFailure(error.exception, error.errors, e.status)
          );
        });
      });
  };
};

export const deletePost = (postId, page) => {
  return (dispatch, getState) => {
    dispatch(loaderShows());
    let paginatedPosts = getState().paginatedPosts.posts;
    let posts = getState().universityFeeds.universityFeeds;
    let departmentPosts = getState().universityDetails.departmentFeeds;
    dispatch(deletePostRequest());
    return request('/post', 'DELETE', { postid: postId })
      .then(response => {
        let updatedPosts = _.filter(
          paginatedPosts,
          item => item.postid !== postId
        );
        dispatch(getPostsSuccess(updatedPosts));
        if (page === 'UNIVERSITY_POST') {
          let updatedUniversityPost = _.filter(
            posts,
            item => item.postid !== postId
          );
          console.log(updatedUniversityPost, 'Updated Post');
          dispatch(getUniversityFeedsSuccess(updatedUniversityPost));
        }
        if (page === 'DEPARTMENT_POST') {
          let updatedDepartmentPost = _.filter(
            departmentPosts,
            item => item.postid !== postId
          );
          console.log(updatedDepartmentPost, 'Updated Post Department');
          dispatch(getDepartmentFeedsSuccess(updatedDepartmentPost));
        }
        dispatch(deletePostSuccess());
        dispatch(loaderHides());
      })
      .catch(e => {
        dispatch(loaderHides());
        e.response.then(error => {
          dispatch(deletePostFailure(error.error, error.message, e.status));
          dispatch(
            dialogShows(
              'Delete post error',
              error.error,
              'inform',
              true,
              null,
              null,
              'Please try again',
              'Refresh'
            )
          );
        });
      });
  };
};

export const reportPost = (data, report) => {
  return (dispatch, getState) => {
    console.log(data, 'Check data');
    dispatch(loaderShows());
    let paginatedPosts = getState().paginatedPosts.posts;
    dispatch(reportPostRequest());
    return request('/post/report', 'POST', data)
      .then(response => {
        let postIndex = _.findIndex(
          paginatedPosts,
          post => post.postid === data.postId
        );
        if (postIndex !== -1) {
          if (report) {
            NavigationActions.goBack();
          }
          paginatedPosts.splice(postIndex, 1);
          dispatch(getPostsSuccess(paginatedPosts));
        }
        dispatch(reportPostSuccess());
        dispatch(loaderHides());
      })
      .catch(e => {
        dispatch(loaderHides());
        e.response.then(error => {
          dispatch(
            dialogShows(
              'Report user error',
              error.message,
              'inform',
              true,
              null,
              null,
              'Please try again',
              'Refresh'
            )
          );
          dispatch(reportPostFailure());
        });
      });
  };
};

export const postLike = postId => {
  return (dispatch, getState) => {
    // dispatch(loaderShows());
    let posts = getState().paginatedPosts.posts;
    dispatch(postLikeRequest());
    return request('/likes', 'POST', { postId: postId })
      .then(response => {
        let likes = response.body;
        let postIndex = _.findIndex(posts, post => post.postid === postId);
        posts[postIndex].likes = likes;
        dispatch(postLikeSuccess());
        dispatch(getPostsSuccess(posts));
        // dispatch(loaderHides());
      })
      .catch(e => {
        e.response.then(error => {
          dispatch(postLikeFailure(error.error, error.message, e.status));
        });
        // dispatch(loaderHides());
      });
  };
};

export const postComment = data => {
  return (dispatch, getState) => {
    let posts = getState().paginatedPosts.posts;
    dispatch(loaderShows());
    dispatch(postCommentRequest());

    return request('/comment', 'POST', data)
      .then(response => {
        let comments = response.body;
        let postIndex = _.findIndex(posts, post => post.postid === data.postId);
        posts[postIndex].comment.push(comments);
        dispatch(postCommentSuccess());
        dispatch(getPostsSuccess(posts));
        dispatch(loaderHides());
      })
      .catch(e => {
        console.log(e, 'Error');
        e.response.then(error => {
          dispatch(postCommentFailure(error.error, error.message, e.status));
        });
        dispatch(loaderHides());
      });
  };
};

export const getPost = postId => {
  return (dispatch, getState) => {
    let posts = getState().paginatedPosts.posts;
    dispatch(loaderShows());
    dispatch(getPostRequest());

    return request(`post/${postId}`, 'GET')
      .then(response => {
        let postIndex = _.findIndex(posts, post => post.postid === postId);
        let postItem = undefined;
        if (postIndex === -1) {
          posts.push(response.body);
          postItem = response.body;
        } else {
          posts[postIndex] = response.body;
          postItem = response.body;
        }
        dispatch(getPostSuccess());
        dispatch(getPostsSuccess(posts));
        NavigationActions.navigate('PostComment', { postItem: postItem });
        dispatch(loaderHides());
      })
      .catch(e => {
        console.log(e, 'Error');
        e.response.then(error => {
          dispatch(getPostFailure());
        });
        dispatch(loaderHides());
      });
  };
};

export const deleteComment = (postId, commentId) => {
  return (dispatch, getState) => {
    let posts = getState().paginatedPosts.posts;
    dispatch(loaderShows());
    dispatch(deleteCommentRequest());

    return request('/comment', 'DELETE', { id: commentId })
      .then(response => {
        let postIndex = _.findIndex(posts, post => post.postid === postId);
        let commentIndex = _.findIndex(
          posts[postIndex].comment,
          comment => comment.id === commentId
        );
        posts[postIndex].comment.splice(commentIndex, 1);
        dispatch(getPostsSuccess(posts));
        dispatch(postCommentSuccess());
        dispatch(loaderHides());
      })
      .catch(e => {
        e.response.then(error => {
          dispatch(deleteCommentFailure(error.error, error.message, e.status));
        });
        dispatch(loaderHides());
      });
  };
};

export const updateComment = (postId, data) => {
  console.log(data, 'update Comment Data');
  return (dispatch, getState) => {
    let posts = getState().paginatedPosts.posts;
    dispatch(loaderShows());
    dispatch(updateCommentRequest());

    return request('/comment', 'PUT', data)
      .then(response => {
        let postIndex = _.findIndex(posts, post => post.postid === postId);
        let commentIndex = _.findIndex(
          posts[postIndex].comment,
          comment => comment.id === data.id
        );
        posts[postIndex].comment[commentIndex].comments = data.comments;
        dispatch(getPostsSuccess(posts));
        dispatch(updateCommentSuccess());
        dispatch(loaderHides());
      })
      .catch(e => {
        console.log(e, 'Error in update');
        e.response.then(error => {
          dispatch(updateCommentFailure(error.error, error.message, e.status));
        });
        dispatch(loaderHides());
      });
  };
};
export const sharePost = postId => {
  // console.log('update Comment Data', postId);
  return (dispatch, getState) => {
    let posts = getState().paginatedPosts.posts;
    let userInfo = getState().userInfoAPI.userInfo;
    // dispatch(loaderShows());
    dispatch(sharePostRequest());

    return request(`/v1/posts/${postId}/publishers`, 'POST')
      .then(response => {
        // console.log(response, 'Share post response');
        let postIndex = _.findIndex(posts, post => post.postid === postId);
        posts[postIndex].publishers.push({
          id: userInfo.id,
          profPicPath: userInfo.profilePicture,
          name: userInfo.firstName + ' ' + userInfo.lastName
        });
        // let commentIndex = _.findIndex(posts[postIndex].comment, comment => comment.id === data.id)
        // posts[postIndex].comment[commentIndex].comments = data.comments
        dispatch(getPostsSuccess(posts));
        dispatch(sharePostSuccess());
        // dispatch(loaderHides());
      })
      .catch(e => {
        console.log(e, 'Error in update');
        e.response.then(error => {
          dispatch(sharePostFailure(error.error, error.message, e.status));
          dispatch(
            dialogShows(
              'Share post error',
              error.errors[0].message,
              'inform',
              true,
              null,
              null,
              'Please try again',
              'Refresh'
            )
          );
        });
        // dispatch(loaderHides());
      });
  };
};
export const unsharePost = postId => {
  // console.log('update Comment Data', postId);
  return (dispatch, getState) => {
    let posts = getState().paginatedPosts.posts;
    let userId = getState().userInfoAPI.userId;
    return request(`/v1/posts/${postId}/publishers`, 'DELETE')
      .then(response => {
        let postIndex = _.findIndex(posts, post => post.postid === postId);
        let updatedPosts = posts;
        updatedPosts[postIndex].publishers = _.filter(
          posts[postIndex].publishers,
          publishObj => publishObj.id !== userId
        );
        dispatch(getPostsSuccess(updatedPosts));
        // console.log(response, 'Share post response');
      })
      .catch(e => {
        console.log(e, 'Error in share');
      });
  };
};

export const deleteFile = (postId, removeIds) => {
  return dispatch => {
    return request(`/v1/posts/${postId}/files`, 'DELETE', removeIds)
      .then(response => {
        console.log('response', response);
      })
      .catch(e => {
        console.log('error', e);
      });
  };
};

export const uploadFile = (images, publishPostObj, postId, removeIds) => {
  return (dispatch, getState) => {
    dispatch(loaderShows());
    dispatch(uploadFileRequest());

    let userId = getState().userInfoAPI.userId;
    var promises = [];

    if (removeIds && removeIds.length > 0) {
      dispatch(deleteFile(postId, removeIds));
    }

    images.map((imageObj, index) => {
      if (imageObj.uri) {
        let filePath = 'user/' + userId + '/post/' + index + '.jpg';
        var formData = new FormData();
        formData.append('file', imageObj);
        formData.append('filePath', filePath);
        let promise = request(
          '/uploadfile',
          'POST',
          formData,
          'multipart/form-data'
        );
        promises.push(promise);
      }
    });

    Promise.all(promises)
      .then(responses => {
        let files = [];
        let http = 'https://';
        responses.map(response => {
          let file = {
            mediumUrl: http + response.body.object['fileS3URL200x200'],
            originalUrl: http + response.body.object['fileS3URL'],
            smallUrl: http + response.body.object['fileS3URL40x40']
          };
          files.push(file);
        });
        if (postId) {
          dispatch(addFilesToPost(files, postId));
        } else {
          publishPostObj.files = files;
        }
        dispatch(publishPost(publishPostObj, postId));
        // dispatch(loaderHides())
      })
      .catch(e => {
        e.response.then(err => {
          console.log(err, 'Check Error Object');
        });
        dispatch(loaderHides());
        console.log(e, 'Its updating');
      });
  };
};

export const addFilesToPost = (files, postId) => {
  let uploadFile = {
    files: files
  };
  // console.log('Removed Files', uploadFile)

  return (dispatch, getState) => {
    let posts = getState().paginatedPosts.posts;

    return request(`v1/posts/${postId}/files`, 'PUT', uploadFile)
      .then(response => {
        console.log(response, 'Upload Response');
      })
      .catch(e => {
        console.log(e, 'Upload File Error');
      });
  };
};

const getPostsRequest = () => ({
  type: FeedConstants.FEED_REQUEST
});
const getPostsSuccess = posts => ({
  type: FeedConstants.FEED_SUCCESS,
  posts
});
const getPostsFailure = (errorType, errorList, errorCode) => ({
  type: FeedConstants.FEED_FAILURE,
  errorType,
  errorList,
  errorCode
});

// Delete Post
const deletePostRequest = () => ({
  type: FeedConstants.DELETE_POST_REQUEST
});
const deletePostSuccess = () => ({
  type: FeedConstants.DELETE_POST_SUCCESS
});
const deletePostFailure = () => ({
  type: FeedConstants.DELETE_POST_FAILURE
});
// Report Post
const reportPostRequest = () => ({
  type: FeedConstants.REPORT_POST_REQUEST
});
const reportPostSuccess = () => ({
  type: FeedConstants.REPORT_POST_SUCCESS
});
const reportPostFailure = () => ({
  type: FeedConstants.REPORT_POST_FAILURE
});
// Like Post
const postLikeRequest = () => ({
  type: FeedConstants.POST_LIKE_REQUEST
});
const postLikeSuccess = () => ({
  type: FeedConstants.POST_LIKE_SUCCESS
});
const postLikeFailure = () => ({
  type: FeedConstants.POST_LIKE_FAILURE
});
// Comment Post
const postCommentRequest = () => ({
  type: FeedConstants.POST_COMMENT_REQUEST
});
const postCommentSuccess = () => ({
  type: FeedConstants.POST_COMMENT_SUCCESS
});
const postCommentFailure = () => ({
  type: FeedConstants.POST_COMMENT_FAILURE
});
// Update Post
const updateCommentRequest = () => ({
  type: FeedConstants.UPDATE_COMMENT_REQUEST
});
const updateCommentSuccess = () => ({
  type: FeedConstants.UPDATE_COMMENT_SUCCESS
});
const updateCommentFailure = () => ({
  type: FeedConstants.UPDATE_COMMENT_FAILURE
});
// Delete Post
const deleteCommentRequest = () => ({
  type: FeedConstants.DELETE_COMMENT_REQUEST
});
const deleteCommentSuccess = () => ({
  type: FeedConstants.DELETE_COMMENT_SUCCESS
});
const deleteCommentFailure = () => ({
  type: FeedConstants.DELETE_COMMENT_FAILURE
});
// Share Post
const sharePostRequest = () => ({
  type: FeedConstants.SHARE_POST_REQUEST
});
const sharePostSuccess = () => ({
  type: FeedConstants.SHARE_POST_SUCCESS
});
const sharePostFailure = () => ({
  type: FeedConstants.SHARE_POST_FAILURE
});
// Share Post
const uploadFileRequest = () => ({
  type: FeedConstants.UPLOAD_FILE_REQUEST
});
const uploadFileSuccess = () => ({
  type: FeedConstants.UPLOAD_FILE_SUCCESS
});
const uploadFileFailure = () => ({
  type: FeedConstants.UPLOAD_FILE_FAILURE
});
// Get Post
const getPostRequest = () => ({
  type: FeedConstants.GET_POST_REQUEST
});
const getPostSuccess = () => ({
  type: FeedConstants.GET_POST_SUCCESS
});
const getPostFailure = () => ({
  type: FeedConstants.GET_POST_FAILURE
});

const getUserConnectionsRequest = () => ({
  type: 'USER_CONNECTIONS_REQUEST'
});

const getUserConnectionsSuccess = connections => ({
  type: 'USER_CONNECTIONS_SUCCESS',
  connections: connections
});

const getUserConnectionsFailure = (errorType, errorList, errorCode) => ({
  type: 'USER_CONNECTIONS_FAILURE',
  errorType,
  errorList,
  errorCode
});

export const setTaggedUsers = userArray => {
  return dispatch => {
    dispatch({ type: 'SET_TAGGED_USERS', userArray: userArray });
  };
};

export const setPostAudience = userArray => {
  return dispatch => {
    dispatch({ type: 'SET_POST_AUDIENCE', userArray: userArray });
  };
};

export const publishPost = (dataObj, postId) => {
  return (dispatch, getState) => {
    dispatch(loaderShows());
    paginatedPosts = getState().paginatedPosts.posts;
    let posts = getState().universityFeeds.universityFeeds;
    let departmentPosts = getState().universityDetails.departmentFeeds;
    if (postId) {
      return request(`post/` + postId, 'PUT', dataObj)
        .then(data => {
          let postIndex = _.findIndex(
            paginatedPosts,
            postObj => postObj.postid === postId
          );
          paginatedPosts[postIndex] = data.body;
          dispatch(getPostsSuccess(paginatedPosts));
          if (dataObj.departmentId) {
            let departmentPostIndex = _.findIndex(
              departmentPosts,
              postObj => postObj.postid === postId
            );
            if (departmentPostIndex !== -1)
              departmentPosts[departmentPostIndex] = data.body;
            dispatch(getDepartmentFeedsSuccess(posts));
          } else if (dataObj.universityId) {
            // Updating university post
            let universityPostIndex = _.findIndex(
              posts,
              postObj => postObj.postid === postId
            );
            if (universityPostIndex !== -1)
              posts[universityPostIndex] = data.body;
            dispatch(getUniversityFeedsSuccess(posts));
          }
          NavigationActions.goBack();
          dispatch(loaderHides());
        })
        .catch(e => {
          console.log(e, '123');
          e.response.then(error => {
            console.log(error, 'Error Obj');
            dispatch(loaderHides());
            dispatch(
              dialogShows(
                'Edit post error',
                error.error,
                'inform',
                true,
                null,
                null,
                'Please try again',
                'Refresh'
              )
            );
          });
        });
    } else {
      return request(`post/`, 'POST', dataObj)
        .then(data => {
          paginatedPosts.unshift(data.body);
          dispatch(getPostsSuccess(paginatedPosts));

          if (dataObj.departmentId) {
            departmentPosts.unshift(data.body);
            let departmentPostIndex = _.findIndex(
              departmentPosts,
              postObj => postObj.postid === postId
            );
            posts[departmentPostIndex] = data.body;
            dispatch(getDepartmentFeedsSuccess(departmentPosts));
          } else if (dataObj.universityId) {
            // Creating university post
            posts.unshift(data.body);
            dispatch(getUniversityFeedsSuccess(posts));
          }

          NavigationActions.goBack();
          dispatch(loaderHides());
        })
        .catch(e => {
          console.log(e, '112');
          let errorMsg = '';
          e.response.then(error => {
            if (error.errors)
              error.errors.map(item => {
                errorMsg =
                  errorMsg +
                  (item.field ? item.field : '') +
                  ' ' +
                  (item.message ? item.message : '') +
                  '\n';
              });
            dispatch(loaderHides());
            dispatch(
              dialogShows(
                'Post compose error',
                errorMsg,
                'inform',
                true,
                null,
                null,
                'Please try again',
                'Refresh'
              )
            );
          });
        });
    }
  };
};
