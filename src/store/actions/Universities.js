import { request } from '../request';
import { UniversitiesConstants } from '../constants';
import { countries } from '../reducers/countries';
import { dialogShows } from './ErrorModal';
import { loaderShows, loaderHides } from './Loader';
import * as NavigationActions from '../../NavigationService'
import { Alert } from 'react-native';

import _ from 'lodash';
import loader from '../../components/layouts/loader/loader';

// getCountries START
export const getAllUniversities = () => {
  return dispatch => {
    dispatch(getAllUniversitiesRequest());

    return request('v1/universities/all', 'GET')
      .then(response => {
        dispatch(getAllUniversitiesSuccess(response.body));
      })
      .catch(e => {
        e.response.then(error => {
          dispatch(
            getAllUniversitiesFailure(error.error, error.message, e.status)
          );
        });
      });
  };
};
const getAllUniversitiesRequest = () => ({
  type: UniversitiesConstants.ALL_UNIVERSITIES_REQUEST
});
const getAllUniversitiesSuccess = universities => ({
  type: UniversitiesConstants.ALL_UNIVERSITIES_SUCCESS,
  universities
});
const getAllUniversitiesFailure = (errorType, errorMessage, errorCode) => ({
  type: UniversitiesConstants.ALL_UNIVERSITIES_FAILURE,
  errorType,
  errorMessage,
  errorCode
});
const getUniversitiesByCountriesFailure = (
  errorType,
  errorMessage,
  errorCode
) => ({
  type: UniversitiesConstants.UNIVERSITIES_BY_COUNTRY_FAILURE,
  errorType,
  errorMessage,
  errorCode
});
// getCountries END

// getPaginatedUniversities START
export const getPaginatedUniversities = (size, page) => {
  return dispatch => {
    dispatch(getPaginatedUniversitiesRequest());
    return request(`v1/universities/page/${size}/${page}`, 'GET')
      .then(response => {
        dispatch(getPaginatedUniversitiesSuccess(response.body));
      })
      .catch(e => {
        e.response.then(error => {
          dispatch(
            getPaginatedUniversitiesFailure(
              error.error,
              error.message,
              e.status
            )
          );
        });
      });
  };
};

const getUniversitiesByNameRequest = () => ({
  type: UniversitiesConstants.GET_UNIVERSITY_BY_NAME_REQUEST
});

const getUniversitiesByNameSuccess = universities => ({
  type: UniversitiesConstants.GET_UNIVERSITY_BY_NAME_SUCCESS,
  universities: universities
});

export const getUniversitiesByName = (size, page, abbr) => {
  return dispatch => {
    dispatch(getUniversitiesByNameRequest());
    var uniNameData = `pageNo=${encodeURIComponent(
      page
    )}&size=${encodeURIComponent(size)}&universityName=${encodeURIComponent(
      abbr
    )}&abbr=${encodeURIComponent(abbr)}`;
    return request(
      `v1/universities/searchByNameOrAbbreviation/${size}/${page}`,
      'POST',
      uniNameData,
      'application/x-www-form-urlencoded'
    )
      .then(response => {
        dispatch(getUniversitiesByNameSuccess(response.body));
      })
      .catch(e => {
        e.response.then(error => {
          console.log(error);
        });
      });
  };
};

const setUniversitiesCount = count => ({
  type: UniversitiesConstants.GET_UNIVERSITY_COUNT,
  count: count
});

export const getUniversitiesCount = () => {
  return dispatch => {
    return request(`v1/universities/count`, 'GET')
      .then(response => {
        dispatch(setUniversitiesCount(response.body));
      })
      .catch(e => {
        console.log(e);
      });
  };
};

const getUniversitiesByCountriesRequest = universities => ({
  type: UniversitiesConstants.UNIVERSITIES_BY_COUNTRIES,
  universities
});
const getUniversitiesByCountriesStart = () => ({
  type: UniversitiesConstants.UNIVERSITIES_BY_COUNTRIES_START
});

export const getUniversitiesByCountries = countries => {
  return dispatch => {
    dispatch(getUniversitiesByCountriesStart());
    return request('v1/universities/searchByCountries', 'POST', {
      countries: countries
    })
      .then(response => {
        console.log(response);
        dispatch(getUniversitiesByCountriesRequest(response.body));
      })
      .catch(e => {
        e.response.then(error => {
          dispatch(
            getUniversitiesByCountriesFailure(
              error.error,
              error.message,
              e.status
            )
          );
        });
      });
  };
};

export const setUniversityFilters = filters => {
  return dispatch => {
    dispatch(setUniversityFiltersRequest(filters));
  };
};

export const setUniversityPostCountryFilters = filters => {
  return dispatch => {
    dispatch(setUniversityPostCountryFilter(filters));
  };
};

const setUniversityFiltersRequest = filters => ({
  type: UniversitiesConstants.COUNTRY_FILTERS_REQUEST,
  filters: filters
});
const setUniversityPostCountryFilter = filters => ({
  type: UniversitiesConstants.SET_POST_COUNTRY_FILTERS,
  filters: filters
});
const getPaginatedUniversitiesRequest = () => ({
  type: UniversitiesConstants.UNIVERSITIES_REQUEST
});
const getPaginatedUniversitiesSuccess = universities => ({
  type: UniversitiesConstants.UNIVERSITIES_SUCCESS,
  universities
});
const getPaginatedUniversitiesFailure = (
  errorType,
  errorMessage,
  errorCode
) => ({
  type: UniversitiesConstants.UNIVERSITIES_FAILURE,
  errorType,
  errorMessage,
  errorCode
});

export const getUniversitiesFollowedByUser = profileId => {
  return dispatch => {
    dispatch(getUniversitiesFollowedByUserRequest());
    return request(`v1/universities/followers/profile/${profileId}`, 'GET')
      .then(response => {
        dispatch(getUniversitiesFollowedByUserSuccess(response.body));
      })
      .catch(e => {
        e.response.then(error => {
          dispatch(
            getUniversitiesFollowedByUserFailure(
              error.error,
              error.message,
              e.status
            )
          );
        });
      });
  };
};
const getUniversitiesFollowedByUserRequest = () => ({
  type: UniversitiesConstants.UNIVERSITIES_FOLLOWED_BY_USER_REQUEST
});
const getUniversitiesFollowedByUserSuccess = universities => ({
  type: UniversitiesConstants.UNIVERSITIES_FOLLOWED_BY_USER_SUCCESS,
  universities
});
const getUniversitiesFollowedByUserFailure = (
  errorType,
  errorMessage,
  errorCode
) => ({
  type: UniversitiesConstants.UNIVERSITIES_FOLLOWED_BY_USER_FAILURE,
  errorType,
  errorMessage,
  errorCode
});

export const getUniversityBySlug = (slug, goback) => {
  return (dispatch, getState) => {
    let userId = getState().userInfoAPI.userId
    dispatch(getUniversityBySlugRequest());
    if (goback) {
      dispatch(loaderShows());
    }
    return request(`v1/universities/searchBySlug/${slug}`, 'GET')
      .then(response => {
        let id = response.body.univ_id
        request(`v1/universities/${id}/followers`, 'GET')
          .then(resp => {
            console.log(resp, 'Check Followers List')
            dispatch(loaderHides());
            dispatch(getUniversityDetailsSuccess(response.body, resp.body));
            let userFollowing = _.find(resp.body, follower => follower.profile.id === userId)
            let universityPeople = {
              myUserId: userId,
              userTypes: ["PROFESSOR", "UNIVERSITY_STAFF"],
              univIds: [id],
              countOnly: true
            }
            if (userFollowing && userFollowing.universityConnectionStatus === 'FOLLOW') {
              universityPeople.userTypes.push("CURRENT_STUDENT", "PROSPECTIVE_STUDENT", "ALUMNI")
              dispatch(setUsertype(userFollowing.userType))
            }
            dispatch(setFollowingUniversity(userFollowing && userFollowing.universityConnectionStatus === 'FOLLOW' ? true : false))
            dispatch(getUniversityPeople(universityPeople))
            dispatch(getUniversityFeeds(id, 20, 0))
            dispatch(getUnivesityDepartments(id, 20, 0))
            if (goback) {
              NavigationActions.goBack();
            }
          })
          .catch(e => {
            e.response.then(error => {
              dispatch(loaderHides());
              dispatch(
                getUniversityDetailsFailure(
                  error.error,
                  error.message,
                  e.status
                )
              );
            }
            );
          });
      })
      .catch(e => {
        e.response.then(error => {
          dispatch(
            getUniversityBySlugFailure(error.error, error.message, e.status)
          );
        });
      });
  };
};
const getUniversityBySlugRequest = () => ({
  type: UniversitiesConstants.UNIVERSITY_BY_SLUG_REQUEST
});
const getUniversityBySlugSuccess = university => ({
  type: UniversitiesConstants.UNIVERSITY_BY_SLUG_SUCCESS,
  university
});
const getUniversityBySlugFailure = (errorType, errorMessage, errorCode) => ({
  type: UniversitiesConstants.UNIVERSITY_BY_SLUG_FAILURE,
  errorType,
  errorMessage,
  errorCode
});
// getUniversityBySlug END

// getPaginatedUniversities START
export const getPaginatedDepartments = (
  universityId,
  size,
  pageNo,
  optionalDeptType
) => {
  //GET /v1/department/searchByUniversityIdAndDeptType/{universityId}/{size}/{pageNo}/{optionalDeptType}
  //TODO: provide list of department type
  // type: all, a
  var endPoint = `v1/department/searchByUniversityIdAndDeptType/${universityId}/${size}/${pageNo}/${optionalDeptType}`;
  return dispatch => {
    dispatch(getPaginatedDepartmentsRequest());

    return request(endPoint, 'GET')
      .then(response => {
        dispatch(getPaginatedDepartmentsSuccess(response.body));
      })
      .catch(e => {
        e.response.then(error => {
          dispatch(
            getPaginatedDepartmentsFailure(error.error, error.message, e.status)
          );
        });
      });
  };
};
const getPaginatedDepartmentsRequest = () => ({
  type: UniversitiesConstants.DEPARTMENT_REQUEST
});
const getPaginatedDepartmentsSuccess = departments => ({
  type: UniversitiesConstants.DEPARTMENT_SUCCESS,
  departments
});
const getPaginatedDepartmentsFailure = (
  errorType,
  errorMessage,
  errorCode
) => ({
  type: UniversitiesConstants.DEPARTMENT_FAILURE,
  errorType,
  errorMessage,
  errorCode
});
// getPaginatedUniversities END

const getUniversityDetailsSuccess = (details, followers) => ({
  type: UniversitiesConstants.GET_UNIVERSITY_DETAILS_SUCCESS,
  details: details,
  followers: followers
});
const getUniversityDetailsFailure = (errorType, errorMessage, errorCode) => ({
  type: UniversitiesConstants.GET_UNIVERSITY_DETAILS_FAILURE,
  errorType,
  errorMessage,
  errorCode
});

const getUniversityDepartmentsSuccess = departments => ({
  type: UniversitiesConstants.GET_UNIVERSITY_DEPARTMENTS_SUCCESS,
  departments: departments
});
const getUniversityDepartmentsFailure = (
  errorType,
  errorMessage,
  errorCode
) => ({
  type: UniversitiesConstants.GET_UNIVERSITY_DEPARTMENTS_FAILURE,
  errorType,
  errorMessage,
  errorCode
});

export const deletePost = postId => {
  return (dispatch, getState) => {
    dispatch(loaderShows());
    let paginatedPosts = getState().paginatedPosts.posts
    dispatch(deletePostRequest());
    return request('/post', 'DELETE', { postid: postId })
      .then(response => {
        let updatedPosts = _.filter(paginatedPosts, item => item.postid !== postId)
        dispatch(getPostsSuccess(updatedPosts))
        dispatch(deletePostSuccess())
        dispatch(loaderHides())
      })
      .catch(e => {
        dispatch(loaderHides())
        e.response.then(error => {
          dispatch(deletePostFailure(error.error, error.message, e.status));
          dispatch(dialogShows('Delete post error', error.error, 'inform', true, null, null, 'Please try again', 'Refresh'))
        });
      });
  };
}

export const getUnivesityDepartments = (univId, size, navigate) => {
  return dispatch => {
    return request(`v1/department/search/${univId}/${size}/0`, 'GET')
      .then(response => {
        console.log(response.body, 'Check LevelOfStuy')
        dispatch(getUniversityDepartmentsSuccess(response.body));
        if (navigate) NavigationActions.goBack()
      })
      .catch(e => {
        e.response.then(error => {
          dispatch(
            getUniversityDepartmentsFailure(
              error.error,
              error.message,
              e.status
            )
          );
        });
      });
  };
};

export const getUniversityDetailsForEditPost = (id, postId) => {
  return dispatch => {
    dispatch(getUniversityDetails(id));
    dispatch(getUniversityFeeds(id, 100, 0, { route: 'UniversityPostEdit', data: postId }))
  }
}

export const getDepartmentDetailsForEditPost = (univId, deptId, postId) => {
  return dispatch => {
    return request(`v1/department/${deptId}`, 'GET')
      .then(response => {
        dispatch(getUniversityDetails(univId));
        dispatch(getUnivesityDepartments(univId, 100));
        dispatch(getDepartmentFeeds(univId, deptId, 100, 0, { route: 'UniversityPostEdit', data: postId, deptId: deptId, majors: response.body.major }));
      })
      .catch(e => {
        console.log(e, '123')
        e.response.then(error => {
          dispatch(
            getUniversityDepartmentsFailure(
              error.error,
              error.message,
              e.status
            )
          );
        });
      });
  }
}

export const confirmMail = (params) => {
  return (dispatch) => {
    dispatch(loaderShows());
    return request(`/v1/universities/${params.univId}/followers/${params.univFollowerId}/verify?token=${params.token}`, 'POST').then(() => {
      dispatch(loaderHides());
      alert('University successfully followed');
    }).catch((e) => {
      dispatch(loaderHides());
      dispatch(dialogShows('Unable to follow university', null, 'inform', true, null, null, 'Please try again', 'Close'))
    })
  }
}

export const getUniversityDetails = id => {
  return (dispatch, getState) => {
    let userId = getState().userInfoAPI.userId
    dispatch(loaderShows());
    return request(`v1/universities/${id}`, 'GET')
      .then(response => {
        request(`v1/universities/${id}/followers`, 'GET')
          .then(resp => {
            console.log(resp, 'Check Followers List')
            dispatch(loaderHides());
            dispatch(getUniversityDetailsSuccess(response.body, resp.body));
            let userFollowing = _.find(resp.body, follower => follower.profile.id === userId)
            console.log(userFollowing, 'USER FOLLOWING!@#')
            let universityPeople = {
              myUserId: userId,
              userTypes: ["PROFESSOR", "UNIVERSITY_STAFF"],
              univIds: [id],
              countOnly: true
            }
            if (userFollowing && userFollowing.universityConnectionStatus === 'FOLLOW') {
              universityPeople.userTypes.push("CURRENT_STUDENT", "PROSPECTIVE_STUDENT", "ALUMNI"),
                dispatch(setUsertype(userFollowing.userType))
            }
            dispatch(setFollowingUniversity(userFollowing && userFollowing.universityConnectionStatus === 'FOLLOW' ? true : false))
            dispatch(getUniversityPeople(universityPeople))
          })
          .catch(e => {
            e.response.then(error => {
              dispatch(loaderHides());
              dispatch(
                getUniversityDetailsFailure(
                  error.error,
                  error.message,
                  e.status
                )
              );
            });
          });
      })
      .catch(e => {
        e.response.then(error => {
          dispatch(loaderHides());
          dispatch(
            getUniversityDetailsFailure(error.error, error.message, e.status)
          );
        });
      });
  };
};

export const getUniversityFeedsSuccess = feeds => ({
  type: UniversitiesConstants.GET_UNIVERSITY_FEED_SUCCESS,
  feeds: feeds
});
const getUniversityFeedsFailure = (errorType, errorMessage, errorCode) => ({
  type: UniversitiesConstants.GET_UNIVERSITY_FEED_FAILURE,
  errorType,
  errorMessage,
  errorCode
});

export const getDepartmentFeedsRequest = () => ({
  type: UniversitiesConstants.GET_DEPARTMENT_FEED_REQUEST
});
export const getDepartmentFeedsSuccess = feeds => ({
  type: UniversitiesConstants.GET_DEPARTMENT_FEED_SUCCESS,
  feeds: feeds
});
const getDepartmentFeedsFailure = (errorType, errorMessage, errorCode) => ({
  type: UniversitiesConstants.GET_DEPARTMENT_FEED_FAILURE,
  errorType,
  errorMessage,
  errorCode
});

export const getUniversityFeeds = (id, size, page, navigation) => {
  return dispatch => {
    dispatch(loaderShows());
    return request(`post/university/${id}/${size}/${page}`, 'GET')
      .then(response => {
        dispatch(loaderHides());
        dispatch(getUniversityFeedsSuccess(response.body));
        if (navigation) NavigationActions.navigate(navigation.route, {
          postId: navigation.data
        })
      })
      .catch(e => {
        e.response.then(error => {
          dispatch(loaderHides());
          dispatch(
            getUniversityFeedsFailure(error.error, error.message, e.status)
          );
        });
      });
  };
};

export const getDepartmentFeeds = (univId, deptId, size, page, navigation) => {
  return dispatch => {
    dispatch(loaderShows());
    dispatch(getDepartmentFeedsRequest());
    return request(`post/university/${univId}/department/${deptId}/${size}/${page}`, 'GET')
      .then(response => {
        dispatch(loaderHides());
        dispatch(getDepartmentFeedsSuccess(response.body));
        if (navigation) {
          NavigationActions.navigate(navigation.route, {
            postId: navigation.data,
            deptRequest: true,
            majors: navigation.majors,
            deptId: navigation.deptId
          })
        }
      })
      .catch(e => {
        e.response.then(error => {
          dispatch(loaderHides());
          dispatch(
            getDepartmentFeedsFailure(error.error, error.message, e.status)
          );
        });
      });
  };
};

// %%%%%%%%%%%%%%%
// Todo, integrate Ankit logic for reducer update withot API call.

export const universityPostLike = (postId, page) => {
  console.log(postId, '123')
  return (dispatch, getState) => {
    let posts = getState().universityFeeds.universityFeeds;
    let departmentPosts = getState().universityDetails.departmentFeeds;

    return request('/likes', 'POST', { postId: postId })
      .then(response => {
        let likes = response.body;
        console.log(response.body, 'Like Response')
        if (page === 'Department') {
          let departmentPostIndex = _.findIndex(departmentPosts, post => post.postid === postId);
          departmentPosts[departmentPostIndex].likes = likes;
          dispatch(getDepartmentFeedsSuccess(departmentPosts));
        } else {
          let postIndex = _.findIndex(posts, post => post.postid === postId);
          posts[postIndex].likes = likes;
          dispatch(getUniversityFeedsSuccess(posts));
        }
      })
      .catch(e => {
        console.log(e, 'Error')
        e.response.then(error => {
          console.log(error);
        });
      });
  };
};

export const updateComment = (postId, data, page) => {
  return (dispatch, getState) => {
    let departmentPosts = getState().universityDetails.departmentFeeds;
    let universityPosts = getState().universityFeeds.universityFeeds;
    return request('/comment', 'PUT', data)
      .then(response => {
        if (page === 'Department') {
          let postIndex = _.findIndex(departmentPosts, post => post.postid === postId);
          let commentIndex = _.findIndex(
            departmentPosts[postIndex].comment,
            comment => comment.id === data.id
          );
          departmentPosts[postIndex].comment[commentIndex].comments = data.comments;
          dispatch(getDepartmentFeedsSuccess(departmentPosts));
        }
        if (page === 'University') {
          let universityPostIndex = _.findIndex(universityPosts, post => post.postid === postId);
          let commentIndex = _.findIndex(
            universityPosts[universityPostIndex].comment,
            comment => comment.id === data.id
          );
          universityPosts[universityPostIndex].comment[commentIndex].comments = data.comments;
          dispatch(getUniversityFeedsSuccess(universityPosts));
        }
        console.log(response);
        dispatch(loaderHides());
      })
      .catch(e => {
        console.log(e, 'Error in update');
        e.response.then(error => {
          console.log(error);
          dispatch(loaderHides());
        });
      });
  };
};

export const followUniversity = (universityId, data) => {
  return dispatch => {
    dispatch(loaderShows())
    return request(`/v1/universities/${universityId}/followers`, 'POST', data)
      .then(response => {
        dispatch(updateFollowedUniversities(response.body));
        dispatch(getUniversityDetails(universityId));
        dispatch(loaderHides());
      })
      .catch(e => {
        console.log(e, 'Error in following');
        // alert('Something unexpected occured')
        e.response.then(error => {
          let errorMsg = ''
          if (!_.isEmpty(error.errors)) {
            errorMsg = error.errors[0].message
          } else if (error.error) {
            errorMsg = error.error
          }
          dispatch(dialogShows('Follow university error', errorMsg, 'inform', true, null, null, 'Please try again', 'Refresh'))
          console.log(error);
          dispatch(loaderHides());
        });
      });
  }
}

export const unfollowUniversity = (universityId, universityFollowerId) => {
  return dispatch => {
    dispatch(loaderShows())
    return request(`/v1/universities/${universityId}/followers/${universityFollowerId}`, 'DELETE')
      .then(response => {
        console.log(response);
        dispatch(loaderHides());
        dispatch(unfollowUniversitySuccess(universityFollowerId))
        dispatch(getUniversityDetails(universityId));
        dispatch(setFollowingUniversity(false))
      })
      .catch(e => {
        alert('Something unexpected occured')
        console.log(e, 'Error in unfollowing');
        e.response.then(error => {
          console.log(error);
          dispatch(loaderHides());
        });
      });
  }
}

export const postComment = (data, page) => {
  return (dispatch, getState) => {
    let universityPosts = getState().universityFeeds.universityFeeds;
    let departmentPosts = getState().universityDetails.departmentFeeds;
    return request('/comment', 'POST', data)
      .then(response => {
        console.log(response);
        let comments = response.body;
        if (page === 'Department') {
          // let postIndex = _.findIndex(posts, post => post.postid === data.postId);
          // posts[postIndex].comment.push(comments);
          let departmentPostIndex = _.findIndex(departmentPosts, post => post.postid === data.postId);
          departmentPosts[departmentPostIndex].comment.push(comments);
          dispatch(getDepartmentFeedsSuccess(departmentPosts));
        }
        if (page === 'University') {
          let universityPostIndex = _.findIndex(universityPosts, post => post.postid === data.postId);
          universityPosts[universityPostIndex].comment.push(comments);
          dispatch(getUniversityFeedsSuccess(universityPosts));
        }
        dispatch(loaderHides());
      })
      .catch(e => {
        e.response.then(error => {
          console.log(error);
        });
        dispatch(loaderHides());
      });
  };
};

export const deleteComment = (postId, commentId, page) => {
  return (dispatch, getState) => {
    let universityPosts = getState().universityFeeds.universityFeeds;
    let departmentPosts = getState().universityDetails.departmentFeeds;
    return request('/comment', 'DELETE', { id: commentId })
      .then(response => {
        dispatch(loaderHides());
        if (page === 'Department') {
          let postIndex = _.findIndex(departmentPosts, post => post.postid === postId);
          let commentIndex = _.findIndex(
            departmentPosts[postIndex].comment,
            comment => comment.id === commentId
          );
          departmentPosts[postIndex].comment.splice(commentIndex, 1);
          dispatch(getDepartmentFeedsSuccess(departmentPosts));
        }
        if (page === 'University') {
          let universityPostIndex = _.findIndex(universityPosts, post => post.postid === postId);
          let commentIndex = _.findIndex(
            universityPosts[universityPostIndex].comment,
            comment => comment.id === commentId
          );
          universityPosts[universityPostIndex].comment.splice(commentIndex, 1);
          dispatch(getUniversityFeedsSuccess(universityPosts));
        }
      })
      .catch(e => {
        e.response.then(error => {
          dispatch(loaderHides());
        });
      });
  };
};

export const reportPost = (data, report) => {
  return (dispatch, getState) => {
    dispatch(loaderShows())
    let posts = getState().universityFeeds.universityFeeds
    return request('/post/report', 'POST', data)
      .then(response => {
        let postIndex = _.findIndex(posts, post => post.postid === data.postId)
        if (postIndex !== -1) {
          if (report) {
            NavigationActions.goBack()
          }
          posts.splice(postIndex, 1)
          dispatch(getUniversityFeedsSuccess(posts));
        }
        dispatch(loaderHides())
      })
      .catch(e => {
        dispatch(loaderHides())
        e.response.then(error => {
          dispatch(dialogShows('Report university error', error.message, 'inform', true, null, null, 'Please try again', 'Refresh'))
          dispatch(reportPostFailure());
        });
      });
  };
};

export const submitMissingRequest = (data, callback) => {
  return dispatch => {
    return request('/v1/universities/missing-requests', 'POST', data)
      .then(response => {
        callback();
        console.log(response, 'Missing req resp');
        setTimeout(() => {
          Alert.alert('Success', 'Thank You for requesting your college/university. We will add your college and notify you')
        }, 1000);
      })
      .catch(e => {
        Alert('Something unexpected occured')
        e.response.then(error => {
          console.log(error, 'Missing req err');
        });
      });
  };
};

export const followDepartment = (departmentId, majorId, degreeId) => {
  return (dispatch, getState) => {
    dispatch(loaderShows())
    let userId = getState().userInfoAPI.userId
    let departments = _.get(getState(), 'universityDetails.departments', {})
    let universityId = getState().universityDetails.details.univ_id
    if (majorId) {
      let requestParam = `/v1/department/${departmentId}/followers/${majorId}`
      if (degreeId) requestParam = `/v1/department/${departmentId}/followers/${majorId}/${degreeId}`
      console.log(requestParam, 'Request API')
      return request(requestParam, 'POST')
        .then(response => {
          console.log(response, 'Check')
          dispatch(getUnivesityDepartments(universityId, departments.totalElements))
          let departmentPeople = {
            myUserId: userId,
            countOnly: true,
            userTypes: ['PROFESSOR', 'UNIVERSITY_STAFF', 'CURRENT_STUDENT', 'PROSPECTIVE_STUDENT', 'ALUMNI'],
            univIds: [universityId],
            deptId: departmentId
          }
          dispatch(getDepartmentPeople(departmentPeople))
          dispatch(loaderHides())
        })
        .catch(e => {
          console.log(e, 'Errore')
          e.response.then(error => {
            console.log(error, 'Follow department failed');
          });
          dispatch(loaderHides())
        });
    } else {
      return request(`/v1/department/${departmentId}/followers`, 'POST')
        .then(response => {
          dispatch(getUnivesityDepartments(universityId, departments.totalElements))
          // let loggedInUserDepartmentFollowerList = []
          // loggedInUserDepartmentFollowerList.push(response.body)
          // console.log(response, '11221')
          // let content = _.get(departments, 'content', [])
          // let deptIndex = _.findIndex(content, department => department.deptid === departmentId)
          // if (deptIndex !== -1) {
          //   departments.content[deptIndex].loggedInUserDepartmentFollowerList = loggedInUserDepartmentFollowerList
          //   dispatch(getUniversityDepartmentsSuccess(departments))
          // }
          let departmentPeople = {
            myUserId: userId,
            countOnly: true,
            userTypes: ['PROFESSOR', 'UNIVERSITY_STAFF', 'CURRENT_STUDENT', 'PROSPECTIVE_STUDENT', 'ALUMNI'],
            univIds: [universityId],
            deptId: departmentId
          }
          dispatch(getDepartmentPeople(departmentPeople))
          dispatch(loaderHides())
        })
        .catch(e => {
          console.log(e, 'Errore')
          e.response.then(error => {
            console.log(error, 'Follow department failed');
          });
          dispatch(loaderHides())
        });
    }
  };
};

export const unfollowDepartment = (departmentId) => {
  return (dispatch, getState) => {
    dispatch(loaderShows())
    let userId = getState().userInfoAPI.userId
    let universityId = getState().universityDetails.details.univ_id
    let departments = _.get(getState(), 'universityDetails.departments', {})
    return request(`/v1/department/${departmentId}/followers`, 'DELETE')
      .then(response => {
        dispatch(getUnivesityDepartments(universityId, departments.totalElements))
        let departmentPeople = {
          myUserId: userId,
          countOnly: true,
          userTypes: ['PROFESSOR', 'UNIVERSITY_STAFF'],
          univIds: [universityId],
          deptId: departmentId
        }
        dispatch(getDepartmentPeople(departmentPeople))
        // let content = _.get(departments, 'content', [])
        // let deptIndex = _.findIndex(content, department => department.deptid === departmentId)
        // if (deptIndex !== -1) {
        //   departments.content[deptIndex].loggedInUserDepartmentFollowerList = null
        //   dispatch(getUniversityDepartmentsSuccess(departments))
        // }
        dispatch(loaderHides())
      })
      .catch(e => {
        e.response.then(error => {
          console.log(error, 'Unfollow department failed');
        });
        dispatch(loaderHides())
      });
  };
};

export const getUniversityPeople = data => {
  console.log(data, 'People List data')
  return dispatch => {
    dispatch(getUniversityPeopleRequest())
    return request('users/mypeoplesearch', 'POST', data)
      .then(response => {
        dispatch(getUniversityPeopleSuccess(response.body.content, response.body.totalElements))
      })
      .catch(e => {
        e.response.then(error => {
          dispatch(getUniversityPeopleFailure())
          console.log(error, 'University People Error');
        });
      });
  };
};

const getUniversityPeopleRequest = () => ({
  type: UniversitiesConstants.GET_UNIVERSITY_PEOPLE_REQUEST,
})

export const getUniversityPeopleSuccess = (universityPeople, count) => {
  return dispatch => {
    dispatch({ type: UniversitiesConstants.GET_UNIVERSITY_PEOPLE_SUCCESS, universityPeople, count });
  };
};

const getUniversityPeopleFailure = () => ({
  type: UniversitiesConstants.GET_UNIVERSITY_PEOPLE_FAILURE,
})

export const getDepartmentPeople = data => {
  return dispatch => {
    dispatch(getDepartmentPeopleRequest())
    return request('users/mypeoplesearch', 'POST', data)
      .then(response => {
        dispatch(getDepartmentPeopleSuccess(response.body.content, response.body.totalElements))
      })
      .catch(e => {
        e.response.then(error => {
          dispatch(getDepartmentPeopleFailure())
          console.log(error, 'Department People Error');
        });
      });
  };
};

export const createDepartment = (data, type) => {
  const api = type === 'academic' ? 'v1/department/academic' : type === 'non-academic' ? '/v1/department/non-academic' : '/v1/department/associationorclub'
  return (dispatch, getState) => {
    let departments = getState().universityDetails.departments
    console.log(data, 'Create department data')
    dispatch(createDepartmentRequest())
    return request(api, 'POST', data)
      .then(response => {
        let newDepartment = response.body
        // newDepartment.loggedInUserDepartmentFollowerList = [{connectionStatus: 'FOLLOW'}]
        departments.content.unshift(response.body)
        dispatch(getPaginatedDepartmentsSuccess(departments))
        // NavigationActions.goBack();
        NavigationActions.replace('Department', { id: response.body.deptid })
        dispatch(getUnivesityDepartments(data.universityId, departments.totalElements + 1))
        dispatch(createDepartmentSuccess())
      })
      .catch(e => {
        e.response.then(error => {
          dispatch(createDepartmentFailure())
          console.log(error, 'Department People Error');
        });
      });
  };
}

export const updateDepartment = (data, deptId) => {
  return (dispatch, getState) => {
    dispatch(loaderShows())
    let departments = getState().universityDetails.departments
    dispatch(updateDepartmentRequest())
    return request(`/v1/department/${deptId}`, 'PUT', data)
      .then(response => {
        let updatedDepartment = response.body
        let deptIndex = _.findIndex(departments.content, department => department.deptid === deptId)
        if (deptIndex !== -1) {
          departments.content[deptIndex] = updatedDepartment
        }
        dispatch(getPaginatedDepartmentsSuccess(departments))
        dispatch(getUnivesityDepartments(data.universityId, departments.totalElements, 'navigate'))
        dispatch(updateDepartmentSuccess())
        dispatch(loaderHides())
      })
      .catch(e => {
        e.response.then(error => {
          dispatch(updateDepartmentFailure())
          console.log(error, 'Department People Error');
        });
        dispatch(loaderHides())
      });
  };
}

export const deleteDepartment = (deptId, universityId) => {
  return (dispatch, getState) => {
    dispatch(loaderShows())
    let departments = getState().universityDetails.departments
    dispatch(deleteDepartmentRequest())
    return request(`/v1/department/${deptId}`, 'DELETE')
      .then(response => {
        if (response) {
          let updatedDepartments = departments.content
          let deptIndex = _.findIndex(departments.content, department => department.deptid === deptId)
          if (deptIndex !== -1) {
            updatedDepartments.splice(deptIndex, 1)
            departments.content = updatedDepartments
          }
          dispatch(getPaginatedDepartmentsSuccess(departments))
          dispatch(getUnivesityDepartments(universityId, 20, 'navigate'))
          dispatch(deleteDepartmentSuccess())
          dispatch(loaderHides())
        }
      })
      .catch(e => {
        e.response.then(error => {
          dispatch(deleteDepartmentFailure())
          console.log(error, 'Department People Error');
        });
        dispatch(loaderHides())
      });
  };
}

export const addMajor = (departmentId, data) => {
  return (dispatch, getState) => {
    dispatch(loaderShows())
    let universityId = getState().universityDetails.details.univ_id
    let departments = getState().universityDetails.departments
    return request(`v1/department/${departmentId}/major`, 'POST', data)
      .then(response => {
        console.log(response, 'Update major')
        dispatch(getUnivesityDepartments(universityId, departments.totalElements, 'navigate'))
        // NavigationActions.goBack()
        dispatch(loaderHides())
      })
      .catch(e => {
        e.response.then(error => {
          console.log(error, 'Update Major Error');
        });
        dispatch(loaderHides())
      });
  };
}

export const addLevelOfStudy = (majorId, data, navigate) => {
  console.log(majorId, data, 'Check The api call')
  return (dispatch, getState) => {
    dispatch(loaderShows())
    console.log(data)
    let universityId = getState().universityDetails.details.univ_id
    let departments = getState().universityDetails.departments
    return request(`v1/department/major/${majorId}/levelofstudy/multiple?levelOfStudy=${data}`, 'POST')
      .then(response => {
        dispatch(getUnivesityDepartments(universityId, departments.totalElements, navigate))
        // if (navigate) NavigationActions.goBack()
        dispatch(loaderHides())
      })
      .catch(e => {
        e.response.then(error => {
          console.log(error, 'Add level Of study Error');
        });
        dispatch(loaderHides())
      });
  };
}

export const updateMajorName = (majorId, majorName, navigate) => {
  return (dispatch, getState) => {
    dispatch(loaderShows())
    let universityId = getState().universityDetails.details.univ_id
    let departments = getState().universityDetails.departments
    return request(`v1/department/major/${majorId}?newMajorName=${majorName}`, 'PUT')
      .then(response => {
        console.log(response, 'Update major name')
        dispatch(getUnivesityDepartments(universityId, departments.totalElements, navigate))
        // if (navigate) NavigationActions.goBack()
        dispatch(loaderHides())
      })
      .catch(e => {
        e.response.then(error => {
          console.log(error, 'Update Major Error');
        });
        dispatch(loaderHides())
      });
  };
}

export const deleteMajor = (majorId) => {
  return (dispatch, getState) => {
    dispatch(loaderShows())
    let universityId = getState().universityDetails.details.univ_id
    let departments = getState().universityDetails.departments
    return request(`v1/department/major/${majorId}`, 'DELETE')
      .then(response => {
        console.log(response, 'Delete major')
        dispatch(getUnivesityDepartments(universityId, departments.totalElements, 'navigate'))
        // NavigationActions.goBack()
        dispatch(loaderHides())
      })
      .catch(e => {
        e.response.then(error => {
          console.log(error, 'Delete major Error');
        });
        dispatch(loaderHides())
      });
  };
}

export const deleteLevelOfStudy = (majorId, levelOfStudyIds) => {
  return (dispatch, getState) => {
    dispatch(loaderShows())
    let universityId = getState().universityDetails.details.univ_id
    let departments = getState().universityDetails.departments
    return request(`v1/department/major/${majorId}/levelofstudy/multiple?levelOfStudyOfferedIds=${levelOfStudyIds}`, 'DELETE')
      .then(response => {
        console.log(response, 'Check Response')
        dispatch(getUnivesityDepartments(universityId, departments.totalElements, 'navigate'))
        // NavigationActions.goBack()
        dispatch(loaderHides())
      })
      .catch(e => {
        e.response.then(error => {
          console.log(error, 'Delete level of study Error');
        });
        dispatch(loaderHides())
      });
  };
}

export const getAdminCandidate = (unvId) => {
  return (dispatch) => {
    dispatch(getAdminCandidateRequest())
    return request(`v1/department/${unvId}/admin-candidate-search`, 'POST', { name: '' })
      .then(response => {
        console.log(response, 'Candiate Response')
        dispatch(getAdminCandidateSuccess(response.body))
      })
      .catch(e => {
        e.response.then(error => {
          dispatch(getAdminCandidateFailure())
          console.log(error, 'Department People Error');
        });
      });
  };
}

const getAdminCandidateRequest = () => ({
  type: UniversitiesConstants.GET_ADMIN_CANDIDATE_REQUEST
});
const getAdminCandidateSuccess = (candidates) => ({
  type: UniversitiesConstants.GET_ADMIN_CANDIDATE_SUCCESS,
  candidates
});
const getAdminCandidateFailure = () => ({
  type: UniversitiesConstants.GET_ADMIN_CANDIDATE_FAILURE
});

const createDepartmentRequest = () => ({
  type: UniversitiesConstants.CREATE_DEPARTMENT_REQUEST
});
const createDepartmentSuccess = () => ({
  type: UniversitiesConstants.CREATE_DEPARTMENT_SUCCESS
});
const createDepartmentFailure = () => ({
  type: UniversitiesConstants.CREATE_DEPARTMENT_FAILURE
});

const updateDepartmentRequest = () => ({
  type: UniversitiesConstants.UPDATE_DEPARTMENT_REQUEST
});
const updateDepartmentSuccess = () => ({
  type: UniversitiesConstants.UPDATE_DEPARTMENT_SUCCESS
});
const updateDepartmentFailure = () => ({
  type: UniversitiesConstants.UPDATE_DEPARTMENT_FAILURE
});

const deleteDepartmentRequest = () => ({
  type: UniversitiesConstants.DELETE_DEPARTMENT_REQUEST
});
const deleteDepartmentSuccess = () => ({
  type: UniversitiesConstants.DELETE_DEPARTMENT_SUCCESS
});
const deleteDepartmentFailure = () => ({
  type: UniversitiesConstants.DELETE_DEPARTMENT_FAILURE
});

export const setFollowingUniversity = followingUniversity => {
  return dispatch => {
    dispatch({ type: UniversitiesConstants.SET_FOLLOWING_UNIVERSITY, followingUniversity });
  }
}

export const setUsertype = usertype => {
  return dispatch => {
    dispatch({ type: UniversitiesConstants.SET_USERTYPE, usertype });
  }
}

const getDepartmentPeopleRequest = () => ({
  type: UniversitiesConstants.GET_DEPARTMENT_PEOPLE_REQUEST,
})

export const getDepartmentPeopleSuccess = (departmentPeople, count) => {
  return dispatch => {
    dispatch({ type: UniversitiesConstants.GET_DEPARTMENT_PEOPLE_SUCCESS, departmentPeople, count });
  };
};

const getDepartmentPeopleFailure = () => ({
  type: UniversitiesConstants.GET_DEPARTMENT_PEOPLE_FAILURE,
})


export const clearUniversityData = () => {
  return dispatch => {
    dispatch(clearUniversityDataAction())
  }
}

export const setPostAudience = array => {
  return dispatch => {
    dispatch({ type: UniversitiesConstants.SET_POST_AUDIENCE, array: array });
  };
};
export const setPostMajor = array => {
  return dispatch => {
    dispatch({ type: UniversitiesConstants.SET_POST_MAJOR, array: array });
  };
};
export const setTaggedUsers = array => {
  return dispatch => {
    dispatch({ type: UniversitiesConstants.SET_POST_TAGS, array: array });
  };
};

export const updateFollowedUniversities = (university) => {
  return dispatch => {
    dispatch({ type: UniversitiesConstants.FOLLOW_UNIVERSITY_SUCCESS, university });
  }
}

export const unfollowUniversitySuccess = (universityFollowId) => {
  return dispatch => {
    dispatch({ type: UniversitiesConstants.UNFOLLOW_UNIVERSITY_SUCCESS, universityFollowId });
  }
}

export const clearUniversityDataAction = () => {
  return dispatch => {
    dispatch({ type: UniversitiesConstants.CLEAR_UNIVERSITY_DATA });
  }
}
