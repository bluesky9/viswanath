import { InviteConstants } from '../constants'
import { request } from '../request';
import { Alert } from 'react-native';
import { dialogShows } from './ErrorModal';

export const invitePeople = (email) => {
  return dispatch => {
    dispatch(inviteRequest())
    let body = {
      email
    }
    return request('invite','POST', body)
      .then((response)=> {
        console.log(response, 'Invite Response')
        Alert.alert('Invite send successfully')
        dispatch(inviteSuccess())
      })
      .catch((e)=> {
        console.log(e, 'PeopleError!!!')
        // dispatch(loaderHides())
        e.response.then((error)=>{
          dispatch(dialogShows('Invite error',error.errors[0].message ,'inform',true, null, null, 'Please try again', 'Refresh' ))
          dispatch(inviteFailure())
        })
      })
  }
}

const inviteRequest = () => ({
  type: InviteConstants.INVITE_REQUEST
});
const inviteSuccess = () => ({
  type: InviteConstants.INVITE_SUCCESS
});
const inviteFailure = () => ({
  type: InviteConstants.INVITE_FAILURE
});