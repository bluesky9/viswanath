import update from 'immutability-helper';
// import { Cookies } from 'react-cookie';
import {request, requestWithoutToken} from '../request'
import {PeopleConstants} from '../constants'
import * as NavigationActions from '../../NavigationService'
import { loaderShows, loaderHides } from './Loader';
import { dialogShows } from './ErrorModal';
import { getUserInfoSuccess, getProfileDetails } from './Profile';
import { getUniversityPeopleSuccess, getDepartmentPeopleSuccess } from './Universities';
import _ from 'lodash';

// getPeople START
/**
 * Get people list for network page, university people list, department people list.
 *
 * @param  {object} data to be sent to the API. Should be different for each list type. Check swagger for more info
 *
 * @return {content:Array, totalElements:Number}
 */
export const getPeople = (data) => {
  return dispatch => {
    dispatch(PeopleRequest())
    // dispatch(loaderShows())
    // const cookies = new Cookies();
    // let updatedData = update(data,{
    //   $merge:{"myUserId": '8' }
    // })
    return request('users/mypeoplesearch','POST', data)
      .then((response)=> {
        // dispatch(loaderHides())
        dispatch(PeopleSuccess(response.body))
      })
      .catch((e)=> {
        console.log(e, 'PeopleError!!!')
        // dispatch(loaderHides())
        e.response.then((error)=>{
          dispatch(PeopleFailure(error.error,error.message,e.status))
        })
      })
  }
}

export const setFilterApplied = (filterApplied) => {
  return dispatch => {
    dispatch({ type: PeopleConstants.SET_FILTER_APPLIED, filterApplied });
  }
}

export const getPeopleInfo = (id, profile, source) => {
  return dispatch => {

    dispatch(loaderShows())
    dispatch(peopleInfoRequest())

    let promiseProfile = request('profileUpdate?userId=' + id,'GET')
    let promiseFollowers = request('userFollow/followers?userId=' + id, 'GET')
    let promiseFollowing = request('userFollow/following?userId=' + id, 'GET')
    let promiseConnection = request(`userConnection/info/${id}`, 'GET')

    Promise.all([promiseProfile, promiseFollowers, promiseFollowing, promiseConnection])
    .then((responses) => {
      let peopleInfo = responses[0].body;
      peopleInfo.followers = responses[1].body;
      peopleInfo.following = responses[2].body;
      peopleInfo.connectionStatus = responses[3].body.status
      peopleInfo.myRequest = responses[3].body.myRequest
      peopleInfo.userConnectionId = responses[3].body.userConnectionId
      dispatch(peopleInfoSuccess(peopleInfo, id))
      dispatch(loaderHides())
      // if (source === 'otherProfile') {
      //   NavigationActions.goBack({id});
      if (!profile)
        NavigationActions.navigate('ProfilePeople', {id})
    })
    .catch((e) => {
      dispatch(loaderHides())
      console.log(e, 'getPeopleInfo Promise All Error')
      dispatch(dialogShows('People profile error','Unable to load people','inform',true, null, null, 'Please try again', 'Refresh' ))
    })
    // dispatch(loaderShows())
    // dispatch(peopleInfoRequest())
    // return request('/profileUpdate?userId=' + id,'GET')
    //   .then((response)=> {

    //     let peopleInfo = response.body;
    //     peopleInfo.followers = [];
    //     peopleInfo.following = [];
    //     return request('userFollow/followers?userId=' + id, 'GET', undefined, undefined, {userID: id})
    //     .then((response))
    //     return request('/user/info', 'GET', undefined, undefined, {userID: id})
    //     .then((response) => {

    //       peopleInfo.followers = response.body.followers
    //       peopleInfo.following = response.body.following
    //       peopleInfo.connections = response.body.connections
    //       return request(`/userConnection/info/${peopleInfo.id}`, 'GET')
    //       .then((response) => {

    //         peopleInfo.connectionStatus = response.body.status
    //         peopleInfo.myRequest = response.body.myRequest
    //         dispatch(peopleInfoSuccess(peopleInfo))
    //         dispatch(loaderHides())
    //         if (!profile)
    //           NavigationActions.navigate('ProfilePeople')
    //       })
    //     })
    //   })
    //   .catch((e)=> {
    //     dispatch(loaderHides())
    //     e.response.then((error)=>{
    //       dispatch(peopleInfoFailure(error.error,error.message,e.status))
    //     })
    //     console.log(e, 'PeopleError!!!')
    //   })
  }
}

export const connectPeople = (connectId, message) => {
  return (dispatch, getState) => {
    dispatch(connectPeopleRequest())
    dispatch(loaderShows())
    let data ={
      connectId,
      message
    }
    return request('userConnection','POST',data)
      .then((response)=> {
        dispatch(getPeopleInfo(connectId, 'Profile')) // Api Call Addedto fetch updated Data
        dispatch(connectPeopleSuccess())
        dispatch(loaderHides())
      })
      .catch((e)=> {
        e.response.then((error)=>{
          console.log(error, 'EEERR')
          dispatch(connectPeopleFailure(error.error,error.message,e.status))
        })
        dispatch(loaderHides())
      })
  }
}

export const disconnectPeople = (connectId) => {
  return (dispatch, getState) => {
    dispatch(disconnectPeopleRequest())
    dispatch(loaderShows())
    let data ={
      connectId
    }
    return request('userConnection','DELETE',data)
      .then((response)=> {
        dispatch(getPeopleInfo(connectId, 'Profile')) // Api Call Addedto fetch updated Data
        dispatch(disconnectPeopleSuccess())
        dispatch(loaderHides())
      })
      .catch((e)=> {
        e.response.then((error)=>{
          dispatch(disconnectPeopleFailure(error.error,error.message,e.status))
        })
        dispatch(loaderHides())
      })
  }

}

export const getBlockedPeople = () => {
  return dispatch => {
    dispatch(getBlockedPeopleRequest())
    dispatch(loaderShows())
    
    return request('/user/blocked','GET')
      .then((response)=> {
        dispatch(getBlockedPeopleSuccess(response.body))
        dispatch(loaderHides())
      })
      .catch((e)=> {
        e.response.then((error)=>{
          console.log(error, 'EEERR')
          dispatch(getBlockedPeopleFailure())
        })
        dispatch(loaderHides())
      })
  }
}

export const blockPeople = (otherId) => {
  return (dispatch, getState) => {
    let userId = getState().userInfoAPI.userId
    dispatch(blockPeopleRequest())
    dispatch(loaderShows())
    let data ={
      otherId
    }
    return request('/user/block','POST',data)
      .then((response)=> {
        dispatch(getPeopleInfo(otherId, 'Profile'))
        dispatch(blockPeopleSuccess())
        dispatch(getProfileDetails(userId))
        dispatch(loaderHides())
      })
      .catch((e)=> {
        e.response.then((error)=>{
          console.log(error, 'EEERR')
          dispatch(blockPeopleFailure(error.error,error.message,e.status))
        })
        dispatch(loaderHides())
      })
  }
}

export const unBlockPeople = (otherId) => {
  return (dispatch, getState) => {
    let blockedPeoples = getState().people.blockedPeople
    dispatch(unBlockPeopleRequest())
    dispatch(loaderShows())
    let data ={
      otherId
    }
    return request('/user/unblock','POST',data)
      .then((response)=> {
        let blockedPeopleIndex = _.findIndex(blockedPeoples, blockedPeople => blockedPeople.id === otherId)
        if (blockedPeopleIndex !== -1) {
          blockedPeoples.splice(blockedPeopleIndex, 1)
          dispatch(getBlockedPeopleSuccess(blockedPeoples))
        }
        dispatch(getPeopleInfo(otherId, 'Profile'))
        dispatch(unBlockPeopleSuccess())
        dispatch(loaderHides())
      })
      .catch((e)=> {
        e.response.then((error)=>{
          console.log(error, 'EEERR')
          dispatch(unBlockPeopleFailure(error.error,error.message,e.status))
        })
        dispatch(loaderHides())
      })
  }
}

export const followPeople = (peopleId, page, userType) => {
  return (dispatch, getState) => {
    let peoples = getState().people.peopleList
    let userInfo = getState().userInfoAPI.userInfo
    let userId = getState().userInfoAPI.userId
    let universityPeople = getState().universityDetails.universityPeople
    let departmentPeople = getState().universityDetails.departmentPeople

    dispatch(followPeopleRequest())
    dispatch(loaderShows())
    return request('userFollow','POST',{followId: peopleId})
      .then((response)=> {
        if (userType === 'other') {
          let peopleIndex = _.findIndex(peoples.content, people => people.id === peopleId)
          
          if (peopleIndex !== -1) {
            peoples.content[peopleIndex].following = true;
            dispatch(PeopleSuccess(peoples))
          }

          if (page === 'Profile') {
            dispatch(getPeopleInfo(peopleId, 'Profile')) // Api Call Addedto fetch updated Data
            let followersIndex = _.findIndex(userInfo.followers, follower => follower.id === peopleId)
            let followingIndex = _.findIndex(userInfo.following, follow => follow.id === peopleId)
            let connectionIndex = _.findIndex(userInfo.connections, connect => connect.id === peopleId)
            
            if (followersIndex !== -1) {
              userInfo.followers[followersIndex].following = true;
            }
            if (followingIndex !== -1) {
              userInfo.following[followingIndex].following = true;
            }
            if (connectionIndex !== -1) {
              userInfo.connections[connectionIndex].following = true;
            }
            dispatch(getUserInfoSuccess(userInfo))
            dispatch(getProfileDetails(userId))
          }

          if (page === 'University') {
            let universityPeopleIndex = _.findIndex(universityPeople, people => people.id === peopleId)
            universityPeople[universityPeopleIndex].following = true;
            dispatch(getUniversityPeopleSuccess(universityPeople))
          }

          if (page === 'Department') {
            let departmentPeopleIndex = _.findIndex(departmentPeople, people => people.id === peopleId)
            departmentPeople[departmentPeopleIndex].following = true;
            dispatch(getDepartmentPeopleSuccess(departmentPeople))
          }
          

          if (page === 'Followers') {
            let peopleInfo = getState().peopleInfo.peopleInfo[peopleId]
            if (peopleInfo) {
              let followersIndex = _.findIndex(peopleInfo.followers, follower => follower.id === peopleId)
              let followingIndex = _.findIndex(peopleInfo.following, follow => follow.id === peopleId)
              if (followersIndex !== -1) {
                peopleInfo.followers[followersIndex].following = true;
              }
              if (followingIndex !== -1) {
                peopleInfo.following[followingIndex].following = true;
              }
              dispatch(peopleInfoSuccess(peopleInfo, peopleId))
            }
            dispatch(getProfileDetails(userId))
          }  
        } else {
          let followersIndex = _.findIndex(userInfo.followers, follower => follower.id === peopleId)
          let followingIndex = _.findIndex(userInfo.following, follow => follow.id === peopleId)
          let connectionIndex = _.findIndex(userInfo.connections, connect => connect.id === peopleId)
          
          if (followersIndex !== -1) {
            userInfo.followers[followersIndex].following = true;
          }
          if (followingIndex !== -1) {
            userInfo.following[followingIndex].following = true;
          }
          if (connectionIndex !== -1) {
            userInfo.connections[connectionIndex].following = true;
          }
          dispatch(getUserInfoSuccess(userInfo))
        } 
        dispatch(followPeopleSuccess())
        dispatch(loaderHides())
      })
      .catch((e)=> {
        console.log(e, '########')
        e.response.then((error)=>{
          dispatch(followPeopleFailure(error.error,error.message,e.status))
        })
        dispatch(loaderHides())
      })
  }
}

export const unFollowPeople = (peopleId, page, userType) => {
  return (dispatch, getState) => {
    let peoples = getState().people.peopleList
    let userInfo = getState().userInfoAPI.userInfo
    let universityPeople = getState().universityDetails.universityPeople
    let departmentPeople = getState().universityDetails.departmentPeople

    dispatch(unFollowPeopleRequest())
    dispatch(loaderShows())
    return request('userFollow','DELETE',{followId: peopleId})
      .then((response)=> {
        if (userType === 'other') {
          let peopleIndex = _.findIndex(peoples.content, people => people.id === peopleId)
          if (peopleIndex !== -1) {
            peoples.content[peopleIndex].following = false;
            dispatch(PeopleSuccess(peoples))
          }
          if(page === 'Profile') {
            dispatch(getPeopleInfo(peopleId, 'Profile')) // Api Call Addedto fetch updated Data
            let followersIndex = _.findIndex(userInfo.followers, follower => follower.id === peopleId)
            let followingIndex = _.findIndex(userInfo.following, follow => follow.id === peopleId)
            let connectionIndex = _.findIndex(userInfo.connections, connect => connect.id === peopleId)
            
            if (followersIndex !== -1) {
              userInfo.followers[followersIndex].following = false;
            }
            if (followingIndex !== -1) {
              console.log(userInfo, followingIndex, 'Before')
              userInfo.following.splice(followingIndex, 1)
              console.log(userInfo, followingIndex, 'After')
            }
            if (connectionIndex !== -1) {
              userInfo.connections[connectionIndex].following = false;
            }
            dispatch(getUserInfoSuccess(userInfo))
          }

          if (page === 'University') {
            let universityPeopleIndex = _.findIndex(universityPeople, people => people.id === peopleId)
            universityPeople[universityPeopleIndex].following = false;
            dispatch(getUniversityPeopleSuccess(universityPeople))
          }

          if (page === 'Department') {
            let departmentPeopleIndex = _.findIndex(departmentPeople, people => people.id === peopleId)
            departmentPeople[departmentPeopleIndex].following = true;
            dispatch(getDepartmentPeopleSuccess(departmentPeople))
          }

          if (page === 'Followers') {
            let peopleInfo = getState().peopleInfo.peopleInfo[peopleId];
            if (peopleInfo) {
              let followersIndex = _.findIndex(peopleInfo.followers, follower => follower.id === peopleId)
              let followingIndex = _.findIndex(peopleInfo.following, follow => follow.id === peopleId)
              let UserFollowersIndex = _.findIndex(userInfo.followers, follower => follower.id === peopleId)
              let UserFollowingIndex = _.findIndex(userInfo.following, follow => follow.id === peopleId)
              let UserConnectionIndex = _.findIndex(userInfo.connections, connect => connect.id === peopleId)
              if (followersIndex !== -1) {
                peopleInfo.followers[followersIndex].following = false;
              }
              if (followingIndex !== -1) {
                peopleInfo.following[followingIndex].following = false;
              }
              if (UserFollowersIndex !== -1) {
                userInfo.followers[UserFollowersIndex].following = false;
              }
              if (UserFollowingIndex !== -1) {
                userInfo.following.splice(UserFollowingIndex, 1)
              }
              if (UserConnectionIndex !== -1) {
                userInfo.connections[UserConnectionIndex].following = false;
              }
              dispatch(peopleInfoSuccess(peopleInfo, peopleId))
            }
            dispatch(getUserInfoSuccess(userInfo))
          }  
        } else {
          let followersIndex = _.findIndex(userInfo.followers, follower => follower.id === peopleId)
          let followingIndex = _.findIndex(userInfo.following, follow => follow.id === peopleId)
          let connectionIndex = _.findIndex(userInfo.connections, connect => connect.id === peopleId)
          
          if (followersIndex !== -1) {
            userInfo.followers[followersIndex].following = false;
          }
          if (followingIndex !== -1) {
            userInfo.following.splice(followingIndex, 1)
          }
          if (connectionIndex !== -1) {
            userInfo.connections[connectionIndex].following = false;
          }
          dispatch(getUserInfoSuccess(userInfo))
        }
        dispatch(unFollowPeopleSuccess())
        dispatch(loaderHides())
      })
      .catch((e)=> {
        e.response.then((error)=>{
          dispatch(unFollowPeopleFailure(error.error,error.message,e.status))
        })
        dispatch(loaderHides())
      })
  }
}

// Follow People
const followPeopleRequest = () => ({
  type: PeopleConstants.FOLLOW_PEOPLE_REQUEST
});
const followPeopleSuccess = () => ({
  type: PeopleConstants.FOLLOW_PEOPLE_SUCCESS
});
const followPeopleFailure = () => ({
  type: PeopleConstants.FOLLOW_PEOPLE_FAILURE
});

// UnFollow People
const unFollowPeopleRequest = () => ({
  type: PeopleConstants.UNFOLLOW_PEOPLE_REQUEST
});
const unFollowPeopleSuccess = () => ({
  type: PeopleConstants.UNFOLLOW_PEOPLE_SUCCESS
});
const unFollowPeopleFailure = () => ({
  type: PeopleConstants.UNFOLLOW_PEOPLE_FAILURE
});

const PeopleRequest = () => ({
  type: PeopleConstants.PEOPLE_REQUEST,
})
const PeopleSuccess = (peopleList) => ({
  type: PeopleConstants.PEOPLE_SUCCESS,
  peopleList
})
const PeopleFailure = (errorType,errorMessage,errorCode) => ({
  type: PeopleConstants.PEOPLE_FAILURE,
  errorType,
  errorMessage,
  errorCode
})
// getPeople END


// getPeopleByName START
export const getPeopleByName = (name) => {
  return dispatch => {
    dispatch(getPeopleByNameRequest())

    return request('users/search','POST',{name:name})
      .then((response)=> {
        dispatch(getPeopleByNameSuccess(response.body))
      })
      .catch((e)=> {
        e.response.then((error)=>{
          dispatch(getPeopleByNameFailure(error.error,error.message,e.status))
        })
      })
  }
}

export const setPeopleFilters = filters => {
  return dispatch => {
    dispatch(setPeopleFiltersRequest(filters));
  };
};

const setPeopleFiltersRequest = filters => ({
  type: PeopleConstants.PEOPLE_FILTERS_REQUEST,
  filters: filters
});

const getPeopleByNameRequest = () => ({
  type: PeopleConstants.SEARCH_PEOPLE_BY_NAME_REQUEST,
})
const getPeopleByNameSuccess = (usersList) => ({
  type: PeopleConstants.SEARCH_PEOPLE_BY_NAME_SUCCESS,
  usersList
})
const getPeopleByNameFailure = (errorType,errorMessage,errorCode) => ({
  type: PeopleConstants.SEARCH_PEOPLE_BY_NAME_FAILURE,
  errorType,
  errorMessage,
  errorCode
})

// getPeopleByName END
const peopleInfoRequest = () => ({
  type: PeopleConstants.PEOPLE_INFO_REQUEST,
})
const peopleInfoSuccess = (peopleInfo, id) => ({
  type: PeopleConstants.PEOPLE_INFO_SUCCESS,
  peopleInfo,
  id
})
const peopleInfoFailure = (errorType,errorMessage,errorCode) => ({
  type: PeopleConstants.PEOPLE_INFO_FAILURE,
  errorType,
  errorMessage,
  errorCode
})
// connect people
const connectPeopleRequest = () => ({
  type: PeopleConstants.CONNECT_PEOPLE_REQUEST,
})
const connectPeopleSuccess = () => ({
  type: PeopleConstants.CONNECT_PEOPLE_SUCCESS
})
const connectPeopleFailure = () => ({
  type: PeopleConstants.CONNECT_PEOPLE_FAILURE,
})
// disconnect people
const disconnectPeopleRequest = () => ({
  type: PeopleConstants.DISCONNECT_PEOPLE_REQUEST,
})
const disconnectPeopleSuccess = () => ({
  type: PeopleConstants.DISCONNECT_PEOPLE_SUCCESS
})
const disconnectPeopleFailure = () => ({
  type: PeopleConstants.DISCONNECT_PEOPLE_FAILURE,
})
// block people
const blockPeopleRequest = () => ({
  type: PeopleConstants.BLOCK_PEOPLE_REQUEST,
})
const blockPeopleSuccess = () => ({
  type: PeopleConstants.BLOCK_PEOPLE_SUCCESS
})
const blockPeopleFailure = () => ({
  type: PeopleConstants.BLOCK_PEOPLE_FAILURE,
})
// unblock people
const unBlockPeopleRequest = () => ({
  type: PeopleConstants.UNBLOCK_PEOPLE_REQUEST,
})
const unBlockPeopleSuccess = () => ({
  type: PeopleConstants.UNBLOCK_PEOPLE_SUCCESS
})
const unBlockPeopleFailure = () => ({
  type: PeopleConstants.UNBLOCK_PEOPLE_FAILURE,
})
// get blocked people
const getBlockedPeopleRequest = () => ({
  type: PeopleConstants.GET_BLOCKED_PEOPLE_REQUEST,
})
const getBlockedPeopleSuccess = (blockedPeople) => ({
  type: PeopleConstants.GET_BLOCKED_PEOPLE_SUCCESS,
  blockedPeople
})
const getBlockedPeopleFailure = () => ({
  type: PeopleConstants.GET_BLOCKED_PEOPLE_FAILURE,
})
