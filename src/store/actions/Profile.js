import update from 'immutability-helper';
import { AsyncStorage } from 'react-native';
// import { Cookies } from 'react-cookie';
import { request } from '../request';
import * as NavigationActions from '../../NavigationService';
import { ProfileConstants } from '../constants';
import { dialogShows } from './ErrorModal';
import { loaderShows, loaderHides } from './Loader';
import { createDepartment } from './Universities';
// Simple hack before the bakend is fixed
//
let gUserId = undefined;

export const getUserConnectionsList = (userId, userInfo) => {
  return dispatch => {
    let promiseFollowers = request(
      'userFollow/followers?userId=' + userId,
      'GET'
    );
    let promiseFollowing = request(
      'userFollow/following?userId=' + userId,
      'GET'
    );
    let promiseConnection = request(`userConnection?userId=${userId}`, 'GET');
    Promise.all([promiseFollowers, promiseFollowing, promiseConnection])
      .then(responses => {
        userInfo.followers = responses[0].body;
        userInfo.following = responses[1].body;
        userInfo.connections = responses[2].body;
        dispatch(getUserInfoSuccess(userInfo));
      })
      .catch(e => {
        dispatch(loaderHides());
        console.log(e, 'User Connection List Promise All Error');
      });
  };
};

export const getProfileDetails = userId => {
  return dispatch => {
    return request('user/info', 'GET', undefined, undefined, { userId: userId })
      .then(data => {
        let userInfo = data.body;
        return AsyncStorage.getItem('userId').then(userId => {
          if (userId) userInfo.id = parseInt(userId, 10);
          dispatch(getUserInfoSuccess(userInfo));
          dispatch(getUserConnectionsList(userId, userInfo));
        });
      })
      .catch(e => {
        e.response.then(error => {
          dispatch(getUserInfoFailure(error.error, error.message, e.status));
        });
      });
  };
};

export const getExtraProfileDetails = (userId, returnResponse = false) => {
  return (dispatch, getState) => {
    dispatch(getExtraProfileDetailsRequest());
    dispatch(loaderShows());
    return request('profileUpdate?userId=' + userId, 'GET')
      .then(profileDetails => {
        dispatch(loaderHides());
        if (returnResponse) {
          return profileDetails.body;
        }
        dispatch(getExtraProfileDetailsSuccess(profileDetails.body));
      })
      .catch(e => {
        e.response.then(error => {
          dispatch(loaderHides());
          dispatch(
            getExtraProfileDetailsFailure(error.error, error.message, e.status)
          );
        });
      });
  };
};

export const saveBasicProfileDetails = userData => {
  console.log(userData, 'USSRRS');
  return dispatch => {
    dispatch(loaderShows());
    return request('profileUpdate/website', 'POST', userData)
      .then(profileDetails => {
        userData.shortDesc = userData.bio;
        userData.longDesc = userData.bio;
        return request('profileUpdate/bio', 'PUT', userData).then(() => {
          dispatch(loaderHides());
          dispatch(getExtraProfileDetailsSuccess(userData));
          NavigationActions.goBack();
        });
      })
      .catch(e => {
        let errorMsg = '';
        dispatch(loaderHides());
        e.response.then(error => {
          if (error.errors) {
            error.errors.map(item => {
              errorMsg =
                errorMsg +
                (item.field ? item.field : '') +
                ' ' +
                (item.message ? item.message : '') +
                '\n';
            });
          }
          dispatch(
            dialogShows(
              'Update error',
              errorMsg,
              'inform',
              true,
              null,
              null,
              'Please try again',
              'Refresh'
            )
          );
        });
      });
  };
};

export const uploadProfilePic = (
  image,
  userData,
  createDepartmentObj,
  departmentType
) => {
  return (dispatch, getState) => {
    dispatch(loaderShows());
    dispatch(uploadProfilePicRequest());

    let userId = getState().userInfoAPI.userId;
    let userInfo = getState().userInfoAPI.userInfo;

    console.log(image, 'Images Action');
    if (image.uri) {
      console.log(image, 'check map');
      let filePath = 'user/' + userId + '/post/' + '.jpg';
      var formData = new FormData();
      formData.append('file', image);
      formData.append('filePath', filePath);
      request('/uploadfile', 'POST', formData, 'multipart/form-data')
        .then(response => {
          console.log(response, 'Image Response');
          let http = 'https://';
          let file = {
            mediumProfilePicPath:
              http + response.body.object['fileS3URL200x200'],
            originalProfilePicPath: http + response.body.object['fileS3URL'],
            smallProfilePicPath: http + response.body.object['fileS3URL40x40']
          };
          if (createDepartmentObj) {
            createDepartmentObj.logo = file.mediumProfilePicPath;
            dispatch(createDepartment(createDepartmentObj, departmentType));
            dispatch(loaderHides());
          } else {
            let query = `originalProfilePicPath=${
              file.originalProfilePicPath
              }&mediumProfilePicPath=${
              file.mediumProfilePicPath
              }&smallProfilePicPath=${file.smallProfilePicPath}`;
            request('profPic/picture?' + query, 'POST')
              .then(profileResponse => {
                console.log(profileResponse, 'Yeah');

                if (userInfo) {
                  userInfo.profilePicture = profileResponse.body.profPicPath;
                }
                dispatch(getUserInfoSuccess(userInfo));
                dispatch(uploadProfilePicSuccess());
                dispatch(loaderHides());
                dispatch(saveBasicProfileDetails(userData));
              })
              .catch(e => {
                dispatch(loaderHides());
                dispatch(uploadProfilePicFailure());
                console.log(e, 'Profile Pic Error');
              });
          }
        })
        .catch(e => {
          console.log('Inside');
          e.response.then(err => {
            console.log(err, 'Checl 123');
          });
          dispatch(loaderHides());
          dispatch(uploadProfilePicFailure());
          console.log(e, 'Profile Pic Error');
        });
    }
  };
};

export const saveExtraProfileDetails = (userData, type) => {
  return (dispatch, getState) => {
    const { userInfoAPI } = getState();
    const userId = userInfoAPI.userId;
    dispatch(loaderShows());
    let method = 'POST';
    if (userData.id) {
      method = 'PUT';
    }
    return request(type, method, userData)
      .then(profileDetails => {
        console.log();
        let updateObj = userData;
        if (
          type === 'education' ||
          type === 'workExperience' ||
          type === 'volunteerWork' ||
          type === 'achievement' ||
          type === 'membership' ||
          type === 'research'
        ) {
          updateObj = {
            type,
            payload: userData
          };
        }
        return dispatch(getExtraProfileDetails(userId, true)).then(
          profileDetailsObj => {
            dispatch(getExtraProfileDetailsSuccess(profileDetailsObj));
            NavigationActions.goBack();
            return profileDetailsObj;
          }
        );
      })
      .catch(e => {
        let errorMsg = '';
        dispatch(loaderHides());
        e.response.then(error => {
          if (error.errors) {
            error.errors.map(item => {
              errorMsg =
                errorMsg +
                (item.field ? item.field : '') +
                ' ' +
                (item.message ? item.message : '') +
                '\n';
            });
          }
          dispatch(
            dialogShows(
              'Profile update error',
              errorMsg,
              'inform',
              true,
              null,
              null,
              'Please try again',
              'Refresh'
            )
          );
        });
      });
  };
  s;
};

export const deleteExtraProfileDetails = (id, type) => {
  return dispatch => {
    dispatch(loaderShows());
    return request(type, 'DELETE', { id })
      .then(profileDetails => {
        dispatch(loaderHides());
        let updateObj = {
          type,
          id
        };
        dispatch(deleteExtraProfileDetailsSuccess(updateObj));
        NavigationActions.goBack();
      })
      .catch(e => {
        console.log(e);
        dispatch(loaderHides());
        alert('Something went wrong');
        e.response.then(error => {
          console.log(error);
        });
      });
  };
};

export const setUserId = id => {
  return dispatch => {
    dispatch(setUserIdRequest(id));
  };
};

export const setUserToken = data => {
  return dispatch => {
    console.log('TOKEN-DATA', data);
    return request('/token', 'POST', data).then((res) => {
      console.log(res, "TOKEN-RESULT");
    }).catch(e => {
      console.log(e, "TOKEN-ERROR");
      dispatch(loaderHides());
      alert('Something went wrong');
      e.response.then(error => {
        console.log(error);
      });
    });
  };
};

const setUserIdRequest = userId => ({
  type: ProfileConstants.SET_USER_ID_REQUEST,
  userId
});

const uploadProfilePicRequest = () => ({
  type: ProfileConstants.UPLOAD_PROFILE_PIC_REQUEST
});

const uploadProfilePicSuccess = () => ({
  type: ProfileConstants.UPLOAD_PROFILE_PIC_SUCCESS
  // updatedProfile
});

const uploadProfilePicFailure = () => ({
  type: ProfileConstants.UPLOAD_PROFILE_PIC_FAILURE
});

const getUserInfoRequest = userId => ({
  type: ProfileConstants.PROFILE_REQUEST
});
export const getUserInfoSuccess = userInfo => ({
  type: ProfileConstants.PROFILE_SUCCESS,
  userInfo
});
const getUserInfoFailure = (errorType, errorList, errorCode) => ({
  type: ProfileConstants.PROFILE_FAILURE,
  errorType,
  errorList,
  errorCode
});

const getExtraProfileDetailsRequest = userId => ({
  type: ProfileConstants.EXTRA_PROFILE_DETAILS_REQUEST
});

const getExtraProfileDetailsSuccess = userInfo => ({
  type: ProfileConstants.EXTRA_PROFILE_DETAILS_SUCCESS,
  userInfo
});

const getExtraProfileDetailsFailure = (errorType, errorList, errorCode) => ({
  type: ProfileConstants.EXTRA_PROFILE_DETAILS_FAILURE,
  errorType,
  errorList,
  errorCode
});

const deleteExtraProfileDetailsSuccess = userInfo => ({
  type: ProfileConstants.DELETE_EXTRA_PROFILE_DETAILS,
  userInfo
});

const savedBasicProfileInfo = userInfo => ({
  type: ProfileConstants.SAVE_BASIC_PROFILE_INFO,
  userInfo
});
