import { request } from '../request'
import { LoginConstants } from '../constants'
import * as NavigationActions from '../../NavigationService'
import { AsyncStorage, Platform, PushNotificationIOS, Linking, Alert } from 'react-native'
import { loaderShows, loaderHides } from './Loader';
import { dialogShows } from './ErrorModal';
import { getProfileDetails, setUserId, setUserToken } from './Profile';
import { getNotifToken, getPushNotifHandler, handleUniversityMailURL } from '../../App';
import DeviceInfo from 'react-native-device-info';
import PushNotification from 'react-native-push-notification';
// import { Cookies } from 'react-cookie';
// const cookies = new Cookies();

export const login = (userName, password) => {
  return dispatch => {
    dispatch(loaderShows())
    dispatch(loginRequest())
    var loginData = `username=${encodeURIComponent(userName)}&password=${encodeURIComponent(password)}`
    return request('login', 'POST', loginData, 'application/x-www-form-urlencoded')
      .then((userData) => {
        dispatch(loaderHides())
        let token = userData.headers.get("My-Authorization")
        // cookies.set('userId',userData.body.id,'/')
        // cookies.set('userType',userData.body.userType,'/')
        // cookies.set('firstName',userData.body.firstName,'/')
        // cookies.set('lastName',userData.body.lastName,'/')
        // cookies.set('slug',userData.body.slug,'/')
        dispatch(loginSuccess(userData.body))
        dispatch(getProfileDetails(userData.body.id))
        if (getNotifToken()) {
          dispatch(setUserToken({
            userId: userData.body.id,
            token: getNotifToken(),
            deviceId: getNotifToken(),
            deviceType: Platform.OS === 'ios' ? 'IOS' : 'ANDROID'
          }))
        }
        if (userData.body.id) {
          dispatch(setUserId(userData.body.id))
        }
        try {
          Linking.addEventListener('url', handleUniversityMailURL);
          AsyncStorage.setItem('token', token)
          AsyncStorage.setItem('userId', userData.body.id.toString())
          NavigationActions.navigate('HomeFeed')
        }
        catch (e) {
          console.log(e, 'Async Storage Error')
        }
      })
      .catch((e) => {
        dispatch(loaderHides())
        e.response.then((error) => {
          dispatch(dialogShows('Login error', error.message, 'inform', true, null, null, 'Please try again', 'Refresh'))
          dispatch(loginFailure(error.error, error.message, e.status))
        })
      })
  }
}

export const logout = () => {
  return dispatch => {
    dispatch(loaderShows())
    if (getNotifToken()) {
      return request('token', 'DELETE', { token: getNotifToken() }).then(() => {
        AsyncStorage.removeItem('token')
        AsyncStorage.removeItem('userId')
        Linking.removeEventListener('url', handleUniversityMailURL);
        // Remove notifications on logout
        if (Platform.OS === 'ios') {
          PushNotificationIOS.removeAllDeliveredNotifications();
        } else if (PushNotification) {
          PushNotification.cancelAllLocalNotifications();
        }
        NavigationActions.reset('LoginScreen')
        dispatch(loaderHides())
      }).catch((e) => {
        console.log(e, 'Check Error')
        dispatch(loaderHides())
        if (e.response) {
          e.response.then((error) => {
            dispatch(dialogShows('Logout error', error.message, 'inform', true, null, null, 'Please try again', 'Refresh'))
          })
        }
      })
    } else {
      AsyncStorage.removeItem('token')
      AsyncStorage.removeItem('userId')
      NavigationActions.reset('LoginScreen')
      dispatch(loaderHides())
    }
  }
}


const loginRequest = () => ({
  type: LoginConstants.LOGIN_REQUEST,
})
const loginSuccess = (userInfo) => ({
  type: LoginConstants.LOGIN_SUCCESS,
  userInfo
})
const loginFailure = (errorType, errorMessage, errorCode) => ({
  type: LoginConstants.LOGIN_FAILURE,
  errorType,
  errorMessage,
  errorCode
})
