// import { request } from '../request'
import { MessagingConstants } from '../constants'
import { request } from '../request'

const { 
  GETMESSAGING_REQUEST,
  GETMESSAGING_FAILURE,
  GETMESSAGING_SUCCESS,

  GETCONVERSATION_REQUEST,
  GETCONVERSATION_SUCCESS,
  GETCONVERSATION_FAILURE,

  GETALLCONVERSATION_REQUEST,
  GETALLCONVERSATION_SUCCESS,
  GETALLCONVERSATION_FAILURE,
  GET_MESSAGES_SUCCESS,
  SENT_MESSAGE,
  CREATE_CONVERSATION_SUCCESS,
  DELETE_MESSAGE_SUCCESS,
  MUTE_CONVERSATION_SUCCESS
} = MessagingConstants

import { loaderShows, loaderHides } from './Loader';
import * as NavigationActions from '../../NavigationService'
import { MUTE_CONVERSATION_SUCCESS } from '../constants/Messaging';

export const getMessage = () => {
  const url = `msg/list/${selectedConversation.convid}/${messagesPageSize}/${currentMessagesPage}`
  return dispatch => {
    var res = null
    return request(url, 'GET')
      .then(function(data) {
        console.log(data)
        dispatch(getUserConnectionSuccess(data))
        return data
      })
    }
}

const getMessageSuccess = (message) => ({
  type: GETMESSAGING_SUCCESS,
  message
})

export const getAllConversations = (hideLoader) => {
  return dispatch => {
    var res = null
    if (!hideLoader) {
      dispatch(loaderShows())
    }
    return request('v1/chat/conversation', 'GET')
      .then(function(data) {
        if (!hideLoader) {
          dispatch(loaderHides())
        }
        dispatch(getAllConversationsSuccess(data.body))
        return data
      }).catch((e)=> {
        if (!hideLoader) {
          dispatch(loaderHides())
        }
        e.response.then((error)=>{
          alert('Something unexpected occured')
        })
        console.log(e, 'Converstaion eror!!!')
      })
    }
}

export const markMessageAsSeen = (chatConversationId) => {
  return dispatch => {
    return request(`/v1/chat/conversation/${chatConversationId}/read`, 'PUT')
           .catch((e) => {
             console.log(e);
             if (e.response) {
              e.response.then((error)=>{
                console.log(error);
              })
             }
           })
  }
}

const getAllConversationsSuccess = (conversations) => ({
  type: GETALLCONVERSATION_SUCCESS,
  conversations
})

export const getMessagesForConverstation = (id, pageNo) => {
  return dispatch => {
    dispatch(loaderShows())
    return request(`v1/chat/conversation/${id}/message?pageNo=${pageNo}&size=20`, 'GET')
      .then(function(data) {
        console.log('getConversation', data)
        dispatch(getMessagesSuccess(data.body))
        dispatch(loaderHides())
      }).catch((e)=> {
        dispatch(loaderHides())
        console.log(e, 'Converstaion eror!!!')
        e.response.then((error)=>{
          alert('Something unexpected occured')
        })
      })
    }
}

export const sendMessage = (conversationId, data) => {
  console.log(data);
  return dispatch => {
    return request(`/v1/chat/conversation/${conversationId}/message`, 'POST', data)
      .then(function(data) {
        console.log('getConversation', data)
        dispatch(sendMessageSuccess(conversationId, data.body))
      }).catch((e)=> {
        console.log(e, 'Converstaion eror!!!')
        alert('Something unexpected occured')
        e.response.then((error)=>{
          alert('Something unexpected occured')
        })
      })
    }
}

export const createConversation = (data) => {
  return dispatch => {
    dispatch(loaderShows())
    return request(`/v1/chat/conversation`, 'POST', data)
      .then(function(data) {
        console.log('getConversation', data)
        dispatch(loaderHides())
        dispatch(createConversationSuccess(data.body))
        dispatch(getAllConversations())
        if (data.body.conversationType === 'GROUP') {
          NavigationActions.goBack();
        }
        NavigationActions.replace('MessagingSingle', {messages: [], conversationId: data.body.id, name: (data.body.conversationType === 'GROUP') ? data.body.name : data.body.memberInfos[0].users.name})
      }).catch((e)=> {
        dispatch(loaderHides())
        console.log(e, 'Converstaion eror!!!')
        alert('Something unexpected occured')
        e.response.then((error)=>{
         console.log(error);
        })
      })
    }
}

export const leaveConversation = (conversationId, userId) => {
  return dispatch => {
    dispatch(loaderShows())
    return request(`v1/chat/conversation/${conversationId}/user/${userId}/leaveConversation`, 'DELETE')
      .then(function(data) {
        dispatch(loaderHides())
        NavigationActions.navigate('Messaging')
      }).catch((e)=> {
        dispatch(loaderHides())
        console.log(e, 'Converstaion eror!!!')
        alert('Something unexpected occured')
        e.response.then((error)=>{
         console.log(error);
        })
      })
    }
}

export const deleteConversation = (conversationId, userId, dontNavigate) => {
  return dispatch => {
    dispatch(loaderShows())
    return request(`v1/chat/conversation/${conversationId}/user/${userId}/deleteConversation`, 'DELETE')
      .then(function(data) {
        return request(`v1/chat/conversation/${conversationId}/user/${userId}/leaveConversation`, 'DELETE').then(() => {
          if (!dontNavigate){
            dispatch(loaderHides())
            NavigationActions.navigate('Messaging')
          } else {
            dispatch(getAllConversations())
          }
        })
      }).catch((e)=> {
        dispatch(loaderHides())
        console.log(e, 'Converstaion eror!!!')
        alert('Something unexpected occured')
        e.response.then((error)=>{
         console.log(error);
        })
      })
    }
}

export const deleteMessage = (conversationId, messageId) => {
  return dispatch => {
    dispatch(loaderShows())
    return request(`v1/chat/conversation/${conversationId}/message/${messageId}/delete`, 'DELETE')
      .then(function(data) {
        dispatch(loaderHides())
        dispatch(deleteMessageSuccess(conversationId, messageId))
      }).catch((e)=> {
        dispatch(loaderHides())
        console.log(e, 'Converstaion eror!!!')
        alert('Something unexpected occured')
        e.response.then((error)=>{
         console.log(error);
        })
      })
    }
}

export const clearConversation = (conversationId, userId) => {
  return dispatch => {
    dispatch(loaderShows())
    return request(`v1/chat/conversation/${conversationId}/user/${userId}/clearMessages`, 'DELETE')
      .then(function(data) {
        dispatch(getMessagesForConverstation(conversationId, 0));
        dispatch(getAllConversations())
        NavigationActions.goBack();
      }).catch((e)=> {
        dispatch(loaderHides())
        console.log(e, 'Converstaion eror!!!')
        alert('Something unexpected occured')
        e.response.then((error)=>{
         console.log(error);
        })
      })
    }
}

export const muteConversation = (conversationId, userId, type) => {
  return dispatch => {
    dispatch(loaderShows())
    return request(`v1/chat/conversation/${conversationId}/user/${userId}/${type}`, 'PUT')
      .then(function(data) {
        dispatch(loaderHides())
        dispatch(muteConversationSuccess(conversationId, userId, type))
      }).catch((e)=> {
        dispatch(loaderHides())
        console.log(e, 'Converstaion eror!!!')
        alert('Something unexpected occured')
        e.response.then((error)=>{
         console.log(error);
        })
      })
    }
}

export const renameConversation = (conversationId, name) => {
  return dispatch => {
    dispatch(loaderShows())
    return request(`/v1/chat/conversation/${conversationId}/rename?newName=${name}`, 'PUT')
      .then(function(data) {
        dispatch(loaderHides())
        NavigationActions.navigate('Messaging')
      }).catch((e)=> {
        dispatch(loaderHides())
        console.log(e, 'Converstaion eror!!!')
        alert('Something unexpected occured')
        e.response.then((error)=>{
         console.log(error);
        })
      })
    }
}

export const addPersonToConversation = (conversationId, userId) => {
  return dispatch => {
    dispatch(loaderShows())
    return request(`/v1/chat/conversation/${conversationId}/user/${userId}`, 'PUT')
      .then(function(data) {
        dispatch(getMessagesForConverstation(conversationId, 0))
        dispatch(getAllConversations())
      }).catch((e)=> {
        dispatch(loaderHides())
        console.log(e, 'Converstaion eror!!!')
        alert('Something unexpected occured')
        e.response.then((error)=>{
         console.log(error);
        })
      })
    }
}

export const removePersonFromConversation = (conversationId, userId) => {
  return dispatch => {
    dispatch(loaderShows())
    return request(`/v1/chat/conversation/${conversationId}/user/${userId}`, 'DELETE')
      .then(function(data) {
        dispatch(getMessagesForConverstation(conversationId, 0))
        dispatch(getAllConversations())
      }).catch((e)=> {
        dispatch(loaderHides())
        console.log(e, 'Converstaion eror!!!')
        alert('Something unexpected occured')
        e.response.then((error)=>{
         console.log(error);
        })
      })
    }
}

const getMessagesSuccess = (messages) => ({
  type: GET_MESSAGES_SUCCESS,
  messages
})

const sendMessageSuccess = (conversationId, message) => ({
  type: SENT_MESSAGE,
  message,
  conversationId
})

const deleteMessageSuccess = (conversationId, messageId) => ({
  type: DELETE_MESSAGE_SUCCESS,
  conversationId,
  messageId
})

const muteConversationSuccess = (conversationId, userId, type) => ({
  type: MUTE_CONVERSATION_SUCCESS,
  conversationId,
  userId,
  actionType: type
})

const getConversationSuccess = (currentConversation) => ({
  type: GETCONVERSATION_SUCCESS,
  currentConversation
})

const createConversationSuccess = (conversation) => ({
  type: CREATE_CONVERSATION_SUCCESS,
  conversation
})
