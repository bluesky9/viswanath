import { DrawerConstants } from '../constants'

export const drawerOpen = () => { return { type: DrawerConstants.DRAWER_OPEN } }
export const drawerClosed = () => { return { type: DrawerConstants.DRAWER_CLOSED } }
