import { request } from '../request';
import { loaderHides, loaderShows } from './Loader';
import { getPeopleInfo } from './People'

const getAllNotificationsSuccess = data => ({
  type: 'GET_NOTIFICATIONS_SUCCESS',
  notifications: data
});
const getAllNotificationsFailure = (errorType, errorMessage, errorCode) => ({
  type: 'GET_NOTIFICATIONS_FAILURE',
  errorType,
  errorMessage,
  errorCode
});

export const getAllNotifications = (size, page) => {
  return dispatch => {
    dispatch({ type: 'GET_NOTIFICATIONS'})
    return request(`notification/everything/${size}/${page}`, 'GET')
      .then(data => {
        dispatch(getAllNotificationsSuccess(data.body));
      })
      .catch(e => {
        e.response.then(error => {
          dispatch(
            getAllNotificationsFailure(error.error, error.message, e.status)
          );
        });
      });
  };
};

const getConnectionNotificationsSuccess = data => ({
  type: 'GET_CONNECTION_NOTIFICATIONS_SUCCESS',
  notifications: data
});
const getConnectionNotificationsFailure = (
  errorType,
  errorMessage,
  errorCode
) => ({
  type: 'GET_CONNECTION_NOTIFICATIONS_FAILURE',
  errorType,
  errorMessage,
  errorCode
});

export const getConnectionNotifications = () => {
  return dispatch => {
    return request(`notification/connection/`, 'GET')
      .then(data => {
        dispatch(getConnectionNotificationsSuccess(data.body));
      })
      .catch(e => {
        e.response.then(error => {
          dispatch(
            getConnectionNotificationsFailure(
              error.error,
              error.message,
              e.status
            )
          );
        });
      });
  };
};

const getNotificationsCountSuccess = data => ({
  type: 'GET_NOTIFICATIONS_COUNT_SUCCESS',
  count: data
});
const getNotificationsCountFailure = (errorType, errorMessage, errorCode) => ({
  type: 'GET_NOTIFICATIONS_COUNT_FAILURE',
  errorType,
  errorMessage,
  errorCode
});

export const getNotificationsUnreadCount = () => {
  return dispatch => {
    return request(`notification/unread/count`, 'GET')
      .then(data => {
        dispatch(getNotificationsCountSuccess(data.body));
      })
      .catch(e => {
        e.response.then(error => {
          dispatch(
            getNotificationsCountFailure(error.error, error.message, e.status)
          );
        });
      });
  };
};

export const markAllAsSeen = () => {
  return dispatch => {
    return request(`notification/everything`, 'PUT')
      .then(data => {
        console.log(data);
      })
      .catch(e => {
        e.response.then(error => {
          console.log(error);
        });
      });
  };
};

export const acceptConnection = (id, page) => {
  return dispatch => {
    dispatch(loaderShows())
    return request('userConnection', 'PUT', {connectId: id})
      .then(data => {
        dispatch(loaderHides())
        if (page) {
          dispatch(getPeopleInfo(id, 'Profile'))
        }
        dispatch(getConnectionNotifications())
      })
      .catch(e => {
        console.log(e);
        dispatch(loaderHides())
        e.response.then(error => {
          console.log(error);
        });
      });
  };
};

export const dismissConnection = (id) => {
  return dispatch => {
    dispatch(loaderShows())
    return request('userConnection', 'DELETE', {connectId: id})
      .then(data => {
        dispatch(loaderHides())
        dispatch(getConnectionNotifications())
      })
      .catch(e => {
        dispatch(loaderHides())
        e.response.then(error => {
          console.log(error);
        });
      });
  };
};
