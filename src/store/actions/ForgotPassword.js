import {request} from '../request'
import {ForgotPasswordConstants} from '../constants'
import { dialogShows } from './ErrorModal';
import * as NavigationActions from '../../NavigationService'

import { loaderShows, loaderHides } from './Loader';


export const forgotPassword = (email) => {
  console.log(email, 'Forgot')
  return dispatch => {
    dispatch(loaderShows())
    dispatch(forgotPasswordRequest())
    const data = {email:email}
    return request('/register/forgot/password','POST',data)
      .then((userData)=> {
        dispatch(loaderHides())
        dispatch(forgotPasswordSuccess(userData))
        NavigationActions.navigate('LoginPasswordReset');
      })
      .catch((e)=> {
        console.log(e);
        dispatch(loaderHides())
        e.response.then((error)=>{
          dispatch(forgotPasswordFailure(error.error,error.message,e.status))
          dispatch(dialogShows('User does not exist','','inform',true, null, null, 'Please try again', 'Try again' ))
        })
      })
  }
}

export const forgotPasswordReset = (newpassword, tokenData) => {
  console.log(newpassword,tokenData, 'Forgot')
  return dispatch => {
    dispatch(loaderShows())
    const data = {
      authCode: tokenData.token,
      password: newpassword
    }
    return request('/users/reset-password','POST',data)
      .then((userData)=> {
        dispatch(loaderHides())
        NavigationActions.navigate('LoginPasswordChanged');
      })
      .catch((e)=> {
        console.log(e);
        dispatch(loaderHides())
        e.response.then((error)=>{
          dispatch(forgotPasswordFailure(error.error,error.message,e.status))
          dispatch(dialogShows('Invalid token','','inform',true, null, null, 'Please try again', 'Try again' ))
        })
      })
  }
}

const forgotPasswordRequest = () => ({
  type: ForgotPasswordConstants.FORGOT_PASSWORD_REQUEST,
})
const forgotPasswordSuccess = (userInfo) => ({
  type: ForgotPasswordConstants.FORGOT_PASSWORD_SUCCESS,
  userInfo
})
const forgotPasswordFailure = (errorType,errorMessage,errorCode) => ({
  type: ForgotPasswordConstants.FORGOT_PASSWORD_FAILURE,
  errorType,
  errorMessage,
  errorCode
})
