import 'whatwg-fetch';
import update from 'immutability-helper';
import { AsyncStorage, Platform, PushNotificationIOS } from 'react-native';
import { Cookies } from 'react-cookie';
import { API_URL, NEW_API_URL, PRODUCTION_API_URL } from './config';
import { map } from 'lodash';
import * as NavigationActions from '../NavigationService';
import PushNotification from 'react-native-push-notification';
// const cookies = new Cookies();
/**
 * Parses the JSON returned by a network request
 *
 * @param  {object} response A response from a network request
 *
 * @return {object}          The parsed JSON from the request
 */
function parseJSON(response) {
  return response.text().then(function (text) {
    if (text) {
      // TODO: check if My-Authorization exists
      // AsyncStorage.setItem('token',response.headers.get("My-Authorization"),'/')
      return { headers: response.headers, body: JSON.parse(text) };
    }
    return {};
  });
}

let checkingToken = false;

/**
 * Checks if a network request came back fine, and throws an error if not
 *
 * @param  {object} response   A response from a network request
 *
 * @return {object|undefined} Returns either the response, or throws an error
 */
function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  if (((response.status === 401) || (response.status === 403)) && !checkingToken) {
    checkingToken = true;
    AsyncStorage.getItem('token').then((val) => {
      if (val) {
        AsyncStorage.removeItem('token');
        AsyncStorage.removeItem('userId');
        if (Platform.OS === 'ios') {
          PushNotificationIOS.removeAllDeliveredNotifications();
        } else if (PushNotification) {
          PushNotification.cancelAllLocalNotifications();
        }
        NavigationActions.reset('LoginScreen');
      }
      checkingToken = false;
    }).catch(() => checkingToken = false);
  }
  let error = new Error();
  error.status = response.status;
  error.response = response.json();
  throw error;
}

// export const request = (endpoint,method,optionalData,contentType) => {
//   return AsyncStorage.getItem('token').then(token => {
//     console.log(token, 'yo')
//     headers.Authorization = token;
//     // let newOptions = Object.assign(
//     //   { method: method, body: JSON.stringify(optionalData) },
//     //   { headers: headers }
//     // );
//     let newOptions = Object.assign({
//       method: method,
//       body:JSON.stringify(optionalData)
//     }, {headers:headers});
//   })
//   if(contentType !== undefined){
//     newOptions = update(newOptions,{
//       headers:{ 'Content-Type':{$set:contentType} },
//       body:{$set:optionalData}
//     })
//   }

//   return fetch(`${API_URL}/${endpoint}`,newOptions)
//     .then(checkStatus)
//     .then(parseJSON)
// }

export const requestWithoutToken = (
  endpoint,
  method,
  optionalData,
  contentType,
  customHeaders = {}
) => {
  const headers = {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'POST,GET,PUT,DELETE',
    Accept: '*/*',
    'Access-Control-Allow-Headers': 'Content-Type',
    Authorization: ''
  };
  map(customHeaders, (header, index) => {
    headers[index] = header;
  });
  let newOptions = Object.assign(
    { method: method, body: JSON.stringify(optionalData) },
    { headers: headers }
  );
  if (contentType !== undefined) {
    newOptions = update(newOptions, {
      headers: { 'Content-Type': { $set: contentType } },
      body: { $set: optionalData }
    });
  }
  return fetch(`${PRODUCTION_API_URL}/${endpoint}`, newOptions)
    .then(checkStatus)
    .then(parseJSON);
};

export const request = (
  endpoint,
  method,
  optionalData,
  contentType,
  customHeaders = {}
) => {
  const headers = {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'POST,GET,PUT,DELETE',
    Accept: '*/*',
    'Access-Control-Allow-Headers': 'Content-Type',
    Authorization: ''
  };
  return AsyncStorage.getItem('token').then(token => {
    headers.Authorization = token;
    if (Platform.OS === 'android' && endpoint === '/register/forgot/password') {
      // Route does not support charset yet
      headers['Content-Type'] = 'application/json;';
    }
    map(customHeaders, (header, index) => {
      headers[index] = header;
    });
    let newOptions = Object.assign(
      { method: method, body: JSON.stringify(optionalData) },
      { headers: headers }
    );
    if (contentType !== undefined) {
      newOptions = update(newOptions, {
        headers: { 'Content-Type': { $set: contentType } },
        body: { $set: optionalData }
      });
    }

    return fetch(`${PRODUCTION_API_URL}/${endpoint}`, newOptions)
      .then(checkStatus)
      .then(parseJSON);
  });
};
