import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import { createLogger } from 'redux-logger'
import api from './middleware/api'
import rootReducer from './reducers'

const configureStore = preloadedState => {
  const store = createStore(
    rootReducer,
    preloadedState,
    compose(
      __DEV__ ? applyMiddleware(thunk, api, createLogger()) : applyMiddleware(thunk, api)
    )
  )

  return store
}

export default configureStore