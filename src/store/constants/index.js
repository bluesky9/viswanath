import * as Login from './Login'
import * as Signup from './Signup'
import * as ForgotPassword from './ForgotPassword'
import * as ResetPassword from './ResetPassword'
import * as People from './People'
import * as Countries from './Countries'
import * as Universities from './Universities'
import * as Feed from './Feed'
import * as Loader from './Loader'
import * as ErrorModal from './ErrorModal'
import * as Profile from './Profile'
import * as Messaging from './Messaging'
import * as Drawer from './Drawer'
import * as Invite from './Invite'

export const LoginConstants = Login;
export const SignupConstants = Signup;
export const ForgotPasswordConstants = ForgotPassword;
export const ResetPasswordConstants = ResetPassword;
export const PeopleConstants = People;
export const CountriesConstants = Countries;
export const UniversitiesConstants = Universities;
export const FeedConstants = Feed;
export const LoaderConstants = Loader;
export const ErrorModalConstants = ErrorModal;
export const ProfileConstants = Profile;
export const MessagingConstants = Messaging;
export const DrawerConstants = Drawer;
export const InviteConstants = Invite;
