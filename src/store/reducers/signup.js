import update from 'immutability-helper';
import { SignupConstants } from '../constants'
const {SIGNUP_REQUEST,SIGNUP_FAILURE,SIGNUP_SUCCESS} = SignupConstants;
const initialState = {
    isWaitingResponse: false,
    isSingupComplete:false,
    errorLoggedIn:false,
    errorType:undefined,
    errorList:[],
    errorCode:undefined
}

export function signupApi(state = initialState, action) {
  switch (action.type) {
    case SIGNUP_REQUEST:
      return update(state,{
        isWaitingResponse: {$set: true}
      })
    case SIGNUP_SUCCESS:
      return update(state,{
        isWaitingResponse: {$set: false},
        isSingupComplete: {$set: true}
      })
    case SIGNUP_FAILURE:
      return update(state,{
        errorType: {$set: action.errorType},
        errorList: {$set: action.errorList},
        errorCode: {$set: action.errorCode},
        errorLoggedIn: {$set: true},
        isWaitingResponse: {$set: false},
      })
    default:
      return state
  }
}
