import update from 'immutability-helper';
import { FeedConstants } from '../constants';
import { concat } from 'lodash';

const { FEED_REQUEST, FEED_FAILURE, FEED_SUCCESS } = FeedConstants;
const paginatedPostsInitialState = {
  isWaitingResponse: false,
  posts: [],
  errorType: undefined,
  errorMessage: undefined,
  errorCode: undefined
};
export function paginatedPosts(state = paginatedPostsInitialState, action) {
  switch (action.type) {
    case FEED_REQUEST:
      return update(state, {
        isWaitingResponse: { $set: true }
      });
    case FEED_SUCCESS:
      return update(state, {
        posts: { $set: concat([], action.posts) },
        isWaitingResponse: { $set: false }
      });
    case FEED_FAILURE:
      return update(state, {
        errorType: { $set: action.errorType },
        errorMessage: { $set: action.errorMessage },
        errorCode: { $set: action.errorCode },
        isWaitingResponse: { $set: false }
      });
    default:
      return state;
  }
}

const connectionInitialState = {
  isWaitingResponse: false,
  connections: [],
  errorType: undefined,
  errorMessage: undefined,
  errorCode: undefined
};

export function getConnections(state = connectionInitialState, action) {
  switch (action.type) {
    case 'USER_CONNECTIONS_REQUEST':
      return update(state, { isWaitingResponse: { $set: true } });
    case 'USER_CONNECTIONS_SUCCESS':
      return update(state, {
        connections: { $set: action.connections },
        isWaitingResponse: { $set: false }
      });
    case 'USER_CONNECTIONS_FAILURE':
      return update(state, {
        errorType: { $set: action.errorType },
        errorMessage: { $set: action.errorMessage },
        errorCode: { $set: action.errorCode },
        isWaitingResponse: { $set: false }
      });
    default:
      return state;
  }
}

const tagStateInitial = {
  userArray: []
};

export function postTaggedUsers(state = tagStateInitial, action) {
  switch (action.type) {
    case 'SET_TAGGED_USERS':
      return update(state, {
        userArray: { $set: action.userArray }
      });
    default:
      return state;
  }
}

const postAudienceInitialState = {
  userArray: []
};

export function postAudience(state = postAudienceInitialState, action) {
  switch (action.type) {
    case 'SET_POST_AUDIENCE':
      return update(state, {
        userArray: { $set: action.userArray }
      });
    default:
      return state;
  }
}
