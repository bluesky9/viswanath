import update from 'immutability-helper';
import { UniversitiesConstants } from '../constants';
import { findIndex, cloneDeep, concat } from 'lodash';
import {
  UNIVERSITIES_BY_COUNTRIES,
  UNIVERSITIES_BY_COUNTRIES_FAILURE
} from '../constants/Universities';
const {
  ALL_UNIVERSITIES_REQUEST,
  ALL_UNIVERSITIES_FAILURE,
  ALL_UNIVERSITIES_SUCCESS
} = UniversitiesConstants;
const {
  UNIVERSITIES_REQUEST,
  UNIVERSITIES_FAILURE,
  UNIVERSITIES_SUCCESS
} = UniversitiesConstants;
const {
  UNIVERSITIES_FOLLOWED_BY_USER_REQUEST,
  UNIVERSITIES_FOLLOWED_BY_USER_FAILURE,
  UNIVERSITIES_FOLLOWED_BY_USER_SUCCESS,
  FOLLOW_UNIVERSITY_SUCCESS,
  UNFOLLOW_UNIVERSITY_SUCCESS
} = UniversitiesConstants;
const {
  UNIVERSITY_BY_SLUG_REQUEST,
  UNIVERSITY_BY_SLUG_FAILURE,
  UNIVERSITY_BY_SLUG_SUCCESS
} = UniversitiesConstants;
const {
  DEPARTMENT_REQUEST,
  DEPARTMENT_FAILURE,
  DEPARTMENT_SUCCESS
} = UniversitiesConstants;

const alluniversitiesInitialState = {
  isWaitingResponse: false,
  universities: [],
  errorType: undefined,
  errorMessage: undefined,
  errorCode: undefined
};
const universitesByCountriesInitialState = {
  isWaitingResponse: false,
  universities: [],
  errorType: undefined,
  errorMessage: undefined,
  errorCode: undefined
};

const paginatedUniversitiesInitialState = {
  isWaitingResponse: false,
  universities: [],
  errorType: undefined,
  errorMessage: undefined,
  errorCode: undefined
};
const universitiesFollowedInitialState = {
  isWaitingResponse: false,
  universities: [],
  errorType: undefined,
  errorMessage: undefined,
  errorCode: undefined
};
const universityBySlugInitialState = {
  isWaitingResponse: false,
  university: {},
  errorType: undefined,
  errorMessage: undefined,
  errorCode: undefined
};
const paginatedDepartmentsInitialState = {
  isWaitingResponse: false,
  departments: [],
  errorType: undefined,
  errorMessage: undefined,
  errorCode: undefined
};
export function alluniversities(state = alluniversitiesInitialState, action) {
  switch (action.type) {
    case ALL_UNIVERSITIES_REQUEST:
      return update(state, {
        isWaitingResponse: { $set: true }
      });
    case ALL_UNIVERSITIES_SUCCESS:
      return update(state, {
        universities: { $set: action.universities },
        isWaitingResponse: { $set: false }
      });
    case ALL_UNIVERSITIES_FAILURE:
      return update(state, {
        errorType: { $set: action.errorType },
        errorMessage: { $set: action.errorMessage },
        errorCode: { $set: action.errorCode },
        isWaitingResponse: { $set: false }
      });
    default:
      return state;
  }
}

export function paginatedUniversities(
  state = paginatedUniversitiesInitialState,
  action
) {
  switch (action.type) {
    case UNIVERSITIES_REQUEST:
      return update(state, {
        isWaitingResponse: { $set: true }
      });
    case UNIVERSITIES_SUCCESS:
      return update(state, {
        universities: { $set: action.universities },
        isWaitingResponse: { $set: false }
      });
    case UNIVERSITIES_FAILURE:
      return update(state, {
        errorType: { $set: action.errorType },
        errorMessage: { $set: action.errorMessage },
        errorCode: { $set: action.errorCode },
        isWaitingResponse: { $set: false }
      });
    default:
      return state;
  }
}

const universitiesByNameInitialState = {
  isWaitingResponse: false,
  universities: [],
  errorType: undefined,
  errorMessage: undefined,
  errorCode: undefined
};

export function universitiesByName(
  state = universitiesByNameInitialState,
  action
) {
  switch (action.type) {
    case UniversitiesConstants.GET_UNIVERSITY_BY_NAME_REQUEST:
      return update(state, { isWaitingResponse: { $set: true } });
    case UniversitiesConstants.GET_UNIVERSITY_BY_NAME_SUCCESS:
      return update(state, {
        universities: { $set: action.universities },
        isWaitingResponse: { $set: false }
      });
    default:
      return state;
  }
}

export function universitiesFollowed(
  state = universitiesFollowedInitialState,
  action
) {
  switch (action.type) {
    case UNIVERSITIES_FOLLOWED_BY_USER_REQUEST:
      return update(state, {
        isWaitingResponse: { $set: true }
      });
    case UNIVERSITIES_FOLLOWED_BY_USER_SUCCESS:
      return update(state, {
        universities: { $set: action.universities },
        isWaitingResponse: { $set: false }
      });
    case UNIVERSITIES_FOLLOWED_BY_USER_FAILURE:
      return update(state, {
        errorType: { $set: action.errorType },
        errorMessage: { $set: action.errorMessage },
        errorCode: { $set: action.errorCode },
        isWaitingResponse: { $set: false }
      });
    case FOLLOW_UNIVERSITY_SUCCESS:
      return update(state, {
        universities: { $set: state.universities.concat(action.university) }
      });
    case UNFOLLOW_UNIVERSITY_SUCCESS:
      let index = findIndex(
        state.universities,
        university => university.id === action.universityFollowId
      );
      if (index > -1) {
        state.universities.splice(index, 1);
      }
      return update(state, {
        universities: { $set: state.universities }
      });
    default:
      return state;
  }
}

export function universityBySlug(state = universityBySlugInitialState, action) {
  switch (action.type) {
    case UNIVERSITY_BY_SLUG_REQUEST:
      return update(state, {
        isWaitingResponse: { $set: true }
      });
    case UNIVERSITY_BY_SLUG_SUCCESS:
      return update(state, {
        university: { $set: action.university },
        isWaitingResponse: { $set: false }
      });
    case UNIVERSITY_BY_SLUG_FAILURE:
      return update(state, {
        errorType: { $set: action.errorType },
        errorMessage: { $set: action.errorMessage },
        errorCode: { $set: action.errorCode },
        isWaitingResponse: { $set: false }
      });
    default:
      return state;
  }
}

export function universitesByCountries(
  state = universitesByCountriesInitialState,
  action
) {
  switch (action.type) {
    case UniversitiesConstants.UNIVERSITIES_BY_COUNTRIES_START:
      return update(state, {
        isWaitingResponse: { $set: true }
      });
    case UNIVERSITIES_BY_COUNTRIES:
      return update(state, {
        universities: { $set: action.universities },
        isWaitingResponse: { $set: false }
      });
    case UNIVERSITIES_BY_COUNTRIES_FAILURE:
      return update(state, {
        errorType: {
          $set: action.errorType
        },
        errorMessage: {
          $set: action.errorMessage
        },
        errorCode: {
          $set: action.errorCode
        },
        isWaitingResponse: {
          $set: false
        }
      });
    default:
      return state;
  }
}
const universityCountInitialState = {
  count: 0
};
export function universityCount(state = universityCountInitialState, action) {
  switch (action.type) {
    case UniversitiesConstants.GET_UNIVERSITY_COUNT:
      return update(state, {
        count: { $set: action.count }
      });
    default:
      return state;
  }
}

const universityCountryFiltersInitialState = {
  filters: []
};

export function universityCountryFilters(
  state = universityCountryFiltersInitialState,
  action
) {
  switch (action.type) {
    case UniversitiesConstants.COUNTRY_FILTERS_REQUEST:
      return update(state, { filters: { $set: action.filters } });
    default:
      return state;
  }
}

const universityPostCountryFiltersInitialState = {
  filters: []
};

export function universityPostCountryFilters(
  state = universityPostCountryFiltersInitialState,
  action
) {
  switch (action.type) {
    case UniversitiesConstants.SET_POST_COUNTRY_FILTERS:
      return update(state, { filters: { $set: action.filters } });
    default:
      return state;
  }
}

export function paginatedDepartments(
  state = universityBySlugInitialState,
  action
) {
  switch (action.type) {
    case DEPARTMENT_REQUEST:
      return update(state, {
        isWaitingResponse: { $set: true }
      });
    case DEPARTMENT_SUCCESS:
      return update(state, {
        departments: { $set: action.departments },
        isWaitingResponse: { $set: false }
      });
    case DEPARTMENT_FAILURE:
      return update(state, {
        errorType: { $set: action.errorType },
        errorMessage: { $set: action.errorMessage },
        errorCode: { $set: action.errorCode },
        isWaitingResponse: { $set: false }
      });
    default:
      return state;
  }
}

const universityDetailsInitialState = {
  isWaitingResponse: false,
  details: undefined,
  followers: undefined,
  departments: undefined,
  errorType: undefined,
  errorMessage: undefined,
  postAudience: [],
  postMajor: [],
  postTags: [],
  universityPeople: [],
  departmentPeople: [],
  departmentPeopleCount: 0,
  universityPeopleCount: 0,
  departmentFeeds: [],
  adminCandidate: [],
  errorCode: undefined,
  followingUniversity: false,
  usertype: undefined
};
const universityFeedsInitialState = {
  isWaitingResponse: false,
  universityFeeds: undefined,
  errorType: undefined,
  errorMessage: undefined,
  errorCode: undefined
};
export function universityFeeds(state = universityFeedsInitialState, action) {
  switch (action.type) {
    case UniversitiesConstants.GET_UNIVERSITY_FEED_SUCCESS:
      return update(state, {
        universityFeeds: { $set: concat([], action.feeds) }
      });
    case UniversitiesConstants.GET_UNIVERSITY_FEED_FAILURE:
      return update(state, {
        errorType: { $set: action.errorType },
        errorMessage: { $set: action.errorMessage },
        errorCode: { $set: action.errorCode }
      });
    default:
      return state;
  }
}

export function universityDetails(
  state = universityDetailsInitialState,
  action
) {
  switch (action.type) {
    case UniversitiesConstants.GET_UNIVERSITY_DETAILS_SUCCESS:
      return update(state, {
        details: { $set: action.details },
        followers: { $set: action.followers }
      });
    case UniversitiesConstants.GET_UNIVERSITY_PEOPLE_SUCCESS:
      return update(state, {
        universityPeople: { $set: concat([], action.universityPeople) },
        universityPeopleCount: { $set: action.count }
      });
    case UniversitiesConstants.SET_FOLLOWING_UNIVERSITY:
      return update(state, {
        followingUniversity: { $set: action.followingUniversity }
      });
    case UniversitiesConstants.SET_USERTYPE:
      return update(state, {
        usertype: { $set: action.usertype }
      });
    case UniversitiesConstants.GET_DEPARTMENT_PEOPLE_SUCCESS:
      return update(state, {
        departmentPeople: { $set: concat([], action.departmentPeople) },
        departmentPeopleCount: { $set: action.count }
      });
    case UniversitiesConstants.GET_ADMIN_CANDIDATE_SUCCESS:
      return update(state, {
        adminCandidate: { $set: concat([], action.candidates) }
      });
    case UniversitiesConstants.GET_DEPARTMENT_FEED_SUCCESS:
      return update(state, {
        departmentFeeds: { $set: concat([], action.feeds) }
      });
    case UniversitiesConstants.GET_UNIVERSITY_DETAILS_FAILURE:
      return update(state, {
        errorType: { $set: action.errorType },
        errorMessage: { $set: action.errorMessage },
        errorCode: { $set: action.errorCode }
      });
    case UniversitiesConstants.GET_UNIVERSITY_DEPARTMENTS_SUCCESS:
      return update(state, {
        departments: { $set: Object.assign({}, action.departments) }
      });
    case UniversitiesConstants.SET_POST_AUDIENCE:
      return update(state, { postAudience: { $set: action.array } });
    case UniversitiesConstants.SET_POST_MAJOR:
      return update(state, { postMajor: { $set: action.array } });
    case UniversitiesConstants.SET_POST_TAGS:
      return update(state, { postTags: { $set: action.array } });
    case FOLLOW_UNIVERSITY_SUCCESS:
      return update(state, {
        followers: { $set: state.followers.concat(action.university) }
      });
    case UniversitiesConstants.CLEAR_UNIVERSITY_DATA:
      return (state = universityDetailsInitialState);
    case UNFOLLOW_UNIVERSITY_SUCCESS:
      let index = findIndex(
        state.followers,
        university => university.id === action.universityFollowId
      );
      if (index > -1) {
        state.followers.splice(index, 1);
      }
      return update(state, {
        followers: { $set: cloneDeep(state.followers) }
      });
    default:
      return state;
  }
}
