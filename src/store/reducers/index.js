import { combineReducers } from 'redux';
import todos from './todos';
import { loginApi } from './login';
import { signupApi } from './signup';
import { forgotPasswordApi } from './forgotpassword';
import { resetPasswordApi } from './resetpassword';
import { people, peopleByName, peopleFilters, peopleInfo } from './people';
import { countries } from './countries';
import { notificationState } from './Notifications';
import {
  alluniversities,
  paginatedUniversities,
  universitiesFollowed,
  universityBySlug,
  paginatedDepartments,
  universitesByCountries,
  universityCountryFilters,
  universityCount,
  universitiesByName,
  universityDetails,
  universityFeeds,
  universityPostCountryFilters
} from './universities';
import {
  paginatedPosts,
  getConnections,
  postTaggedUsers,
  postTaggedUsers,
  postAudience
} from './feed';
import { loaderApi } from './Loader';
import { errorModal } from './errormodal';
import { userInfoAPI } from './profile';
import { messagingStore } from './messaging';
import { drawerReducer } from './drawer';

const rootReducer = combineReducers({
  todos,
  loginApi,
  signupApi,
  forgotPasswordApi,
  resetPasswordApi,
  people,
  peopleByName,
  peopleFilters,
  peopleInfo,
  countries,
  alluniversities,
  paginatedUniversities,
  universitiesFollowed,
  universityBySlug,
  paginatedDepartments,
  paginatedPosts,
  loaderApi,
  errorModal,
  universitesByCountries,
  universityCountryFilters,
  universityCount,
  universitiesByName,
  getConnections,
  userInfoAPI,
  postTaggedUsers,
  postAudience,
  notificationState,
  universityDetails,
  universityFeeds,
  messagingStore,
  universityPostCountryFilters,
  drawerReducer
});

export default rootReducer;
