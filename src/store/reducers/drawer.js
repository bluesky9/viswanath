import { DrawerConstants } from '../constants'

const INITIAL_DIALOG_STATE = {
  isOpened: false
};


//title,message,dialogType,onConfirm,onCancel

export function drawerReducer(state = INITIAL_DIALOG_STATE, action) {
  switch (action.type) {
    case DrawerConstants.DRAWER_OPEN:  return { ...state, isOpened:true };
    case DrawerConstants.DRAWER_CLOSED:  return { INITIAL_DIALOG_STATE };
    default: return state;
  }
}
