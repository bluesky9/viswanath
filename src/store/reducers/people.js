import update from 'immutability-helper';
import { PeopleConstants } from '../constants';
import { concat } from 'lodash';

const {PEOPLE_REQUEST,PEOPLE_FAILURE,PEOPLE_SUCCESS} = PeopleConstants;
const {SEARCH_PEOPLE_BY_NAME_REQUEST,SEARCH_PEOPLE_BY_NAME_SUCCESS,SEARCH_PEOPLE_BY_NAME_FAILURE} = PeopleConstants;
const peopleInitialState = {
    isWaitingResponse: false,
    peopleList:{
      content: [],
      totalElements: 0
    },
    blockedPeople: [],
    errorType:undefined,
    errorMessage:undefined,
    errorCode:undefined
}
const peopleInfoInitialState = {
    isWaitingResponse: false,
    selectedPeopleId: undefined,
    peopleInfo: {},
    errorType:undefined,
    errorMessage:undefined,
    errorCode:undefined
}
const peopleByNameInitialState = {
  isWaitingResponse: false,
  people:[],
  errorType:undefined,
  errorMessage:undefined,
  errorCode:undefined
}

const peopleFiltersInitialState = {
  filters: {
    userTypes: [],
    univIds: [],
    countries: [],
    pageSize: 15,
    pageNo: 0,
    userIds: [],
    countOnly: false,
  },
  filterApplied: false
};

export function people(state = peopleInitialState, action) {
  switch (action.type) {
    case PEOPLE_REQUEST:
      return update(state,{
        isWaitingResponse: {$set: true}
      })
    case PEOPLE_SUCCESS:
      return update(state,{
        peopleList: {$set: Object.assign({}, action.peopleList)},
        isWaitingResponse: {$set: false}
      })
    case PEOPLE_FAILURE:
      return update(state,{
        errorType: {$set: action.errorType},
        errorMessage: {$set: action.errorMessage},
        errorCode: {$set: action.errorCode},
        isWaitingResponse: {$set: false}
      })
    case PeopleConstants.GET_BLOCKED_PEOPLE_SUCCESS:
      return update(state,{
        blockedPeople: {$set: concat([], action.blockedPeople)},
      })
    default:
      return state
  }
}

export function peopleByName(state = peopleByNameInitialState, action) {
  switch (action.type) {
    case SEARCH_PEOPLE_BY_NAME_REQUEST:
      return update(state,{
        isWaitingResponse: {$set: true}
      })
    case SEARCH_PEOPLE_BY_NAME_SUCCESS:
      return update(state,{
        people: {$set: action.usersList},
        isWaitingResponse: {$set: false}
      })
    case SEARCH_PEOPLE_BY_NAME_FAILURE:
      return update(state,{
        errorType: {$set: action.errorType},
        errorMessage: {$set: action.errorMessage},
        errorCode: {$set: action.errorCode},
        isWaitingResponse: {$set: false}
      })
    default:
      return state
  }
}

export function peopleFilters(
  state = peopleFiltersInitialState,
  action
) {
  switch (action.type) {
    case PeopleConstants.PEOPLE_FILTERS_REQUEST:
      return update(state, { filters: { $set: action.filters } });
    case PeopleConstants.SET_FILTER_APPLIED:
      return update(state, { filterApplied: { $set: action.filterApplied}} )
    default:
      return state;
  }
}

export function peopleInfo(
  state = peopleInfoInitialState,
  action
) {
  switch (action.type) {
    case PeopleConstants.PEOPLE_INFO_REQUEST:
      return update(state,{
        isWaitingResponse: {$set: true}
      })
    case PeopleConstants.PEOPLE_INFO_SUCCESS:
      state.peopleInfo[action.id] = action.peopleInfo
      return Object.assign({}, state, {
        peopleInfo: state.peopleInfo,
        isWaitingResponse: {$set: false}
      })
    case PeopleConstants.PEOPLE_INFO_FAILURE:
      return update(state,{
        errorType: {$set: action.errorType},
        errorMessage: {$set: action.errorMessage},
        errorCode: {$set: action.errorCode},
        isWaitingResponse: {$set: false}
      })
    case PeopleConstants.PEOPLE_SELECTED_ID:
      return update(state,{
        selectedPeopleId: {$set: action.selectedPeopleId}
      })
    default:
      return state
  }
}
