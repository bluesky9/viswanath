import { ErrorModalConstants } from '../constants'

const INITIAL_DIALOG_STATE = {
  isShown:false,
  title:null,
  message:null,
  dialogType:"inform",
  closable:true,
  onConfirm:()=>{},
  onCancel:()=>{},
  instructionMessage: null,
  button: null,
  images: [],
  index: undefined
};


//title,message,dialogType,onConfirm,onCancel

export function errorModal(state = INITIAL_DIALOG_STATE, action) {
  switch (action.type) {
    case ErrorModalConstants.DIALOG_SHOWS:  return { ...state, isShown:true, title:action.title,message:action.message,closable:action.closable,dialogType:action.dialogType,onConfirm:action.onConfirm,onCancel:action.onCancel,instructionMessage:action.instructionMessage,button:action.button, images: action.images, index: action.index};
    case ErrorModalConstants.DIALOG_HIDES:  return { INITIAL_DIALOG_STATE };
    default: return state;
  }
}
