import update from 'immutability-helper';
import { ResetPasswordConstants } from '../constants'
const {RESET_PASSWORD_REQUEST,RESET_PASSWORD_FAILURE,RESET_PASSWORD_SUCCESS} = ResetPasswordConstants;
const initialState = {
    isWaitingResponse: false,
    userInfo:{},
    errorType:undefined,
    errorMessage:undefined,
    errorCode:undefined
}

export function resetPasswordApi(state = initialState, action) {
  switch (action.type) {
    case RESET_PASSWORD_REQUEST:
      return update(state,{
        isWaitingResponse: {$set: true}
      })
    case RESET_PASSWORD_SUCCESS:
      return update(state,{
        userInfo: {$set: action.userInfo},
        isWaitingResponse: {$set: false}
      })
    case RESET_PASSWORD_FAILURE:
      return update(state,{
        errorType: {$set: action.errorType},
        errorMessage: {$set: action.errorMessage},
        errorCode: {$set: action.errorCode},
        isWaitingResponse: {$set: false}
      })
    default:
      return state
  }
}
