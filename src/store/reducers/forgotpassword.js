import update from 'immutability-helper';
import { ForgotPasswordConstants } from '../constants'
const {FORGOT_PASSWORD_REQUEST,FORGOT_PASSWORD_FAILURE,FORGOT_PASSWORD_SUCCESS} = ForgotPasswordConstants;
const initialState = {
    isWaitingResponse: false,
    userInfo:{},
    errorType:undefined,
    errorMessage:undefined,
    errorCode:undefined
}

export function forgotPasswordApi(state = initialState, action) {
  switch (action.type) {
    case FORGOT_PASSWORD_REQUEST:
      return update(state,{
        isWaitingResponse: {$set: true}
      })
    case FORGOT_PASSWORD_SUCCESS:
      return update(state,{
        userInfo: {$set: action.userInfo},
        isWaitingResponse: {$set: false}
      })
    case FORGOT_PASSWORD_FAILURE:
      return update(state,{
        errorType: {$set: action.errorType},
        errorMessage: {$set: action.errorMessage},
        errorCode: {$set: action.errorCode},
        isWaitingResponse: {$set: false}
      })
    default:
      return state
  }
}
