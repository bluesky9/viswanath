import update from 'immutability-helper';
import { MessagingConstants } from '../constants'
import { findIndex, cloneDeep } from 'lodash';
const { 
  GETMESSAGING_REQUEST,
  GETMESSAGING_FAILURE,
  GETMESSAGING_SUCCESS,
  GETCONVERSATION_REQUEST,
  GETCONVERSATION_SUCCESS,
  GETCONVERSATION_FAILURE,

  GETALLCONVERSATION_REQUEST,
  GETALLCONVERSATION_SUCCESS,
  GETALLCONVERSATION_FAILURE,
  GET_MESSAGES_SUCCESS,
  SENT_MESSAGE,
  CREATE_CONVERSATION_SUCCESS,
  DELETE_MESSAGE_SUCCESS,
  MUTE_CONVERSATION_SUCCESS
} = MessagingConstants

const initialState = {
  data: [],
  allConversations: [],
  currentConversation: {},
  messages: {
    last: false,
    number: 0,
    content: []
  }
}

let index = -1;

export function messagingStore(state = initialState, action) {
  switch (action.type) {
    case GETMESSAGING_REQUEST:
      return state;
    case GETMESSAGING_SUCCESS:
      return state;
    case GETMESSAGING_FAILURE:
      return state;
    case GETCONVERSATION_REQUEST:
      return Object.assign({}, state, {
        currentConversation: action.currentConversation
      });
    case GETCONVERSATION_SUCCESS:
      return state;
    case GETCONVERSATION_FAILURE:
      return state;
    case GETALLCONVERSATION_SUCCESS:
      return Object.assign({}, state, {
        allConversations: action.conversations
      });
    case MUTE_CONVERSATION_SUCCESS:
      index = findIndex(state.allConversations, (conversation) => action.conversationId === conversation.id)
      let memberInfoIndex = -1;
      if (index > -1) {
        memberInfoIndex = findIndex(state.allConversations[index].memberInfos, (member) => member.users.id === action.userId);
        if (memberInfoIndex > -1) {
          state.allConversations[index].memberInfos[memberInfoIndex].muted = (action.actionType === 'mute')
        }
      }
      return Object.assign({}, state, {
        allConversations: cloneDeep(state.allConversations)
      })
    case SENT_MESSAGE:
    let conversation = [];
      index = findIndex(state.allConversations, (conversation) => action.conversationId === conversation.id)
      if (index > -1) {
        if (state.allConversations[index].messages == null) {
          state.allConversations[index].messages = [action.message]
        } else {
          state.allConversations[index].messages.push(action.message)
        }
        conversation = state.allConversations[index];
        state.allConversations.splice(index,1);
      }
      return Object.assign({}, state, {
        allConversations: cloneDeep([].concat(conversation,state.allConversations))
      } )
    case DELETE_MESSAGE_SUCCESS:
    index = findIndex(state.allConversations, (conversation) => action.conversationId === conversation.id)
    if (index > -1) {
      if (state.allConversations[index].messages == null) {
        let messageIndex = findIndex(state.allConversations[index].messages, (message) => message.id === action.messageId)
        if (messageIndex > -1) {
          state.allConversations[index].messages.splice(messageIndex, 1)
        }
      }
    }
    messageIndex = findIndex(state.messages.content, (message) => action.messageId === message.id)
    if (messageIndex > -1) {
      state.messages.content.splice(messageIndex, 1)
    }
    return Object.assign({}, state, {
      allConversations: cloneDeep(state.allConversations),
      messages: {
        last: state.messages.last,
        number: state.messages.number,
        content: state.messages.content
      }
    } )
    case CREATE_CONVERSATION_SUCCESS:
      state.allConversations.push(action.conversation)
      return Object.assign({}, state, {
        allConversations: cloneDeep(state.allConversations)
      } )
    case GET_MESSAGES_SUCCESS:
      return Object.assign({}, state, {
        messages: {
          last: action.messages.last,
          number: action.messages.number,
          content: action.messages.number === 0 ? action.messages.content : state.messages.content.concat(action.messages.content)
        }
      })
    default:
      return state
  }
}
