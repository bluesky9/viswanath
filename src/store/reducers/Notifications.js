import update from 'immutability-helper';
const initialState = {
  isWaitingResponse: false,
  allNotifications: [],
  connectionNotifications: [],
  unreadCount: undefined,
  errorType: undefined,
  errorMessage: undefined,
  errorCode: undefined
};

export function notificationState(state = initialState, action) {
  switch (action.type) {
    case 'GET_NOTIFICATIONS':
      return update(state, {
        isWaitingResponse: { $set: true }
      })
    case 'GET_NOTIFICATIONS_SUCCESS':
      return update(state, {
        isWaitingResponse: { $set: false },
        allNotifications: { $set: action.notifications }
      });
    case 'GET_NOTIFICATIONS_FAILURE':
      return update(state, {
        isWaitingResponse: { $set: false },
        errorType: { $set: action.errorType },
        errorMessage: { $set: action.errorMessage },
        errorCode: { $set: action.errorCode }
      });
    case 'GET_CONNECTION_NOTIFICATIONS_SUCCESS':
      return update(state, {
        isWaitingResponse: { $set: false },
        connectionNotifications: { $set: action.notifications }
      });
    case 'GET_CONNECTION_NOTIFICATIONS_FAILURE':
      return update(state, {
        isWaitingResponse: { $set: false },
        errorType: { $set: action.errorType },
        errorMessage: { $set: action.errorMessage },
        errorCode: { $set: action.errorCode }
      });
    case 'GET_NOTIFICATIONS_COUNT_SUCCESS':
      return update(state, {
        isWaitingResponse: { $set: false },
        unreadCount: { $set: action.count }
      });
    case 'GET_NOTIFICATIONS_COUNT_FAILURE':
      return update(state, {
        isWaitingResponse: { $set: false },
        errorType: { $set: action.errorType },
        errorMessage: { $set: action.errorMessage },
        errorCode: { $set: action.errorCode }
      });
    default:
      return state;
  }
}
