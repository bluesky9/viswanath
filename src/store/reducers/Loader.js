import { LoaderConstants } from '../constants'

const INITIAL_LOADER_STATE = {
  isShown:false
};

export function loaderApi(state = INITIAL_LOADER_STATE, action) {
  switch (action.type) {
    case LoaderConstants.LOADER_SHOWS:  return { ...state, isShown:true};
    case LoaderConstants.LOADER_HIDES:  return { ...state, isShown:false};
    default: return state;
  }
}
