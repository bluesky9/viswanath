import update from 'immutability-helper';
import { LoginConstants } from '../constants'
const { LOGIN_REQUEST, LOGIN_FAILURE, LOGIN_SUCCESS } = LoginConstants;
const initialState = {
  isWaitingResponse: false,
  errorLoggedIn: false,
  userInfo: {},
  errorType: undefined,
  errorMessage: undefined,
  errorCode: undefined
}

export function loginApi(state = initialState, action) {
  switch (action.type) {
    case LOGIN_REQUEST:
      return update(state, {
        isWaitingResponse: { $set: true }
      })
    case LOGIN_SUCCESS:
      return update(state, {
        userInfo: { $set: action.userInfo },
        isWaitingResponse: { $set: false },
        errorType: { $set: undefined },
        errorMessage: { $set: undefined },
        errorCode: { $set: undefined }
      })
    case LOGIN_FAILURE:
      return update(state, {
        errorType: { $set: action.errorType },
        errorMessage: { $set: action.errorMessage },
        errorCode: { $set: action.errorCode },
        errorLoggedIn: { $set: true },
      })
    default:
      return state
  }
}
