import update from 'immutability-helper';
import { CountriesConstants } from '../constants'
const {COUNTRIES_REQUEST,COUNTRIES_FAILURE,COUNTRIES_SUCCESS} = CountriesConstants;

const peopleByNameInitialState = {
  isWaitingResponse: false,
  countries:[],
  errorType:undefined,
  errorMessage:undefined,
  errorCode:undefined
}
export function countries(state = peopleByNameInitialState, action) {
  switch (action.type) {
    case COUNTRIES_REQUEST:
      return update(state,{
        isWaitingResponse: {$set: true}
      })
    case COUNTRIES_SUCCESS:
      return update(state,{
        countries: {$set: action.countries},
        isWaitingResponse: {$set: false}
      })
    case COUNTRIES_FAILURE:
      return update(state,{
        errorType: {$set: action.errorType},
        errorMessage: {$set: action.errorMessage},
        errorCode: {$set: action.errorCode},
        isWaitingResponse: {$set: false}
      })
    default:
      return state
  }
}
