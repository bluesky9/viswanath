import update from 'immutability-helper';
import { ProfileConstants } from '../constants'
import { findIndex, cloneDeep } from 'lodash';

const initialState = {
    isWaitingResponse: false,
    userInfo:{},
    errorType:undefined,
    errorMessage:undefined,
    errorCode:undefined,
    userId: undefined,
}

const IdsType = {
  'education': 'eduId',
  'workExperience': 'workId',
  'achievement': 'achieveId',
  'membership': 'membershipId',
  'research': 'researchId',
  'volunteerWork': 'volunteerId'
}

export function userInfoAPI(state = initialState, action) {
    switch (action.type) {
      case ProfileConstants.PROFILE_REQUEST:
        return update(state,{
          isWaitingResponse: {$set: true}
        })
      case ProfileConstants.PROFILE_SUCCESS:
        return update(state,{
          userInfo: {$set: Object.assign({}, action.userInfo)},
          isWaitingResponse: {$set: false}
        })
      case ProfileConstants.PROFILE_FAILURE:
      case ProfileConstants.SET_USER_ID_REQUEST:
        return update(state,{
          userId: {$set: action.userId}
        })
      case ProfileConstants.EXTRA_PROFILE_DETAILS_FAILURE:
        return update(state,{
          errorType: {$set: action.errorType},
          errorMessage: {$set: action.errorMessage},
          errorCode: {$set: action.errorCode},
          isWaitingResponse: {$set: false}
        })
    case ProfileConstants.EXTRA_PROFILE_DETAILS_REQUEST:
        return update(state,{
            isWaitingResponse: {$set: true}
        })
    // case ProfileConstants.UPLOAD_PROFILE_PIC_SUCCESS:
    //     return update(state,{
    //         userInfo: { userInfo, $merge: action.updatedProfile}
    //     })
    case ProfileConstants.EXTRA_PROFILE_DETAILS_SUCCESS:
        if (action.userInfo.type === 'education' ||
            action.userInfo.type === 'volunteerWork' ||
            action.userInfo.type === 'achievement' ||
            action.userInfo.type === 'membership' ||
            action.userInfo.type === 'research'
        ) {
          let index = -1;
          if (action.userInfo.payload.id) {
            action.userInfo.payload[IdsType[action.userInfo.type]] = action.userInfo.payload.id;
            index = findIndex(state.userInfo[action.userInfo.type], data => data[IdsType[action.userInfo.type]] === action.userInfo.payload.id);
            state.userInfo[action.userInfo.type][index] = action.userInfo.payload;
          } else {
            state.userInfo[action.userInfo.type].push(cloneDeep(action.userInfo.payload));
          }
          action.userInfo = {};
        } else if (action.userInfo.type === 'workExperience') {
          let index = -1;
          if (action.userInfo.payload.id) {
            action.userInfo.payload[IdsType[action.userInfo.type]] = action.userInfo.payload.id;
            index = findIndex(state.userInfo.workExps, data => data[IdsType[action.userInfo.type]] === action.userInfo.payload.id);
            state.userInfo.workExps[index] = action.userInfo.payload;
          } else {
            state.userInfo.workExps.push(cloneDeep(action.userInfo.payload));
          }
          action.userInfo = {};
        }
        return update(state,{
            userInfo: {$set: Object.assign({}, cloneDeep(state.userInfo), action.userInfo)},
            isWaitingResponse: {$set: false}
        })
      case ProfileConstants.DELETE_EXTRA_PROFILE_DETAILS:
        let index = -1;
        index = findIndex(state.userInfo[action.userInfo.type], data => data[IdsType[action.userInfo.type]] === action.userInfo.id);

        if (action.userInfo.type === 'workExperience') {
          index = findIndex(state.userInfo.workExps, data => data[IdsType[action.userInfo.type]] === action.userInfo.id);
          if (index > -1) {
            state.userInfo.workExps.splice(index, 1);
          }
        } else {
          if (index > -1) {
            state.userInfo[action.userInfo.type].splice(index, 1);
          }
        }
        return update(state,{
          userInfo: {$set: Object.assign({}, state.userInfo)},
          isWaitingResponse: {$set: false}
      })
      default:
        return state
    }
  }
