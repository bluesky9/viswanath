/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

import React, { Component } from 'react';
import { NavigationScreenProps } from 'react-navigation';
import AppWithFooter from './AppWithFooter';
import { Provider } from 'react-redux';
import setupStore from './store/configureStore.dev.js';
// import { setNavigator } from './NavigationService';
import { AsyncStorage, PushNotificationIOS, Platform, AppState, Linking } from 'react-native';
import PushNotification from 'react-native-push-notification';
import { Spinner } from 'native-base';
// import DeviceInfo from 'react-native-device-info';
import SocketIOClient from 'socket.io-client';
import * as NavigationActions from './NavigationService';
import { getNotificationsUnreadCount } from './store/actions/Notifications.js';
import { getAllConversations } from './store/actions/Messaging.js';
import { confirmMail } from './store/actions/Universities';

import { setUserToken } from './store/actions/Profile';
import { getPost } from './store/actions/Feed';
let notifToken = undefined;
const prodBoardingUrl = /https:\/\/uple.co\/firstLogin\//;
const prodResetPasswordUrl = /https:\/\/uple.co\/new-password\?code=/;
const boardingUrl = /https:\/\/dev.uple.co\/firstLogin\//;
const resetPasswordUrl = /https:\/\/dev.uple.co\/new-password\?code=/;
const prodUniversityMail = /https:\/\/uple.co\/acceptUnivFollow\?univId=/;
const universityMail = /https:\/\/dev.uple.co\/acceptUnivFollow\?univId=/;

export const getNotifToken = () => {
  return notifToken;
}

const store = setupStore();

export const handleUniversityMailURL = (event) => {
  if (event.url) {
    let url = event.url;
    let params = {};
    let paramsArray = url.split('?')[1].split('&');
    for (var i = 0; i < paramsArray.length; i++) {
      var pair = paramsArray[i].split('=');
      params[pair[0]] = decodeURIComponent(pair[1]);
    }
    if (prodUniversityMail.test(url)) {
      store.dispatch(confirmMail(params));
      // NavigationActions.navigate('SignUpUserDetails', {authCode : url.replace(boardingUrl, '')});
    } else if (universityMail.test(url)) {
      store.dispatch(confirmMail(params));
      // NavigationActions.navigate('LoginNewPassword', {token: url.replace(resetPasswordUrl, '')});
    }
  }
}

const socket = SocketIOClient('http://devchat.uple.co', {
  path: '/messages',
  reconnect: true,
  transports: ['websocket']
} as any);

export const getSocketConnection = () => {
  return socket;
}

interface Props extends NavigationScreenProps<{}> { }
type State = {
  token;
  loading;
  userId;
  appState: any;
  notification: any;
};
export default class App extends Component<Props, State> {
  navigator: any;

  constructor(props) {
    super(props);
    this.state = {
      token: undefined,
      loading: true,
      userId: undefined,
      appState: AppState.currentState,
      notification: undefined
    };
  }

  componentDidMount() {
    // Universal Linking
    this.getUserId().then(val => {
      if (!val) {
        Linking.getInitialURL()
          .then((url) => {
            if (url) {
              if (boardingUrl.test(url)) {
                NavigationActions.navigate('SignUpUserDetails', { authCode: url.replace(boardingUrl, '') });
              } else if (resetPasswordUrl.test(url)) {
                NavigationActions.navigate('LoginNewPassword', { token: url.replace(resetPasswordUrl, '') });
              } else if (prodBoardingUrl.test(url)) {
                NavigationActions.navigate('SignUpUserDetails', { authCode: url.replace(prodBoardingUrl, '') });
              } else if (prodResetPasswordUrl.test(url)) {
                NavigationActions.navigate('LoginNewPassword', { token: url.replace(prodResetPasswordUrl, '') });
              }
            }
          })
          .catch((e) => console.log(e));
        Linking.addEventListener('url', this.handleOpenURL);
      } else {
        Linking.getInitialURL()
          .then((url) => {
            if (url) {
              let params = {};
              let paramsArray = url.split('?')[1].split('&');
              for (var i = 0; i < paramsArray.length; i++) {
                var pair = paramsArray[i].split('=');
                params[pair[0]] = decodeURIComponent(pair[1]);
              }
              if (prodUniversityMail.test(url)) {
                store.dispatch(confirmMail(params));
                // NavigationActions.navigate('SignUpUserDetails', {authCode : url.replace(boardingUrl, '')});
              } else if (universityMail.test(url)) {
                store.dispatch(confirmMail(params));
                // NavigationActions.navigate('LoginNewPassword', {token: url.replace(resetPasswordUrl, '')});
              }
            }
          })
          .catch((e) => console.log(e));
        Linking.addEventListener('url', handleUniversityMailURL);
      }
    });

    const parentRef = this;
    if (Platform.OS === 'ios') {
      PushNotificationIOS.requestPermissions();
      AppState.addEventListener('change', this._handleAppStateChange);
      PushNotificationIOS.addEventListener('register', (token) => {
        console.log(token, 'register');
        notifToken = token;
        parentRef.getUserId().then((userId) => {
          if (userId && token) {
            store.dispatch(setUserToken({
              userId: userId,
              token: token,
              deviceId: token,
              deviceType: Platform.OS === 'ios' ? 'IOS' : 'ANDROID'
            }))
          }
        })
      })
      PushNotificationIOS.addEventListener('registrationError', (error) => {
        console.log(error, 'error');
      })
      PushNotificationIOS.addEventListener('notification', (payload: any) => {
        console.log(payload, 'notification');
        if (this.state.appState.match(/inactive|background/)) {
          this.setState({
            notification: payload
          })
        } else {
          if (payload) {
            switch (payload._data.notiType) {
              case 'CONNECTION':
              case 'GENERAL': store.dispatch(getNotificationsUnreadCount())
                break;
              case 'MESSAGE': if (NavigationActions.getLastRouteName() === 'Messaging') {
                store.dispatch(getAllConversations(true))
              }
                break;
              default: break;
            }
          }
        }
      })
      PushNotificationIOS.getInitialNotification().then((payload: any) => {
        console.log(payload, 'initial');
        if (payload) {
          switch (payload._data.notiType) {
            case 'CONNECTION':
              setTimeout(() => {
                if (RegExp('started following you').test(payload._alert.body)) {
                  NavigationActions.navigate('MyProfile', {});
                } else if (RegExp('would like to connect').test(payload._alert.body)) {
                  NavigationActions.navigate('Notifications', { displayNotif: true });
                } else {
                  NavigationActions.navigate('MyProfile', {});
                }
              }, 500)
              break;
            case 'GENERAL': setTimeout(() => {
              // NavigationActions.navigate('Notifications', {});
              store.dispatch(getPost(parseInt(payload._data.id, 10)))
            }, 500)
              break;
            case 'MESSAGE': setTimeout(() => {
              store.dispatch(getAllConversations()).then(() => {
                const globalStore = store.getState();
                const conversations = globalStore.messagingStore.allConversations;
                if (conversations && conversations.length) {
                  conversations.forEach((conversation, index) => {
                    if (parseInt(payload._data.convId, 10) === conversation.id) {
                      let muted = false;
                      let user = undefined;
                      conversation.memberInfos.forEach((member) => {
                        if (member.users.id === globalStore.userInfoAPI.userId) {
                          muted = member.muted;
                        }
                        if (member.users.id !== globalStore.userInfoAPI.userId) {
                          user = member.users;
                        }
                      })
                      if (!user && conversation.memberInfos && conversation.memberInfos[0]) {
                        user = conversation.memberInfos[0].users;
                      }
                      if (NavigationActions.getLastRouteName() !== 'MessagingSingle') {
                        NavigationActions.navigate('MessagingSingle', {
                          conversationId: conversation.id,
                          name: conversation.conversationType === 'ONE_TO_ONE' ? user.name : conversation.name,
                          muted: muted,
                          conversationIndex: index
                        });
                      }
                    }
                  });
                } else {
                  NavigationActions.navigate('Messaging', {});
                }
              })
            }, 200)
              break;
            default: break;
          }
        }
      })
    } else if (PushNotification) {

      PushNotification.configure({
        onRegister: function ({ token }) {
          console.log('TOKEN:', token);
          notifToken = token;
          parentRef.getUserId().then((userId) => {
            if (userId && token) {
              store.dispatch(setUserToken({
                userId: userId,
                token: token,
                deviceId: token,
                deviceType: Platform.OS === 'ios' ? 'IOS' : 'ANDROID'
              }))
              // store.dispatch(setUserToken({
              //   deviceId: token, token, userId, deviceType:
              //     Platform.OS === 'ios' ? 'IOS' : 'ANDROID'
              // }))
            }
          })
        },
        // (required) Called when a remote or local notification is opened or received
        onNotification: function (notification: any) {
          console.log('NOTIFICATION:', notification);
          if (notification.userInteraction) {
            switch (notification.notiType) {
              case 'CONNECTION':
                setTimeout(() => {
                  if (RegExp('started following you').test(notification.message)) {
                    NavigationActions.navigate('MyProfile', {});
                  } else if (RegExp('would like to connect').test(notification.message)) {
                    NavigationActions.navigate('Notifications', { displayNotif: true });
                  } else {
                    NavigationActions.navigate('MyProfile', {});
                  }
                }, 500)
                break;
              case 'GENERAL': setTimeout(() => {
                store.dispatch(getPost(parseInt(notification.id, 10)))
              }, 500)
                break;
              case 'MESSAGE': setTimeout(() => {
                store.dispatch(getAllConversations()).then(() => {
                  const globalStore = store.getState();
                  const conversations = globalStore.messagingStore.allConversations;
                  if (conversations && conversations.length) {
                    conversations.forEach((conversation, index) => {
                      if (parseInt(notification.convId, 10) === conversation.id) {
                        let muted = false;
                        let user = undefined;
                        conversation.memberInfos.forEach((member) => {
                          if (member.users.id === globalStore.userInfoAPI.userId) {
                            muted = member.muted;
                          }
                          if (member.users.id !== globalStore.userInfoAPI.userId) {
                            user = member.users;
                          }
                        })
                        if (!user && conversation.memberInfos && conversation.memberInfos[0]) {
                          user = conversation.memberInfos[0].users;
                        }
                        if (NavigationActions.getLastRouteName() !== 'MessagingSingle') {
                          NavigationActions.navigate('MessagingSingle', {
                            conversationId: conversation.id,
                            name: conversation.conversationType === 'ONE_TO_ONE' ? user.name : conversation.name,
                            muted: muted,
                            conversationIndex: index
                          });
                        }
                      }
                    });
                  } else {
                    NavigationActions.navigate('Messaging', {});
                  }
                })
              }, 200)
                break;
              default: break;
            }
          } else if (notification.foreground) {
            switch (notification.notiType) {
              case 'CONNECTION':
              case 'GENERAL': store.dispatch(getNotificationsUnreadCount())
                break;
              case 'MESSAGE': if (NavigationActions.getLastRouteName() === 'Messaging') {
                store.dispatch(getAllConversations(true))
              }
                break;
              default: break;
            }
          }
        },
        // ANDROID ONLY: GCM Sender ID (optional - not required for local notifications, but is need to receive remote push notifications)
        senderID: '716809306806',
        // Should the initial notification be popped automatically
        // default: true
        popInitialNotification: true
      });
    }
    //
    // setNavigator(this.navigator)
    this.getUserId().then(val => {
      socket.on('connect', () => {
        console.log('Connected')
        // socket.emit('joinRoom', {roomName: val})
      });
      this.setState({ userId: val });
    });
    socket.on('disconnect', () => {
      console.log('Disconnected')
    });
    socket.on('error', (error) => {
      console.log('Socket error', error);
    });
    socket.on('connect_error', (error) => {
      console.log(error)
    });
    this.getToken().then(val => {
      this.setState({ token: val, loading: false });
    });
  }

  componentWillUnmount() {
    Linking.removeEventListener('url', this.handleOpenURL);
    Linking.removeEventListener('url', handleUniversityMailURL);
  }

  handleOpenURL = (event) => {
    if (event.url) {
      if (boardingUrl.test(event.url) && NavigationActions.getLastRouteName() !== 'SignUpUserDetails') {
        NavigationActions.navigate('SignUpUserDetails', { authCode: event.url.replace(boardingUrl, '') });
      } else if (resetPasswordUrl.test(event.url) && NavigationActions.getLastRouteName() !== 'LoginNewPassword') {
        NavigationActions.navigate('LoginNewPassword', { token: event.url.replace(resetPasswordUrl, '') });
      } else if (prodBoardingUrl.test(event.url) && NavigationActions.getLastRouteName() !== 'SignUpUserDetails') {
        NavigationActions.navigate('SignUpUserDetails', { authCode: event.url.replace(prodBoardingUrl, '') });
      } else if (prodResetPasswordUrl.test(event.url) && NavigationActions.getLastRouteName() !== 'LoginNewPassword') {
        NavigationActions.navigate('LoginNewPassword', { token: event.url.replace(prodResetPasswordUrl, '') });
      }
    }
  }

  componentWillMount() {
    if (Platform.OS === 'ios') {
      AppState.addEventListener('change', this._handleAppStateChange);
    }
  }

  _handleAppStateChange = (nextAppState) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active' && this.state.notification) {
      let payload = this.state.notification;
      switch (this.state.notification._data.notiType) {
        case 'GENERAL': setTimeout(() => {
          store.dispatch(getPost(parseInt(payload._data.id, 10)))
          // NavigationActions.navigate('Notifications', {});
        }, 500)
          break;
        case 'CONNECTION': setTimeout(() => {
          if (RegExp('started following you').test(payload._alert.body)) {
            NavigationActions.navigate('MyProfile', {});
          } else if (RegExp('would like to connect').test(payload._alert.body)) {
            NavigationActions.navigate('Notifications', { displayNotif: true });
          } else {
            NavigationActions.navigate('MyProfile', {});
          }
        }, 500)
          break;
        case 'MESSAGE': setTimeout(() => {
          store.dispatch(getAllConversations()).then(() => {
            const globalStore = store.getState();
            const conversations = globalStore.messagingStore.allConversations;
            if (conversations && conversations.length) {
              conversations.forEach((conversation, index) => {
                if (parseInt(payload._data.convId, 10) === conversation.id) {
                  let muted = false;
                  let user = undefined;
                  conversation.memberInfos.forEach((member) => {
                    if (member.users.id === globalStore.userInfoAPI.userId) {
                      muted = member.muted;
                    }
                    if (member.users.id !== globalStore.userInfoAPI.userId) {
                      user = member.users;
                    }
                  })
                  if (!user && conversation.memberInfos && conversation.memberInfos[0]) {
                    user = conversation.memberInfos[0].users;
                  }
                  if (NavigationActions.getLastRouteName() !== 'MessagingSingle') {
                    NavigationActions.navigate('MessagingSingle', {
                      conversationId: conversation.id,
                      name: conversation.conversationType === 'ONE_TO_ONE' ? user.name : conversation.name,
                      muted: muted,
                      conversationIndex: index
                    });
                  }
                }
              });
            } else {
              NavigationActions.navigate('Messaging', {});
            }
          })
        }, 200)
          break;
        default: break;
      }
    }
    this.setState({ appState: nextAppState, notification: undefined });
  }

  async getToken() {
    return await AsyncStorage.getItem('token');
  }

  async getUserId() {
    return await AsyncStorage.getItem('userId');
  }

  async removeToken() {
    await AsyncStorage.removeItem('token');
  }

  render() {
    // this.removeToken();
    if (this.state.loading) {
      return <Spinner />;
    } else {
      return (
        <Provider store={store}>
          <AppWithFooter token={this.state.token} userId={this.state.userId} />
        </Provider>
      );
    }
  }
}
