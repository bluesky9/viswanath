import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  ScrollView,
  Image
} from 'react-native';
// import homeStyles from './home-styles';
import { NavigationScreenProps } from 'react-navigation';
import { bindActionCreators } from 'redux';
import MSAHeader from '../layouts/header/header';
import { setPostAudience } from '../../store/actions/Feed';
import { constants } from '../../constants'

// import PeopleFilterHeader from '../people/people-filter-header';
import { filterUsertypeStyles, filterUniversityStyles } from '../people/people-filter-styles';

import { connect } from 'react-redux';

import { filter, map, find} from 'lodash';

interface ILoginScreenProps extends NavigationScreenProps<{}> {
  actions: any;
  audienceState: any;
}

interface State {
  userTypes;
  rightTitle;
  selectAll;
}

class HomePostAudience extends Component<ILoginScreenProps, State> {
  constructor(props) {
    super(props);
    this.state = {
      userTypes: undefined,
      rightTitle: undefined,
      selectAll: false
    };
  }

  componentDidMount() {
    // this.props.audienceState.postAudience.userArray.map(item => {
    //   this.state.audienceFields.map((item2, index2) => {
    //     if (item.usertype === item2.usertype) {
    //       let arr = this.state.audienceFields;
    //       arr[index2].status = true;
    //       this.setState({
    //         audienceFields: arr
    //       });
    //     }
    //   });
    // });

    let filteredUserTypes = this.props.audienceState.postAudience.userArray;

    let userTypes = [];
    Object.keys(constants.USERTYPES.PLURAL).map((item) => {
      userTypes.push({ usertype: item, name: constants.USERTYPES.PLURAL[item]})
    })
    if (filteredUserTypes.length > 0) {
      userTypes.map((item, index) => {
        if (find(filteredUserTypes, obj => obj === item.usertype)) {
          userTypes[index].status = true;
        } else {
          userTypes[index].status = false;
        }
      })
      this.setState({ rightTitle: 'CLEAR' })
    } else {
      this.setState({ selectAll: true })
      this.setState({ rightTitle: 'DONE' })
    }
    this.setState({ userTypes });
  }

  _handleOnApplyPress = () => {

    if (this.state.rightTitle === 'CLEAR') {
      let userTypes = this.state.userTypes;
      map(userTypes, (item) => item.status = false);
      this.setState({ userTypes: userTypes, rightTitle: 'DONE', selectAll: true });
      return ;
    }

    let userFilters = [];
    filter(this.state.userTypes, (obj) => {
      if (obj.status)
        return userFilters.push(obj.usertype);
      else {
        return undefined;
      }
    })
    this.props.actions.setPostAudience(userFilters);
    this.props.navigation.goBack();
  }

  onSelectAllPressed = () => {
    let userTypes = this.state.userTypes
    userTypes.map((item) => {
      item.status = false
    })
    this.setState({ userTypes, selectAll: true, rightTitle: 'DONE' })
  }

  _renderItem(obj, index) {
    return (
      <TouchableOpacity
        style={filterUsertypeStyles.itemContainer}
        onPress={() => {
          let userTypes = this.state.userTypes;

          if (userTypes[index].status) {
            userTypes[index].status = false
          } else {
            userTypes[index].status = true
          }

          let allSelected = filter(userTypes, userType => !userType.status)
          if (allSelected.length === 0 || allSelected.length === 5) {
            userTypes.map((item) => {
              item.status = false
            })
            this.setState({ selectAll: true })
          } else {
            this.setState({ selectAll: false })
          }
          this.setState({ userTypes: userTypes, rightTitle: 'DONE' })
        }}

        key={index}
      >
        { obj.status ?
          <Image source={require('../../../assets/app_icons/Icons/Checkbox-active.png')} style={filterUsertypeStyles.checkboxContainer}
            resizeMode='stretch'
          />
          : <Image source={require('../../../assets/app_icons/Icons/Checkbox-default.png')} style={filterUsertypeStyles.checkboxContainer}
            resizeMode='stretch'
          />
        }
        <Text style={filterUsertypeStyles.itemText}>{obj.name}</Text>
      </TouchableOpacity>
    )
  }

  render() {
    // console.log(this.state.userTypes, '123')
    return (
      <View style={filterUsertypeStyles.container}>
        <MSAHeader
          title={'Audience'}
          rightText={this.state.rightTitle}
          onPress={() => this.props.navigation.goBack()}
          onRightTextPress={() => this._handleOnApplyPress()}
        />
        <View style={filterUsertypeStyles.mainContent}>
          <ScrollView>
            <TouchableOpacity style={[filterUniversityStyles.itemContainer, { marginHorizontal: 15 }]}
              onPress={this.onSelectAllPressed}
            >
              { this.state.selectAll ? (
                <Image
                  source={require('../../../assets/app_icons/Icons/Checkbox-active.png')}
                  style={filterUniversityStyles.checkboxContainer}
                  resizeMode={'stretch'}
                />
              ) : (
                <Image
                  source={require('../../../assets/app_icons/Icons/Checkbox-default.png')}
                  style={filterUniversityStyles.checkboxContainer}
                  resizeMode={'stretch'}
                />
              )}
              <Text style={filterUniversityStyles.itemText}>
                Everybody
              </Text>
            </TouchableOpacity>
            { this.state.userTypes && this.state.userTypes.map((item, index) => {
              return <View key={index}>
                {this._renderItem(item, index)}
                </View>
            })}
          </ScrollView>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    audienceState: state
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      setPostAudience: setPostAudience
    },
    dispatch
  )
});

export default connect(mapStateToProps, mapDispatchToProps)(HomePostAudience);
