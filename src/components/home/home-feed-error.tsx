import React, { Component } from 'react';
import { View, Image, Text } from 'react-native';
// const { width } = Dimensions.get('window');
import homeStyles from './home-styles';
import AppButton from '../layouts/buttons/button';

interface ILoginScreenProps {}
export default class HomeFeedsEmptyState extends Component<ILoginScreenProps> {
  render() {
    return (
      <View style={homeStyles.fullWidthContainer}>
        <View
          style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
        >
          <Image source={require('../../../assets/feedLoadError.png')} />
        </View>
        <View
          style={{
            flex: 1,
            justifyContent: 'flex-start',
            alignItems: 'center'
          }}
        >
          <Text style={homeStyles.feedErrorHeadText}>
            The feed couldn't load..
          </Text>
          <Text style={homeStyles.feedErrorInstructionText}>
            Try refreshing the app
          </Text>
          <AppButton
            type={'primary'}
            label={'Reload feed'}
            containerWidth={'34.5%'}
            containerHeight={'45%'}
            textFamily={'Rubik-Regular'}
            textSize={13}
          />
        </View>
        <View
          style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
        />
      </View>
    );
  }
}
