import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  TextInput,
  Dimensions,
  Platform,
  ScrollView,
  Image,
  Switch
} from 'react-native';
import homeStyles from './home-styles';
import ImagePicker from 'react-native-image-crop-picker';
import { Icon } from 'native-base';
import MSAHeader from '../layouts/header/header';
import { NavigationScreenProps } from 'react-navigation';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { style } from '../../styles/variables';
import { publishPost, setPostAudience, setTaggedUsers, uploadFile } from '../../store/actions/Feed.js';
import { bindActionCreators } from 'redux';
import { constants } from '../../constants'
import { get, find, map, isEmpty } from 'lodash';

import { connect } from 'react-redux';

const { width } = Dimensions.get('window');
let MESSAGE = 'If you turn this option ON the posts will be displayed to people other than your connections/Followers'
const IMAGE_UPLOAD_LIMIT = 5;

interface ILoginScreenProps extends NavigationScreenProps<{}> {
  composeState: any;
  actions: any;
  setTaggedUsers: any;
  uploadFile: any;
  paginatedPosts: any;
}
interface State {
  imagesArray: any;
  title: string;
  message: string;
  publicPost: boolean;
  postId: undefined;
  deletedIds: any;
}

class HomePostCompose extends Component<ILoginScreenProps, State> {
  constructor(props) {
    super(props);
    this.state = {
      postId: get(this.props.navigation.state.params, 'postId', undefined),
      imagesArray: [],
      title: '',
      message: '',
      publicPost: false,
      deletedIds: []
    };
  }

  componentDidMount() {
    let postId = get(this.props.navigation.state.params, 'postId', undefined)
    let post = find(this.props.paginatedPosts, postObj => postObj.postid === postId)
    if (post) {
      console.log(post, 'Choosed Post')
      let postAudience = get(post, 'audience', []);

      this.props.actions.setPostAudience((postAudience.length === 0 || postAudience.length === 5) ? [] : postAudience);
      this.props.actions.setTaggedUsers(get(post, 'taguser', []));
      this.setState({ title: get(post, 'title', '') })
      this.setState({ message: get(post, 'postMessages', '') })
      this.setState({ publicPost: !get(post, 'restricted', false) })
      console.log('here')
      let images = post.files ? post.files : []
      console.log(images, 'here 2')
      let imagesArray = this.state.imagesArray
      images.map((item) => {
        let imageObj = {
          mediumUrl: item.mediumUrl,
          originalUrl: item.originalUrl,
          smallUrl: item.smallUrl
        }
        imagesArray.push(imageObj);
      })
      console.log(imagesArray, 'Images')
      this.setState({ imagesArray })
    }
  }

  removeImage = (index) => {
    let arr = this.state.imagesArray;

    let postId = get(this.props.navigation.state.params, 'postId', undefined)
    let post = find(this.props.paginatedPosts, postObj => postObj.postid === postId)
    let images = post.files ? post.files : []

    let mediumUrl = get(arr[index], 'mediumUrl', '');
    let originalUrl = get(arr[index], 'originalUrl', '');
    let deletedId = find(images, image => image.mediumUrl === mediumUrl || image.originalUrl === originalUrl)
    if (deletedId) {
      let deletedIds = this.state.deletedIds
      deletedIds.push(deletedId.id)
      this.setState({ deletedIds })
    }
    arr.splice(index, 1);
    this.setState({ imagesArray: arr });
  }

  // componentWillReceiveProps(nextProps) {
  //   if (nextProps.navigation.state.params && nextProps.navigation.state.params.image) {
  //     let imagesArray = this.state.imagesArray;
  //     imagesArray.push(nextProps.navigation.state.params.image)
  //     this.setState({ imagesArray })
  //   }
  // }

  publishPost() {
    let taggedUserIds = [];
    if (this.props.composeState.postTaggedUsers.userArray) {
      this.props.composeState.postTaggedUsers.userArray.map(
        (item) => {
          taggedUserIds.push(item.id)
        })
    }

    let imagesArray = this.state.imagesArray

    // For Android specifically
    if (imagesArray.length > IMAGE_UPLOAD_LIMIT) {
      let diff = IMAGE_UPLOAD_LIMIT - imagesArray.length
      imagesArray.splice(5, diff)
    }

    let publishPostObj = {
      userTypes: this.props.composeState.postAudience.userArray,
      title: this.state.title,
      message: this.state.message,
      postExpiry: 'MONTH',
      restricted: !this.state.publicPost,
      taggedUserIds
    }

    if (imagesArray.length > 0 || this.state.deletedIds.length > 0) {
      this.props.actions.uploadFile(imagesArray, publishPostObj, parseInt(this.state.postId, 10), this.state.deletedIds)
    } else {
      this.props.actions.publishPost(publishPostObj, parseInt(this.state.postId, 10));
    }
    // this.props.navigation.goBack();
  }

  openPicker() {
    if (this.state.imagesArray.length < IMAGE_UPLOAD_LIMIT)
      ImagePicker.openPicker({
        cropping: false,
        multiple: true,
        compressImageQuality: Platform.OS === 'ios' ? 0 : 0.5,
        maxFiles: IMAGE_UPLOAD_LIMIT - this.state.imagesArray.length
      })
        .then(images => {
          let arr = this.state.imagesArray;
          map(images, (image) => {
            let imageObj = {
              uri: get(image, 'path', ''),
              name: get(image, Platform.OS === 'ios' ? 'creationDate' : 'modificationDate', ''),
              type: get(image, 'mime', '')
            }
            if (arr.length < IMAGE_UPLOAD_LIMIT) {
              arr.push(imageObj);
            }
          })
          this.setState({
            imagesArray: arr
          });
        })
        .catch(Error);
  }
  openCamera() {
    if (this.state.imagesArray.length < IMAGE_UPLOAD_LIMIT)
      ImagePicker.openCamera({
        cropping: false,
        compressImageQuality: Platform.OS === 'ios' ? 0 : 0.5
      })
        .then(image => {
          let arr = this.state.imagesArray;
          let imageObj = {
            uri: get(image, 'path', ''),
            name: new Date().getTime().toString(),
            type: get(image, 'mime', '')
          }
          arr.push(imageObj);
          this.setState({
            imagesArray: arr
          });
        })
        .catch((e) => {
          console.log(e, 'Error')
        });
  }
  render() {
    let checkPost = this.state.title !== '' && (this.state.message !== '' || this.state.imagesArray.length > 0)
    return (
      <View style={{ flex: 1 }}>
        <MSAHeader
          title={'Edit post'}
          leftText={'CANCEL'}
          rightText={'PUBLISH'}
          onPress={() => this.props.navigation.goBack()}
          onRightTextPress={() => this.publishPost()}
          disableRightText={!checkPost}
        />
        <KeyboardAwareScrollView>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('PostAudience')}
            style={[homeStyles.newPostItemWrap]}
          >
            <View>
              <Text style={homeStyles.newPostItemHead}>
                AUDIENCE
              </Text>
              <Text style={{ width: width - 50, marginTop: 5 }}>
                {this.props.composeState.postAudience.userArray && this.props.composeState.postAudience.userArray.length > 0 ?
                  this.props.composeState.postAudience.userArray.map(
                    (item, index) => {
                      return (
                        <Text key={index} style={homeStyles.newPostItemLow}>
                          <Text>{constants.USERTYPES.PLURAL[item]}</Text>
                          {(this.props.composeState.postAudience.userArray.length - 1) !== index &&
                            <Text>{', '}</Text>
                          }
                        </Text>
                      );
                    }
                  )
                  : <Text style={homeStyles.newPostItemLow}>
                    Everybody
                </Text>
                }
              </Text>
            </View>
            <View
              style={{
                position: 'absolute',
                alignSelf: 'center',
                left: width - 25
              }}
            >
              <Icon
                style={{ fontSize: 20, color: 'rgb(197,207,219)' }}
                name={'arrow-forward'}
              />
            </View>
          </TouchableOpacity>
          <View
            style={[
              homeStyles.newPostItemWrap,
              { justifyContent: 'flex-start' }
            ]}
          >
            <Text
              style={[
                homeStyles.newPostItemHead,
                { paddingBottom: 5 }
              ]}
            >
              TITLE
            </Text>
            <TextInput
              onChangeText={txt =>
                this.setState({
                  title: txt
                })
              }
              style={[homeStyles.newPostInput]}
              value={this.state.title}
              underlineColorAndroid={'rgba(0,0,0,0)'}
              maxLength={40}
            />
            <Text
              style={[
                homeStyles.newPostItemHead,
                { paddingBottom: 5 }
              ]}
            >
              POST
            </Text>
            <TextInput
              onChangeText={txt =>
                this.setState({
                  message: txt
                })
              }
              multiline={true}
              numberOfLines={5}
              style={[homeStyles.newPostInput, { paddingTop: 12, marginBottom: 0, height: undefined }]}
              value={this.state.message}
              underlineColorAndroid={'rgba(0,0,0,0)'}
            />
          </View>
          <View style={{ alignSelf: 'stretch' }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('TagPeople')}
              style={homeStyles.newPostItemWrap}
            >
              <Text style={homeStyles.newPostItemHead}>
                TAG CONNECTIONS
              </Text>
              {!isEmpty(this.props.composeState.postTaggedUsers.userArray) &&
                <View
                  style={{ flexDirection: 'row', marginTop: 5 }}
                >
                  <Text style={{ width: width - 50 }}>
                    {this.props.composeState.postTaggedUsers.userArray && this.props.composeState.postTaggedUsers.userArray.map(
                      (item, index) => {
                        return (
                          <Text key={index} style={[homeStyles.newPostItemLow]}>
                            {item.name}
                            {',  '}
                          </Text>
                        );
                      }
                    )}
                  </Text>
                </View>
              }
              <View
                style={{
                  position: 'absolute',
                  alignSelf: 'center',
                  left: width - 25
                }}
              >
                <Icon
                  style={{ fontSize: 20, color: 'rgb(197,207,219)' }}
                  name={'arrow-forward'}
                />
              </View>
            </TouchableOpacity>
            <View
              // onPress={() => this.openPicker()}
              style={[homeStyles.newPostItemWrap, { flexDirection: 'row', alignItems: 'center' }]}
            >
              <TouchableOpacity
                onPress={() => this.openPicker()}
                style={{ flex: 1 }}
              >
                <Text style={homeStyles.newPostItemHead}>ADD IMAGES</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.openCamera()}
              >
                <Icon
                  style={{ fontSize: 20, color: 'rgb(197,207,219)' }}
                  name={'camera'}
                  active
                />
              </TouchableOpacity>
            </View>
            {!isEmpty(this.state.imagesArray) &&
              <View
                style={[
                  homeStyles.newPostItemWrap,
                  { height: this.state.imagesArray.length === 0 ? 0 : 120 }
                ]}
              >
                <ScrollView
                  horizontal={true}
                  contentContainerStyle={{ alignItems: 'center' }}
                >
                  {this.state.imagesArray.map((item, index) => {
                    return (
                      <View
                        key={index}
                        style={{
                          height: 90,
                          width: 90,
                          marginHorizontal: 15,
                          borderRadius: 3
                        }}
                      >
                        <Image
                          source={{
                            // uri: Platform.OS === 'ios' ? item.uri : item.path // Check For android
                            uri: get(item, 'originalUrl', false) ? item.originalUrl : item.uri
                          }}
                          style={{ height: 90, width: 90 }}
                        />
                        <TouchableOpacity
                          onPress={() => { this.removeImage(index) }}
                          style={{
                            position: 'absolute',
                            top: 0,
                            right: 0,
                            padding: 5,
                            margin: 5,
                            borderRadius: 1,
                            backgroundColor: 'rgba(0,0,0,0.5)'
                          }}
                        >
                          <Image
                            style={{ tintColor: 'white' }}
                            source={require('../../../assets/app_icons/Icons/Close-dark.png')}
                          />
                        </TouchableOpacity>
                      </View>
                    );
                  })}
                </ScrollView>
              </View>
            }
            <View style={{ backgroundColor: 'white', padding: 15 }}>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Text style={{ flex: 1, fontFamily: style.font.family.rubikRegular, color: style.color.coal }}>Public post</Text>
                <Switch value={this.state.publicPost} onValueChange={(bool) => this.setState({ publicPost: bool })}
                  thumbTintColor={style.color.primary}
                  onTintColor={style.color.cream}
                  style={{
                    transform: [{ scaleX: Platform.OS === 'ios' ? .7 : 1 },
                    { scaleY: Platform.OS === 'ios' ? .7 : 1 }]
                  }}
                />
              </View>
              <Text style={{ fontSize: 12, color: style.color.dustyGrey, paddingTop: 10 }}>{MESSAGE}</Text>
            </View>
          </View>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    composeState: state,
    paginatedPosts: state.paginatedPosts.posts
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      publishPost: publishPost,
      setPostAudience: setPostAudience,
      setTaggedUsers: setTaggedUsers,
      uploadFile: uploadFile
    },
    dispatch
  )
});

export default connect(mapStateToProps, mapDispatchToProps)(HomePostCompose);