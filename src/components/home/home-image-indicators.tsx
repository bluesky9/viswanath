import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Dimensions,
  TouchableOpacity
} from 'react-native';
import { CachedImage } from 'react-native-img-cache';

const { width } = Dimensions.get('window');

type homePostsProps = {
  images;
  imageModal;
};

type homePostsState = {
  imageIndexSeleted: Number;
}

export default class HomeImageIndicators extends Component<homePostsProps, homePostsState> {
  constructor(props) {
    super(props)
    this.state = {
      imageIndexSeleted: 1
    }
  }

  _onScrolledToBanner = (e) => {
    let contentOffset = e.nativeEvent.contentOffset.x + 10;
    let viewSize = e.nativeEvent.layoutMeasurement.width;
    let pageNum = Math.floor(contentOffset / viewSize) + 1;
    this.setState({ imageIndexSeleted: pageNum })
  }

  renderIndicators = () => {
    let i = 0
    if (this.props.images.length > 1 )
    return (
      this.props.images.map(() => {
        i++
        return (
          <View style=
            {{ height: 7, width: 7, borderRadius: 3.5, backgroundColor: this.state.imageIndexSeleted === i ? 'white' : '#53575e', marginRight: 2 }}
            key={i}
          />
        )
      })
    )
    else return <View />
  }

  render() {
    return(
      <View style={{ alignItems: 'center', marginBottom: 16 }}>
        <ScrollView horizontal={true} pagingEnabled={true} onMomentumScrollEnd={this._onScrolledToBanner}>
          {this.props.images.map((image, index) => {
            return (
              <TouchableOpacity
                onPress={() => this.props.imageModal(this.props.images, index)}
                key={index}
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  width: width,
                  height: width
                }}
              >
                <CachedImage
                  source={{ uri: image.originalUrl ? image.originalUrl : image.mediumUrl }}
                  style={{ flex: 1,
                    width: width,
                    height: width,
                    resizeMode: 'cover'
                  }}
                />
              </TouchableOpacity>
            );
          })}
        </ScrollView>
        <View style={{ position: 'absolute', bottom: 5, flexDirection: 'row' }}>
          {this.renderIndicators()}
        </View>
      </View>
    )
  }
}