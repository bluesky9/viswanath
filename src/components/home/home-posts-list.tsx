import React, { Component } from 'react';
import {
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  FlatList,
  Platform,
  Linking,
  ActivityIndicator
} from 'react-native';
import homeStyles from './home-styles';
import { Card, CardItem, Text, Left, Body, Icon } from 'native-base';
import { NavigationScreenProps } from 'react-navigation';
import ActionSheet from 'react-native-actionsheet';
import Autolink from 'react-native-autolink';
// import Contacts from 'react-native-contacts' For Contacts Fetch
// var Contacts = require('react-native-contacts')

import {urlify} from '../../validator/regx-validator';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import LinkPreview from 'react-native-link-preview';

import { FeedApi, DialogModal, PeopleApi, UniversitiesApi } from '../../store/actions';
import { style } from '../../styles/variables';
import HomeImageIndicators from './home-image-indicators';
import { transformForPosts } from '../../utils';

import {
  getUniversityDetails,
  getUniversityFeeds,
  getUnivesityDepartments
} from '../../store/actions/Universities.js';

import { findIndex, find, isEmpty, get, filter } from 'lodash';
import 'moment-duration-format';

const CANCEL_INDEX = 0;
const userOptions = ['Cancel', 'Edit post', 'Delete post'];
const options = ['Cancel', 'Report post'];
let actionsheet = undefined;
let userActionSheet = undefined;

let postLength = undefined;
let postsUrl = [];

const { height, width } = Dimensions.get('window');

let linkPreviewCount = 0;
let linkPreviewData = 0;

interface ILoginScreenProps extends NavigationScreenProps<{}> {
  paginatedPosts;
  feedApi;
  dialog;
  likes;
  userId;
  peopleApi;
  flatListRefFn;
  getUniversityDetails: Function;
  getUniversityFeeds: Function;
  getUnivesityDepartments: Function;
  universitiesApi;
  userInfo;
}
interface State {
  pageSize;
  selectedPostId;
  refreshing;
  postPreview: any;
}
class HomePostsList extends Component<ILoginScreenProps, State> {
  listRef;
  constructor(props) {
    super(props);
    this.state = {
      pageSize: 20,
      selectedPostId: undefined,
      refreshing: false,
      postPreview: []
    };
    this.getLinkDetails = this.getLinkDetails.bind(this);
  }

  // componentDidMount() {
  //   console.log(Contacts, 'a112')
  //   Contacts.checkPermission((err, permission) => {
  //     if (err) console.log(err)
  //     if (permission === 'undefined') {
  //       Contacts.requestPermission((error, permissions) => {
  //         if (err) console.log(err)
  //         if (permissions === 'authorized') {
  //           Contacts.getAll((errorObj, contacts) => {
  //             console.log(errorObj, contacts, 'Caa12')
  //           })
  //         }
  //       })
  //     }
  //     if (permission === 'authorized') {
  //       Contacts.getAll((errorObj, contacts) => {
  //         console.log(errorObj, contacts, 'Caa12')
  //       })
  //     }
  //     if (permission === 'denied') {
  //       console.log(permission, '123')
  //     }
  //   })
  // }

  componentWillReceiveProps(nextProps) {
    if (nextProps.paginatedPosts.length !== postLength) {
      this.setState({ refreshing: false,  postPreview: []})
      if (nextProps.paginatedPosts.length === postLength + 1) {
        this.listRef.scrollToIndex({index: 0, animated: true})
      }
    }
  }

  getLinkDetails(message, post, postIndex, setState = false) {
    linkPreviewCount++;
    let regex = /(http|https|Https|Http):\/\/([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?/
    if (regex.test(message)) {
      let url = message.match(regex)[0];
      LinkPreview.getPreview(url)
      .then(data => {
        post.preview = data;
        this.state.postPreview[postIndex] = data;
        if (setState) {
          this.setState({
            postPreview: this.state.postPreview
          })
        }
      })
      .catch((error) => {
        linkPreviewData++;
        console.log(error)
        if (linkPreviewCount === linkPreviewData) {
          this.forceUpdate();
        }
      });
    }
  }

  showActionSheet(userPost, postId) {
    this.setState({ selectedPostId: postId });
    if (userPost) userActionSheet.show();
    else actionsheet.show();
  }

  onDeletePost() {
    this.props.feedApi.deletePost(this.state.selectedPostId);
  }

  _handleUserActionSheetPressed = index => {
    if (index === 2) {
      this.props.dialog.dialogShows(
        'Delete post',
        'Do you want to delete the post',
        'action',
        true,
        () => this.onDeletePost()
      );
    }
    if (index === 1) {
      let postIndex = findIndex(this.props.paginatedPosts, paginatedPost => paginatedPost.postid === this.state.selectedPostId)
      let post = this.props.paginatedPosts[postIndex]
      if (post.department) {
        this.props.universitiesApi.getDepartmentDetailsForEditPost(post.university.univ_id, post.department.deptid, this.state.selectedPostId)
      } else if (post.university) {
        this.props.universitiesApi.getUniversityDetailsForEditPost(post.university.univ_id, this.state.selectedPostId)
      } else {
        this.props.navigation.navigate('EditPost', {
          postId: this.state.selectedPostId
        });
      }
      // this.props.navigation.navigate(universityPost, {
      //   postId: this.state.selectedPostId
      // });
    }
  };

  onReportPost = reason => {
    let data = {
      postId: this.state.selectedPostId,
      reason: reason
    };
    this.props.feedApi.reportPost(data);
  };

  _handleActionSheetPressed = index => {
    if (index === 1) {
      this.props.dialog.dialogShows(
        undefined,
        undefined,
        'report',
        true,
        reason => this.onReportPost(reason)
      );
    }
  };

  onEndReached = () => {
    if (this.state.pageSize <= this.props.paginatedPosts.length) {
      this.props.feedApi.getPosts(1, this.state.pageSize + 20, 0);
      this.setState({ pageSize: this.state.pageSize + 20 });
    }
  };

  showImageModal = (images, index) => {
     this.props.dialog.dialogShows(
        'images',
        undefined,
        'image',
        true,
        undefined,
        undefined,
        undefined,
        undefined,
        images,
        index
      );
  };

  renderPosts = (item, postIndex) => {
    let m = transformForPosts(item.postTimeStamp);
    if (postsUrl[postIndex] && postsUrl[postIndex] !== item.postMessages) {
      postsUrl[postIndex] = item.postMessages;
      this.getLinkDetails(item.postMessages, item, postIndex, true);
    } else {
      postsUrl[postIndex] = item.postMessages;
      this.getLinkDetails(item.postMessages, item, postIndex);
    }
    // let publishers = cloneDeep(get(item, 'publishers', []).splice(0, 1))
    let publishers = get(item, 'publishers', [])
    let postUserId = get(item, 'user.id')
    let findUserSharedPost = filter(publishers, (publishObj, index) => index !== 0 && publishObj.id === this.props.userId)
    let userSharesPost = !isEmpty(findUserSharedPost) ? true : false;
    let followingUser = filter(get(this.props, 'userInfo.following', []), followingObj => followingObj.id === postUserId)
    // console.log(isEmpty(followingUser) && item.sharedByName && !userSharesPost, followingUser, item.sharedByName, userSharesPost)
    return (
      <View
        style={{ overflow: 'hidden', width: width }}
        //  onPress={() =>
        //   this.props.navigation.navigate('PostComment', {
        //     postItem: item
        //   })
        // }
      >
        <Card style={{ transform: [{ scaleX: 1 }], marginLeft: 0, marginRight: 0, borderLeftWidth: 0, borderRightWidth: 0 }}>
          { isEmpty(followingUser) && item.sharedByName && !userSharesPost &&
            <View style={{ flexDirection: 'row', paddingTop: 10, paddingHorizontal: 15, alignItems: 'center' }}>
              <View>
                <Icon name='md-share' style={{ fontSize: 14, color: '#adb5c4' }} />
              </View>
              <View style={{ flexDirection: 'row', flexWrap: 'wrap'}}>
                <View style={{ maxWidth: width - 90}}>
                  <Text
                    style={{ paddingLeft: 9, fontFamily: style.font.family.rubikRegular, fontSize: 10, color: '#adb5c4' }}
                    numberOfLines={1}>
                    {item.sharedByName}
                  </Text>
                </View>
                <Text
                  style={{ fontFamily: style.font.family.rubikRegular, fontSize: 10, color: '#adb5c4' }}>
                  {' ' + 're-posted'}
                </Text>
              </View>
            </View>
          }
          <CardItem
            style={{paddingTop: 16, paddingBottom: 0}}
          >
            {item.title ?
              <Text style={homeStyles.title}>{item.title}</Text> : undefined
            }
          </CardItem>
          <CardItem
            // style={{ paddingTop: 10 }}
            button
            onPress={() =>
              this.props.navigation.navigate('PostComment', {
                postItem: item
              })
            }
          >
            <Left>
              <TouchableOpacity
                onPress={() => {
                  if (item.user.id === this.props.userId)
                    this.props.navigation.navigate('MyProfile');
                  else
                    this.props.peopleApi.getPeopleInfo(item.user.id);
                }}
              >
                <Image
                  source={
                    item.user.profPicPath
                      ? { uri: item.user.profPicPath }
                      : require('../../../assets/images/user_dummy_image.png')
                  }
                  style={[homeStyles.personThumbnail, { marginHorizontal: 0 }]}
                />
              </TouchableOpacity>
              <Body style={{marginLeft: 5}}>
                <Text
                  style={[homeStyles.newPostItemLow,
                  { marginVertical: 0, fontSize: 13, lineHeight: 15 }]}
                >
                  <Text
                    onPress={() => {
                      if (item.user.id === this.props.userId)
                        this.props.navigation.navigate('MyProfile');
                      else
                        this.props.peopleApi.getPeopleInfo(item.user.id);
                    }}
                    style={[homeStyles.newPostItemLow, { marginVertical: 0 }]}
                  >
                    {item.user.name}
                  </Text>
                  <Text
                    onPress={() => {
                      if (item.university) {
                        this.props.getUniversityDetails(item.university.univ_id);
                        this.props.getUniversityFeeds(item.university.univ_id, 20, 0);
                        this.props.getUnivesityDepartments(item.university.univ_id, 20, 0);
                        this.props.navigation.navigate('UniversityProfile');
                      }
                    }}
                    style={[homeStyles.newPostItemLow, { marginVertical: 0 }]}
                  >
                    {item.university && (' • ' + (item.department ? item.university.abbr : item.university.name))}
                    {item.department && ' - ' + item.department.deptAbbr}
                  </Text>
                </Text>
                <Text note style={homeStyles.postNoteText}>
                  {m}
                </Text>
              </Body>
            </Left>
          </CardItem>
          <CardItem
            button
            onPress={() =>
              this.props.navigation.navigate('PostComment', {
                postItem: item
              })
            }
            style={{paddingTop: 8, paddingLeft: 15.4}}
          >
            <Text
              style={[
                homeStyles.postMessage,
                { marginBottom: 6, alignSelf: 'center', letterSpacing: 0.2 }
              ]}
            >
              { item.postMessages &&
                <Autolink text={item.postMessages}/>
              }
              {/* {item.postMessages} */}
            </Text>
          </CardItem>
          {this.state.postPreview[postIndex] &&
            <TouchableOpacity
              style={homeStyles.linkContainer}
              onPress={() => Linking.openURL(urlify(item.postMessages))
                .catch(err => console.error('An error occurred', err))}
            >
              <View style={{height: 60, width: 60, overflow: 'hidden', marginRight: 15, alignItems: 'center'}}>
                {this.state.postPreview[postIndex].images.length > 0 && <Image source={ {uri: this.state.postPreview[postIndex].images[0]} }
                style={homeStyles.linkImage}/>}
              </View>
              <View style={{ flex: 1, flexWrap: 'wrap' }}>
                <Text style={homeStyles.linkContainerTitle}>{get(this.state.postPreview[postIndex], 'title', 'NA')}</Text>
                <Text style={homeStyles.linkContainerDescription} numberOfLines={2}>
                  {get(this.state.postPreview[postIndex], 'description', 'NA')}
                </Text>
              </View>
            </TouchableOpacity>
          }
          {item.files &&
            item.files.length > 0 && (
              <HomeImageIndicators
                images={item.files}
                imageModal={this.showImageModal}
              />
            )}
          <CardItem style={{ justifyContent: 'space-between', paddingTop: 5, paddingBottom: 21 }}>
            <View style={{ flexDirection: 'row' }}>
              <TouchableOpacity
                onPress={() => {
                  this.props.feedApi.postLike(item.postid);
                }}
              >
                {item.likes.length > 0 &&
                find(
                  item.likes,
                  likedObj => likedObj.user.id === this.props.userId
                ) ? (
                    <Icon name='md-heart' style={{ color: 'rgb(255, 97, 116)', fontSize: 20, width: 20 }} />
                ) : (
                    <Icon name='md-heart-outline' style={{ color: 'rgb(207, 208, 220)', fontSize: 20, width: 20 }} />
                )}
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate('PostLikes', {
                    likes: item.likes
                  })
                }
              >
                {item.likes.length > 0 &&
                  find(
                    item.likes,
                    likedObj => likedObj.user.id === this.props.userId
                  ) ? (
                    <Text style={homeStyles.postBarLikesText}>
                      {item.likes.length > 0 && item.likes.length} Likes
                    </Text>
                  ) : (
                    <Text style={homeStyles.postBarLikesTextDefault}>
                      {item.likes.length > 0 && item.likes.length} Likes
                    </Text>
                  )
                }
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              style={{ flexDirection: 'row' }}
              onPress={() =>
                this.props.navigation.navigate('PostComment', {
                  postItem: item
                })
              }
            >
              <Icon name='md-text' style={{ color: 'rgb(207, 208, 220)', fontSize: 20, width: 20 }} />
              <Text style={homeStyles.postBarComText}>
                {item.comment.length > 0 && item.comment.length} Comments
              </Text>
            </TouchableOpacity>
            <View style={{ flexDirection: 'row' }}>
              <TouchableOpacity
                onPress={() => {
                  userSharesPost ? this.props.feedApi.unsharePost(item.postid) : this.props.feedApi.sharePost(item.postid)
                }}
              >
                { userSharesPost ?
                  <Icon name='md-share' style={{ color: '#2E7CE2', fontSize: 20, width: 20 }} />
                  : <Icon name='md-share' style={{ color: 'rgb(207, 208, 220)', fontSize: 20, width: 20 }} />
                }
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('PostShare', { share: publishers })}
              >
                <Text style={homeStyles.postBarComText}>
                  {!isEmpty(item.publishers) && item.publishers.length > 1 && item.publishers.length - 1 } Share
                </Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                height: '100%',
                alignItems: 'center'
              }}
              onPress={() =>
                this.showActionSheet(
                  item.user.id === this.props.userId ? true : false,
                  item.postid
                )
              }
            >
              <Icon name='ios-more' style={{ color: 'rgb(207, 208, 220)', width: 20 }} />
            </TouchableOpacity>
          </CardItem>
        </Card>
      </View>
    );
  };

  render() {
    if (this.props.paginatedPosts) {
      postLength = this.props.paginatedPosts.length
    }
    return (
      <View
        style={{
          height: 6 * height / 7.5,
          width: width,
          backgroundColor: 'rgb(250,251,252)',
          paddingBottom: 60
        }}
      >
        <ActionSheet
          ref={o => (userActionSheet = o)}
          options={userOptions}
          cancelButtonIndex={CANCEL_INDEX}
          destructiveButtonIndex={2}
          onPress={i => this._handleUserActionSheetPressed(i)}
        />
        {Platform.OS === 'ios' ? (
          <View style={{ alignSelf: 'stretch', backgroundColor: 'white' }} />
        ) : (
          undefined
        )}
        <ActionSheet
          ref={o => (actionsheet = o)}
          options={options}
          cancelButtonIndex={CANCEL_INDEX}
          destructiveButtonIndex={1}
          onPress={i => this._handleActionSheetPressed(i)}
        />
        {Platform.OS === 'ios' ? (
          <View style={{ alignSelf: 'stretch', backgroundColor: 'white' }} />
        ) : (
          undefined
        )}
        <FlatList
          ref={ref => {
            this.listRef = ref;
            if (!isEmpty(ref)) {
              this.props.flatListRefFn(ref);
            }
          }}
          data={this.props.paginatedPosts}
          renderItem={obj => this.renderPosts(obj.item, obj.index)}
          keyExtractor={(item, index) => item.postid.toString() + index}
          showsVerticalScrollIndicator={false}
          refreshing={this.state.refreshing}
          onRefresh={() => this.props.feedApi.getPosts(1, 20, 0)}
          onEndReached={() => this.onEndReached()}
          onEndReachedThreshold={0}
          extraData={this.props}
          ListFooterComponent={() => {
            if (this.state.pageSize <= this.props.paginatedPosts.length)
              return (
                <View style={{ paddingTop: 20 }}>
                  <ActivityIndicator
                    size={'large'}
                    color={style.color.primary}
                  />
                </View>
              );
            else return <View />;
          }}
        />
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    paginatedPosts: state.paginatedPosts.posts,
    // userInfo: state.userInfoAPI.userInfo,
    userId: state.userInfoAPI.userId,
    userInfo: state.userInfoAPI.userInfo
  };
};

const mapDispatchToProps = dispatch => ({
  feedApi: bindActionCreators(FeedApi, dispatch),
  peopleApi: bindActionCreators(PeopleApi, dispatch),
  dialog: bindActionCreators(DialogModal, dispatch),
  getUniversityDetails: bindActionCreators(getUniversityDetails, dispatch),
  getUniversityFeeds: bindActionCreators(getUniversityFeeds, dispatch),
  getUnivesityDepartments: bindActionCreators(getUnivesityDepartments, dispatch),
  universitiesApi: bindActionCreators(UniversitiesApi, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(HomePostsList);
