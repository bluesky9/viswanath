import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  TextInput,
  Dimensions,
  Platform,
  ScrollView,
  Image,
  Switch
} from 'react-native';
import homeStyles from './home-styles';
import ImagePicker from 'react-native-image-crop-picker';
import { Icon } from 'native-base';
import { NavigationScreenProps } from 'react-navigation';
import MSAHeader from '../layouts/header/header';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { style } from '../../styles/variables';
import { publishPost, setPostAudience, setTaggedUsers, uploadFile } from '../../store/actions/Feed.js';
import { bindActionCreators } from 'redux';
import { constants } from '../../constants'
import { get, findIndex, isEmpty, map } from 'lodash';

import { DialogModal } from '../../store/actions'

import { connect } from 'react-redux';

const { width } = Dimensions.get('window');
let MESSAGE = 'If you turn this option ON the posts will be displayed to people other than your connections/Followers'
const IMAGE_UPLOAD_LIMIT = 5;

interface ILoginScreenProps extends NavigationScreenProps<{}> {
  postAudience: any;
  postTaggedUsers: any;
  actions: any;
  setTaggedUsers: any;
  uploadFile: any
}
interface State {
  imagesArray: any;
  title: string;
  message: string;
  publicPost: boolean;
}

class HomePostCompose extends Component<ILoginScreenProps, State> {
  constructor(props) {
    super(props);
    this.state = {
      imagesArray: [],
      title: '',
      message: '',
      publicPost: false
    };
  }

  componentDidMount() {
    this.props.actions.setPostAudience([]);
    this.props.actions.setTaggedUsers([]);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.navigation.state.params) {
      let imageIndex = findIndex(this.state.imagesArray, image => image.path === nextProps.navigation.state.params.image.path)
      if (imageIndex === -1) {
        let imagesArray = this.state.imagesArray;
        imagesArray.push(nextProps.navigation.state.params.image)
        this.setState({ imagesArray })
      }
    }
  }

  publishPost() {

    let errorMsg = [];
    let checkPost = this.state.title !== '' && (this.state.message !== '' || this.state.imagesArray.length > 0)
    let imagesArray = this.state.imagesArray

    if (this.state.title === '') errorMsg.push(`Title: required \n`)
    if (this.state.message === '') errorMsg.push(`Post: required`)

    // For Android specifically
    if (imagesArray.length > IMAGE_UPLOAD_LIMIT) {
      let diff = IMAGE_UPLOAD_LIMIT - imagesArray.length
      imagesArray.splice(5, diff)
    }

    if (!checkPost) {
      this.props.actions.dialogModal(
        'Please check and fill in the required fields', errorMsg, 'inform', true, undefined, undefined, 'Please try again', 'Refresh'
      )
    } else {
      let taggedUserIds = [];
      this.props.postTaggedUsers.userArray.map(
        (item) => {
          taggedUserIds.push(item.id)
        }
      )

      let publishPostObj = {
        userTypes: this.props.postAudience.userArray,
        title: this.state.title,
        message: this.state.message,
        postExpiry: 'MONTH',
        restricted: !this.state.publicPost,
        taggedUserIds
      }

      if (imagesArray.length > 0) {
        this.props.actions.uploadFile(imagesArray, publishPostObj)
      } else {
        this.props.actions.publishPost(publishPostObj);
      }
    }
    // this.props.navigation.goBack();
  }

  openPicker() {
    console.log('123')
    if (this.state.imagesArray.length < IMAGE_UPLOAD_LIMIT)
      ImagePicker.openPicker({
        cropping: false,
        multiple: true,
        compressImageQuality: Platform.OS === 'ios' ? 0 : 0.5,
        maxFiles: IMAGE_UPLOAD_LIMIT - this.state.imagesArray.length
      })
        .then(images => {
          let arr = this.state.imagesArray;
          map(images, (image) => {
            let imageObj = {
              uri: get(image, 'path', ''),
              name: get(image, Platform.OS === 'ios' ? 'creationDate' : 'modificationDate', ''),
              type: get(image, 'mime', '')
            }
            if (arr.length < IMAGE_UPLOAD_LIMIT) {
              arr.push(imageObj);
            }
          })
          this.setState({
            imagesArray: arr
          });
        })
        .catch(Error);
  }
  openCamera() {
    if (this.state.imagesArray.length < IMAGE_UPLOAD_LIMIT)
      ImagePicker.openCamera({
        cropping: false,
        compressImageQuality: Platform.OS === 'ios' ? 0 : 0.5
      })
        .then(image => {
          let arr = this.state.imagesArray;
          let imageObj = {
            uri: get(image, 'path', ''),
            name: get(image, 'size', 123).toString(),
            type: get(image, 'mime', '')
          }
          arr.push(imageObj);
          this.setState({
            imagesArray: arr
          });
        })
        .catch((e) => {
          console.log(e, 'Error')
        });
  }
  render() {
    let checkPost = this.state.title !== '' && (this.state.message !== '' || this.state.imagesArray.length > 0)
    // console.log(checkPost, '112')
    return (
      <View style={{ flex: 1 }}>
        <MSAHeader
          title={'New post'}
          leftText={'CANCEL'}
          rightText={'PUBLISH'}
          onPress={() => this.props.navigation.goBack()}
          onRightTextPress={() => this.publishPost()}
          disableRightText={!checkPost}
        />
        <KeyboardAwareScrollView>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('PostAudience')}
            style={[homeStyles.newPostItemWrap]}
          >
            <View>
              <Text style={[homeStyles.newPostItemHead]}>
                AUDIENCE
              </Text>
              <Text style={{
                width: width - 30, marginTop: 11, marginBottom: 0,
                fontSize: 12, lineHeight: 14
              }}>
                {this.props.postAudience.userArray && this.props.postAudience.userArray.length > 0 ?
                  this.props.postAudience.userArray.map(
                    (item, index) => {
                      return (
                        <Text key={index} style={homeStyles.newPostItemLow}>
                          <Text>{constants.USERTYPES.PLURAL[item]}</Text>
                          {(this.props.postAudience.userArray.length - 1) !== index &&
                            <Text>{', '}</Text>
                          }
                        </Text>
                      );
                    }
                  )
                  : <Text style={homeStyles.newPostItemLow}>
                    Everybody
                </Text>
                }
              </Text>
            </View>
            <View
              style={{
                position: 'absolute',
                alignSelf: 'center',
                left: width - 25
              }}
            >
              <Icon
                style={{ fontSize: 20, color: 'rgb(197,207,219)' }}
                name={'arrow-forward'}
              />
            </View>
          </TouchableOpacity>
          <View
            style={[
              homeStyles.newPostItemWrap,
              { justifyContent: 'flex-start', paddingHorizontal: 15 }
            ]}
          >
            <Text
              style={[
                homeStyles.newPostItemHead,
                { marginBottom: 4 }
              ]}
            >
              TITLE
            </Text>
            <TextInput
              onChangeText={txt =>
                this.setState({
                  title: txt.trim()
                })
              }
              style={[homeStyles.newPostInput, { marginBottom: 10 }]}
              placeholderTextColor={style.color.dustyGrey}
              placeholder={'Title is optional'}
              underlineColorAndroid={'rgba(0,0,0,0)'}
              maxLength={40}
            />
            <Text
              style={[
                homeStyles.newPostItemHead,
                { paddingBottom: 6, paddingTop: 0, marginTop: 0 }
              ]}
            >
              POST
            </Text>
            <TextInput
              onChangeText={txt =>
                this.setState({
                  message: txt.trim()
                })
              }
              multiline={true}
              numberOfLines={5}
              style={[homeStyles.newPostInput, { marginBottom: 0, paddingTop: 12, height: undefined, textAlignVertical: 'top' }]}
              placeholderTextColor={style.color.dustyGrey}
              placeholder={'Type your message..'}
              underlineColorAndroid={'rgba(0,0,0,0)'}
            />
          </View>
          <View style={{ alignSelf: 'stretch' }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('TagPeople')}
              style={homeStyles.newPostItemWrap}
            >
              <Text style={[homeStyles.newPostItemHead]}>
                TAG CONNECTIONS
              </Text>
              {!isEmpty(this.props.postTaggedUsers.userArray) &&
                <View
                  style={{ flexDirection: 'row' }}
                >
                  <Text style={{ width: width / 1.4, marginTop: 11 }}>
                    {this.props.postTaggedUsers.userArray.map(
                      (item, index) => {
                        return (
                          <Text key={index} style={[homeStyles.newPostItemLow]}>
                            {item.name}
                            {this.props.postTaggedUsers.userArray.length - 1 === index ? '' : ',  '}
                          </Text>
                        );
                      }
                    )}
                  </Text>
                </View>
              }
              <View
                style={{
                  position: 'absolute',
                  alignSelf: 'center',
                  left: width - 25
                }}
              >
                <Icon
                  style={{ fontSize: 20, color: 'rgb(197,207,219)' }}
                  name={'arrow-forward'}
                />
              </View>
            </TouchableOpacity>
            <View
              // onPress={() => this.openPicker()}
              style={[homeStyles.newPostItemWrap, { flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingRight: 15 }]}
            >
              <TouchableOpacity
                onPress={() => this.openPicker()}
                style={{ flex: 1 }}
              >
                <Text style={homeStyles.newPostItemHead}>ADD IMAGES</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.openCamera()}
              >
                <Icon
                  style={{ fontSize: Platform.OS === 'ios' ? 25 : 20, lineHeight: 20, color: 'rgb(197,207,219)' }}
                  name={'camera'}
                  active
                />
              </TouchableOpacity>
            </View>
            {!isEmpty(this.state.imagesArray) &&
              <View
                style={[
                  homeStyles.newPostItemWrap,
                  {
                    height: this.state.imagesArray.length === 0 ? 0 : 120,
                    marginTop: this.state.imagesArray.length === 0 ? 0 : -2,
                    paddingTop: 10, paddingLeft: 10, paddingRight: 10
                  }
                ]}
              >
                <ScrollView
                  horizontal={true}
                  contentContainerStyle={{ alignItems: 'center' }}
                >
                  {this.state.imagesArray.map((item, index) => {
                    return (
                      <View
                        key={index}
                        style={{
                          height: 90,
                          width: 90,
                          marginHorizontal: 5,
                          borderRadius: 3
                        }}
                      >
                        <Image
                          source={{
                            // uri: Platform.OS === 'ios' ? item.uri : item.path // Check For android
                            uri: Platform.OS === 'ios' ?
                              'file://' + item.uri : item.uri
                          }}
                          style={{ height: 90, width: 90 }}
                        />
                        <TouchableOpacity
                          onPress={() => {
                            let arr = this.state.imagesArray;
                            arr.splice(index, 1);
                            this.setState({ imagesArray: arr });
                          }}
                          style={{
                            position: 'absolute',
                            top: 0,
                            right: 0,
                            padding: 3,
                            margin: 5,
                            borderRadius: 3,
                            backgroundColor: 'rgba(0,0,0,0.5)'
                          }}
                        >
                          <Image
                            style={{ tintColor: 'white' }}
                            source={require('../../../assets/app_icons/Icons/Close-dark.png')}
                          />
                        </TouchableOpacity>
                      </View>
                    );
                  })}
                </ScrollView>
              </View>
            }
            <View style={{ backgroundColor: 'white', padding: 15 }}>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Text
                  style={{
                    flex: 1,
                    fontFamily: style.font.family.rubikMedium,
                    color: style.color.coal,
                    fontSize: 14,
                    lineHeight: 17
                  }}
                >
                  Public post
                </Text>
                <Switch value={this.state.publicPost}
                  onValueChange={(bool) => this.setState({ publicPost: bool })}
                  thumbTintColor={style.color.primary}
                  onTintColor={style.color.cream}
                  style={{
                    transform: [{ scaleX: Platform.OS === 'ios' ? .7 : 1 },
                    { scaleY: Platform.OS === 'ios' ? .7 : 1 }]
                  }}
                />
              </View>
              <Text style={{ fontSize: 14, lineHeight: 17, color: style.color.dustyGrey, paddingTop: 8 }}>{MESSAGE}</Text>
            </View>
          </View>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const mapStateToProps = ({ postTaggedUsers, postAudience }) => {
  return {
    postTaggedUsers,
    postAudience
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      publishPost: publishPost,
      setPostAudience: setPostAudience,
      setTaggedUsers: setTaggedUsers,
      uploadFile: uploadFile,
      dialogModal: DialogModal.dialogShows
    },
    dispatch
  )
});

export default connect(mapStateToProps, mapDispatchToProps)(HomePostCompose);
