import React, { Component } from 'react';
import { View, Image, TouchableOpacity, Platform, TextInput, Animated, Keyboard, Linking, Dimensions } from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
import { Card, CardItem, Left, Body, Text, Icon } from 'native-base';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import homeStyles from './home-styles';
import AppHeader from '../layouts/header/header';
import ActionSheet from 'react-native-actionsheet';
import HomeImageIndicators from './home-image-indicators';
import Autolink from 'react-native-autolink';
import LinkPreview from 'react-native-link-preview';
import {urlify} from '../../validator/regx-validator';

import { style } from '../../styles/variables';
import { transformForPosts } from '../../utils';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { FeedApi, DialogModal, PeopleApi, UniversitiesApi }  from '../../store/actions';

import {
  getUniversityDetails,
  getUniversityFeeds,
  getUnivesityDepartments
} from '../../store/actions/Universities.js';

import { isEmpty, find, get, filter } from 'lodash';

const { width } = Dimensions.get('window');

const CANCEL_INDEX = 0;
const userOptions = ['Cancel', 'Edit post', 'Delete post'];
const commentOptions = ['Cancel', 'Edit comment', 'Delete comment'];
const options = ['Cancel', 'Report post'];
let actionsheet = undefined;
let userActionSheet = undefined;
let commentActionSheet = undefined;

let previousUrl = '';

interface ILoginScreenProps extends NavigationScreenProps<{}> {
  navigation;
  feedApi;
  postWithComment;
  dialog;
  userId;
  peopleApi;
  // postComment;
  // getPosts;
  getUniversityDetails;
  getUnivesityDepartments;
  getUniversityFeeds;
  universitiesApi;
}
interface State {
  comment: string;
  actionSheetOptions;
  selectedComment;
  editComment;
  keyboardHeight;
  postPreview: any;
}

class HomeFeedsEmptyState extends Component<ILoginScreenProps, State> {
  keyboardShowListener;
  keyboardHideListener;
  scrollView: any;
  constructor(props) {
    super(props);
    this.state = {
      comment: undefined,
      actionSheetOptions: userOptions,
      selectedComment: undefined,
      editComment: false,
      keyboardHeight: new Animated.Value(0),
      postPreview: undefined
    };
    this.getLinkDetails = this.getLinkDetails.bind(this);
  }

  // componentWillReceiveProps(nextProps) {
  //   this.scrollView.scrollToEnd(); // Have to enhance ScrollToEnd When New Comment is Added
  // }

  componentDidMount() {
    this.keyboardShowListener = Keyboard.addListener(
      'keyboardWillShow',
      this.keyboardWillShow
    );
    this.keyboardHideListener = Keyboard.addListener(
      'keyboardWillHide',
      this.keyboardWillHide
    );
  }

  getLinkDetails(message) {
    let regex = /(http|https|Https|Http):\/\/([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?/
    if (regex.test(message)) {
      let url = message.match(regex)[0];
      if (url !== previousUrl) {
        previousUrl = url;
        LinkPreview.getPreview(url)
        .then(data => {
          this.setState({
            postPreview: data
          })
        })
        .catch((error) => {
          console.log(error)
        });
      }
    }
  }

  keyboardWillShow = e => {
    let keyboardHeight = e.endCoordinates.height;
    let animateDuration = e.duration;
    if (this.scrollView) this.scrollView.scrollToEnd()
    Animated.timing(this.state.keyboardHeight, {
      toValue: keyboardHeight,
      duration: animateDuration
    }).start();
  };

  keyboardWillHide = e => {
    let animateDuration = e.duration;
    Animated.timing(this.state.keyboardHeight, {
      toValue: 0,
      duration: animateDuration
    }).start();
  };

  componentWillUnmount() {
    this.keyboardShowListener.remove();
    this.keyboardHideListener.remove();
    previousUrl = '';
  }

  showActionSheet(userPost) {
    if (userPost) userActionSheet.show();
    else actionsheet.show();
  }

  handleTextChange = text => {
    this.setState({ comment: text });
  };

  onDeletePost() {
    this.props.navigation.goBack();
    this.props.feedApi.deletePost(this.props.postWithComment.postid);
    // this.props.feedApi.getPosts(1, 20, 0);
  }

  _handleUserActionSheetPressed = index => {
    if (index === 2) {
      this.props.dialog.dialogShows(
        'Delete post',
        'Do you want to delete the post',
        'action',
        true,
        () => this.onDeletePost()
      );
    }
    if (index === 1) {
      if (this.props.postWithComment.department) {
        this.props.universitiesApi.getDepartmentDetailsForEditPost(
          this.props.postWithComment.university.univ_id, this.props.postWithComment.department.deptid, this.props.postWithComment.postid
        )
      } else if (this.props.postWithComment.university) {
        this.props.universitiesApi.getUniversityDetailsForEditPost(
          this.props.postWithComment.university.univ_id, this.props.postWithComment.postid
        )
      } else {
        this.props.navigation.navigate('EditPost', {
          postId: this.props.postWithComment.postid
        });
      }
      // let universityPost = (this.props.postWithComment.department || this.props.postWithComment.university) ?
      //                     'UniversityPostEdit' : 'EditPost'
      // this.props.navigation.navigate(universityPost, {
      //   postId: this.props.postWithComment.postid
      // });
    }
  };

  onSubmitComment = postId => {
    Keyboard.dismiss();
    if (!isEmpty(this.state.comment)) {
      let data = {
        postId: postId,
        comment: this.state.comment
      };
      this.props.feedApi.postComment(data).then(() => {
        setTimeout(() => {
          if (this.scrollView) this.scrollView.scrollToEnd()
        }, 500)
      });
      this.setState({ comment: undefined });
    }
  };

  onEditComment = () => {
    Keyboard.dismiss();
    if (!isEmpty(this.state.comment)) {
      let data = {
        id: this.state.selectedComment,
        comments: this.state.comment
      };
      this.props.feedApi.updateComment(this.props.postWithComment.postid, data);
      this.setState({ comment: undefined, editComment: false });
    }
  };

  onDeleteComment = () => {
    this.props.feedApi.deleteComment(
      this.props.postWithComment.postid,
      this.state.selectedComment
    );
    this.setState({ comment: undefined });
  };

  handleOnLongPress = id => {
    this.setState({ selectedComment: id });
    commentActionSheet.show();
  };

  _handleCommentActionSheet = i => {
    let oldComment = find(
      this.props.postWithComment.comment,
      comment => comment.id === this.state.selectedComment
    );
    if (i === 2) {
      this.props.dialog.dialogShows(
        'Delete comment',
        'Do you want to delete the comment',
        'action',
        true,
        () => this.onDeleteComment()
      );
    }
    if (i === 1) {
      this.setState({ comment: oldComment.comments, editComment: true });
    }
  };

  onReportPost = reason => {
    let data = {
      postId: this.props.postWithComment.postid,
      reason: reason
    }
    this.props.feedApi.reportPost(data, true);
  }

  _handleActionSheetPressed = index => {
    if (index === 1) {
      this.props.dialog.dialogShows(
        undefined,
        undefined,
        'report',
        true,
        reason => this.onReportPost(reason)
      );
      // this.props.feedApi.getPosts(1, 20, 0);
    }
  };

  showImageModal = (images, index) => {
    this.props.dialog.dialogShows(
      'images',
      undefined,
      'image',
      true,
      undefined,
      undefined,
      undefined,
      undefined,
      images,
      index
    );
  };

  renderTextInput = () => {
    return (
      <TextInput
        style={{
          backgroundColor: 'white',
          width: '100%',
          height: '100%',
          paddingLeft: 10,
          fontSize: 14,
          lineHeight: 17,
          fontFamily: style.font.family.rubikRegular,
          // opacity: this.state.editComment ? 0.5 : 1,
          color: 'rgb(59,62,68)'
        }}
        placeholderTextColor={'rgba(108,111,125,0.5)'}
        placeholder={'Tap to write a comment'}
        onChangeText={text => this.handleTextChange(text)}
        value={this.state.comment}
        spellCheck={false}
        autoCorrect={false}
        autoCapitalize={'none'}
      />
    );
  };

  render() {
    console.log(this.props.userId, 'Comments');
    let post = this.props.postWithComment;
    this.getLinkDetails(post.postMessages);
    let m = transformForPosts(get(post, 'postTimeStamp', 0));
    let publishers = get(post, 'publishers', [])
    let postUserId = get(post, 'user.id')
    let findUserSharedPost = filter(publishers, (publishObj, index) => index !== 0 && publishObj.id === this.props.userId)
    let userSharesPost = !isEmpty(findUserSharedPost) ? true : false;
    let followingUser = filter(get(this.props, 'userInfo.following', []), followingObj => followingObj.id === postUserId)
    console.log(publishers, postUserId, findUserSharedPost, userSharesPost, followingUser, 'Yo')
    if (post)
      return <View style={homeStyles.fullWidthContainer}>
          <View>
            <AppHeader title={post.user.firstName + `'s` + ' post'} onPress={() => this.props.navigation.goBack()} />
          </View>
          <ActionSheet ref={o =>
            (userActionSheet = o)}
            options={userOptions} cancelButtonIndex={CANCEL_INDEX} destructiveButtonIndex={2} onPress={i => this._handleUserActionSheetPressed(i)} />
          {Platform.OS === 'ios' ? <View style={{ alignSelf: 'stretch', backgroundColor: 'white' }} /> : undefined}
          <ActionSheet ref={o =>
            (commentActionSheet = o)}
            options={commentOptions} cancelButtonIndex={CANCEL_INDEX} destructiveButtonIndex={2} onPress={i => this._handleCommentActionSheet(i)} />
          {Platform.OS === 'ios' ? <View style={{ alignSelf: 'stretch', backgroundColor: 'white' }} /> : undefined}
          <ActionSheet ref={o =>
            (actionsheet = o)}
            options={options} cancelButtonIndex={CANCEL_INDEX} destructiveButtonIndex={1} onPress={i => this._handleActionSheetPressed(i)} />
          {Platform.OS === 'ios' ? <View style={{ alignSelf: 'stretch', backgroundColor: 'white' }} /> : undefined}
          <View style={{ flex: 1 }}>
            <KeyboardAwareScrollView ref={ref => (this.scrollView = ref)}>
                { isEmpty(followingUser) && post.sharedByName && !userSharesPost &&
                <View style={{ flexDirection: 'row', paddingTop: 10, paddingHorizontal: 15, alignItems: 'center', backgroundColor: 'white' }}>
                  <View>
                    <Icon name='md-share' style={{ fontSize: 14, color: '#adb5c4' }} />
                  </View>
                  <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                    <View style={{ maxWidth: width - 90 }}>
                      <Text
                        style={{ paddingLeft: 9, fontFamily: style.font.family.rubikRegular, fontSize: 10, color: '#adb5c4' }}
                        numberOfLines={1}>
                        {post.sharedByName}
                      </Text>
                    </View>
                    <Text
                      style={{ fontFamily: style.font.family.rubikRegular, fontSize: 10, color: '#adb5c4' }}>
                      {' ' + 're-posted'}
                    </Text>
                  </View>
                </View>
              }
              <View style={{ flex: 1.45, elevation: 3 }}>
                <Card style={{ marginTop: 0, marginBottom: 0, transform: [{ scaleX: 1.02 }], borderTopWidth: 0 }}>
                  <CardItem
                      style={{paddingTop: 16, paddingBottom: 0, borderTopWidth: 0, borderWidth: 0}}
                    >
                    {post.title ?
                      <Text style={homeStyles.title}>{post.title}</Text> : undefined
                    }
                  </CardItem>
                  <CardItem>
                    <Left>
                      <TouchableOpacity onPress={() => {
                          if (post.user.id === this.props.userId)
                          this.props.navigation.navigate('MyProfile');
                          else
                          this.props.peopleApi.getPeopleInfo(post.user.id)}
                        }>
                        <Image
                        source={post.user.profPicPath ?
                        { uri: post.user.profPicPath } :
                        require('../../../assets/images/user_dummy_image.png')} style={[homeStyles.personThumbnail, { marginHorizontal: 0 }]} />
                      </TouchableOpacity>
                      <Body>
                        <Text
                          style={[
                            homeStyles.newPostItemLow,
                            { marginVertical: 0, fontSize: 13, lineHeight: 15 }
                          ]}
                        >
                          <Text
                            onPress={() => {
                              if (post.user.id === this.props.userId)
                                this.props.navigation.navigate('MyProfile');
                              else
                              this.props.peopleApi.getPeopleInfo(post.user.id)}
                            }
                            style={[
                              homeStyles.newPostItemLow,
                              { marginVertical: 0 }
                            ]}
                          >
                            {post.user.name}
                          </Text>
                          <Text
                            style={[
                              homeStyles.newPostItemLow,
                              { marginVertical: 0 }
                            ]}
                            onPress={() => {
                              if (post.university) {
                                this.props.getUniversityDetails(post.university.univ_id);
                                this.props.getUniversityFeeds(post.university.univ_id, 20, 0);
                                this.props.getUnivesityDepartments(post.university.univ_id, 20, 0);
                                this.props.navigation.navigate('UniversityProfile');
                              }
                            }}
                          >
                            {post.university && (' • ' + (post.department ? post.university.abbr : post.university.name))}
                            {post.department && ' - ' + post.department.deptAbbr}
                          </Text>
                        </Text>
                        <Text note style={homeStyles.postNoteText}>
                          {m}
                        </Text>
                      </Body>
                    </Left>
                  </CardItem>
                  <CardItem>
                    <Text
                      style={[
                        homeStyles.postMessage,
                        { marginBottom: 6, alignSelf: 'center' }
                      ]}
                    >
                      <Autolink text={post.postMessages}/>
                    </Text>
                  </CardItem>
                  {this.state.postPreview &&
                    <TouchableOpacity
                      style={homeStyles.linkContainer}
                      onPress={() => Linking.openURL(urlify(post.postMessages))
                        .catch(err => console.error('An error occurred', err))}
                    >
                      <View style={{height: 60, width: 60, overflow: 'hidden', marginRight: 15, alignItems: 'center'}}>
                        {this.state.postPreview.images.length > 0 && <Image source={ {uri: this.state.postPreview.images[0]} }
                        style={homeStyles.linkImage}/>}
                      </View>
                      <View style={{ flex: 1, flexWrap: 'wrap' }}>
                        <Text style={homeStyles.linkContainerTitle}>{get(this.state.postPreview, 'title', 'NA')}</Text>
                        <Text style={homeStyles.linkContainerDescription} numberOfLines={2}>
                          {get(this.state.postPreview, 'description', 'NA')}
                        </Text>
                      </View>
                    </TouchableOpacity>
                  }
                  {post.files && post.files.length > 0 && <HomeImageIndicators images={post.files} imageModal={this.showImageModal} />}
                  <CardItem style={{ justifyContent: 'space-between', paddingTop: 5, paddingBottom: 21 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                      <TouchableOpacity
                        onPress={() =>
                          this.props.feedApi.postLike(post.postid)
                        }
                      >
                        {post.likes && post.likes.length > 0 &&
                        find(
                          post.likes,
                          likedObj =>
                            likedObj.user.id === this.props.userId
                        ) ? (
                          <Icon name='md-heart' style={{ color: 'rgb(255, 97, 116)', fontSize: 20, width: 20 }} />
                        ) : (
                          <Icon name='md-heart-outline' style={{ color: 'rgb(207, 208, 220)', fontSize: 20, width: 20 }} />
                        )}
                      </TouchableOpacity>
                      <TouchableOpacity onPress={() => this.props.navigation.navigate(
                            'PostLikes',
                            {
                              likes: post.likes
                            }
                          )}>
                        <Text style={homeStyles.postBarLikesText}>
                          {post.likes && post.likes.length > 0 && post.likes.length}{' '}
                          Likes
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <TouchableOpacity style={{ flexDirection: 'row' }}>
                      <Icon name='md-text' style={{ color: 'rgb(207, 208, 220)', fontSize: 20, width: 20 }} />
                      <Text
                        style={homeStyles.postBarComText}
                      >
                        {post.comment && post.comment.length > 0 && post.comment.length}{' '}
                        Comments
                      </Text>
                    </TouchableOpacity>
                    <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity
                      onPress={() => {
                        userSharesPost ? this.props.feedApi.unsharePost(post.postid) : this.props.feedApi.sharePost(post.postid)
                      }}
                    >
                      { userSharesPost ?
                        <Icon name='md-share' style={{ color: '#2E7CE2', fontSize: 20, width: 20 }} />
                      :
                        <Icon name='md-share' style={{ color: 'rgb(207, 208, 220)', fontSize: 20, width: 20 }} />
                      }
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => this.props.navigation.navigate('PostShare', { share: publishers })}
                    >
                      <Text style={homeStyles.postBarComText}>
                        {!isEmpty(post.publishers) && post.publishers.length > 1 && post.publishers.length - 1 } Share
                      </Text>
                    </TouchableOpacity>
                  </View>
                    <TouchableOpacity
                    style={{ flexDirection: 'row' }}
                    onPress={() => this.showActionSheet(post.user.id === this.props.userId ? true : false)}>
                      <Icon name='ios-more' style={{ color: 'rgb(207, 208, 220)', width: 20 }} />
                    </TouchableOpacity>
                  </CardItem>
                </Card>
              </View>
              { !isEmpty(post.comment) &&
                <View
                style={[homeStyles.postCommentContainer, { paddingTop: 4, paddingBottom: 19, paddingRight: 15, paddingLeft: 15 }]}
              >
                { post.comment && post.comment.map((item, index) => {
                  let time = transformForPosts(item.commentTimeStamp);
                  return (
                    <TouchableOpacity
                      style={{ flexDirection: 'row', marginTop: 15 }}
                      key={index}
                      disabled={item.user.id !== this.props.userId}
                      onLongPress={() => this.handleOnLongPress(item.id)}
                    >
                      <View
                        style={{
                          // flex: 0.7,
                          justifyContent: 'flex-start',
                          alignItems: 'flex-start'
                        }}
                      >
                        <TouchableOpacity onPress={() => {
                          if (item.user.id === this.props.userId)
                          this.props.navigation.navigate('MyProfile');
                          else
                          this.props.peopleApi.getPeopleInfo(item.user.id)}
                        }>
                          <Image
                            source={item.user.profPicPath ? { uri: item.user.profPicPath } : require('../../../assets/images/user_dummy_image.png') }
                            style={[homeStyles.personThumbnail, {marginLeft: 0, marginRight: 10}]}
                          />
                        </TouchableOpacity>
                      </View>
                      <View style={{ flex: 4, justifyContent: 'flex-start' }}>
                        <Text
                          style={[homeStyles.newPostItemLow,
                          { marginVertical: 0, fontSize: 13, lineHeight: 15 }]}
                          onPress={() => {
                            if (item.user.id === this.props.userId)
                              this.props.navigation.navigate('MyProfile');
                            else
                            this.props.peopleApi.getPeopleInfo(item.user.id)}
                          }>
                          {item.user.name}
                        </Text>
                        <Text style={homeStyles.commentText}><Autolink text={item.comments}/></Text>
                        <Text style={homeStyles.commentTime}>{time}</Text>
                      </View>
                    </TouchableOpacity>
                  );
                })}
              </View>
              }
            </KeyboardAwareScrollView>
          </View>
          <Animated.View
          style={{ height: 54, backgroundColor: 'rgb(250,251,252)', paddingHorizontal: 15, paddingVertical: 10, bottom: this.state.keyboardHeight }}>
            {this.state.editComment ?
              <View style={{ flexDirection: 'row' }}>
                <View
                  style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  flex: 1,
                  marginRight: 15,
                  borderRadius: 5,
                  flexDirection: 'row',
                  backgroundColor: 'white'
                  }}
                >
                  <View style={{ flex: 1}}>
                    {this.renderTextInput()}
                  </View>
                </View>
                <TouchableOpacity
                style={{ alignItems: 'center', justifyContent: 'center' }}
                onPress={() => this.onEditComment()}>
                  <Icon name='md-send' style={{ color: 'rgb(207, 208, 220)', fontSize: 20 }} />
                </TouchableOpacity>
              </View>
              : <View style={{ flexDirection: 'row' }}>
              {/* <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center', height: 20, width: 20}}> */}
              {/* <Image
                source={require('../../../assets/app_icons/Icons/Attachment-grey.png')}
                style={{ height: 20, width: 20 }}
              /> */}
              {/* </TouchableOpacity> */}
              <View style={{
                alignItems: 'center',
                justifyContent: 'center',
                flex: 1,
                marginRight: 15,
                borderRadius: 5,
                flexDirection: 'row',
                backgroundColor: 'white'
              }}>
                <View style={{ flex: 1}}>
                  {this.renderTextInput()}
                </View>
                {/* <TouchableOpacity style={{ paddingRight: 10 }}>
                  <Image
                    source={require('../../../assets/app_icons/Icons/Emoji-default.png')}
                    style={{ height: 20, width: 20 }}
                  />
                </TouchableOpacity> */}
                </View>
                <TouchableOpacity
                style={{ alignItems: 'center', justifyContent: 'center' }} onPress={() => this.onSubmitComment(this.props.postWithComment.postid)}>
                  <Icon name='md-send' style={{ color: 'rgb(207, 208, 220)', fontSize: 20 }} />
                </TouchableOpacity>
              </View>}
          </Animated.View>
        </View>;
    else {
      return <View />;
    }
  }
}

const mapStateToProps = (state, navState) => {
  let navParams = navState.navigation.state.params.postItem
  let paginatedPosts = state.paginatedPosts
  return {
    paginatedPosts : state.paginatedPosts,
    postWithComment: find(paginatedPosts.posts, post => post.postid === navParams.postid) || navParams,
    userId: state.userInfoAPI.userId,
    userInfo: state.userInfoAPI.userInfo
  };
};

const mapDispatchToProps = dispatch => ({
  feedApi: bindActionCreators(FeedApi, dispatch),
  dialog: bindActionCreators(DialogModal, dispatch),
  peopleApi: bindActionCreators(PeopleApi, dispatch),
  getUniversityDetails: bindActionCreators(getUniversityDetails, dispatch),
  getUniversityFeeds: bindActionCreators(getUniversityFeeds, dispatch),
  getUnivesityDepartments: bindActionCreators(getUnivesityDepartments, dispatch),
  universitiesApi: bindActionCreators(UniversitiesApi, dispatch)
  // getPosts: bindActionCreators(getPosts, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeFeedsEmptyState);