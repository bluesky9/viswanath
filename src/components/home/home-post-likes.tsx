import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native';
import AppHeader from '../layouts/header/header';
import { NavigationScreenProps } from 'react-navigation';
import { PeopleApi } from '../../store/actions'
import homeStyles from './home-styles';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

interface ILoginScreenProps extends NavigationScreenProps<{}> {
  navigation;
  peopleApi;
}
class HomePostLikes extends Component<ILoginScreenProps> {
  render() {
    // console.log(this.props.navigation.state.params, 'Likes')
    let likeObj = this.props.navigation.state.params
    return (
      <View style={{ flex: 1 }}>
        <View style={{ backgroundColor: 'white' }}>
          <AppHeader
            title={'Likes'}
            onPress={() => this.props.navigation.goBack()}
          />
        </View>
        <View style={homeStyles.likesContent}>
          <View style={homeStyles.likesCountContainer}>
            <Text style={homeStyles.likesCountText}>
              {likeObj.likes.length + ' likes'}
            </Text>
          </View>
          <ScrollView style={{ flex: 1 }}>
            {likeObj.likes.map((item, index) => {
              return (
                <TouchableOpacity
                  key={index}
                  style={[homeStyles.likesListItem, { marginBottom: 9 }]}
                  onPress={() => this.props.peopleApi.getPeopleInfo(item.user.id)}
                >
                  <Image
                    source={item.user.profPicPath ? { uri: item.user.profPicPath } : require('../../../assets/images/user_dummy_image.png') }
                    style={homeStyles.personThumbnail}
                  />
                  <Text style={homeStyles.likesItemText} numberOfLines={1}>{item.user.name}</Text>
                </TouchableOpacity>
              );
            })}
          </ScrollView>
        </View>
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => ({
    peopleApi: bindActionCreators(PeopleApi, dispatch)
})

export default connect(
  undefined,
  mapDispatchToProps
)(HomePostLikes)
