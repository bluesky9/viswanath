import { StyleSheet, Dimensions, Platform } from 'react-native';
import { style } from '../../styles/variables';
const { width } = Dimensions.get('window');

const homeStyles = StyleSheet.create({
  fullWidthContainer: { flex: 1, width: width },
  feedErrorHeadText: {
    fontSize: 20,
    lineHeight: 28,
    fontFamily: style.font.family.ptSerifBold,
    textAlign: 'center',
    color: 'rgb(59,62,68)'
  },
  feedErrorInstructionText: {
    fontSize: 16,
    lineHeight: 21,
    textAlign: 'center',
    width: width - 120,
    color: 'rgb(53,71,91)',
    marginTop: 10,
    marginBottom: 11,
    fontFamily: style.font.family.rubikRegular,
    letterSpacing: 0.2
  },
  homePageSearchBar: {
    fontSize: 16,
    lineHeight: 21,
    fontFamily: style.font.family.rubikRegular
  },
  headerContainer: {
    width: width,
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: 'rgb(233,239,247)',
    backgroundColor: 'white',
    paddingHorizontal: 15,
    paddingVertical: 12,
    alignItems: 'center',
    justifyContent: 'center'
  },
  statusbarBlank: {
    height: style.header.height,
    width: width,
    backgroundColor: 'white'
  },
  homeHeaderWrap: {
    backgroundColor: 'white'
  },
  homeInputboxWrap: {
    // height: 64,
    paddingHorizontal: 15,
    paddingVertical: 12,
    alignSelf: 'stretch',
    justifyContent: 'center',
    backgroundColor: 'rgb(250,251,252)',
    alignItems: 'center'
  },
  homeMainFeedWrap: {
    flex: 1,
    backgroundColor: 'rgb(250,251,252)'
  },
  dualHeaderContainer: {
    paddingVertical: 16,
    alignSelf: 'stretch',
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: 2,
    borderColor: 'rgb(233,239,247)',
    backgroundColor: 'white'
  },
  dualHeaderLeftText: {
    fontSize: 14,
    lineHeight: 17,
    fontFamily: style.font.family.rubikMedium,
    color: 'rgb(53,71,91)',
    letterSpacing: 0.5
  },
  dualHeaderCenterText: {
    fontSize: 16,
    lineHeight: 19,
    fontFamily: style.font.family.rubikMedium,
    color: 'rgb(53,71,91)',
    letterSpacing: 0.2
  },
  dualHeaderRightText: {
    fontSize: 14,
    lineHeight: 17,
    fontFamily: style.font.family.rubikMedium,
    color: 'rgb(53,135,230)',
    letterSpacing: 0.5
  },
  title: {
    fontSize: 18,
    lineHeight: 21,
    color: 'rgb(59, 62, 68)',
    fontFamily: style.font.family.ptSerifBold
  },
  audienceListItem: {
    width: width,
    height: 54,
    borderBottomWidth: 1,
    borderColor: 'rgb(233,239,247)',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'white'
  },
  audienceItemText: {
    color: 'rgb(83,87,94)',
    fontSize: 14,
    fontFamily: style.font.family.rubikRegular
  },
  newPostItemWrap: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 15,
    paddingRight: 15,
    borderBottomWidth: 1,
    borderColor: 'rgb(233,239,247)',
    backgroundColor: 'white',
    paddingVertical: 20
  },
  newPostItemHead: {
    fontSize: 12,
    lineHeight: 14,
    fontWeight: 'bold',
    color: 'rgb(83,87,94)',
    fontFamily: style.font.family.rubikMedium
  },
  newPostItemLow: {
    fontSize: 14,
    lineHeight: 17,
    marginVertical: 4,
    color: 'rgb(53,135,230)',
    fontFamily: style.font.family.rubikRegular
  },
  newPostInput: {
    alignSelf: 'stretch',
    paddingVertical: 12,
    paddingHorizontal: 12,
    borderWidth: 1,
    borderRadius: 3,
    borderColor: style.color.lighterGrey,
    fontSize: 14,
    lineHeight: 17,
    marginBottom: 15,
    height: Platform.OS === 'ios' ? undefined : 42,
    maxHeight: 125
  },
  likesContent: {
    flex: 1,
    alignSelf: 'stretch',
    backgroundColor: 'rgb(242, 245, 249)'
  },
  likesCountContainer: {
    alignSelf: 'stretch',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingLeft: 15,
    paddingVertical: 15
  },
  likesCountText: {
    color: 'rgb(83,87,94)',
    fontFamily: style.font.family.rubikRegular
  },
  likesListContainer: {
    flex: 9,
    alignSelf: 'stretch',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  likesListItem: {
    width: width,
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  likesItemText: {
    fontSize: 16,
    lineHeight: 21,
    color: 'rgb(53,135,230)',
    fontFamily: style.font.family.rubikRegular
  },
  personThumbnail: {
    marginHorizontal: 15,
    height: 41,
    width: 41,
    borderRadius: 20.5
  },
  tagSearchbarContainer: {
    paddingVertical: 12,
    paddingHorizontal: 15,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgb(242,245,249)'
  },
  tagInput: {
    height: Platform.OS === 'ios' ? undefined : 37,
    alignSelf: 'stretch',
    padding: 12,
    paddingLeft: 45,
    fontSize: 14,
    lineHeight: 17,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    backgroundColor: 'white',
    shadowOffset: {
      height: 1,
      width: -1
    },
    zIndex: 3,
    elevation: 3,
    shadowOpacity: 0.2,
    borderRadius: 3
  },
  tagSearchIcon: {
    position: 'absolute',
    height: 15,
    width: 15,
    alignSelf: 'center',
    left: 30,
    zIndex: 4,
    elevation: 4
  },
  tagInstructionContainer: {
    padding: 15,
    borderWidth: 1,
    borderColor: 'rgb(233,239,247)',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'rgb(242,245,249)'
  },
  tagInst: {
    marginLeft: 15,
    fontSize: 14,
    lineHeight: 17,
    color: 'rgb(83,87,94)',
    fontFamily: style.font.family.rubikRegular
  },
  tagListContainer: {
    backgroundColor: 'rgb(242, 245, 249)',
    flex: 6,
    alignSelf: 'stretch',
    justifyContent: 'flex-start'
  },
  tagItemBox: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    borderColor: 'rgb(233,239,247)',
    borderBottomWidth: 1,
    paddingVertical: 11
  },
  tagButtonText: {
    fontSize: 14,
    lineHeight: 17,
    color: 'rgb(53,135,230)',
    fontFamily: style.font.family.rubikMedium,
    letterSpacing: 0.5
  },
  postNoteText: {
    fontSize: 13,
    lineHeight: 15,
    marginTop: 2,
    color: 'rgb(108,111,125)',
    fontFamily: style.font.family.rubikRegular,
    opacity: 0.5
  },
  postMessage: {
    fontSize: 14,
    lineHeight: 21,
    color: 'rgb(83, 87, 94)',
    fontFamily: style.font.family.rubikRegular
  },
  postButtonBar: {
    flex: 0.8,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  postBarLikesText: {
    fontSize: 14,
    lineHeight: 17,
    marginLeft: 5,
    color: 'rgb(59,62,68)',
    fontFamily: style.font.family.rubikRegular
  },
  postBarLikesTextDefault: {
    fontSize: 14,
    lineHeight: 17,
    marginLeft: 5,
    color: 'rgb(149,159,171)',
    fontFamily: style.font.family.rubikRegular
  },
  postBarComText: {
    fontSize: 14,
    lineHeight: 17,
    marginLeft: 5,
    color: 'rgb(149,159,171)',
    fontFamily: style.font.family.rubikRegular
  },
  postCommentContainer: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'rgb(242, 245, 249)',
    borderTopWidth: 2,
    borderColor: 'rgb(233,239,247)',
    top: Platform.OS === 'ios' ? 0 : -10
  },
  commentText: {
    fontSize: 14,
    lineHeight: 21,
    color: 'rgb(83,87,94)',
    fontFamily: style.font.family.rubikRegular,
    marginTop: 2,
    width: '90%'
  },
  commentTime: {
    fontSize: 13,
    lineHeight: 15,
    marginTop: 3,
    color: 'rgba(108,111,125,0.8)',
    marginVertical: 2,
    fontFamily: style.font.family.rubikRegular
  },
  likeImg: {
    height: 20,
    width: 20,
    resizeMode: 'contain'
  },
  linkContainer: {
    marginHorizontal: 15,
    marginBottom: 30,
    borderWidth: 1,
    borderColor: 'rgb(197, 207, 219)',
    borderRadius: 4,
    paddingTop: 17,
    paddingBottom: 9,
    paddingHorizontal: 14,
    flexDirection: 'row'
  },
  linkImage: {
    height: 60,
    width: 60,
    resizeMode: 'cover'
  },
  linkContainerTitle: {
    fontSize: 14,
    lineHeight: 21,
    fontFamily: style.font.family.rubikMedium,
    color: 'rgb(47, 120, 204)',
    marginBottom: 3
  },
  linkContainerDescription: {
    fontSize: 14,
    lineHeight: 21,
    fontFamily: style.font.family.rubikRegular,
    letterSpacing: 0.2,
    color: 'rgb(83, 87, 94)',
    marginBottom: 3
  }
});
export default homeStyles;
