import React, { Component, ReactNode } from 'react';
import { View } from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
import HomeHeader from './home-header';
import HomeSearchBar from './home-searchBar';
import HomePostsList from './home-posts-list';
import HomeFeedsEmptyState from './home-feed-empty-state';
import homeStyles from './home-styles';

import { get } from 'lodash';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getNotificationsUnreadCount } from '../../store/actions/Notifications.js';
import { DrawerActions } from '../../store/actions';

import { getPosts } from '../../store/actions/Feed.js';

interface ILoginScreenProps extends NavigationScreenProps<{}> {
  getPosts;
  paginatedPosts;
  getNotificationsUnreadCount;
  drawerActions;
  drawerOpen;
  profilePic: string;
}
interface ILoginScreenState {
  drawerOpen: boolean;
}
class MainFeeds extends Component<ILoginScreenProps, ILoginScreenState> {
  flatListRef;
  constructor(props) {
    super(props);
    this.state = {
      drawerOpen: false
    };
  }

  componentDidMount() {
    this.props.getPosts(1, 20, 0);
    this.props.getNotificationsUnreadCount();
  }

  renderFeed(): ReactNode {
    // if (this.state.feedCondition === 0) {
    //   return <HomeFeedsError />;
    // }
    if (this.props.paginatedPosts.length === 0) {
      return <HomeFeedsEmptyState navigation={this.props.navigation}/>;
    }
    return <HomePostsList navigation={this.props.navigation} flatListRefFn={(ref) => this.flatListRef = ref} />;
  }

  _onProfileImagePressed = () => {
    this.props.drawerActions.drawerOpen();
  }

  _scrollToTop = () => {
    if (this.flatListRef) {
      this.flatListRef.scrollToIndex({index: 0, animated: true})
    }
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: 'rgb(242,245,249)' }}>
        <View style={homeStyles.statusbarBlank} />
        <HomeHeader
          leftPress={() => this._onProfileImagePressed()}
          navigation={this.props.navigation}
          iconPress={() => this._scrollToTop()}
          searchPress={() => console.log('Searched Pressed')}
          userProfilePic={this.props.profilePic}
        />
        <View style={homeStyles.homeInputboxWrap}>
          <HomeSearchBar navigation={this.props.navigation} />
        </View>
        <View style={homeStyles.homeMainFeedWrap}>{this.renderFeed()}</View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    paginatedPosts: state.paginatedPosts.posts,
    drawerOpen: state.drawerReducer.isOpened,
    profilePic: get(state.userInfoAPI.userInfo, 'profilePicture', undefined)
  };
};

const mapDispatchToProps = dispatch => ({
  getPosts: bindActionCreators(getPosts, dispatch),
  drawerActions: bindActionCreators(DrawerActions, dispatch),
  getNotificationsUnreadCount: bindActionCreators(
    getNotificationsUnreadCount,
    dispatch
  )
});

export default connect(mapStateToProps, mapDispatchToProps)(MainFeeds);
