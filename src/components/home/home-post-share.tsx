import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native';
import AppHeader from '../layouts/header/header';
import { NavigationScreenProps } from 'react-navigation';
import { PeopleApi } from '../../store/actions'
import homeStyles from './home-styles';
import { get } from 'lodash';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

interface ILoginScreenProps extends NavigationScreenProps<{}> {
  navigation;
  peopleApi;
}
class HomePostShare extends Component<ILoginScreenProps> {
  render() {
    // console.log(this.props.navigation.state.params, 'Likes')
    let shareObj = get(this.props.navigation.state.params, 'share', [])
    return (
      <View style={{ flex: 1 }}>
        <View style={{ backgroundColor: 'white' }}>
          <AppHeader
            title={'Share'}
            onPress={() => this.props.navigation.goBack()}
          />
        </View>
        <View style={homeStyles.likesContent}>
          <View style={homeStyles.likesCountContainer}>
            <Text style={homeStyles.likesCountText}>
              {(shareObj.length - 1) + ' share'}
            </Text>
          </View>
          <ScrollView style={{ flex: 1 }}>
            {shareObj.map((item, index) => {
              if (index > 0) {
                return (
                  <TouchableOpacity
                    key={index}
                    style={[homeStyles.likesListItem, { marginBottom: 9 }]}
                    onPress={() => this.props.peopleApi.getPeopleInfo(item.id)}
                  >
                    <Image
                      source={item.profPicPath ? { uri: item.profPicPath } : require('../../../assets/images/user_dummy_image.png') }
                      style={homeStyles.personThumbnail}
                    />
                    <Text style={homeStyles.likesItemText} numberOfLines={1}>{item.name}</Text>
                  </TouchableOpacity>
                );
              } else {
                return  <View key={index}/>
              }
            })}
          </ScrollView>
        </View>
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => ({
    peopleApi: bindActionCreators(PeopleApi, dispatch)
})

export default connect(
  undefined,
  mapDispatchToProps
)(HomePostShare)
