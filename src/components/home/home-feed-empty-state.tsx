import React, { Component } from 'react';
import { View, Image, Text } from 'react-native';
// const { width } = Dimensions.get('window');
import AppButton from '../layouts/buttons/button';
import { style } from '../../styles/variables';
import homeStyles from './home-styles';
import { NavigationScreenProps } from 'react-navigation';

interface ILoginScreenProps extends NavigationScreenProps<{}> {
  hideButton?: boolean;
  emptyMessage?: string;
}

export default class HomeFeedsEmptyState extends Component<ILoginScreenProps> {
  render() {
    return (
      <View style={homeStyles.fullWidthContainer}>
        <View
          style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
        >
          <Image source={require('../../../assets/emptyFeedError.png')} />
        </View>
        <View
          style={{
            flex: 1,
            justifyContent: 'flex-start',
            alignItems: 'center'
          }}
        >
          <Text style={homeStyles.feedErrorHeadText}>You're missing out!</Text>
          <Text style={homeStyles.feedErrorInstructionText}>
            {this.props.emptyMessage ? this.props.emptyMessage : 'Start following people and universities to see their activity'}
          </Text>
          { !this.props.hideButton && <AppButton
            type={'primary'}
            label={'Search people'}
            containerWidth={'40%'}
            onPress={() => this.props.navigation.navigate('PeopleScreen')}
            textSize={style.font.size.large}
            textLineHeight={style.font.lineHeight.normal.normal}
            textFamily={style.font.family.rubikRegular}
          />}
        </View>
        <View
          style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
        />
      </View>
    );
  }
}
