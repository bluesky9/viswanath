import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Platform } from 'react-native';
import { Icon } from 'native-base';
import { NavigationScreenProps } from 'react-navigation';
import ImagePicker from 'react-native-image-crop-picker';
import { get } from 'lodash';

import homeStyles from './home-styles';
interface ILoginScreenProps extends NavigationScreenProps<{}> {}

interface ILoginScreenProps {}
export default class HomeSearchBar extends Component<ILoginScreenProps> {

  openCamera = () => {
    ImagePicker.openCamera({
      cropping: false,
      compressImageQuality: Platform.OS === 'ios' ? 0 : 0.5
    })
      .then(image => {
        let imageObj = {
          uri: get(image, 'path', ''),
          name: new Date().getTime().toString(),
          type: get(image, 'mime', '')
        }
        this.props.navigation.navigate('ComposePost', { image: imageObj })
      })
      .catch((e) => {
        console.log(e, 'Error')
      });
  }

  render() {
    return (
      <View
        style={{
          // height: 40,
          // width: width - 30,
          paddingVertical: 13,
          paddingHorizontal: 15,
          alignItems: 'center',
          shadowOffset: { width: -2, height: 2 },
          shadowOpacity: 0.1,
          zIndex: 3,
          elevation: 3,
          flexDirection: 'row',
          borderRadius: 3,
          backgroundColor: 'white'
        }}
      >
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('ComposePost')}
          style={{ flex: 1 }}
        >
          <Text
            style={[
              homeStyles.homePageSearchBar,
              { color: 'rgba(108,111,125,0.5)' }
            ]}
          >
            Write to your followers..
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.openCamera()}
          style={{ justifyContent: 'center', paddingLeft: 11 }}
        >
          <Icon
            style={{ fontSize: 20, color: 'rgb(197, 207, 219)' }}
            name={'md-camera'}
          />
        </TouchableOpacity>
      </View>
    );
  }
}
