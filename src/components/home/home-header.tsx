import React, { Component } from 'react';
import { View, Image, TouchableOpacity } from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
import homeStyles from './home-styles';

import { connect } from 'react-redux';

interface ILoginScreenProps extends NavigationScreenProps<{}> {
  leftPress: Function;
  iconPress: Function;
  searchPress: Function;
  userInfo;
}

class HomeHeader extends Component<ILoginScreenProps> {
  onIconPress = () => {
    this.props.iconPress();
  }
  render() {
    return (
      <View style={homeStyles.headerContainer}>
        <View style={{flex: 2}}>
          <TouchableOpacity onPress={() => this.props.leftPress()}>
            <Image
              source={ this.props.userInfo && this.props.userInfo.profilePicture ? {uri: this.props.userInfo.profilePicture}
              : require('../../../assets/images/user_dummy_image.png')}
              style={{ height: 30, width: 30, borderRadius: 15 }}
            />
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          onPress={() => this.onIconPress()}
          style={{ flex: 6, justifyContent: 'center', alignItems: 'center' }}
        >
          <Image
            source={require('../../../assets/images/main_icon.png')}
            style={{ height: 30, resizeMode: 'contain' }}
          />
        </TouchableOpacity>
        <View style={{ flex: 2 }}>
          {/* <TouchableOpacity
            onPress = {() =>  this.props.searchPress()}
          >
            <Icon
              style={{ fontSize: 25, color: 'rgb(197, 207, 219)'}}
              name={'md-search'}
            />
          </TouchableOpacity> */}
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    userInfo: state.userInfoAPI.userInfo
  };
};

export default connect(mapStateToProps, undefined)(HomeHeader);
