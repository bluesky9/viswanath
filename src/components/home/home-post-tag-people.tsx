import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  Text,
  TextInput
} from 'react-native';
import homeStyles from './home-styles';
import { NavigationScreenProps } from 'react-navigation';
import MSAHeader from '../layouts/header/header';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { getUserConnections, setTaggedUsers } from '../../store/actions/Feed';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { get, filter, isEmpty } from 'lodash';

interface ILoginScreenProps extends NavigationScreenProps<{}> {
  actions: any;
  postTagState: any;
  userId: any;
}
interface ILoginScreenState {
  searchPeople: string;
  tagStates: boolean[];
}
class PostTag extends Component<ILoginScreenProps, ILoginScreenState> {
  constructor(props) {
    super(props);
    this.state = {
      searchPeople: '',
      tagStates: new Array(200).fill(false)
    };
  }
  componentDidMount() {
    console.log(this.props.userId, 'aass')
    this.props.actions.getUserConnections(this.props.userId);
    this.props.postTagState.postTaggedUsers.userArray.map(item => {
      let ind = this.props.postTagState.getConnections.connections.indexOf(
        item
      );
      this.state.tagStates[ind] = true;
    });
  }
  updateTags() {
    let userArray = [];
    this.state.tagStates.map((item, index) => {
      if (item) {
        userArray.push(
          this.props.postTagState.getConnections.connections[index]
        );
      }
    });
    this.props.actions.setTaggedUsers(userArray);
    this.props.navigation.goBack();
  }

  render() {
    let connections = get(this.props, 'postTagState.getConnections.connections', [])
    if (this.state.searchPeople !== '') {
      connections = filter(connections, connectionOBj => connectionOBj.name.toLowerCase().startsWith(this.state.searchPeople))
    }
    return (
      <View style={{ flex: 1, backgroundColor: 'rgb(250,251,252)' }}>
        <MSAHeader
          title={'Tag people'}
          leftText={'CANCEL'}
          rightText={'DONE'}
          onPress={() => this.props.navigation.goBack()}
          onRightTextPress={() => this.updateTags()}
        />
        <View style={homeStyles.tagSearchbarContainer}>
          <TextInput
            onChangeText={txt => this.setState({ searchPeople: txt })}
            style={homeStyles.tagInput}
            placeholder={'Search people'}
            autoCapitalize={'none'}
          />
          <View style={homeStyles.tagSearchIcon}>
            <Image
              style={{ height: 15, width: 15 }}
              source={require('../../../assets/app_icons/Icons/search.png')}
            />
          </View>
        </View>
        { this.state.searchPeople === '' && <View style={homeStyles.tagInstructionContainer}>
          <Text style={homeStyles.tagInst}>Your connections</Text>
        </View>}
        <View style={{ flex: 6, backgroundColor: 'rgb(242,245,249)' }}>
          <KeyboardAwareScrollView>
            {connections.map(
              (item, index) => {
                return (
                  <View key={index} style={[homeStyles.tagItemBox, { marginHorizontal: 15 }]}>
                    <Image
                      source={isEmpty(item.profilePicture) ?
                        require('../../../assets/images/user_dummy_image.png')
                        :
                        { uri: item.profilePicture }
                      }
                      style={[homeStyles.personThumbnail, { marginHorizontal: 0, marginRight: 9 }]}
                    />
                    <Text style={[homeStyles.likesItemText, { flex: 1 }]}
                      numberOfLines={1}
                    >
                      {item.name}
                    </Text>
                    <TouchableOpacity
                      onPress={() => {
                        let arr = this.state.tagStates;
                        arr[index] = !arr[index];
                        this.setState({
                          tagStates: arr
                        });
                      }}
                    >
                      <Text
                        style={[
                          homeStyles.tagButtonText,
                          {
                            color: this.state.tagStates[index]
                              ? 'rgb(204,20,51)'
                              : 'rgb(53,135,230)'
                          }
                        ]}
                      >
                        {this.state.tagStates[index] ? 'UNTAG' : 'TAG'}
                      </Text>
                    </TouchableOpacity>
                  </View>
              );
            })
          }
          </KeyboardAwareScrollView>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    postTagState: state,
    userId : state.userInfoAPI.userId
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      getUserConnections: getUserConnections,
      setTaggedUsers: setTaggedUsers
    },
    dispatch
  )
});

export default connect(mapStateToProps, mapDispatchToProps)(PostTag);
