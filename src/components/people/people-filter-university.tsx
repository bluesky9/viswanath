import React, { Component } from 'react';
import { View, TouchableOpacity, Text, Image } from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import MSAHeader from '../layouts/header/header';

// import PeopleFilterHeader from './people-filter-header';
import PeopleFilterSearch from './people-filter-search';

import { filterUniversityStyles } from './people-filter-styles';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { getAllUniversities } from '../../store/actions/Universities.js';
import { setPeopleFilters, getPeople } from '../../store/actions/People.js'

import { get, find, cloneDeep, filter, isEmpty } from 'lodash';

interface IPeopleFilterNavParams {}
interface IPeopleFilterProps
  extends NavigationScreenProps<IPeopleFilterNavParams> {
    setPeopleFilters;
    getPeople;
    peopleFilters;
    getAllUniversities;
    alluniversities;
    userId;
  }

interface State {
  pageSize;
  selectAll;
  rightTitle;
  universities;
  searchUniversities;
}

class PeopleFilterUniversity extends Component<IPeopleFilterProps, State> {

  constructor(props) {
    super(props);
    this.state = {
      pageSize: 20,
      selectAll: false,
      rightTitle: undefined,
      universities: undefined,
      searchUniversities: undefined
    }
  }

  componentDidMount() {
    this.props.getAllUniversities();
  }

  componentWillReceiveProps(nextProps) {
    let universities = [];
    let universityList = nextProps.alluniversities;
    let peopleFilters = get(this.props.navigation.state.params, 'peopleFilters', undefined)
    let checkUniversities = peopleFilters ? peopleFilters.univIds : [];

    universityList.map((item) => {
      if (find(checkUniversities, obj => obj === item.univ_id )) {
        universities.push({ name: item.name, status: true, id: item.univ_id })
      } else {
        universities.push({ name: item.name, status: false, id: item.univ_id })
      }
    })

    if (checkUniversities.length > 0) {
      this.setState({ rightTitle: 'CLEAR' })
    } else {
      this.setState({ selectAll: true })
      this.setState({ rightTitle: 'APPLY' })
    }
    this.setState({ universities })
  }

  _handleOnApplyPress = () => {
    let peopleFilters = cloneDeep(get(this.props.navigation.state.params, 'peopleFilters', {}));
    let updateUserTypesFn = get(this.props.navigation.state.params, 'updateUserTypes', undefined)

    peopleFilters.univIds = [];

    if (this.state.rightTitle === 'APPLY') {
      filter(this.state.universities, (obj) => {
        if (obj.status)
          return peopleFilters.univIds.push(obj.id);
        else {
          return undefined;
        }
      })
    }

    if (updateUserTypesFn) {
      updateUserTypesFn(peopleFilters)
    }
    this.props.navigation.goBack();
  }

  handleTextChange = (text) => {
    this.setState({ searchUniversities: text });
  }

  _renderItem(index, item) {
    return(
      <TouchableOpacity
        onPress={() => {
          let universities = this.state.universities
          if (item.status) {
            universities[index].status = false;
          } else {
            universities[index].status = true;
          }
          this.setState({ universities: universities, rightTitle: 'APPLY', selectAll: false })
        }}
        key={item.code}
        style={filterUniversityStyles.itemContainer}
      >
        { item.status ?
          <Image source={require('../../../assets/app_icons/Icons/Checkbox-active.png')} style={filterUniversityStyles.checkboxContainer}
            resizeMode='stretch'
          />
          : <Image source={require('../../../assets/app_icons/Icons/Checkbox-default.png')} style={filterUniversityStyles.checkboxContainer}
            resizeMode='stretch'
          />
        }
        <Text style={filterUniversityStyles.itemText}>{item.name}</Text>
      </TouchableOpacity>
    )
  }

  render() {
    return (
      <View style={filterUniversityStyles.container}>
        <MSAHeader
          title={'University/College'}
          rightText={this.state.rightTitle}
          onPress={() => this.props.navigation.goBack()}
          onRightTextPress={() => this._handleOnApplyPress()}
        />
        {/* <PeopleFilterHeader
          navigation={this.props.navigation}
          rightTitle={this.state.rightTitle}
          onRightPress={this._handleOnApplyPress}
          title={'University/College'}
        /> */}
        <View style={filterUniversityStyles.mainContent}>
          <PeopleFilterSearch placeHolder={'Search University'} onTextChange={this.handleTextChange}/>
        </View>
        <View style={{ marginHorizontal: 15, flex: 1 }}>
          { this.props.alluniversities.length > 0 &&
              <KeyboardAwareScrollView>
                <TouchableOpacity style={filterUniversityStyles.itemContainer}>
                  { this.state.selectAll ? (
                    <Image
                      source={require('../../../assets/app_icons/Icons/Checkbox-active.png')}
                      style={filterUniversityStyles.checkboxContainer}
                      resizeMode={'stretch'}
                    />
                  ) : (
                    <Image
                      source={require('../../../assets/app_icons/Icons/Checkbox-default.png')}
                      style={filterUniversityStyles.checkboxContainer}
                      resizeMode={'stretch'}
                    />
                  )}
                  <Text style={filterUniversityStyles.itemText}>
                    All
                  </Text>
                </TouchableOpacity>
                { this.state.universities && this.state.universities.map((item, index) => {
                  if (!isEmpty(this.state.searchUniversities)) {
                    if (item.name.startsWith(this.state.searchUniversities))
                      return <View key={index}>
                        {this._renderItem(index, item)}
                        </View>
                    else return undefined;
                  } else {
                    return <View key={index}>
                      {this._renderItem(index, item)}
                      </View>
                  }
                })}
              </KeyboardAwareScrollView>
            }
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return ({
    peopleFilters: state.peopleFilters.filters,
    alluniversities: state.alluniversities.universities,
    userId: state.userInfoAPI.userId
  })
}

const mapDispatchToProps = dispatch => ({
  setPeopleFilters: bindActionCreators(setPeopleFilters, dispatch),
  getPeople: bindActionCreators(getPeople, dispatch),
  getAllUniversities: bindActionCreators(getAllUniversities, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PeopleFilterUniversity)
