import React, { Component } from 'react';
import { View, Image, Text, FlatList, TouchableOpacity, ActivityIndicator, Dimensions } from 'react-native';
import { NavigationScreenProps } from 'react-navigation';

import MSAButton from '../layouts/buttons/button';
import PeopleHeader from './people-header';
import PeopleFilterSearch from './people-filter-search';

import { get, cloneDeep, isEmpty, merge } from 'lodash';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { PeopleApi, DialogModal, DrawerActions, UniversitiesApi } from '../../store/actions'
import {getPeople, getPeopleByName, setPeopleFilters } from '../../store/actions/People.js'

import { peopleScreenStyles } from './people-screen-styles';
import { style } from '../../styles/variables';
import { constants } from '../../constants';

const { width } = Dimensions.get('window');

const UNFOLLOW_POST = 'Are you sure to unfollow'
const UNFOLLOW_MSG = 'You will no longer see the posts or activity in your feed.'

interface ILoginScreenNavParams {}
interface ILoginScreenProps
  extends NavigationScreenProps<ILoginScreenNavParams> {
    getPeople?: any;
    peopleData?: any;
    getPeopleByName?: any;
    peopleFilters?: any;
    setPeopleFilters?: any;
    peopleApi?: any;
    dialogModal?: any;
    userId?: any;
    drawerActions?: any;
    filtersApplied?: any;
    profilePic: string;
    universitiesActions;
  }

type State = {
  showSearchBar?: boolean;
  peopleSearch;
  filterActive;
  myUserId;
  refreshing;
};

class PeopleScreen extends Component<ILoginScreenProps, State> {
  constructor(props) {
    super(props);
    this.state = {
      showSearchBar: false,
      filterActive: false,
      myUserId: undefined,
      peopleSearch: {
        userTypes: [],
        countries: [],
        pageSize: 15,
        pageNo: 0,
        univIds: [],
        userIds: [],
        countOnly: false
      },
      refreshing: false
    };
  }

  componentDidMount() {
    this.props.setPeopleFilters(this.state.peopleSearch)
    let peopleSearch = cloneDeep(this.state.peopleSearch)
    peopleSearch.myUserId = this.props.userId
    this.props.getPeople(peopleSearch);
    this.setState({ myUserId: this.state.myUserId })
  }

  componentWillReceiveProps(nextProps) {
    // let peopleFilter = this.state.peopleSearch

    // if (nextProps.peopleFilters && nextProps.peopleFilters.myUserId) {
    //   peopleFilter = merge(peopleFilter, { myUserId: nextProps.peopleFilters.myUserId})
    // }

    if (nextProps.peopleData) {
      this.setState({ refreshing: false })
    }

    if (nextProps.filtersApplied) {
      this.setState({ filterActive: true })
    } else {
      this.setState({ filterActive: false })
    }
  }

  _handleSearchPress() {
    this.setState({ showSearchBar: true });
  }

  onEndReached = () => {
    let peopleFilters = this.props.peopleFilters;
    let peopleSearch = cloneDeep(this.state.peopleSearch);
    if (peopleSearch.pageSize < this.props.peopleData.totalElements ) {
      peopleSearch.pageSize += 15;
      peopleFilters.pageSize = peopleSearch.pageSize;
      this.props.setPeopleFilters(peopleFilters);
      this.setState({ peopleSearch });
      this.props.getPeople( merge(peopleSearch, {myUserId: this.props.userId}))
    }
  }

  _handleOnTextChange = (text) => {
    this.props.getPeopleByName(text);
  }

  onUnfollowPress = (peopleId) => {
    this.props.dialogModal.dialogShows
    (
      UNFOLLOW_POST, UNFOLLOW_MSG, 'action', true, () => this.props.peopleApi.unFollowPeople(peopleId, undefined, 'other')
    )
  }

  onFollowPress = (peopleId) => {
    this.props.peopleApi.followPeople(peopleId, undefined, 'other')
  }

  onProfilePicPressed = (id) => {
    this.props.peopleApi.getPeopleInfo(id)
  }

  onRefresh = () => {
    let peopleSearch = cloneDeep(this.state.peopleSearch)
    peopleSearch.myUserId = this.props.userId
    peopleSearch.pageSize = 15
    this.props.getPeople(peopleSearch);
  }

  renderPeople = (obj) => {
    let isUserProfile = obj.id ===  this.props.userId
    return (
      <View style={peopleScreenStyles.peopleCardContainer}>
        <TouchableOpacity onPress={() => this.onProfilePicPressed(obj.id)}>
          <Image
            source={obj.profilePicture ? { uri: obj.profilePicture } : require('../../../assets/images/user_dummy_image.png') }
            style={peopleScreenStyles.profileIconContainer}
          />
        </TouchableOpacity>
        <View style={peopleScreenStyles.peopleDetailsConatiner}>
          <View style={[peopleScreenStyles.nameContainer, {borderBottomWidth: 0}]}>
            <View style={{ flex: 1 }}>
              <Text onPress={() => this.props.peopleApi.getPeopleInfo(obj.id)} style={peopleScreenStyles.peopleName}>{obj.name}</Text>
              <Text style={peopleScreenStyles.peopleLocation}>
                {obj.country}
              </Text>
            </View>
            { !isUserProfile && <View style={peopleScreenStyles.followContainer}>
              {obj.following ? (
                <MSAButton
                  onPress={() => this.onUnfollowPress(obj.id)}
                  type={'inverted'}
                  bordered={true}
                  label={'Following'}
                  containerWidth={'55%'}
                  customButtonStyle={peopleScreenStyles.followButton}
                  textLineHeight={14}
                  textSize={12}
                />
              ) : (
                <MSAButton
                  onPress={() => this.onFollowPress(obj.id)}
                  type={'primary'}
                  label={'Follow'}
                  containerWidth={'55%'}
                  customButtonStyle={peopleScreenStyles.followButton}
                  textLineHeight={14}
                  textSize={12}
                />
              )}
            </View>}
          </View>
          { obj.userTypes &&
            obj.userTypes.map((item, index) => {
              return (
              item.userType !== 'PROSPECTIVE_STUDENT' ?
                <Text style={peopleScreenStyles.normalTextView} key={index}>
                  <Text style={peopleScreenStyles.normalText}>{constants.USERTYPES.CONTEXT[item.userType] + ' '} </Text>
                  <Text style={[peopleScreenStyles.normalText, { color: style.color.primary, width: width - 250 }]}
                    onPress={() => {
                      this.props.universitiesActions.getUniversityBySlug(item.universitySlug)
                      this.props.navigation.navigate('UniversityProfile')
                    }}
                  >
                    {item.universityName}
                  </Text>
                </Text> :
                undefined
              )
            }
            )
          }
          {/* <Text style={peopleScreenStyles.normalText}>
            <Text>Currently studying at </Text>
            <Text style={{ color: style.color.primary }}>
              {'Something'}
            </Text>
          </Text> */}
          { obj.interests &&
            <Text style={[peopleScreenStyles.normalText, {marginTop: 7}]}>
            {
              obj.interests.map((item, index) => {
                if (index < 3)
                  return <Text key={index}>
                    <Text>{item.interestArea}</Text>
                    <Text>{obj.interests.length - 1 === index ? '' : ', '}</Text>
                  </Text>
                else
                  return <View />
              })
            }
            { obj.interests.length > 3 &&
              <Text style={{ color: style.color.primary }}>{obj.interests.length - 3}</Text>
            }
          </Text>
          }
        </View>
      </View>
    );
  }

  render() {
    return (
      <View style={peopleScreenStyles.container}>
        {this.state.showSearchBar ? (
          <View style={peopleScreenStyles.searchBarContainer}>
            <View style={{ flex: 1 }}>
              <PeopleFilterSearch onTextChange={this._handleOnTextChange} />
            </View>
            <TouchableOpacity
              onPress={() => this.setState({ showSearchBar: false })}
              style={{
                alignItems: 'flex-end',
                justifyContent: 'center',
                paddingLeft: 20
              }}
            >
              <Text style={peopleScreenStyles.cancelText}>CANCEL</Text>
            </TouchableOpacity>
          </View>
        ) : (
          <PeopleHeader
            navigation={this.props.navigation}
            onSearchPress={this._handleSearchPress.bind(this)}
            title={'People'}
            onFilterPress={() =>
              this.props.navigation.navigate('PeopleFilters')
            }
            filtersApplied={this.state.filterActive}
            profileIconPress={this.props.drawerActions.drawerOpen}
            profilePic={this.props.profilePic}
          />
        )}
        <View style={peopleScreenStyles.peopleNumberContainer}>
          <Text style={peopleScreenStyles.peopleNumberText}>
            We found {this.props.peopleData.totalElements} people to connect
          </Text>
        </View>
        {(!isEmpty(this.props.peopleData) && this.props.peopleData.content.length > 0) &&
        <FlatList
          data={this.props.peopleData.content}
          renderItem={obj => this.renderPeople(obj.item)}
          refreshing={this.state.refreshing}
          onRefresh={this.onRefresh}
          keyExtractor={item => item.id.toString()}
          contentContainerStyle={{ paddingBottom: 60 }}
          showsVerticalScrollIndicator={false}
          onEndReached={this.onEndReached}
          onEndReachedThreshold={0}
          ListFooterComponent={() => {
            if (this.props.peopleData.totalElements >= this.state.peopleSearch.pageSize)
              return <View style={{ paddingTop: 20 }}>
              <ActivityIndicator size='large' color={style.color.primary}/>
              </View>
            else return <View />
          }}
        />
        }
      </View>
    );
  }
}

const mapStateToProps = state => {
  return ({
    peopleData: state.people.peopleList,
    peopleFilters: state.peopleFilters.filters,
    userId: state.userInfoAPI.userId,
    filtersApplied: state.peopleFilters.filterApplied,
    profilePic: get(state.userInfoAPI.userInfo, 'profilePicture', undefined)
  })
}

const mapDispatchToProps = dispatch => ({
    getPeople: bindActionCreators(getPeople, dispatch),
    getPeopleByName: bindActionCreators(getPeopleByName, dispatch),
    setPeopleFilters: bindActionCreators(setPeopleFilters, dispatch),
    peopleApi: bindActionCreators(PeopleApi, dispatch),
    dialogModal: bindActionCreators(DialogModal, dispatch),
    drawerActions: bindActionCreators(DrawerActions, dispatch),
    universitiesActions: bindActionCreators(UniversitiesApi, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PeopleScreen)
