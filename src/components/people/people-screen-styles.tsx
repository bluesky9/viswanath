import { StyleSheet } from 'react-native';

import { style } from '../../styles/variables';

export const peopleScreenStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: style.color.whiteCream
  },
  content: {
    paddingHorizontal: 15
  },
  peopleNumberContainer: {
    paddingVertical: 18,
    paddingHorizontal: 15,
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderColor: style.color.lightGrey,
    borderBottomColor: style.color.cream
  },
  peopleNumberText: {
    fontFamily: style.font.family.rubikRegular,
    fontSize: 14,
    lineHeight: 17,
    color: style.color.coal
  },
  profileIconContainer: {
    height: 41,
    width: 41,
    borderRadius: 20.5
  },
  followButton: {
    marginVertical: 0,
    paddingVertical: 7,
    paddingHorizontal: 8
  },
  peopleName: {
    fontFamily: style.font.family.rubikMedium,
    color: style.color.primary,
    fontSize: style.font.size.normal,
    lineHeight: style.font.lineHeight.normal.normal
  },
  peopleLocation: {
    fontSize: style.font.size.normal,
    lineHeight: style.font.lineHeight.normal.normal,
    color: style.color.grey,
    fontFamily: style.font.family.rubikRegular,
    paddingVertical: 6
  },
  normalTextView: {
    paddingTop: 2,
    // flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 6
  },
  normalText: {
    color: style.color.nero,
    fontFamily: style.font.family.rubikRegular,
    fontSize: style.font.size.normal,
    lineHeight: style.font.lineHeight.normal.normal
  },
  peopleCardContainer: {
    flexDirection: 'row',
    paddingHorizontal: 15,
    paddingTop: 12,
    paddingBottom: 17,
    borderBottomWidth: 1,
    backgroundColor: style.color.white,
    borderBottomColor: '#e9eef1'
  },
  peopleDetailsConatiner: {
    flex: 1,
    marginLeft: 15
  },
  nameContainer: {
    flex: 1,
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: style.color.cream
  },
  followContainer: {
    flex: 1,
    alignItems: 'flex-end'
  },
  searchBarContainer: {
    paddingHorizontal: 15,
    paddingTop: style.header.height + 9,
    paddingBottom: 9,
    flexDirection: 'row',
    backgroundColor: style.color.white
  },
  cancelText: {
    letterSpacing: 0.5,
    fontSize: 10,
    fontFamily: style.font.family.rubikMedium,
    color: style.color.primary
  }
});

export const peopleHeaderStyles = StyleSheet.create({
  container: {
    paddingTop: 12 + style.header.height,
    paddingBottom: 12,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: style.color.lightGrey,
    backgroundColor: style.color.white
  },
  leftContainer: {
    alignItems: 'flex-start'
  },
  profileIconContainer: {
    height: 30,
    width: 30,
    borderRadius: 15
  },
  rightContainer: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  bodyText: {
    color: style.color.bluishGrey,
    flex: 1,
    textAlign: 'center',
    fontFamily: style.font.family.rubikRegular,
    letterSpacing: 0.2,
    fontSize: 16,
    lineHeight: 19
  },
  activeFilter: {
    tintColor: style.color.bloodRed
  }
});
