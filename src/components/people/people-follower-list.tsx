import React, { Component } from 'react';
import { View, Text, Image, FlatList, TouchableOpacity } from 'react-native';
import AppHeader from '../layouts/header/header';
import { NavigationScreenProps } from 'react-navigation';

import { PeopleApi, DialogModal, UniversitiesApi } from '../../store/actions'
import MSAButton from '../layouts/buttons/button';
import { peopleScreenStyles } from './people-screen-styles';
import homeStyles from '../home/home-styles';
import { style } from '../../styles/variables';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {get, isEmpty} from 'lodash'

const UNFOLLOW_POST = 'Are you sure to unfollow'
const UNFOLLOW_MSG = 'You will no longer see the posts or activity in your feed.'

interface ILoginScreenProps extends NavigationScreenProps<{
  source: string;
  id: string;
}> {
  navigation;
  userId: string;
  peopleApi;
  dialogModal;
  peopleInfo;
  userInfo;
  universitiesActions;
}
class PeopleFollowerList extends Component<ILoginScreenProps> {

  onUnfollowPress = (peopleId, userType) => {
    this.props.dialogModal.dialogShows(
      UNFOLLOW_POST, UNFOLLOW_MSG, 'action', true, () => this.props.peopleApi.unFollowPeople(peopleId, 'Followers', userType)
    )
  }

  onFollowPress = (peopleId, userType) => {
    this.props.peopleApi.followPeople(peopleId, 'Followers', userType)
  }

  renderPeople = (obj, userType) => {
    return (
      <View style={peopleScreenStyles.peopleCardContainer}>
        <TouchableOpacity
          onPress={() => this.props.peopleApi.getPeopleInfo(obj.id, undefined, this.props.navigation.state.params.source)}
        >
          <Image
            source={obj.profilePicture ? { uri: obj.profilePicture } : require('../../../assets/images/user_dummy_image.png') }
            style={peopleScreenStyles.profileIconContainer}
          />
        </TouchableOpacity>
        <View style={peopleScreenStyles.peopleDetailsConatiner}>
          <View style={[peopleScreenStyles.nameContainer, { borderBottomWidth: 0 }]}>
            <TouchableOpacity
              style={{ flex: 1 }}
              onPress={() => this.props.peopleApi.getPeopleInfo(obj.id, undefined, this.props.navigation.state.params.source)}
            >
              <Text style={peopleScreenStyles.peopleName}>{obj.name}</Text>
              { obj.country ?
                <Text style={[peopleScreenStyles.peopleLocation, {paddingBottom: 0}]}>
                  {obj.country}
                </Text> :
                undefined
              }
            </TouchableOpacity>
            {obj.id !== this.props.userId ?
            <View style={peopleScreenStyles.followContainer}>
              {obj.following ? (
                <MSAButton
                  onPress={() => this.onUnfollowPress(obj.id, userType)}
                  type={'inverted'}
                  bordered={true}
                  label={'Following'}
                  containerWidth={'54%'}
                  customButtonStyle={peopleScreenStyles.followButton}
                  textSize={12}
                />
              ) : (
                <MSAButton
                  onPress={() => this.onFollowPress(obj.id, userType)}
                  type={'primary'}
                  label={'Follow'}
                  containerWidth={'54%'}
                  customButtonStyle={peopleScreenStyles.followButton}
                  textSize={12}
                />
              )}
            </View> : undefined}
          </View>
          { obj.userTypes && obj.userTypes.length ?
            obj.userTypes.map((item, index) => {
              return (
                item.userType !== 'PROSPECTIVE_STUDENT' ?
                <Text style={[peopleScreenStyles.normalText, {marginTop: 7}]} key={index}>
                  <Text>{item.userType === 'ALUMNI' ? 'Almuni' : 'Currently studying'}  at </Text>
                  <Text
                    style={{ color: style.color.primary }}
                    onPress={() => {
                      this.props.universitiesActions.getUniversityBySlug(item.universitySlug)
                      this.props.navigation.navigate('UniversityProfile')
                    }}
                  >
                    {item.universityName}
                  </Text>
                </Text> :
                undefined
              )
            }
            ) : undefined
          }
          {/* <Text style={peopleScreenStyles.normalText}>
            <Text>Currently studying at </Text>
            <Text style={{ color: style.color.primary }}>
              {'Something'}
            </Text>
          </Text> */}
          {obj.interests && obj.interests.length ?
            <Text style={[peopleScreenStyles.normalText, { marginTop: 7 }]}>
              {
                obj.interests.map((item, index) => {
                  if (index < 3)
                    return <Text key={index}>{item.interestArea + ', '}</Text>
                  else
                    return <View />
                })
              }
              { (obj.interests && obj.interests.length > 3) && <Text style={{ color: style.color.primary }}>{obj.interests.length - 3}</Text>
              }
            </Text> : undefined
          }
        </View>
      </View>
    );
  }

  render() {
    let userType = this.props.navigation.state.params.userType
    let title = this.props.navigation.state.params.title
    let userBlocked = this.props.navigation.state.params.blocked
    let data = [];
    if (userType === 'loggedInUser') {
      data = get(this.props.userInfo, title.toLowerCase(), [])
    } else {
      data = get(this.props.peopleInfo, title.toLowerCase(), [])
    }

    return (
      <View style={{ flex: 1, backgroundColor: 'rgb(250,251,252)' }}>
        <View style={{ backgroundColor: 'white' }}>
          <AppHeader
            title={title}
            onPress={() => this.props.navigation.goBack()}
          />
        </View>
        <View style={{ padding: 15, borderBottomWidth: 1, borderBottomColor: 'rgb(233,238,241)' }}>
          <Text style={homeStyles.likesCountText}>
            {data.length + ' ' + title}
          </Text>
        </View>
        {(!isEmpty(data)) && !userBlocked ?
          <FlatList
            data={data}
            renderItem={obj => this.renderPeople(obj.item, userType)}
            keyExtractor={item => item.id.toString()}
            showsVerticalScrollIndicator={false}
            extraData={this.props}
          /> : undefined
        }
      </View>
    );
  }
}

const mapStateToProps = (state, props) => {
  return ({
    peopleData: state.people.peopleList,
    userId: state.userInfoAPI.userId,
    peopleInfo: state.peopleInfo.peopleInfo[props.navigation.state.params.id],
    userInfo: state.userInfoAPI.userInfo
  })
}

const mapDispatchToProps = dispatch => ({
    peopleApi: bindActionCreators(PeopleApi, dispatch),
    dialogModal: bindActionCreators(DialogModal, dispatch),
    universitiesActions: bindActionCreators(UniversitiesApi, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PeopleFollowerList)
