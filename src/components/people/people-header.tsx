import React, { Component } from 'react';
import { View, Image, TouchableOpacity, Text } from 'react-native';
import { Icon } from 'native-base';
import { NavigationScreenProps } from 'react-navigation';

import { peopleHeaderStyles } from './people-screen-styles';
import { style } from '../../styles/variables';

import { connect } from 'react-redux';

// import MSAButton from '../layouts/buttons/button'
// Need to add status bar height

interface ILoginScreenNavParams {}
interface ILoginScreenProps
  extends NavigationScreenProps<ILoginScreenNavParams> {
  onSearchPress?: Function;
  onFilterPress?: Function;
  onAddPress?: Function;
  title?: string;
  filtersApplied?: boolean;
  profileIconPress?: Function;
  onBackPress?: Function;
  disableExtraButton?: boolean;
  userInfo;
}

class PeopleHeader extends Component<ILoginScreenProps> {
  constructor(props) {
    super(props);
    this.onUserIconPress = this.onUserIconPress.bind(this);
    this.onSearchIconPress = this.onSearchIconPress.bind(this);
    // this.onFilterIconPress = this.onFilterIconPress.bind(this);
  }

  onUserIconPress() {
    this.props.profileIconPress()
  }

  onAddPress() {
    this.props.onAddPress()
  }

  onSearchIconPress() {
    this.props.onSearchPress();
  }

  onBackIconPress() {
    this.props.onBackPress();
  }

  onFilterIconPress() {
    this.props.onFilterPress();
    // this.props.navigation.navigate('UniversityFilters');
    // console.log('Filter Icon Press');
  }

  render() {
    return (
      <View style={peopleHeaderStyles.container}>
        { this.props.onBackPress ?
          <TouchableOpacity
          style={peopleHeaderStyles.leftContainer}
          onPress={() => this.onBackIconPress()}
          >
            <Icon
              name={'md-arrow-back'}
              style={{ fontSize: 30, color: 'rgb(197, 207, 219)'}}
            />
          </TouchableOpacity>
          : <TouchableOpacity
            style={peopleHeaderStyles.leftContainer}
            onPress={() => this.onUserIconPress()}
          >
            <Image
              source={ this.props.userInfo && this.props.userInfo.profilePicture ? {uri: this.props.userInfo.profilePicture}
              : require('../../../assets/images/user_dummy_image.png')}
              style={peopleHeaderStyles.profileIconContainer}
            />
          </TouchableOpacity>
        }
        <View style={[peopleHeaderStyles.leftContainer, { marginLeft: 30 }]} />
        <Text style={peopleHeaderStyles.bodyText}>{this.props.title}</Text>
        { !this.props.disableExtraButton && <View>
          {this.props.onAddPress ?
            <TouchableOpacity
            style={peopleHeaderStyles.rightContainer}
            onPress={() => this.onAddPress()}
            >
              <Icon
                style={{ fontSize: 30, color: 'rgb(197, 207, 219)' }}
                name={'md-add'}
              />
            </TouchableOpacity>
            : <TouchableOpacity
              style={peopleHeaderStyles.rightContainer}
              onPress={() => this.onSearchIconPress()}
            >
              <Icon
                style={{ fontSize: 30, color: 'rgb(197, 207, 219)' }}
                name={'md-search'}
              />
            </TouchableOpacity>
          }
         </View>
        }
        <TouchableOpacity
          style={[peopleHeaderStyles.rightContainer, { marginLeft: 20 }]}
          onPress={() => this.onFilterIconPress()}
        >
          {this.props.filtersApplied ? (
            <Icon
              style={{ fontSize: 30, color: style.color.bloodRed }}
              name={'md-options'}
            />
          ) : (
              <Icon
                style={{ fontSize: 30, color: 'rgb(197, 207, 219)' }}
                name={'md-options'}
              />
          )}
        </TouchableOpacity>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    userInfo: state.userInfoAPI.userInfo
  };
};

export default connect(mapStateToProps, undefined)(PeopleHeader);
