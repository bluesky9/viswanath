import { StyleSheet, Platform } from 'react-native';

import { style } from '../../styles/variables';

export const peopleFiltersStyles = StyleSheet.create({
  container: {
    backgroundColor: style.color.whiteCream,
    flex: 1
  },
  filterItemContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 19,
    paddingBottom: 15,
    borderBottomWidth: 1,
    paddingHorizontal: 15,
    borderColor: style.color.lightGrey,
    backgroundColor: 'white',
    flexDirection: 'row'
  },
  filterTitleText: {
    fontSize: 12,
    lineHeight: 14,
    marginVertical: 4,
    fontFamily: style.font.family.rubikMedium,
    letterSpacing: 1,
    color: style.color.coal
  },
  filterItemText: {
    fontSize: 14,
    lineHeight: 17,
    marginTop: 11,
    color: style.color.primary,
    fontFamily: style.font.family.rubikRegular,
    letterSpacing: 0.2
  },
  iconConatiner: {
    flex: 1,
    alignItems: 'flex-end'
  },
  icon: {
    width: 14,
    height: 14
  },
  filterTitleContainer: {
    flex: 4,
    alignItems: 'flex-start'
  }
});

export const filterHeaderStyles = StyleSheet.create({
  container: {
    paddingTop: 17 + style.header.height,
    paddingBottom: 17,
    paddingHorizontal: 15,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: style.color.lightGrey,
    backgroundColor: style.color.white
  },
  leftItemContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  leftItemText: {
    fontSize: 14,
    lineHeight: 17,
    color: 'rgb(53,71,91)',
    fontFamily: style.font.family.rubikMedium,
    letterSpacing: 0.5
  },
  bodyTextContainer: {
    flex: 3,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white'
  },
  bodyText: {
    color: style.color.bluishGrey,
    fontSize: style.font.size.large,
    fontFamily: style.font.family.rubikRegular,
    lineHeight: 19,
    letterSpacing: 0.2
  },
  rightItemContainer: {
    flex: 1,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  rightItemText: {
    fontSize: 14,
    lineHeight: 17,
    color: 'rgb(53,135,230)',
    fontFamily: style.font.family.rubikMedium,
    letterSpacing: 0.5
  }
});

export const filterUsertypeStyles = StyleSheet.create({
  container: {
    backgroundColor: style.color.whiteCream,
    flex: 1
  },
  mainContent: {
    backgroundColor: 'white',
    paddingBottom: 5
  },
  itemContainer: {
    backgroundColor: 'white',
    paddingVertical: 20,
    marginHorizontal: 15,
    borderBottomWidth: 1,
    borderBottomColor: style.color.lightGrey,
    flexDirection: 'row',
    alignItems: 'center'
  },
  checkboxContainer: {
    height: 20,
    width: 20,
    borderRadius: 7
  },
  itemText: {
    paddingLeft: 10,
    fontSize: 16,
    lineHeight: 21,
    fontFamily: style.font.family.rubikRegular,
    color: style.color.coal
  }
});

export const filterSearchStyles = StyleSheet.create({
  container: {
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 11,
    backgroundColor: style.color.white,
    flexDirection: 'row',
    shadowOffset: {
      height: 2,
      width: -1
    },
    shadowOpacity: 0.1,
    elevation: 1,
    borderRadius: 5
  },
  tagInput: {
    paddingLeft: 10,
    flex: 1,
    fontSize: 14,
    lineHeight: 17,
    height: Platform.OS === 'ios' ? undefined : 15,
    paddingVertical: 0
  },
  iconContainer: {
    height: 16,
    width: 16,
    resizeMode: 'contain'
  }
})

export const filterUniversityStyles = StyleSheet.create({
  container: {
    backgroundColor: style.color.whiteCream,
    flex: 1
  },
  checkboxContainer: {
    height: 20,
    width: 20,
    borderRadius: 7
  },
  itemText: {
    paddingLeft: 10,
    fontSize: 16,
    lineHeight: 21,
    fontFamily: style.font.family.rubikRegular,
    color: style.color.coal
  },
  itemContainer: {
    borderBottomWidth: 1,
    borderBottomColor: style.color.lightGrey,
    paddingVertical: 19,
    flexDirection: 'row',
    alignItems: 'center'
  },
  mainContent: {
    paddingHorizontal: 15,
    paddingVertical: 12,
    borderBottomWidth: 1,
    borderBottomColor: style.color.lightGrey
  }
})