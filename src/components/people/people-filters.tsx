import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { NavigationScreenProps } from 'react-navigation';

import { peopleFiltersStyles } from './people-filter-styles';
// import MSAButton from '../layouts/buttons/button'
import MSAHeader from '../layouts/header/header';
// import PeopleFilterHeader from './people-filter-header';
// import { style } from '../../styles/variables';
import { constants } from '../../constants';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { setPeopleFilters, getPeople, setFilterApplied } from '../../store/actions/People.js'
import  _ from 'lodash';

let initialFilter = {
  countOnly: false,
  pageNo: 0,
  countries: [],
  pageSize: 15,
  univIds: [],
  userIds: [],
  userTypes: []
}

interface IPeopleFilterNavParams {}
interface IPeopleFilterProps
  extends NavigationScreenProps<IPeopleFilterNavParams> {
    peopleFilters;
    setPeopleFilters;
    getPeople;
    alluniversities;
    userId;
    filterApplied;
    setFilterApplied;
  }
interface State {
  univName;
  rightTitle;
  prevFilters;
}

class PeopleFilters extends Component<IPeopleFilterProps, State > {

  constructor(props) {
    super(props);
    this.state = {
      univName: undefined,
      rightTitle: 'APPLY',
      prevFilters: initialFilter
    }
  }

  _handleOnApplyPress = () => {
    if (this.state.rightTitle === 'CLEAR') {
      this.setState({ prevFilters: this.props.peopleFilters })
      this.props.setPeopleFilters(initialFilter);
      let peopleFilters = _.cloneDeep(initialFilter);
      peopleFilters.myUserId = this.props.userId
      this.props.getPeople(peopleFilters);
      this.props.setFilterApplied(false);
      this.props.navigation.goBack();
    } else {
      // console.log(this.props.peopleFilters, 'Check People Filter')
      let peopleFilters = _.cloneDeep(this.props.peopleFilters);
      if (_.isEqual(initialFilter, this.props.peopleFilters)) {
        console.log('Initial Filter')
        peopleFilters.myUserId = this.props.userId
        this.props.getPeople(peopleFilters);
        this.props.setFilterApplied(false);
      } else {
        console.log('Filtered')
        peopleFilters.myUserId = this.props.userId
        this.props.getPeople(peopleFilters);
        this.props.setFilterApplied(true);
      }
      this.props.navigation.goBack();
      // peopleFilters.myUserId = this.props.userId
      // this.props.getPeople(peopleFilters);
      // this.props.navigation.goBack();
    }
  }

  componentDidMount() {
    if (!_.isEmpty(this.props.peopleFilters.univIds)) {
      let univName = [];
      this.props.peopleFilters.univIds.map((id) => {
        let univ = _.find(this.props.alluniversities, univObj => univObj.univ_id === id)
        if (univ) univName.push(univ.name);
      })
      this.setState({ univName });
    }

    // if (this.props.peopleFilters.myUserId) {
    //   initialFilter = _.merge(initialFilter, { myUserId: this.props.peopleFilters.myUserId})
    // }

    if (this.props.filterApplied) {
      this.setState({ rightTitle: 'CLEAR' })
    } else {
      this.setState({ rightTitle: 'APPLY' })
    }

    // if (_.isEqual(initialFilter, this.props.peopleFilters)) {
    //   this.setState({ rightTitle: 'APPLY' })
    // } else {
    //   this.setState({ rightTitle: 'CLEAR' })
    // }

  }

  componentWillReceiveProps(nextProps) {
    if (!_.isEmpty(nextProps.peopleFilters.univIds)) {
      let univName = [];
      nextProps.peopleFilters.univIds.map((id) => {
        let univ = _.find(this.props.alluniversities, univObj => univObj.univ_id === id)
        if (univ) univName.push(univ.name);
      })
      this.setState({ univName });
    }

    // if (_.isEqual(nextProps.peopleFilters, initialFilter)) {
    //   console.log('nextProps.peopleFilters', nextProps.peopleFilters)
    //   this.setState({ rightTitle: 'APPLY' })
    // }

    // if (nextProps.peopleFilters.myUserId) {
    //   initialFilter = _.merge(initialFilter, { myUserId: nextProps.peopleFilters.myUserId})
    // }

    // if (this.props.filterApplied) {
    //   this.setState({ rightTitle: 'CLEAR' })
    // } else {
    //   this.setState({ rightTitle: 'APPLY' })
    // }

    if (nextProps.peopleFilters) {
      this.setState({ rightTitle: 'APPLY' })
    }

    // if (_.isEqual(initialFilter, nextProps.peopleFilters)) {
    //   this.setState({ rightTitle: 'APPLY' })
    // } else {
    //   this.setState({ rightTitle: 'CLEAR' })
    // }
  }

  handleOnBackPress = () => {
    console.log(this.state.prevFilters, '1221', this.state.rightTitle)
    if (this.state.rightTitle === 'APPLY' && this.state.prevFilters) {
      this.props.setPeopleFilters(this.state.prevFilters)
    }
    this.props.navigation.goBack();
  }

  updateUserTypes = (userTypes) => {
    this.setState({ prevFilters: this.props.peopleFilters, rightTitle: 'APPLY' })
    this.props.setPeopleFilters(userTypes)
    console.log(userTypes, 'Yo', this.props.peopleFilters)
  }

  render() {
    console.log('Y1o', this.props.peopleFilters, this.state.prevFilters)
    let peopleFilters = this.props.peopleFilters;
    return (
      <View style={peopleFiltersStyles.container}>
        <MSAHeader
          title={'Filters'}
          rightText={this.state.rightTitle}
          onPress={() => this.props.navigation.goBack()}
          onRightTextPress={() => this._handleOnApplyPress()}
        />
        {/* <PeopleFilterHeader
          navigation={this.props.navigation} title='Filters' rightTitle={this.state.rightTitle} onRightPress={this._handleOnApplyPress}
          onBackPress={this.handleOnBackPress}
        /> */}
        <TouchableOpacity onPress = {() => this.props.navigation.navigate('PeopleFilterUsertype', {
            peopleFilters: this.props.peopleFilters,
            updateUserTypes: this.updateUserTypes
          }
        )}
         style={peopleFiltersStyles.filterItemContainer}>
          <View style={peopleFiltersStyles.filterTitleContainer}>
            <Text style={peopleFiltersStyles.filterTitleText}>
              USER TYPE
            </Text>
            <Text style={peopleFiltersStyles.filterItemText}>
              {peopleFilters.userTypes.length > 0 ?
                peopleFilters.userTypes.map((item, index) => {
                  return (
                    <Text key={index}>
                      <Text>{constants.USERTYPES.PLURAL[item]}</Text>
                      { (peopleFilters.userTypes.length - 1) !== index &&
                          <Text>{', '}</Text>
                        }
                    </Text>
                  )
                })
                : <Text>Everybody</Text>
              }
            </Text>
          </View>
          <View style={peopleFiltersStyles.iconConatiner}>
            <Image source={require('../../../assets/app_icons/Icons/Chevron-right-grey.png')} style={peopleFiltersStyles.icon} />
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress = {() => this.props.navigation.navigate('PeopleFilterUniversity', {
              peopleFilters: this.props.peopleFilters,
              updateUserTypes: this.updateUserTypes
            }
          )}
         style={peopleFiltersStyles.filterItemContainer}>
          <View style={peopleFiltersStyles.filterTitleContainer}>
            <Text style={peopleFiltersStyles.filterTitleText}>
              UNIVERSITY / COLLEGE
            </Text>
            <Text style={peopleFiltersStyles.filterItemText}>
              {peopleFilters.univIds.length > 0  ?
                this.state.univName && this.state.univName.map((item, index) => {
                  return (
                    <Text key={index}>
                      {item}
                      {this.state.univName.length - 1 === index ? '' : ', '}
                    </Text>
                  )
                })
                : <Text>{'All'}</Text>
              }
            </Text>
          </View>
          <View style={peopleFiltersStyles.iconConatiner}>
            <Image source={require('../../../assets/app_icons/Icons/Chevron-right-grey.png')} style={peopleFiltersStyles.icon} />
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress = {() => this.props.navigation.navigate('PeopleFilterCountry', {
              peopleFilters: this.props.peopleFilters,
              updateUserTypes: this.updateUserTypes
            }
        )}
         style={peopleFiltersStyles.filterItemContainer}>
          <View style={peopleFiltersStyles.filterTitleContainer}>
            <Text style={peopleFiltersStyles.filterTitleText}>
              COUNTRY
            </Text>
            <Text style={peopleFiltersStyles.filterItemText}>
              {peopleFilters.countries.length > 0 ?
                peopleFilters.countries.map((item, index) => {
                  return (
                    <Text key={index}>
                      {item}
                      {peopleFilters.countries.length - 1 === index ? '' : ', '}
                    </Text>
                  )
                })
                : <Text>{'All'}</Text>
              }
            </Text>
          </View>
          <View style={peopleFiltersStyles.iconConatiner}>
            <Image source={require('../../../assets/app_icons/Icons/Chevron-right-grey.png')} style={peopleFiltersStyles.icon} />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return ({
    peopleData: state.people.peopleList,
    peopleFilters: state.peopleFilters.filters,
    filterApplied: state.peopleFilters.filterApplied,
    alluniversities: state.alluniversities.universities,
    userId: state.userInfoAPI.userId
  })
}

const mapDispatchToProps = dispatch => ({
    setPeopleFilters: bindActionCreators(setPeopleFilters, dispatch),
    setFilterApplied: bindActionCreators(setFilterApplied, dispatch),
    getPeople: bindActionCreators(getPeople, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PeopleFilters)
