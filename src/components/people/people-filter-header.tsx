import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Icon } from 'native-base';
import { NavigationScreenProps } from 'react-navigation';

import { filterHeaderStyles } from './people-filter-styles';

interface IPeopleFilterNavParams {}
interface IPeopleFilterProps
  extends NavigationScreenProps<IPeopleFilterNavParams> {
    title?: string;
    rightTitle?: string;
    onRightPress?: Function;
    onBackPress?: any;
  }

export default class PeopleFilterHeader extends Component<IPeopleFilterProps> {
  render() {
    return (
      <View style={filterHeaderStyles.container}>
        <TouchableOpacity
          onPress={() => {
            if (this.props.onBackPress) {
              this.props.onBackPress()
            } else {
              this.props.navigation.goBack()
            }
          }}
          style={filterHeaderStyles.leftItemContainer}
        >
          <Icon name='md-arrow-back' style={{ color: 'rgb(207, 208, 220)', fontSize: 20 }} />
        </TouchableOpacity>
        <View style={filterHeaderStyles.bodyTextContainer}>
          <Text style={filterHeaderStyles.bodyText}>{this.props.title}</Text>
        </View>
        <View style={filterHeaderStyles.rightItemContainer}>
          <TouchableOpacity onPress = {() => this.props.onRightPress() }>
            <Text style={filterHeaderStyles.rightItemText}>
              {this.props.rightTitle}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
