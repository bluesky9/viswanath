import React, { Component } from 'react';
import { View, TextInput } from 'react-native';
import { Icon } from 'native-base';

import { filterSearchStyles } from './people-filter-styles';

type Props = {
  placeHolder?: string;
  onTextChange?: Function;
};

export default class PeopleFilterSearch extends Component<Props> {
  componentWillUnmount() {
    if (this.props.onTextChange) {
      this.props.onTextChange('');
    }
  }
  render() {
    return (
      <View style={filterSearchStyles.container}>
        <Icon name='md-search' style={{ color: '#C6D0DC', fontSize: 20 }} />
        <TextInput
          style={filterSearchStyles.tagInput}
          placeholder={this.props.placeHolder}
          underlineColorAndroid={'rgba(0,0,0,0)'}
          onChangeText={text => this.props.onTextChange(text)}
          autoCorrect={false}
          spellCheck={false}
        />
      </View>
    );
  }
}
