import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native';
import { NavigationScreenProps } from 'react-navigation';

import { filterUsertypeStyles, filterUniversityStyles } from './people-filter-styles';
import MSAHeader from '../layouts/header/header';
// import PeopleFilterHeader from './people-filter-header';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { setPeopleFilters, getPeople } from '../../store/actions/People.js'
import { constants } from '../../constants'

import { get, find, cloneDeep, filter } from 'lodash';

// const initialData = [
//   {
//     usertype: 'CURRENT_STUDENT',
//     name: 'Current Student',
//     status: false
//   },
//   {
//     usertype: 'UNIVERSITY_STAFF',
//     name: 'Staff and Administration',
//     status: false
//   },
//   {
//     usertype: 'PROSPECTIVE_STUDENT',
//     name: 'Prospective Student',
//     status: false
//   },
//   {
//     usertype: 'ALUMNI',
//     name: 'Alumni',
//     status: false
//   },
//   {
//     usertype: 'PROFESSOR',
//     name: 'Professor',
//     status: false
//   }
// ]

interface IPeopleFilterNavParams {}
interface IPeopleFilterProps
  extends NavigationScreenProps<IPeopleFilterNavParams> {
    peopleFilters;
    setPeopleFilters;
    getPeople;
    userId;
  }

interface State {
  userTypes;
  rightTitle;
  selectAll;
}

class PeopleFilterUserType extends Component<IPeopleFilterProps, State> {

  constructor(props) {
    super(props);
    this.state = {
      userTypes: undefined,
      rightTitle: undefined,
      selectAll: false
    }
  }

  componentDidMount() {
    // let filteredUserTypes = this.props.peopleFilters.userTypes;
    let filteredPeopleFilters = get(this.props.navigation.state.params, 'peopleFilters', undefined)
    console.log(filteredPeopleFilters, '112')
    let filteredUserTypes = filteredPeopleFilters ? filteredPeopleFilters.userTypes : []

    let userTypes = [];
    Object.keys(constants.USERTYPES.PLURAL).map((item) => {
      userTypes.push({ usertype: item, name: constants.USERTYPES.PLURAL[item]})
    })
    if (filteredUserTypes.length > 0) {
      userTypes.map((item, index) => {
        if (find(filteredUserTypes, obj => obj === item.usertype)) {
          userTypes[index].status = true;
        } else {
          userTypes[index].status = false;
        }
      })
      this.setState({ rightTitle: 'CLEAR' })
    } else {
      this.setState({ selectAll: true })
      this.setState({ rightTitle: 'APPLY' })
    }
    this.setState({ userTypes });
  }

  _handleOnApplyPress = () => {
    // let peopleFilters = cloneDeep(this.props.peopleFilters);
    let peopleFilters = cloneDeep(get(this.props.navigation.state.params, 'peopleFilters', {}));
    let updateUserTypesFn = get(this.props.navigation.state.params, 'updateUserTypes', undefined)

    peopleFilters.userTypes = [];

    if (this.state.rightTitle === 'APPLY') {
      filter(this.state.userTypes, (obj) => {
        if (obj.status)
          return peopleFilters.userTypes.push(obj.usertype);
        else {
          return undefined;
        }
      })
    }

    if (updateUserTypesFn) {
      updateUserTypesFn(peopleFilters)
    }
    this.props.navigation.goBack();
  }

  _renderItem(obj, index) {
    return (
      <TouchableOpacity
        style={filterUsertypeStyles.itemContainer}
        onPress={() => {
          let userTypes = this.state.userTypes;

          if (userTypes[index].status) {
            userTypes[index].status = false
          } else {
            userTypes[index].status = true
          }
          this.setState({ userTypes: userTypes, rightTitle: 'APPLY', selectAll: false })
        }}

        key={index}
      >
        { obj.status ?
          <Image source={require('../../../assets/app_icons/Icons/Checkbox-active.png')} style={filterUsertypeStyles.checkboxContainer}
            resizeMode='stretch'
          />
          : <Image source={require('../../../assets/app_icons/Icons/Checkbox-default.png')} style={filterUsertypeStyles.checkboxContainer}
            resizeMode='stretch'
          />
        }
        <Text style={filterUsertypeStyles.itemText}>{obj.name}</Text>
      </TouchableOpacity>
    )
  }

  render() {
    return (
      <View style={filterUsertypeStyles.container}>
        <MSAHeader
          title={'User type'}
          rightText={this.state.rightTitle}
          onPress={() => this.props.navigation.goBack()}
          onRightTextPress={() => this._handleOnApplyPress()}
        />
        {/* <PeopleFilterHeader
          navigation={this.props.navigation}
          title='User type'
          rightTitle={this.state.rightTitle}
          onRightPress={this._handleOnApplyPress}
        /> */}
        <View style={filterUsertypeStyles.mainContent}>
          <ScrollView>
            <TouchableOpacity style={[filterUniversityStyles.itemContainer, { marginHorizontal: 15 }]}>
              { this.state.selectAll ? (
                <Image
                  source={require('../../../assets/app_icons/Icons/Checkbox-active.png')}
                  style={filterUniversityStyles.checkboxContainer}
                  resizeMode={'stretch'}
                />
              ) : (
                <Image
                  source={require('../../../assets/app_icons/Icons/Checkbox-default.png')}
                  style={filterUniversityStyles.checkboxContainer}
                  resizeMode={'stretch'}
                />
              )}
              <Text style={filterUniversityStyles.itemText}>
                Everybody
              </Text>
            </TouchableOpacity>
            { this.state.userTypes && this.state.userTypes.map((item, index) => {
              return <View key={index}>
                {this._renderItem(item, index)}
                </View>
            })}
          </ScrollView>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return ({
    peopleFilters: state.peopleFilters.filters,
    userId: state.userInfoAPI.userId
  })
}

const mapDispatchToProps = dispatch => ({
    setPeopleFilters: bindActionCreators(setPeopleFilters, dispatch),
    getPeople: bindActionCreators(getPeople, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PeopleFilterUserType)
