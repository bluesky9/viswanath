import React, { Component } from 'react';
import { View, TouchableOpacity, Text, Image } from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import MSAHeader from '../layouts/header/header';

import PeopleFilterSearch from './people-filter-search';
// import PeopleFilterHeader from './people-filter-header';

import { filterUniversityStyles } from './people-filter-styles';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { getCountries } from '../../store/actions/Countries.js';
import { setPeopleFilters, getPeople } from '../../store/actions/People.js';

import { get, isEmpty, cloneDeep, filter, find } from 'lodash';

interface IPeopleFilterNavParams {}
interface IPeopleFilterProps
  extends NavigationScreenProps<IPeopleFilterNavParams> {
  getCountries?: any;
  countries?: Array<Object>;
  setPeopleFilters;
  getPeople;
  peopleFilters;
  userId;
}

interface State {
  stateCountries;
  searchCountries;
  selectAll;
  rightTitle;
}

class PeopleFilterCountry extends Component<IPeopleFilterProps, State> {
  constructor(props) {
    super(props);
    this.state = {
      stateCountries: [],
      searchCountries: undefined,
      selectAll: false,
      rightTitle: undefined
    };
  }

  componentDidMount() {
    this.props.getCountries();
  }

  componentWillReceiveProps(nextProps) {
    let stateCountries = [];
    let peopleFilters = get(this.props.navigation.state.params, 'peopleFilters', undefined)
    let checkStates = peopleFilters ? peopleFilters.countries : [];

    nextProps.countries.map(item => {
      if (find(checkStates, obj => obj === item.countryName)) {
        stateCountries.push({ countryName: item.countryName, status: true });
      } else {
        stateCountries.push({ countryName: item.countryName, status: false });
      }
    });

    if (checkStates.length > 0) {
      this.setState({ rightTitle: 'CLEAR' });
    } else {
      this.setState({ selectAll: true });
      this.setState({ rightTitle: 'APPLY' });
    }
    this.setState({ stateCountries });
  }

  _renderItem(item, index) {
    return (
      <TouchableOpacity
        onPress={() => {
          let stateCountries = this.state.stateCountries;
          if (item.status) {
            stateCountries[index].status = false;
          } else {
            stateCountries[index].status = true;
          }
          this.setState({
            stateCountries: stateCountries,
            rightTitle: 'APPLY',
            selectAll: false
          });
        }}
        key={item.code}
        style={filterUniversityStyles.itemContainer}
      >
        {item.status ? (
          <Image
            source={require('../../../assets/app_icons/Icons/Checkbox-active.png')}
            style={filterUniversityStyles.checkboxContainer}
            resizeMode={'stretch'}
          />
        ) : (
          <Image
            source={require('../../../assets/app_icons/Icons/Checkbox-default.png')}
            style={filterUniversityStyles.checkboxContainer}
            resizeMode={'stretch'}
          />
        )}
        <Text style={filterUniversityStyles.itemText}>
          {item ? item.countryName : ''}
        </Text>
      </TouchableOpacity>
    );
  }

  _handleOnApplyPress = () => {
    let peopleFilters = cloneDeep(get(this.props.navigation.state.params, 'peopleFilters', {undefined}));
    let updateUserTypesFn = get(this.props.navigation.state.params, 'updateUserTypes', undefined)

    peopleFilters.countries = [];

    if (this.state.rightTitle === 'APPLY') {
      filter(this.state.stateCountries, obj => {
        if (obj.status) return peopleFilters.countries.push(obj.countryName);
        else {
          return undefined;
        }
      });
    }

    if (updateUserTypesFn) {
      updateUserTypesFn(peopleFilters)
    }

    this.props.navigation.goBack();
  };

  handleTextChange = text => {
    this.setState({ searchCountries: text });
  };

  render() {
    return (
      <View style={filterUniversityStyles.container}>
        <MSAHeader
          title={'Country'}
          rightText={this.state.rightTitle}
          onPress={() => this.props.navigation.goBack()}
          onRightTextPress={() => this._handleOnApplyPress()}
        />
        {/* <PeopleFilterHeader
          navigation={this.props.navigation}
          title={'Country'}
          rightTitle={this.state.rightTitle}
          onRightPress={this._handleOnApplyPress}
        /> */}
        <View style={filterUniversityStyles.mainContent}>
          <PeopleFilterSearch
            placeHolder={'Search Country'}
            onTextChange={this.handleTextChange}
          />
        </View>
        <View style={{ marginHorizontal: 15, flex: 1 }}>
          {this.props.countries.length > 0 && (
            <KeyboardAwareScrollView>
              <TouchableOpacity style={filterUniversityStyles.itemContainer}>
                {this.state.selectAll ? (
                  <Image
                    source={require('../../../assets/app_icons/Icons/Checkbox-active.png')}
                    style={filterUniversityStyles.checkboxContainer}
                    resizeMode={'stretch'}
                  />
                ) : (
                  <Image
                    source={require('../../../assets/app_icons/Icons/Checkbox-default.png')}
                    style={filterUniversityStyles.checkboxContainer}
                    resizeMode={'stretch'}
                  />
                )}
                <Text style={filterUniversityStyles.itemText}>All</Text>
              </TouchableOpacity>
              {this.state.stateCountries.map((item, index) => {
                if (!isEmpty(this.state.searchCountries)) {
                  if (item.countryName.startsWith(this.state.searchCountries))
                    return (
                      <View key={index}>{this._renderItem(item, index)}</View>
                    );
                  else return undefined;
                } else {
                  return (
                    <View key={index}>{this._renderItem(item, index)}</View>
                  );
                }
              })}
            </KeyboardAwareScrollView>
          )}
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    countries: state.countries.countries,
    peopleFilters: state.peopleFilters.filters,
    userId: get(state.userInfoAPI.userInfo, 'id', undefined)
  };
};

const mapDispatchToProps = dispatch => ({
  getCountries: bindActionCreators(getCountries, dispatch),
  setPeopleFilters: bindActionCreators(setPeopleFilters, dispatch),
  getPeople: bindActionCreators(getPeople, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(
  PeopleFilterCountry
);
