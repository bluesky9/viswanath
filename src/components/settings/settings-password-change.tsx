import React, { Component } from 'react';
import {
  View,
  Text,
  Platform,
  TextInput
} from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
import MSAHeader from '../layouts/header/header';
import settingsStyles from './settings-styles';
import validator from '../../validator/regx-validator';
import { isEmpty } from 'lodash';
import { style } from '../../styles/variables';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { ResetPasswordApi, DialogModal } from '../../store/actions';

interface IPeopleFilterNavParams {}
interface IPeopleFilterProps
  extends NavigationScreenProps<IPeopleFilterNavParams> {
    resetPasswordApi;
    dialogModal;
  }

interface State {
  formValue;
  newPassErr;
  confirmPassErr;
}

class SettingsMain extends Component<IPeopleFilterProps, State> {
  constructor(props) {
    super(props);
    this.state = {
      newPassErr: '',
      confirmPassErr: '',
      formValue: {
        oldPassword: '',
        password: '',
        confirmPassword: ''
      }
    }
  }

  _handleOnTextChange = (text, formField) => {
    let formValue = this.state.formValue
    formValue[formField] = text
    this.setState({ formValue, newPassErr: '', confirmPassErr: '' })
  }

  _onSavePressed = () => {
    if (this.state.formValue.password === this.state.formValue.confirmPassword) {
      this.props.resetPasswordApi.resetPassword(this.state.formValue.oldPassword, this.state.formValue.password)
    } else {
      this.props.dialogModal.dialogShows(
        'Password error', "Your password doesn't match", 'inform', true, undefined, undefined, 'Please try again', 'Try again'
      )
    }
  }

  render() {
    return (
      <View style={{ backgroundColor: 'white' }}>
        <MSAHeader
          title={'Password'}
          rightText={'SAVE'}
          leftText={'CANCEL'}
          onPress={() => this.props.navigation.goBack()}
          onRightTextPress={() => this._onSavePressed()}
        />
        <View
          style={[settingsStyles.accountListBlock]}
        >
          <Text
            style={[
              settingsStyles.boxHead,
              {
                color: 'rgb(83,87,94)',
                marginVertical: 0
              }
            ]}
          >
            CURRENT PASSWORD
          </Text>
          <View style={{ paddingTop: 12 }}>
            <TextInput
              style={[settingsStyles.skillInp, {color: style.color.coal}]}
              placeholder={'Type in your current password'}
              placeholderTextColor={'rgb(153,153,153)'}
              underlineColorAndroid={'rgba(0,0,0,0)'}
              autoCorrect={false}
              spellCheck={false}
              secureTextEntry={true}
              onChangeText={(text) => this._handleOnTextChange(text, 'oldPassword')}
            />
          </View>
        </View>
        <View
          style={[settingsStyles.accountListBlock]}
        >
          <Text
            style={[
              settingsStyles.boxHead,
              {
                color: 'rgb(83,87,94)',
                marginVertical: 0
              }
            ]}
          >
            NEW PASSWORD
          </Text>
          <View style={{ paddingTop: 12 }}>
            <TextInput
              style={[settingsStyles.skillInp, {color: style.color.coal}]}
              placeholder={'Type in a new password'}
              placeholderTextColor={'rgb(153,153,153)'}
              underlineColorAndroid={'rgba(0,0,0,0)'}
              autoCorrect={false}
              spellCheck={false}
              secureTextEntry={true}
              onChangeText={(text) => this._handleOnTextChange(text, 'password')}
              onBlur={() => this.setState({ newPassErr: validator('Password', this.state.formValue.password) }) }
            />
            {!isEmpty(this.state.newPassErr) &&
              <Text
                style={{
                  height: 18,
                  fontSize: 10,
                  fontFamily: style.font.family.rubikRegular,
                  color:
                    this.state.newPassErr === 'Weak Password' ||
                    this.state.newPassErr === 'Improper Password'
                      ? 'rgb(204,20,51)'
                      : this.state.newPassErr === 'Fair Password'
                        ? 'orange'
                        : this.state.newPassErr === 'Strong Password'
                          ? 'green'
                          : 'rgb(204,20,51)',
                  alignSelf: 'flex-start'
                }}
              >{this.state.newPassErr}</Text>
            }
          </View>
        </View>
        <View
          style={[settingsStyles.accountListBlock]}
        >
          <Text
            style={[
              settingsStyles.boxHead,
              { color: 'rgb(83,87,94)', bottom: Platform.OS === 'ios' ? 7 : 0 }
            ]}
          >
            CONFIRM PASSWORD
          </Text>
          <View style={{ paddingTop: 12 }}>
            <TextInput
              style={[settingsStyles.skillInp, {color: style.color.coal}]}
              placeholder={'Retype in a new password'}
              placeholderTextColor={'rgb(153,153,153)'}
              underlineColorAndroid={'rgba(0,0,0,0)'}
              autoCorrect={false}
              spellCheck={false}
              secureTextEntry={true}
              onChangeText={(text) => this._handleOnTextChange(text, 'confirmPassword')}
              onBlur={() => {
                if (this.state.formValue.confirmPassword !== this.state.formValue.password) {
                  this.setState({ confirmPassErr: 'Passwords do not match'})
                }
              }}
            />
            {!isEmpty(this.state.confirmPassErr) &&
              <Text
                style={{
                  height: 18,
                  fontSize: 10,
                  fontFamily: style.font.family.rubikRegular,
                  color: 'rgb(204,20,51)',
                  alignSelf: 'flex-start'
                }}
              >{this.state.confirmPassErr}</Text>
            }
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return state
};

const mapDispatchToProps = dispatch => ({
  resetPasswordApi: bindActionCreators(ResetPasswordApi, dispatch),
  dialogModal: bindActionCreators(DialogModal, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(SettingsMain);