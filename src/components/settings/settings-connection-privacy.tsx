import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  Image,
  FlatList
} from 'react-native';
const { width } = Dimensions.get('window');
import { Content, Container } from 'native-base';
import MSAHeader from '../layouts/header/header';
import { NavigationScreenProps } from 'react-navigation';
import settingsStyles from './settings-styles';
import { isEmpty } from 'lodash';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { PeopleApi, DialogModal } from '../../store/actions';

const UNBLOCK_POST = 'Do you want to unblock this user'
const UNBLOCK_MSG = 'After you unblock, the user will be able to see your posts.'

interface IPeopleFilterNavParams {}
interface IPeopleFilterProps
  extends NavigationScreenProps<IPeopleFilterNavParams> {
    peopleApi;
    blockedPeople;
    dialogModal;
  }
class SettingsMain extends Component<IPeopleFilterProps> {

  componentDidMount() {
    this.props.peopleApi.getBlockedPeople()
  }

  _handleUnBlockPress = (id) => {
    this.props.dialogModal.dialogShows(
      UNBLOCK_POST, UNBLOCK_MSG, 'action', true, () => this.props.peopleApi.unBlockPeople(id)
    )
  }

  renderPeople = (obj) => {
    return (
      <View style={settingsStyles.settingView}>
            <Image
              style={{ height: 41, width: 41, borderRadius: 20.5 }}
              source={obj.profPicPath ? { uri: obj.profPicPath } : require('../../../assets/images/user_dummy_image.png') }
            />
            <Text
              style={[
                settingsStyles.boxLow,
                { marginVertical: 0, marginHorizontal: 15 }
              ]}
            >
              { obj.name }
            </Text>
            <TouchableOpacity
              style={{
                alignSelf: 'center',
                position: 'absolute',
                left: width - 80
              }}
              onPress={() => this._handleUnBlockPress(obj.id)}
            >
              <Text
                style={[
                  settingsStyles.dualHeaderLeftText,
                  { color: 'rgb(204,20,51)' }
                ]}
              >
                UNBLOCK
              </Text>
            </TouchableOpacity>
          </View>
    )
  }

  render() {
    return (
      <Container>
        <MSAHeader
          title={'Connection Privacy'}
          rightText={'DONE'}
          leftText={'CANCEL'}
          onPress={() => this.props.navigation.goBack()}
          onRightTextPress={() => this.props.navigation.goBack()}
        />
        <Content>
          <View style={settingsStyles.settingView}>
            <Text
              style={[
                settingsStyles.titleText,
                { color: 'rgb(83,87,94)' }
              ]}
            >
              BLOCKED USERS
            </Text>
          </View>
          { !isEmpty(this.props.blockedPeople) ?
            <FlatList
              data={this.props.blockedPeople}
              renderItem={obj => this.renderPeople(obj.item)}
              keyExtractor={item => item.id.toString()}
              contentContainerStyle={{  }}
              showsVerticalScrollIndicator={false}
              extraData={this.props}
            />
            : <View />
          }
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = ({ people }) => {
  return {
    blockedPeople: people.blockedPeople
  };
};

const mapDispatchToProps = dispatch => ({
  peopleApi: bindActionCreators(PeopleApi, dispatch),
  dialogModal: bindActionCreators(DialogModal, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(SettingsMain);
