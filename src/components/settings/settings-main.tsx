import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image
} from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
import MSAHeader from '../layouts/header/header';
import settingsStyles from './settings-styles';

interface IPeopleFilterNavParams {}
interface IPeopleFilterProps
  extends NavigationScreenProps<IPeopleFilterNavParams> {}
export default class SettingsMain extends Component<IPeopleFilterProps> {
  render() {
    return (
      <View style={{ backgroundColor: 'white' }}>
        <MSAHeader
          title={'Settings'}
          onPress={() => this.props.navigation.goBack()}
        />
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('SettingsAccount')}
        >
          <View style={settingsStyles.optionListBlock}>
            <Text style={settingsStyles.settingListItem}>Account</Text>
            <Image
              source={require('../../../assets/app_icons/Icons/arrow-right-default.png')}
            />
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() =>
            this.props.navigation.navigate('SettingsPasswordChange')
          }
        >
          <View style={settingsStyles.optionListBlock}>
            <Text style={settingsStyles.settingListItem}>Change Password</Text>
            <Image
              source={require('../../../assets/app_icons/Icons/arrow-right-default.png')}
            />
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('SettingsPrivacy')}
        >
          <View style={settingsStyles.optionListBlock}>
            <Text style={settingsStyles.settingListItem}>
              Connection Privacy
            </Text>
            <Image
              source={require('../../../assets/app_icons/Icons/arrow-right-default.png')}
            />
          </View>
        </TouchableOpacity>
        <TouchableOpacity>
          <View style={settingsStyles.optionListBlock}>
            <Text style={settingsStyles.settingListItem}>Share Experience</Text>
            <Image
              source={require('../../../assets/app_icons/Icons/arrow-right-default.png')}
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
