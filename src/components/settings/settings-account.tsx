import React, { Component } from 'react';
import {
  View,
  Text,
  TextInput
} from 'react-native';
import { NavigationScreenProps } from 'react-navigation';

import { get, cloneDeep, isEqual } from 'lodash';
import MSAHeader from '../layouts/header/header';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
  saveBasicProfileDetails
} from '../../store/actions/Profile';

import settingsStyles from './settings-styles';
interface IPeopleFilterNavParams {}
interface IPeopleFilterProps
  extends NavigationScreenProps<IPeopleFilterNavParams> {
    userInfo: {
      profilePic: string;
    };
    saveUserInfo: Function;
  }

  interface State {
    firstName: string;
    lastName: string;
    website: string;
    bio: string;
    country: string;
  }
class SettingsMain extends Component<IPeopleFilterProps, State> {
  prevDetails: any;
  constructor(props) {
    super(props);
    this.state = {
      firstName: get(props, 'userInfo.firstName', ''),
      lastName: get(props, 'userInfo.lastName', ''),
      bio: get(props, 'userInfo.bio', ''),
      country: get(props, 'userInfo.country', ''),
      website: get(props, 'userInfo.website', '')
    }
    this.prevDetails = cloneDeep(this.state)
  }
  changedText(text, field) {
    this.setState({[field]: text});
  }
  onSavePressed = () => {
    let userInfo = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      bio: this.state.bio,
      country: this.state.country,
      website: this.state.website
    }
    this.props.saveUserInfo(userInfo)
  }
  render() {
    return (
      <View style={{ backgroundColor: 'white' }}>
        <MSAHeader
          title={'Account'}
          rightText={'SAVE'}
          leftText={'CANCEL'}
          onPress={() => this.props.navigation.goBack()}
          onRightTextPress={() => this.onSavePressed()}
          disableRightText={isEqual(this.state, this.prevDetails)}
        />
        <View
          style={[settingsStyles.accountListBlock]}
        >
          <Text
            style={[
              settingsStyles.boxHead,
              {
                color: 'rgb(83,87,94)',
                marginVertical: 0
              }
            ]}
          >
            FIRST NAME
          </Text>
          <View style={{ paddingTop: 12 }}>
            <TextInput
              style={[settingsStyles.skillInp, { color: 'rgb(83,87,94)'}]}
              placeholder={'First Name'}
              value={this.state.firstName}
              placeholderTextColor={'rgb(83,87,94)'}
              underlineColorAndroid={'rgba(0,0,0,0)'}
              onChangeText={(text) => this.changedText(text, 'firstName')}
              maxLength={25}
            />
          </View>
        </View>
        <View
          style={[settingsStyles.accountListBlock]}
        >
          <Text
            style={[
              settingsStyles.boxHead,
              {
                color: 'rgb(83,87,94)',
                marginVertical: 0
              }
            ]}
          >
            LAST NAME
          </Text>
          <View style={{ paddingTop: 12 }}>
            <TextInput
              style={[settingsStyles.skillInp, { color: 'rgb(83,87,94)'}]}
              placeholder={'Last Name'}
              value={this.state.lastName}
              placeholderTextColor={'rgb(83,87,94)'}
              underlineColorAndroid={'rgba(0,0,0,0)'}
              onChangeText={(text) => this.changedText(text, 'lastName')}
              maxLength={25}
            />
          </View>
        </View>
        {/* <View
          style={[settingsStyles.accountListBlock]}
        >
          <Text
            style={[
              settingsStyles.boxHead,
              {
                color: 'rgb(83,87,94)',
                marginVertical: 0
              }
            ]}
          >
            E-MAIL
          </Text>
          <View style={{paddingTop: 12}}>
            <TextInput
              style={settingsStyles.skillInp}
              placeholder={'jasbir@geekyants.com'}
              placeholderTextColor={'rgb(83,87,94)'}
              underlineColorAndroid={'rgba(0,0,0,0)'}
            />
          </View>
        </View> */}
        {/* <View
          style={[settingsStyles.accountListBlock]}
        >
          <Text
            style={[
              settingsStyles.boxHead,
              {
                color: 'rgb(83,87,94)',
                marginVertical: 0
              }
            ]}
          >
            USER TYPE
          </Text>
          <View style={{paddingTop: 12}}>
            <TextInput
              style={settingsStyles.skillInp}
              placeholder={'Student Applicant'}
              placeholderTextColor={'rgb(83,87,94)'}
              underlineColorAndroid={'rgba(0,0,0,0)'}
            />
          </View>
        </View> */}
        <View
          style={[settingsStyles.accountListBlock]}
        >
          <Text
            style={[
              settingsStyles.boxHead,
              {
                color: 'rgb(83,87,94)',
                marginVertical: 0
              }
            ]}
          >
            COUNTRY
          </Text>
          <View style={{ paddingTop: 12 }}>
            <Text
              style={[settingsStyles.skillInp, {color: 'rgb(83,87,94)'}]}
            >{this.state.country}</Text>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    userInfo: state.userInfoAPI.userInfo
  };
};

const mapDispatchToProps = dispatch => ({
  saveUserInfo: bindActionCreators(saveBasicProfileDetails, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(SettingsMain);
