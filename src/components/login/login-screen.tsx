import React, { Component } from 'react';
import { View, Image, TouchableOpacity, Text } from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
// import { Content, Container } from 'native-base';
import JInput from '../layouts/input/input';
import Validator from '../../validator/regx-validator';
import * as NavigationActions from '../../NavigationService'

import MSAButton from '../layouts/buttons/button';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { login } from '../../store/actions/Login.js';
import { style } from '../../styles/variables';

import LoginScreenStyles from './login-screen-styles';
interface ILoginScreenProps extends NavigationScreenProps<{}> {
  actions: any;
  loginApiState: any;
}
interface ILoginScreenState {
  formValue;
  emailError: string;
  passErr: string;
}
class LoginScreen extends Component<ILoginScreenProps, ILoginScreenState> {
  secondTextInput;
  constructor(props) {
    super(props);
    this.state = {
      formValue: {
        Email: '',
        Password: ''
      },
      emailError: '',
      passErr: ''
    };
  }

  _handleErrorPress() {
    console.log('Modal Button Pressed');
  }

  _handleOnChangeText = (text, formField) => {
    const formValue = this.state.formValue;
    formValue[formField] = text;

    if (this.state.formValue !== formValue) {
      this.setState({ formValue });
    }

    if (formField === 'Email') {
      this.setState({ emailError: '' });
    }
    if (formField === 'Password') {
      this.setState({ passErr: '' });
    }

  };

  _handleLoginPress() {
    this.props.actions(
      this.state.formValue.Email,
      this.state.formValue.Password
    );
    // this.props.navigation.navigate('HomeFeed')
  }

  _handleOnBlur = (formField) => {
    const formValue = this.state.formValue;
    formValue[formField] = this.state.formValue.Email.replace(' ', '');
    this.setState({ formValue })
  }

  setRef = (refs) => {
    this.secondTextInput = refs;
  }

  render() {
    return (
      <View style={LoginScreenStyles.container}>
        <View style={{flex: 1, alignItems: 'center'}}>
          <View style={LoginScreenStyles.iconContainer}>
            <Image
              style={{ width: 97, height: 30 }}
              source={require('../../../assets/images/main_icon.png')}
            />
          </View>
          <Text style={LoginScreenStyles.title}>
            Welcome back!
          </Text>
          <View style={LoginScreenStyles.inputContainer}>
            <JInput
              style={{marginBottom: 15}}
              placeholder={'Email'}
              error={this.state.emailError}
              onSubmit={() => { if (this.secondTextInput) this.secondTextInput.focus()}}
              keyboard={'email-address'}
              changeText={text => this._handleOnChangeText(text.trim(), 'Email')}
              onFocusLost={() => this._handleOnBlur('Email')}
              returnKey={'next'}
            />
            <JInput
              inputRef={this.setRef}
              style={{ marginBottom: 15 }}
              placeholder={'Password'}
              secure={true}
              error={this.state.passErr}
              changeText={text => {
                this._handleOnChangeText(text, 'Password');
                // this.setState({
                //   passErr: text === '' ? '' : Validator('Password', text)
                // })
              }}
            />
          </View>
          <View style={{paddingHorizontal: 15}}>
            <MSAButton
              type={'primary'}
              label={'Login'}
              containerWidth={'100%'}
              enableShadow={true}
              textSize={style.font.size.large}
              textLineHeight={style.font.lineHeight.normal.large}
              textFamily={style.font.family.rubikRegular}
              onPress={() => {
                if (this.state.formValue.Email !== '') {
                  Validator('Email', this.state.formValue.Email) === ''
                    ? this._handleLoginPress()
                    : this.setState({
                        emailError: Validator('Email', this.state.formValue.Email)
                      });
                } else {
                  this.setState({
                    emailError: 'Required',
                    passErr: 'Required'
                  });
                }
              }}
            />
          </View>
          <View style={{marginTop: 15}}>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('LoginForgotPassword')
              }
            >
              <Text style={LoginScreenStyles.forgotPasswordText}>
                Forgot password
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={LoginScreenStyles.lowerView}>
          <Text style={LoginScreenStyles.noAccountText}>No account?</Text>
          <TouchableOpacity
            onPress={() => NavigationActions.replace('SignUpScreen', {})}
          >
            <Text style={LoginScreenStyles.signupText}>Sign up!</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    loginApiState: state.signupApi
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(login, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
