import { StyleSheet, Dimensions } from 'react-native';
import { style } from '../../styles/variables';

const { width } = Dimensions.get('window');

const loginScreenStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center'
  },
  iconContainer: {
    paddingTop: style.header.height + 21,
    paddingBottom: 42
  },
  title: {
    textAlign: 'center',
    fontSize: 24,
    fontFamily: style.font.family.ptSerif,
    color: style.color.nero,
    lineHeight: 30
  },
  launchIcon: {
    width: 97,
    height: 30
  },
  welcomeText: {
    alignSelf: 'center',
    fontSize: 24,
    fontFamily: style.font.family.ptSerif,
    color: 'rgb(59,62,68)'
  },
  inputContainer: {
    alignSelf: 'stretch',
    paddingHorizontal: 15,
    paddingTop: 29,
    paddingBottom: 5
  },
  forgotPasswordText: {
    alignSelf: 'center',
    fontSize: style.font.size.normal,
    lineHeight: 17,
    color: style.color.grey,
    fontFamily: style.font.family.rubikRegular
  },
  lowerView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 35,
    paddingTop: 15
  },
  noAccountText: {
    fontSize: style.font.size.normal,
    lineHeight: 17,
    color: style.color.grey,
    fontFamily: style.font.family.rubikRegular
  },
  signupText: {
    fontSize: style.font.size.normal,
    lineHeight: 17,
    color: style.color.skyBlue,
    fontFamily: style.font.family.rubikRegular,
    marginLeft: 3
  },
  headText: {
    textAlign: 'center',
    width: width - 140,
    fontSize: 24,
    lineHeight: 30,
    fontFamily: style.font.family.ptSerif,
    fontWeight: '300',
    color: 'rgb(59,62,68)',
    marginTop: 29
  },
  instructionText: {
    textAlign: 'center',
    width: width - 140,
    color: 'rgb(83,87,94)',
    fontSize: style.font.size.normal,
    lineHeight: style.font.lineHeight.normal.normal,
    fontFamily: style.font.family.rubikRegular
  },
  lowerViewForgot: {
    borderTopWidth: 2,
    paddingVertical: 32,
    paddingHorizontal: 15,
    width: '100%',
    borderColor: style.color.lightGrey,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row'
  },
  noAccountForgotText: {
    fontSize: 12,
    lineHeight: 14,
    color: style.color.grey
  },
  loginLowerText: {
    fontSize: 12,
    lineHeight: 14,
    color: style.color.skyBlue,
    marginLeft: 2
  }
});

export default loginScreenStyles;
