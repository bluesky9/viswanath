import React, { Component } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import { NavigationScreenProps, NavigationActions } from 'react-navigation';
import MSAButton from '../layouts/buttons/button';
import { style } from '../../styles/variables';

import MSAHeader from '../layouts/header/header';

import LoginPasswordResetStyles from './login-screen-styles';

interface ILoginScreenProps extends NavigationScreenProps<{}> {}
interface ILoginScreenState {}

export default class LaunchScreen extends Component<
  ILoginScreenProps,
  ILoginScreenState
> {
  resetLoginStack() {
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName: 'LoginScreen'
        })
      ]
    });
    this.props.navigation.dispatch(resetAction);
  }

  handleBackPress() {
    this.props.navigation.goBack();
  }

  render() {
    return (
      <View style={LoginPasswordResetStyles.container}>
        <MSAHeader
          title={'Password Reset'}
          onPress={this.handleBackPress.bind(this)}
        />
        <View style={{flex: 1, alignItems: 'center'}}>
          <Text style={LoginPasswordResetStyles.headText}>
            Reset instructions sent to your email
          </Text>
          <Text style={[LoginPasswordResetStyles.instructionText, { marginTop: 24 }]}>
            We have sent you an email. Click the link and follow the instructions
          </Text>
          <View style={{ marginTop: 14, paddingHorizontal: 15 }}>
            <MSAButton
              type={'primary'}
              label={'Go to login page'}
              containerWidth={'100%'}
              enableShadow={true}
              textSize={style.font.size.large}
              textLineHeight={style.font.lineHeight.normal.large}
              textFamily={style.font.family.rubikRegular}
              onPress={() => this.resetLoginStack()}
            />
          </View>
        </View>
        <View style={LoginPasswordResetStyles.lowerViewForgot}>
          <Text style={LoginPasswordResetStyles.noAccountForgotText}>
            Did not receive email?
          </Text>
          <TouchableOpacity
            onPress={() => this.props.navigation.goBack()}
          >
            <Text style={LoginPasswordResetStyles.loginLowerText}>Resend</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
