import React, { Component } from 'react';
import { View, Text, Dimensions } from 'react-native';
const { width } = Dimensions.get('window');
import { NavigationScreenProps, NavigationActions } from 'react-navigation';
import LoginPasswordChangedStyles from './login-screen-styles';
import MSAHeader from '../layouts/header/header';
import MSAButton from '../layouts/buttons/button';
import { style } from '../../styles/variables';

interface ILoginScreenProps extends NavigationScreenProps<{}> {}
interface ILoginScreenState {}
export default class LaunchScreen extends Component<
  ILoginScreenProps,
  ILoginScreenState
> {
  resetLoginStack() {
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName: 'LoginScreen'
        })
      ]
    });
    this.props.navigation.dispatch(resetAction);
  }
  render() {
    return (
      <View style={LoginPasswordChangedStyles.container}>
        <MSAHeader
          title={'Password Changed'}
          onPress={() => this.props.navigation.goBack()}
        />
        <Text
          style={[
            LoginPasswordChangedStyles.headText,
            { width: width - 120 }
          ]}
        >
          Your password has been changed
        </Text>
        <Text style={[LoginPasswordChangedStyles.instructionText, { marginTop: 24 }]}>
          Use your new password to log In to Uple
        </Text>
        <View style={{ marginTop: 19, paddingHorizontal: 15 }}>
          <MSAButton
            type={'primary'}
            label={'Go to login page'}
            containerWidth={'100%'}
            enableShadow={true}
            onPress={() => this.resetLoginStack()}
            textSize={style.font.size.large}
            textLineHeight={style.font.lineHeight.normal.large}
            textFamily={style.font.family.rubikRegular}
          />
        </View>
      </View>
    );
  }
}
