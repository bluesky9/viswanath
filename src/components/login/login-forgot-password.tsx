import React, { Component } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import validator from '../../validator/regx-validator';
import JInput from '../layouts/input/input';
import { NavigationScreenProps, NavigationActions } from 'react-navigation';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {forgotPassword} from '../../store/actions/ForgotPassword.js'
import { style } from '../../styles/variables';
import MSAButton from '../layouts/buttons/button';
import MSAHeader from '../layouts/header/header';
import ForgotPasswordStyles from './login-screen-styles';

interface ILoginScreenProps extends NavigationScreenProps<{}> {
  actions: any;
}
interface ILoginScreenState {
  emailErr: string;
  email: string;
}
class LoginScreen extends Component<
  ILoginScreenProps,
  ILoginScreenState
> {
  constructor(props) {
    super(props);
    this.state = {
      emailErr: '',
      email: ''
    };
  }
  resetLoginStack() {
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'LoginScreen' })]
    });
    this.props.navigation.dispatch(resetAction);
  }

  handleBackPress() {
    this.props.navigation.goBack();
  }

  _handleOnChangeText(text) {
    let email = text.replace(' ', '');
    this.setState({ email, emailErr: '' });
  }

  onResetPasswordPressed = () => {
    this.props.actions(this.state.email);
  }

  render() {
    return (
      <View style={ForgotPasswordStyles.container}>
        <MSAHeader
          title={'Forgot Password'}
          onPress={this.handleBackPress.bind(this)}
        />
        <View style={{flex: 1, alignItems: 'center'}}>
          <Text style={ForgotPasswordStyles.headText}>
            Forgot your password?
          </Text>
          <Text style={[ForgotPasswordStyles.instructionText, { marginTop: 24 }]}>
            Enter your email below to receive your password reset instructions
          </Text>
          <View style={{alignSelf: 'stretch', marginTop: 32, paddingHorizontal: 15}}>
            <JInput
              placeholder={'Email address'}
              changeText={(text) => this._handleOnChangeText(text)}
              error={this.state.emailErr}
            />
          </View>
          <View style={{ marginTop: 20, paddingHorizontal: 15 }}>
            <MSAButton
              type={'primary'}
              label={'Send Instructions'}
              containerWidth={'100%'}
              enableShadow={true}
              textSize={style.font.size.large}
              textLineHeight={style.font.lineHeight.normal.large}
              textFamily={style.font.family.rubikRegular}
              onPress={() => {
                if (this.state.email !== '') {
                  validator('Email', this.state.email) === ''
                    ? this.onResetPasswordPressed()
                    : this.setState({
                        emailErr: validator('Email', this.state.email)
                      });
                } else {
                  this.setState({ emailErr: 'Required' });
                }
              }}
            />
          </View>
        </View>
        <View style={ForgotPasswordStyles.lowerViewForgot}>
          <Text style={ForgotPasswordStyles.noAccountForgotText}>
            Already have an account?
          </Text>
          <TouchableOpacity onPress={() => this.resetLoginStack()}>
            <Text style={ForgotPasswordStyles.loginLowerText}>Log in!</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return ({
    singupApiState: state.signupApi
  })
}

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(forgotPassword, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginScreen)
