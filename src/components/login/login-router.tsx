import { StackNavigator } from 'react-navigation';
import LoginScreen from './login-screen';
import LoginPasswordReset from './login-password-reset';
import LoginPasswordChanged from './login-password-changed';
import LoginNewPassword from './login-new-password';
import LoginForgotPassword from './login-forgot-password';
import HomeFeed from '../home/home-main-feed';
import PostComment from '../home/home-post-with-comments';
import LandingScreen from '../landing/landing-screen';
import SignUpScreen from '../signup/signup-screen';
import SignUpSuccess from '../signup/signup-success';
import SignUpUserDetails from '../signup/signup-user-details';
import ComposePost from '../home/home-post-compose';
import EditPost from '../home/home-post-edit';
import TagPeople from '../home/home-post-tag-people';
import PostAudience from '../home/home-post-audience';
import PostLikes from '../home/home-post-likes';
import PeopleScreen from '../people/people-screen';
import PeopleFilters from '../people/people-filters';
import PeopleFilterUniversity from '../people/people-filter-university';
import PeopleFilterCountry from '../people/people-filter-country';
import PeopleFilterUsertype from '../people/people-filter-usertype';
import PeopleFollowerList from '../people/people-follower-list';
import MyProfile from '../profile/profile-myprofile';
import ProfileSkills from '../profile/profile-skills';
import ProfileInterests from '../profile/profile-interests';
import ProfileEdit from '../profile/profile-edit';
import ProfileEducationHist from '../profile/profile-education-history';
import ProfileNewEducation from '../profile/profile-new-education';
import ProfileExperience from '../profile/profile-experience';
import ProfileNewExperience from '../profile/profile-new-experience';
import ProfileMembership from '../profile/profile-memberships';
import ProfileNewMembership from '../profile/profile-new-membership';
import ProfileProjects from '../profile/profile-projects';
import ProfileNewProject from '../profile/profile-new-project';
import ProfileAward from '../profile/profile-awards';
import ProfileNewAward from '../profile/profile-awards-new';
import ProfilePeople from '../profile/profile-people';
import University from '../university/university';
import UniversityProfile from '../university/university-profile';
import UniversityFilters from '../university/university-filters';
import UniversityPostWithComments from '../university/university-post-with-comments';
import UniversityPostCompose from '../university/university-post-compose';
import UniversityAudience from '../university/university-post-audience';
import UniversityTag from '../university/university-tag-connections';
import UniversityPostCountryFilter from '../university/university-post-country-filters';
import Messaging from '../messaging/messaging';
import MessagingAddPeople from '../messaging/messaging-add-people';
import MessagingChatInformation from '../messaging/messaging-chat-information';
import MessagingSingle from '../messaging/messaging-single';
import SettingsMain from '../settings/settings-main';
import SettingsAccount from '../settings/settings-account';
import SettingsPrivacy from '../settings/settings-connection-privacy';
import SettingsPasswordChange from '../settings/settings-password-change';
import MessagingCreateGroup from '../messaging/messaging-create-group';
import UniversityPostEdit from '../university/university-post-edit';
import UniversityDepartments from '../university/university-departments';
import UniversityPeople from '../university/university-people';
import Department from '../university/department/department';
import DepartmentMajor from '../university/department/department-major';
import UniversityPostMajorFilter from '../university/university-post-major-filters';
import DepartmentPostWithComments from '../university/department/department-post-with-comments';
import PostShare from '../home/home-post-share';
import DepartmentCreate from '../university/department/department-create';
import Invite from '../invite/invite';
import DepartmentCreateStep2 from '../university/department/department-create-step2';
import DepartmentCreateStep3 from '../university/department/department-create-step3';
import InviteContacts from '../invite/invite-contacts';
import DepartmentCreateNewMajor from '../university/department/department-create-new-major';
import DepartmentCreateAddAdministrator from '../university/department/department-create-add-administrator';
import DepartmentCreateSelectLocation from '../university/department/department-create-select-location';
import DepartmentCreateTiming from '../university/department/department-create-timing';
import DepartmentCreateSearchableTags from '../university/department/department-create-searchable-tags';
import DepartmentEditMajor from '../university/department/department-edit-major';

import Notifications from '../notification/notification';

const loginRouter = StackNavigator(
  {
    LandingScreen: { screen: LandingScreen },
    LoginScreen: { screen: LoginScreen },
    LoginPasswordReset: { screen: LoginPasswordReset },
    LoginNewPassword: { screen: LoginNewPassword },
    LoginPasswordChanged: { screen: LoginPasswordChanged },
    LoginForgotPassword: { screen: LoginForgotPassword },
    HomeFeed: { screen: HomeFeed },
    PostComment: { screen: PostComment },
    SignUpScreen: { screen: SignUpScreen },
    SignUpSuccess: { screen: SignUpSuccess },
    SignUpUserDetails: { screen: SignUpUserDetails },
    ComposePost: { screen: ComposePost },
    EditPost: { screen: EditPost },
    TagPeople: { screen: TagPeople },
    PostAudience: { screen: PostAudience },
    PostLikes: { screen: PostLikes },
    PeopleScreen: { screen: PeopleScreen },
    PeopleFilters: { screen: PeopleFilters },
    PeopleFilterUniversity: { screen: PeopleFilterUniversity },
    PeopleFilterCountry: { screen: PeopleFilterCountry },
    PeopleFilterUsertype: { screen: PeopleFilterUsertype },
    PeopleFollowerList: { screen: PeopleFollowerList },
    MyProfile: { screen: MyProfile },
    ProfileSkills: { screen: ProfileSkills },
    ProfileEdit: { screen: ProfileEdit },
    ProfileEducationHist: { screen: ProfileEducationHist },
    ProfilePeople: { screen: ProfilePeople },
    University: { screen: University },
    UniversityFilters: { screen: UniversityFilters },
    UniversityProfile: { screen: UniversityProfile },
    Messaging: { screen: Messaging },
    MessagingAddPeople: { screen: MessagingAddPeople },
    MessagingChatInformation: { screen: MessagingChatInformation },
    MessagingSingle: { screen: MessagingSingle },
    ProfileNewEducation: { screen: ProfileNewEducation },
    SettingsMain: { screen: SettingsMain },
    SettingsAccount: { screen: SettingsAccount },
    SettingsPrivacy: { screen: SettingsPrivacy },
    SettingsPasswordChange: { screen: SettingsPasswordChange },
    MessagingCreateGroup: { screen: MessagingCreateGroup },
    Notifications: { screen: Notifications },
    ProfileInterests: { screen: ProfileInterests },
    ProfileExperience: { screen: ProfileExperience },
    ProfileNewExperience: { screen: ProfileNewExperience },
    ProfileMembership: { screen: ProfileMembership },
    ProfileNewMembership: { screen: ProfileNewMembership },
    ProfileProject: { screen: ProfileProjects },
    ProfileNewProject: { screen: ProfileNewProject },
    ProfileAward: { screen: ProfileAward },
    ProfileNewAward: { screen: ProfileNewAward },
    UniversityPostWithComments: { screen: UniversityPostWithComments },
    UniversityPostCompose: { screen: UniversityPostCompose },
    UniversityAudience: { screen: UniversityAudience },
    UniversityTag: { screen: UniversityTag },
    UniversityPostCountryFilter: { screen: UniversityPostCountryFilter },
    UniversityPostEdit: { screen: UniversityPostEdit },
    UniversityDepartments: { screen: UniversityDepartments},
    UniversityPeople: { screen: UniversityPeople},
    Department: { screen: Department},
    DepartmentMajor: { screen: DepartmentMajor},
    UniversityPostMajorFilter: { screen: UniversityPostMajorFilter},
    DepartmentPostWithComments: { screen: DepartmentPostWithComments},
    PostShare: { screen: PostShare},
    DepartmentCreate: { screen: DepartmentCreate},
    Invite: { screen: Invite },
    DepartmentCreateStep2: { screen: DepartmentCreateStep2 },
    DepartmentCreateStep3: { screen: DepartmentCreateStep3 },
    InviteContacts: { screen: InviteContacts },
    DepartmentCreateNewMajor: { screen: DepartmentCreateNewMajor },
    DepartmentCreateAddAdministrator: { screen: DepartmentCreateAddAdministrator },
    DepartmentCreateSelectLocation: { screen: DepartmentCreateSelectLocation },
    DepartmentCreateTiming: { screen: DepartmentCreateTiming },
    DepartmentCreateSearchableTags: { screen: DepartmentCreateSearchableTags },
    DepartmentEditMajor: { screen: DepartmentEditMajor }
  },
  {
    headerMode: 'none'
  }
);
export default loginRouter;
