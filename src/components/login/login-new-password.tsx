import React, { Component } from 'react';
import { View, Text, Dimensions } from 'react-native';
const { width } = Dimensions.get('window');
import validator from '../../validator/regx-validator';
import JInput from '../layouts/input/input';
import { NavigationScreenProps } from 'react-navigation';
import MSAButton from '../layouts/buttons/button';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {forgotPasswordReset} from '../../store/actions/ForgotPassword.js';
import { style } from '../../styles/variables';
import NewPasswordStyles from './login-screen-styles';
import MSAHeader from '../layouts/header/header';

interface ILoginScreenProps extends NavigationScreenProps<{
  token: string
}> {
  forgotPasswordReset: Function
}
interface ILoginScreenState {
  passErr: string;
  pass: string;
  passRepErr: string;
}
class LaunchScreen extends Component<
  ILoginScreenProps,
  ILoginScreenState
> {
  constructor(props) {
    super(props);
    this.state = {
      pass: '',
      passErr: '',
      passRepErr: ''
    };
  }
  componentDidMount() {
    console.log(this.props.navigation.state.params)
  }
  handleBackPress() {
    const navigate = this.props.navigation as any;
    navigate.replace('LoginScreen', {});
  }
  render() {
    return (
      <View style={NewPasswordStyles.container}>
        <MSAHeader
          title={'New Password'}
          onPress={this.handleBackPress.bind(this)}
        />
        <Text
          style={[NewPasswordStyles.headText, { width: width - 120 }]}
        >
          Create New Password
        </Text>
        <View style={{paddingHorizontal: 15, alignSelf: 'stretch'}}>
          <JInput
            style={{ marginTop: 32, marginBottom: 15 }}
            placeholder={'New password, minimum 8 characters'}
            changeText={txt =>
              this.setState({
                passErr: txt !== '' ? validator('Password', txt) : '',
                pass: txt
              })
            }
            secure={true}
            error={this.state.passErr}
          />
          <JInput
            placeholder={'Repeat password'}
            changeText={txt =>
              this.setState({
                passRepErr:
                  this.state.pass === txt ? '' : txt === '' ? '' : 'No Match'
              })
            }
            secure={true}
            error={this.state.passRepErr}
          />
        </View>
        <View style={{ paddingHorizontal: 15, marginTop: 22 }}>
          <MSAButton
            type={'primary'}
            label={'Change password'}
            containerWidth={'100%'}
            enableShadow={true}
            textSize={style.font.size.large}
            textLineHeight={style.font.lineHeight.normal.large}
            textFamily={style.font.family.rubikRegular}
            onPress={() =>
              this.props.forgotPasswordReset(this.state.pass, this.props.navigation.state.params)
            }
          />
        </View>
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  forgotPasswordReset: bindActionCreators(forgotPasswordReset, dispatch)
})

export default connect(
  undefined,
  mapDispatchToProps
)(LaunchScreen)
