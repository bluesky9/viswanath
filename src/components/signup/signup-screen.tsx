import React, { Component } from 'react';
import { View, Text, Linking } from 'react-native';

import { NavigationScreenProps } from 'react-navigation';
import JInput from '../layouts/input/input';
import validator from '../../validator/regx-validator';
// import DatePicker from 'react-native-datepicker';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { style } from '../../styles/variables';

import MSAButton from '../layouts/buttons/button';
import MSAHeader from '../layouts/header/header';
import signUpScreenStyles from './signup-screen-styles';
import * as NavigationActions from '../../NavigationService'

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { get } from 'lodash';

import {signup} from '../../store/actions/Signup.js';

interface ISignUpScreenProps extends NavigationScreenProps<{}> {
  singupApiState: any;
  actions: any;
  routes;
}
interface ISignUpScreenState {
  formValue;
  repeatPassword: string;
  passErr: string;
  emailErr: string;
  passRepErr: string;
}

class SignUpScreen extends Component<
  ISignUpScreenProps,
  ISignUpScreenState
> {
  secondTextInput;
  constructor(props) {
    super(props);
    this.state = {
      formValue: {
        BirthDate: 'Date of Birth',
        Email: undefined,
        Password: undefined,
        birthDay: undefined,
        birthMonth: undefined,
        birthYear: undefined,
        userType: 'PROSPECTIVE_STUDENT'
      },
      repeatPassword: '',
      passErr: '',
      emailErr: '',
      passRepErr: ''
    };
  }

  handleBackPress() {
    this.props.navigation.goBack();
  }

  _handleSignUpPress() {

    // this.props.navigation.navigate('SignUpUserDetails');

    // Need to move this inside handleOnChangeText Function

    const formValue = this.state.formValue;

    if (this.state.formValue.BirthDate) {
      const birthDateArray = formValue.BirthDate.split('/');
      formValue.birthYear = birthDateArray[2];
      formValue.birthMonth = birthDateArray[0];
      formValue.birthDay = birthDateArray[1];
    }

    if (this.state.formValue !== formValue) {
      this.setState({ formValue });
    }

    if (!this.state.formValue.Password) {
      this.setState({ passErr: 'Required' });
    }

    if (this.state.repeatPassword !== this.state.formValue.Password) {
      this.setState({ passRepErr: 'Password Not Matched' });
    }

    if (validator('Email', this.state.formValue.Email)) {
      this.setState({ emailErr: validator('Email', this.state.formValue.Email)})
      return;
    }

    // console.log(validator('Email', this.state.formValue.Email))

    console.log((this.state.formValue.Email) !== '',
    this.state.formValue.Password,
    !(this.state.repeatPassword !== this.state.formValue.Password), '111')

    let check =  this.state.formValue.Email !== '' &&
     this.state.formValue.Password && !(this.state.repeatPassword !== this.state.formValue.Password)

    validator('Email', (check
      ? this.props.actions({
        email: this.state.formValue.Email,
        password: this.state.formValue.Password,
        userType: 'PROSPECTIVE_STUDENT'
        // birthYear: 2004,
        // birthDay: 29,
        // birthMonth: 2
      })
      : this.setState({
          emailErr: validator('Email', this.state.formValue.Email)
        }))
      )
    }

  _handleErrorPress() {
    console.log('Modal Button Pressed');
  }

  _handleOnChangeText = (text, formField) => {
    const formValue = this.state.formValue;
    formValue[formField] = text;

    if (this.state.formValue !== formValue) {
      this.setState({ formValue });
    }

    this.setState({ emailErr: '', passErr: '', passRepErr: '' })
    // console.log(text, formField, '111', this.state.formValue)
  }

  setRef = (refs) => {
    this.secondTextInput = refs;
  }

  render() {
    return (
      <View style={signUpScreenStyles.container}>
        <MSAHeader
          title={'Sign up'}
          onPress={this.handleBackPress.bind(this)}
          disableLeftIcon={this.props.routes.length === 1}
        />
        <View style={{flex: 1, alignSelf: 'stretch'}}>
          <KeyboardAwareScrollView style={signUpScreenStyles.mainContent}>
            <View style={signUpScreenStyles.titleContainer}>
              <Text style={signUpScreenStyles.welcomeText}>Join Uple</Text>
            </View>
            <View style={signUpScreenStyles.flexViewStyle}>
              {/* <View style={signUpScreenStyles.dobContainer}>
                <View style={signUpScreenStyles.dateView}>
                  <Text
                    style={[this.state.formValue.BirthDate === 'Date of Birth' ?
                      signUpScreenStyles.dateText :
                      signUpScreenStyles.dateTextDark]}>
                    {this.state.formValue.BirthDate}
                  </Text>
                </View>
                <DatePicker
                  style={signUpScreenStyles.iconContainer}
                  onDateChange={date => {
                    let formValue = this.state.formValue;
                    formValue.BirthDate = date;
                    this.setState({ formValue })}
                  }
                  format={'MM/DD/YYYY'}
                  confirmBtnText={'Confirm'}
                  cancelBtnText={'Cancel'}
                  showIcon={true}
                  hideText={true}
                  minDate={'01-01-1900'}
                />
              </View> */}
              <JInput
                style={{marginBottom: 15}}
                placeholder={'Email address'}
                changeText={txt => this._handleOnChangeText(txt, 'Email')}
                onSubmit={() => { if (this.secondTextInput) this.secondTextInput.focus()}}
                keyboard={'email-address'}
                returnKey={'next'}
                error={this.state.emailErr}
              />
              <JInput
                inputRef={this.setRef}
                style={{marginBottom: 15}}
                placeholder={'Password, min 8 characters'}
                changeText={txt => {
                  this._handleOnChangeText(txt, 'Password')
                }
                }
                onFocusLost={() => this.setState({
                  passErr: this.state.formValue.Password !== '' ? validator('Password', this.state.formValue.Password) : ''
                  })
                }
                secure={true}
                error={this.state.passErr}
              />
              <JInput
                style={{marginBottom: 15}}
                placeholder={'Repeat password'}
                changeText={txt => {
                  this.setState({
                    repeatPassword: txt
                  })
                }
                }
                onFocusLost = {() => {
                  this.setState({
                    passRepErr:
                      this.state.formValue.Password === this.state.repeatPassword ? '' : this.state.repeatPassword === '' ? '' : 'No Match'
                  })
                }}
                secure={true}
                error={this.state.passRepErr}
              />
              <MSAButton
                onPress={this._handleSignUpPress.bind(this)}
                type={'primary'}
                label={'Sign up'}
                enableShadow={true}
                textSize={style.font.size.large}
                textLineHeight={style.font.lineHeight.normal.large}
                textFamily={style.font.family.rubikRegular}
              />
            </View>
          </KeyboardAwareScrollView>
        </View>
        <View style={signUpScreenStyles.lowerView}>
          <View style={signUpScreenStyles.lowerViewInner}>
            <Text style={signUpScreenStyles.lowerViewText}>Already have an account?</Text>
            <Text
              style={signUpScreenStyles.loginText}
              onPress={() => NavigationActions.replace('LoginScreen', {})}
            >
              {' '}
              Log in!
            </Text>
          </View>
          <Text style={signUpScreenStyles.privacyText}>
            By signing up you agree to{' '}
            <Text
              style={{ fontFamily: style.font.family.rubikMedium, color: style.color.grey }}
              onPress={() => { Linking.openURL('https://medium.com/@UpleOfficial/privacy-policy-2eac16361e4e')}}
            >
              Uple's Policy
            </Text>
            {' '}&amp;{' '}
            <Text
              style={{ fontFamily: style.font.family.rubikMedium, color: style.color.grey }}
              onPress={() => { Linking.openURL('https://medium.com/@UpleOfficial/uple-terms-of-use-49022cace3e4')}}
            >
              Terms of Use
            </Text>
          </Text>
        </View>
        {/* <View style={{ height: 20, backgroundColor: 'transparent' }}  /> */}
      </View>
    );
  }
}

const mapStateToProps = (state, navState) => {
  return ({
    singupApiState: state.signupApi,
    routes: get(navState, 'screenProps.state.nav.routes', [])
  })
}

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(signup, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignUpScreen)