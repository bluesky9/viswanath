import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, Platform } from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
import { Content } from 'native-base';
import MSAButton from '../layouts/buttons/button';
import userDetailsStyles from './signup-user-details-styles';
import ImagePicker from 'react-native-image-crop-picker';
import Picker from 'react-native-picker';
import ActionSheet from 'react-native-actionsheet';
import JInput from '../layouts/input/input';
import { getCountries } from '../../store/actions/Countries.js';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { style } from '../../styles/variables';

import { signupUserDetails, checkAuthCode } from '../../store/actions/Signup.js';

import { get, isEmpty, map } from 'lodash';

// const EMAIL_MESSAGE = `We've sent you a verification email.\n Click the link in the email to \n activate your account.`

const CANCEL_INDEX = 0;
const uploadOption = ['Cancel', 'Open camera', 'Open gallery'];
let actionsheet = undefined;

interface ISignUpUserDetailsProps extends NavigationScreenProps<{
  authCode?: any;
}> {
  signupUserDetails;
  checkAuthCode?: any;
  getCountries;
  countries;
}
interface ISignUpUserDetailsState {
  profilePicture;
  formValue;
  firstNameErr;
  lastNameErr;
  language;
  profilePic;
}

class SignUpUserDetails extends Component<
  ISignUpUserDetailsProps,
  ISignUpUserDetailsState
> {
  constructor(props) {
    super(props);
    this.state = {
      firstNameErr: undefined,
      lastNameErr: undefined,
      profilePicture: undefined,
      language: undefined,
      formValue: {
        file: undefined,
        ext: undefined,
        country: 'select country',
        firstName: undefined,
        lastName: undefined,
        authCode: props.navigation.state.params ? props.navigation.state.params.authCode : undefined
      },
      profilePic: undefined
    };
  }
  handleBackPress() {
    this.props.navigation.goBack();
  }

  showActionSheet = () => {
    if (actionsheet) actionsheet.show();
  }

  onSelectUniversity(countries) {
    Picker.init({
      pickerData: countries,
      pickerTitleText: 'Select university',
      pickerCancelBtnText: 'Cancel',
      pickerConfirmBtnText: 'Confirm',
      selectedValue: [],
      onPickerConfirm: data => {
          let formValue = this.state.formValue
          formValue.country = data[0]
          this.setState({ formValue });
          Picker.hide();
      },
      onPickerCancel: () => {
          Picker.hide();
      }
    });
    Picker.show();
  }

  componentDidMount() {
    this.props.getCountries();
    if (this.props.navigation.state.params && this.props.navigation.state.params.authCode) {
      this.props.checkAuthCode(this.props.navigation.state.params.authCode);
    } else {
      this.props.navigation.goBack();
    }
  }

  _onCompletePress() {
    if (isEmpty(this.state.formValue.firstName)) {
      this.setState({ firstNameErr: 'Empty Field' });
    }
    if (isEmpty(this.state.formValue.firstName)) {
      this.setState({ lastNameErr: 'Empty Field' });
    }
    // if (this.state.firstNameErr || this.state.lastNameErr) {
    //   return;
    // }
    // console.log(this.state.formValue, 'Check Form Value', this.state.profilePic)
    this.props.signupUserDetails(this.state.formValue, this.state.profilePic);
  }

  handleUploadPhoto = () => {
    //
  };

  _handleOnChangeText = (text, formField) => {
    const formValue = this.state.formValue;
    formValue[formField] = text;

    if (this.state.formValue !== formValue) {
      this.setState({ formValue });
    }
  };

  openGallery = () => {
    ImagePicker.openPicker({
      cropping: true,
      compressImageQuality: Platform.OS === 'ios' ? 0 : 0.5
    })
      .then(image => {
        let imageObj = {
          uri: get(image, 'path', ''),
          name: get(image, Platform.OS === 'ios' ? 'creationDate' : 'modificationDate' , ''),
          type: get(image, 'mime', '')
        }
        this.setState({
          profilePic: imageObj,
          profilePicture: get(image, 'path', '')
        });
      })
      .catch(Error);
  }

  openCamera = () => {
    ImagePicker.openCamera({
      cropping: true,
      compressImageQuality: Platform.OS === 'ios' ? 0 : 0.5
    })
      .then(image => {
        let imageObj = {
          uri: get(image, 'path', ''),
          name: new Date().getTime().toString(),
          type: get(image, 'mime', '')
        }
        this.setState({
          profilePic: imageObj,
          profilePicture: get(image, 'path', '')
        });
      })
      .catch((e) => {
        console.log(e, 'Error')
      });
  }

  render() {
    return (
      <Content style={{ backgroundColor: 'white'}}>
        <ActionSheet
            ref={o => (actionsheet = o)}
            options={uploadOption}
            cancelButtonIndex={CANCEL_INDEX}
            onPress={i => {
              i === 1 ? this.openCamera() : this.openGallery();
            }}
          />
        <View style={userDetailsStyles.container}>
          <View style={userDetailsStyles.iconContainer}>
            <Image
              style={{ height: 30, width: 97 }}
              source={require('../../../assets/images/main_icon.png')}
              resizeMode={'contain'}
            />
          </View>
          <View style={{ paddingTop: 36, paddingBottom: 12 }}>
            <Text style={userDetailsStyles.title}>Welcome to Uple</Text>
          </View>
          <View
            style={{ flexDirection: 'row', paddingTop: 15, paddingBottom: 30 }}
          >
            <View style={userDetailsStyles.userImageContainer}>
              <Image
                source={ !isEmpty(this.state.profilePicture) ?
                  { uri : this.state.profilePicture }
                  : require('../../../assets/images/user_dummy_image.png')
                }
                style={userDetailsStyles.userImage}
              />
            </View>
            <View style={{ paddingLeft: 20, height: 80, flex: 1}}>
              <TouchableOpacity
                style={userDetailsStyles.uploadBtn}
                onPress={this.showActionSheet}
              >
                <Text style={userDetailsStyles.uploadBtnText}>
                  Upload your photo
                </Text>
              </TouchableOpacity>
              <Text style={userDetailsStyles.uploadFormatTextTop}>
                Allowed formats: jpg.png.gif
              </Text>
              <Text style={userDetailsStyles.uploadFormatText}>
                Maximum resolution: 1280x1280.
              </Text>
            </View>
          </View>
          <JInput
            style={{ marginBottom: 15 }}
            placeholder={'First name'}
            changeText={txt => this._handleOnChangeText(txt, 'firstName')}
            error={this.state.firstNameErr}
            maxChar={25}
          />
          <JInput
            style={{ marginBottom: 15 }}
            placeholder={'Last name'}
            changeText={txt => this._handleOnChangeText(txt, 'lastName')}
            error={this.state.lastNameErr}
            maxChar={25}
          />
          <TouchableOpacity
            style={userDetailsStyles.nationalityContainer}
            onPress={() => this.onSelectUniversity(this.props.countries)}
          >
            <Text style={userDetailsStyles.nationalityText}>
              {this.state.formValue.country
                ? this.state.formValue.country
                : 'Nationality'}
            </Text>
            <Image
              source={require('../../../assets/images/triangle.png')}
              style={userDetailsStyles.triangleContainer}
            />
          </TouchableOpacity>
          <View style={{paddingTop: 5}}>
            <MSAButton
              onPress={this._onCompletePress.bind(this)}
              type={'primary'}
              label={'Complete'}
              enableShadow={true}
              textSize={style.font.size.large}
              textLineHeight={style.font.lineHeight.normal.large}
              textFamily={style.font.family.rubikRegular}
            />
          </View>
        </View>
      </Content>
    );
  }
}

const mapStateToProps = state => {
  return {
    singupApiState: state.signupApi,
    countries: map(state.countries.countries, 'countryName')
  };
};

const mapDispatchToProps = dispatch => ({
  signupUserDetails: bindActionCreators(signupUserDetails, dispatch),
  checkAuthCode: bindActionCreators(checkAuthCode, dispatch),
  getCountries: bindActionCreators(getCountries, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(SignUpUserDetails);
