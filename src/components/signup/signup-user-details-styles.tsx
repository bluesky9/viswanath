import { StyleSheet } from 'react-native';
import { style } from '../../styles/variables';

const userDetailsStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingHorizontal: 15
  },
  iconContainer: {
    paddingTop: style.header.height + 21
  },
  title: {
    textAlign: 'center',
    fontSize: 24,
    lineHeight: 30,
    fontFamily: style.font.family.ptSerif,
    color: style.color.nero
  },
  userImageContainer: {
    height: 80,
    width: 80,
    borderRadius: 40,
    overflow: 'hidden'
  },
  userImage: {
    resizeMode: 'contain',
    height: 80,
    width: 80,
    borderRadius: 40,
    borderTopRightRadius: 40,
    borderBottomRightRadius: 40,
    borderBottomLeftRadius: 40,
    borderTopLeftRadius: 40
  },
  uploadBtn: {
    borderWidth: 0.5,
    borderColor: 'rgb(177,175,177)',
    borderRadius: 5,
    paddingVertical: 7,
    paddingHorizontal: 12,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.12)',
    alignSelf: 'flex-start'
  },
  uploadBtnText: {
    color: 'rgb(36,36,36)',
    fontSize: 13,
    lineHeight: style.font.lineHeight.tight.large,
    fontFamily: style.font.family.helveticaNeue,
    paddingLeft: 0,
    paddingRight: 0
  },
  uploadFormatTextTop: {
    color: style.color.grey,
    fontSize: 12,
    lineHeight: 21,
    fontFamily: style.font.family.rubikRegular,
    letterSpacing: 0.2,
    marginTop: 10,
    alignSelf: 'flex-start'
  },
  uploadFormatText: {
    color: style.color.grey,
    fontSize: 12,
    lineHeight: 21,
    fontFamily: style.font.family.rubikRegular,
    letterSpacing: 0.2,
    alignSelf: 'flex-start'
  },
  emailMessageText: {
    textAlign: 'center',
    fontSize: style.font.size.small,
    lineHeight: 18,
    color: style.color.coal,
    paddingTop: 20,
    paddingBottom: 35
  },
  textInput: {
    width: '100%',
    height: 42,
    borderWidth: 1,
    borderRadius: 3,
    borderColor: style.color.cream,
    marginBottom: 15,
    fontSize: 12,
    fontFamily: style.font.family.rubikRegular,
    padding: 15
  },
  nationalityContainer: {
    width: '100%',
    height: 42,
    borderWidth: 1,
    borderRadius: 3,
    borderColor: style.color.cream,
    marginBottom: 15,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  nationalityText: {
    fontSize: 12,
    fontFamily: style.font.family.rubikRegular,
    paddingLeft: 15,
    paddingVertical: 15,
    color: style.color.dustyGrey,
    flex: 1
  },
  triangleContainer: {
    marginRight: 12
  }
});

export default userDetailsStyles;
