import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
import * as NavigationActions from '../../NavigationService'
import { get } from 'lodash';

import MSAButton from '../layouts/buttons/button';
import signupSuccessStyles from './signup-success-styles';
import { style } from '../../styles/variables';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { SignupApi } from '../../store/actions'

const EMAIL_MESSAGE = `We've sent you a verification email.\n Click the link in the email to \n activate your account.`;

interface ISignUpSuccessProps extends NavigationScreenProps<{}> {
  signupActions?: any;
}
interface ISignUpSuccessState {}

class SignUpSuccess extends Component<
  ISignUpSuccessProps,
  ISignUpSuccessState
> {
  handleBackPress() {
    this.props.navigation.goBack();
  }

  goToLoginPage() {
    NavigationActions.reset('LoginScreen', {})
  }

  onResendPressed = (email) => {
    this.props.signupActions.resendActivation(email);
  }

  render() {
    let email = get(this.props.navigation.state.params, 'email', undefined);
    return (
      <View style={signupSuccessStyles.container}>
        <View style={signupSuccessStyles.mainContent}>
          <View style={signupSuccessStyles.iconContainer}>
            <Image
              style={{ width: 97, height: 30 }}
              source={require('../../../assets/images/main_icon.png')}
              resizeMode={'contain'}
            />
          </View>
          <Text style={signupSuccessStyles.title}>
            Thank you {'\n'}for signing up
          </Text>
          <Image style={signupSuccessStyles.mailImage} source={require('../../../assets/images/invitationSent.jpg')} />
          <Text style={signupSuccessStyles.emailMessageText}>
            {EMAIL_MESSAGE}
          </Text>
          <View style={signupSuccessStyles.buttonWrapper}>
            <MSAButton
              onPress={this.goToLoginPage.bind(this)}
              type={'primary'}
              label={'Go to login page'}
              enableShadow={true}
              textSize={style.font.size.large}
              textLineHeight={style.font.lineHeight.normal.large}
              textFamily={style.font.family.rubikRegular}
            />
          </View>
        </View>
        <View style={signupSuccessStyles.lowerView}>
          <View style={signupSuccessStyles.lowerViewInner}>
            <Text style={signupSuccessStyles.lowerViewText}>Did not recieve the email?</Text>
            <Text
              style={signupSuccessStyles.resendText}
              onPress={() => this.onResendPressed(email)}
            >
              Resend
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => ({
    signupActions: bindActionCreators(SignupApi, dispatch)
})

export default connect(
  undefined,
  mapDispatchToProps
)(SignUpSuccess)
