import { StyleSheet, Platform } from 'react-native';
import { style } from '../../styles/variables';

const signupSuccessStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  mainContent: {
    paddingHorizontal: 15,
    paddingTop: Platform.OS === 'ios' ? 41 : 21,
    flex: 1,
    alignItems: 'center'
  },
  iconContainer: {
    // paddingTop: style.header.height + 21,
    paddingTop: 0,
    paddingBottom: 32
  },
  title: {
    textAlign: 'center',
    fontSize: 24,
    fontFamily: style.font.family.ptSerif,
    color: style.color.nero,
    lineHeight: 30
  },
  mailImage: {
    marginTop: 23,
    height: 104,
    width: 85
  },
  emailMessageText: {
    marginTop: 23,
    textAlign: 'center',
    fontSize: style.font.size.normal,
    lineHeight: style.font.lineHeight.normal.normal,
    color: style.color.coal,
    fontFamily: style.font.family.rubikRegular,
    letterSpacing: 0.2
  },
  buttonWrapper: {
    paddingTop: 20
  },
  lowerView: {
    paddingVertical: 32,
    borderTopWidth: 2,
    width: '100%',
    borderColor: style.color.lightGrey,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 15
  },
  lowerViewInner: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  lowerViewText: {
    color: style.color.grey,
    fontSize: style.font.size.small,
    fontFamily: style.font.family.rubikRegular,
    lineHeight: style.font.lineHeight.tight.normal
  },
  resendText: {
    marginLeft: 3,
    color: style.color.skyBlue,
    fontFamily: style.font.family.rubikRegular,
    fontSize: style.font.size.small,
    lineHeight: style.font.lineHeight.tight.normal
  }
});

export default signupSuccessStyles;
