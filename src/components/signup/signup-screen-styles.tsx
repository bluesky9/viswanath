import { StyleSheet } from 'react-native';
import { style } from '../../styles/variables';

const signupScreenStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  mainContent: {
    paddingHorizontal: 15,
    flex: 1
  },
  titleContainer: {
    paddingVertical: 31
  },
  welcomeText: {
    alignSelf: 'center',
    fontSize: 24,
    fontFamily: style.font.family.ptSerif,
    lineHeight: 30,
    color: style.color.nero
  },
  flexViewStyle: {
    // alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 15
  },
  dobContainer: {
    marginBottom: 15,
    width: '100%',
    position: 'relative'
  },
  dateView: {
    width: '100%',
    borderWidth: 1,
    borderRadius: 3,
    borderColor: style.color.cream,
    paddingVertical: 14,
    paddingLeft: 15,
    paddingRight: 45,
    justifyContent: 'center'
  },
  dateText: {
    fontSize: style.font.size.normal,
    lineHeight: style.font.lineHeight.normal.normal,
    fontFamily: style.font.family.rubikRegular,
    color: 'rgb(153, 153, 153)'
  },
  dateTextDark: {
    fontSize: style.font.size.normal,
    lineHeight: style.font.lineHeight.normal.normal,
    fontFamily: style.font.family.rubikRegular,
    color: 'rgb(59, 62, 68)'
  },
  iconContainer: {
    position: 'absolute',
    right: 7,
    top: 2,
    width: 32
  },
  textInput: {
    width: '100%',
    height: 42,
    borderWidth: 1,
    borderRadius: 3,
    borderColor: style.color.cream,
    marginBottom: 15,
    fontSize: 12,
    fontFamily: style.font.family.rubikRegular,
    padding: 15
  },
  forgotPasswordText: {
    alignSelf: 'center',
    fontSize: 14,
    color: '#808691',
    fontFamily: style.font.family.rubikRegular
  },
  lowerView: {
    borderTopWidth: 2,
    paddingVertical: 17,
    paddingHorizontal: 15,
    width: '100%',
    borderColor: style.color.lightGrey,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'flex-end'
  },
  lowerViewInner: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  lowerViewText: {
    fontSize: style.font.size.small,
    lineHeight: 17,
    fontFamily: style.font.family.rubikRegular,
    color: style.color.grey
  },
  loginText: {
    color: style.color.skyBlue,
    fontSize: style.font.size.small,
    lineHeight: 17
  },
  privacyText: {
    color: 'rgba(128,134,145,0.5)',
    fontSize: style.font.size.small,
    lineHeight: 17,
    fontFamily: style.font.family.rubikRegular
  },
  noAccountText: {
    alignSelf: 'center',
    fontSize: 14,
    color: '#808691',
    fontFamily: style.font.family.rubikRegular,
    margin: 5
  }
});

export default signupScreenStyles;
