import React, { Component } from 'react';
import { View, Image } from 'react-native';
import { NavigationScreenProps } from 'react-navigation';

import MSAButton from '../layouts/buttons/button'
import LaunchScreenStyles from './launch-screen-styles';

import { style } from '../../styles/variables';

interface ILoginScreenProps extends NavigationScreenProps<{}> {}

export default class LaunchScreen extends Component<ILoginScreenProps> {

  onLoginPress() {
    this.props.navigation.navigate('LoginScreen');
  }

  onSignupPress() {
    this.props.navigation.navigate('SignUpScreen');
  }

  render() {
    return (
      <View style={LaunchScreenStyles.container}>
        <Image
          style={LaunchScreenStyles.bg}
          source={require('../../../assets/splash-screen.png')}
        />
        <View style={LaunchScreenStyles.btnContainer}>
          <MSAButton onPress={this.onLoginPress.bind(this)} type={'primary'} label={'Login'} enableShadow={true}
            textFamily={style.font.family.rubikRegular}
            textSize={style.font.size.large}
            textLineHeight={style.font.lineHeight.normal.large}
          />
          <MSAButton onPress={this.onSignupPress.bind(this)} type={'inverted'} bordered={true} label={'Sign up'}
            textFamily={style.font.family.rubikRegular}
            textSize={style.font.size.large}
            textLineHeight={style.font.lineHeight.normal.large}
          />
        </View>
      </View>
    );
  }
}
