import { StyleSheet } from 'react-native';
import { style } from '../../styles/variables';

const launchScreenStyles = StyleSheet.create({
  container: {
    flex: 1
  },
  bg: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover'
  },
  btnContainer: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    paddingHorizontal: 40,
    paddingBottom: 54,
    justifyContent: 'flex-end'
  },
  loginButton: {
    width: '100%',
    paddingVertical: 12,
    backgroundColor: 'rgb(53,135,230)',
    borderRadius: 5,
    marginVertical: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  loginText: {
    fontWeight: 'bold',
    color: 'white',
    fontSize: style.font.size.large,
    fontFamily: style.font.family.rubikRegular,
    lineHeight: style.font.lineHeight.normal.large
  },
  signupButton: {
    width: '100%',
    paddingVertical: 12,
    borderRadius: 5,
    marginVertical: 10,
    borderWidth: 1,
    borderColor: 'rgb(53,135,230)',
    justifyContent: 'center',
    alignItems: 'center'
  },
  signupText: {
    color: 'rgb(53,135,230)',
    fontSize: style.font.size.large,
    fontFamily: style.font.family.rubikRegular,
    lineHeight: style.font.lineHeight.normal.large
  }
});

export default launchScreenStyles;
