import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  TextInput,
  Dimensions,
  Platform,
  ScrollView,
  Image,
  Switch
} from 'react-native';
import universityStyles from './university-styles';
import ImagePicker from 'react-native-image-crop-picker';
import MSAHeader from '../layouts/header/header';
import { Icon } from 'native-base';
const { width } = Dimensions.get('window');
import { NavigationScreenProps } from 'react-navigation';
import { style } from '../../styles/variables';
import {
  getUniversityFeeds,
  setPostAudience,
  setUniversityPostCountryFilters,
  setPostMajor
} from '../../store/actions/Universities.js';
import { publishPost, uploadFile } from '../../store/actions/Feed.js';
import { bindActionCreators } from 'redux';
import { get, find, map, isEmpty } from 'lodash';
import { constants } from '../../constants';

import { connect } from 'react-redux';

const IMAGE_UPLOAD_LIMIT = 5;
let MESSAGE =
  'If you turn this option ON, VIsitors of this page who are not following the unversity can view this post';

interface ILoginScreenProps extends NavigationScreenProps<{}> {
  universityDetails: any;
  universityPostCountryFilters: any;
  actions: any;
  universityFeeds: any;
}
interface State {
  imagesArray: any;
  title: string;
  message: string;
  publicPost: boolean;
  postId: any;
  deletedIds: any;
}

class UniversityPostCompose extends Component<ILoginScreenProps, State> {
  constructor(props) {
    super(props);
    this.state = {
      postId: get(this.props.navigation.state.params, 'postId', undefined),
      imagesArray: [],
      title: '',
      message: '',
      publicPost: false,
      deletedIds: []
    };
  }

  componentDidMount() {
    let postId = get(this.props.navigation.state.params, 'postId', undefined)
    let deptRequest = get(this.props.navigation, 'state.params.deptRequest', false);
    let totalMajors = get(this.props.navigation, 'state.params.majors.length', 0);
    let post = undefined;
    let majors = [];
    if (deptRequest) {
      post = find(this.props.universityDetails.departmentFeeds, feed => feed.postid === postId)
      if (post.major && totalMajors !== post.major.length) {
        post.major.map(item => {
          majors.push(item.majorid)
        })
      }
    } else post = find(this.props.universityFeeds, postObj => postObj.postid === postId);
    if (post) {
      let postAudience = get(post, 'audience', []);
      let countryFilters = get(post, 'country', []);

      if (deptRequest) this.props.actions.setPostMajor(majors)

      this.props.actions.setPostAudience((postAudience.length === 0 || postAudience.length === 5) ? [] : postAudience);
      this.props.actions.setUniversityPostCountryFilters(countryFilters.length === (0 || 243) ? [] : countryFilters);
      // this.props.actions.setTaggedUsers(get(post, 'taguser', []));
      this.setState({ title: get(post, 'title', '') })
      this.setState({ message: get(post, 'postMessages', '') })
      this.setState({ publicPost: !get(post, 'restricted', false) })
      let images = post.files ? post.files : []
      let imagesArray = this.state.imagesArray
      images.map((item) => {
        let imageObj = {
          mediumUrl: item.mediumUrl,
          originalUrl: item.originalUrl,
          smallUrl: item.smallUrl
        }
        imagesArray.push(imageObj);
      })
      this.setState({ imagesArray })
    }
  }

  publishPost() {
    let majorIdsKey = 'majorIds';
    let departmentIdKey = 'departmentId'
    let deptRequest = get(this.props.navigation, 'state.params.deptRequest', false);
    let deptId = get(this.props.navigation, 'state.params.deptId', undefined);
    let checkPost = this.state.title !== '' && (this.state.message !== '' || this.state.imagesArray.length > 0)
    let majors = get(this.props.navigation, 'state.params.majors', []);

    let imagesArray = this.state.imagesArray

    // For Android specifically
    if (imagesArray.length > IMAGE_UPLOAD_LIMIT) {
      let diff = IMAGE_UPLOAD_LIMIT - imagesArray.length
      imagesArray.splice(5, diff)
    }

    if (checkPost) {
      let publishPostObj = {
        userTypes: this.props.universityDetails.postAudience,
        countries: this.props.universityPostCountryFilters.filters,
        title: this.state.title,
        message: this.state.message,
        postExpiry: 'MONTH',
        restricted: !this.state.publicPost,
        universityId: this.props.universityDetails.details.univ_id
      }

      if (deptRequest) {
        let majorIds = this.props.universityDetails.postMajor;
        if (isEmpty(majorIds) && !isEmpty(majors)) {
          majors.map(item => {
            majorIds.push(item.majorid)
          })
        }
        publishPostObj[majorIdsKey] = majorIds;
        publishPostObj[departmentIdKey] = deptId
      }

      if (imagesArray.length > 0 || this.state.deletedIds.length > 0) {
        this.props.actions.uploadFile(imagesArray, publishPostObj, parseInt(this.state.postId, 10), this.state.deletedIds)
      } else {
        this.props.actions.publishPost(publishPostObj, parseInt(this.state.postId, 10));
      }
      // this.props.actions.publishPost(publishPostObj, parseInt(this.state.postId, 10));
    }
  }

  removeImage = (index) => {
    let arr = this.state.imagesArray;

    let postId = get(this.props.navigation.state.params, 'postId', undefined)
    let post = find(this.props.universityFeeds, postObj => postObj.postid === postId)
    let images = post.files ? post.files : []

    let mediumUrl = get(arr[index], 'mediumUrl', '');
    let originalUrl = get(arr[index], 'originalUrl', '');
    let deletedId = find(images, image => image.mediumUrl === mediumUrl || image.originalUrl === originalUrl)
    if (deletedId) {
      let deletedIds = this.state.deletedIds
      deletedIds.push(deletedId.id)
      this.setState({ deletedIds })
    }
    arr.splice(index, 1);
    this.setState({ imagesArray: arr });
  }

  openPicker() {
    if (this.state.imagesArray.length < IMAGE_UPLOAD_LIMIT)
      ImagePicker.openPicker({
        cropping: false,
        multiple: true,
        compressImageQuality: Platform.OS === 'ios' ? 0 : 0.5,
        maxFiles: IMAGE_UPLOAD_LIMIT - this.state.imagesArray.length
      })
        .then(images => {
          let arr = this.state.imagesArray;
          map(images, (image) => {
            let imageObj = {
              uri: get(image, 'path', ''),
              name: get(image, Platform.OS === 'ios' ? 'creationDate' : 'modificationDate', ''),
              type: get(image, 'mime', '')
            }
            if (arr.length < IMAGE_UPLOAD_LIMIT) {
              arr.push(imageObj);
            }
          })
          this.setState({
            imagesArray: arr
          });
        })
        .catch(Error);
  }

  openCamera() {
    if (this.state.imagesArray.length < IMAGE_UPLOAD_LIMIT)
      ImagePicker.openCamera({
        cropping: false,
        compressImageQuality: Platform.OS === 'ios' ? 0 : 0.5
      })
        .then(image => {
          let arr = this.state.imagesArray;
          let imageObj = {
            uri: get(image, 'path', ''),
            name: new Date().getTime().toString(),
            type: get(image, 'mime', '')
          }
          arr.push(imageObj);
          this.setState({
            imagesArray: arr
          });
        })
        .catch((e) => {
          console.log(e, 'Error')
        });
  }

  render() {
    let deptRequest = get(this.props.navigation, 'state.params.deptRequest', false);
    let majorData = isEmpty(get(this.props.navigation, 'state.params.majors')) ? [] : get(this.props.navigation.state.params, 'majors');
    let countries = this.props.universityPostCountryFilters.filters && this.props.universityPostCountryFilters.filters.length === 0 ?
      'All countries' : this.props.universityPostCountryFilters.filters && this.props.universityPostCountryFilters.filters.map(
        (item) => {
          return (item + ', ');
        }
      )
    let selectedMajor = this.props.universityDetails.postMajor.map((item, index) => {
      let majorExists = find(majorData, majorObj => majorObj.majorid === item)
      if (majorExists) {
        let majorName = majorExists.majorname + (this.props.universityDetails.postMajor.length - 1 === index ? '' : ', ');
        return majorName;
      } else {
        return undefined
      }
    })
    let majors = (this.props.universityDetails.postMajor.length === 0 || this.props.universityDetails.postMajor.length === majorData.length ?
      'All majors' : selectedMajor);
    let checkPost = this.state.title !== '' && (this.state.message !== '' || this.state.imagesArray.length > 0)
    return (
      <View style={{ flex: 1 }}>
        <MSAHeader
          title={'Edit post'}
          rightText={'PUBLISH'}
          leftText={'CANCEL'}
          onPress={() => this.props.navigation.goBack()}
          onRightTextPress={() => this.publishPost()}
          disableRightText={!checkPost}
        />
        <ScrollView>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('UniversityAudience', { majorData, deptRequest })}
            style={[universityStyles.newPostItemWrap]}
            disabled={this.props.universityDetails.usertype && this.props.universityDetails.usertype === 'PROSPECTIVE_STUDENT'}
          >
            <View>
              <Text
                style={universityStyles.newPostItemHead}
              >
                AUDIENCE
              </Text>
              <Text
                style={{ width: width - 50, marginTop: 5 }}
              >
                {this.props.universityDetails.postAudience ? (
                  this.props.universityDetails.postAudience
                    .length === 0 ? (
                      <Text style={universityStyles.newPostItemLow}>
                        Everybody
                    </Text>
                    ) : (
                      undefined
                    )
                ) : (
                    <Text style={universityStyles.newPostItemLow}>Everybody</Text>
                  )}
                {this.props.universityDetails.postAudience &&
                  this.props.universityDetails.postAudience.map(
                    (item, index) => {
                      return (
                        <Text
                          key={index}
                          style={universityStyles.newPostItemLow}
                        >
                          {constants.USERTYPES.PLURAL[item]}
                          {', '}
                        </Text>
                      );
                    }
                  )}
                <Text style={universityStyles.newPostItemLow}>{' from '}</Text>
                <Text style={universityStyles.newPostItemLow}>
                  {deptRequest ? majors : countries}
                </Text>
              </Text>
            </View>
            <View
              style={{
                position: 'absolute',
                alignSelf: 'center',
                left: width - 25
              }}
            >
              <Icon
                style={{ fontSize: 20, color: 'rgb(197,207,219)' }}
                name={'arrow-forward'}
              />
            </View>
          </TouchableOpacity>
          <View
            style={[
              universityStyles.newPostItemWrap,
              { justifyContent: 'flex-start' }
            ]}
          >
            <Text
              style={[
                universityStyles.newPostItemHead,
                { paddingBottom: 5 }
              ]}
            >
              TITLE
            </Text>
            <TextInput
              onChangeText={txt =>
                this.setState({
                  title: txt
                })
              }
              style={[universityStyles.newPostInput]}
              value={this.state.title}
              underlineColorAndroid={'rgba(0,0,0,0)'}
              maxLength={40}
            />
            <Text
              style={[
                universityStyles.newPostItemHead,
                { paddingBottom: 5 }
              ]}
            >
              POST
            </Text>
            <TextInput
              onChangeText={txt =>
                this.setState({
                  message: txt
                })
              }
              multiline={true}
              numberOfLines={5}
              style={[universityStyles.newPostInput, { marginBottom: 0, paddingTop: 12, height: undefined }]}
              value={this.state.message}
              underlineColorAndroid={'rgba(0,0,0,0)'}
            />
          </View>
          <View style={{ alignSelf: 'stretch' }}>
            <View
              // onPress={() => this.openPicker()}
              style={[universityStyles.newPostItemWrap, { height: 54, flexDirection: 'row', alignItems: 'center' }]}
            >
              <TouchableOpacity
                onPress={() => this.openPicker()}
                style={{ flex: 1 }}
              >
                <Text style={universityStyles.newPostItemHead}>ADD IMAGES</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.openCamera()}
              >
                <Icon
                  style={{ fontSize: 20, color: 'rgb(197,207,219)' }}
                  name={'camera'}
                  active
                />
              </TouchableOpacity>
            </View>
            {!isEmpty(this.state.imagesArray) &&
              <View
                style={[
                  universityStyles.newPostItemWrap,
                  { height: this.state.imagesArray.length === 0 ? 0 : 120 }
                ]}
              >
                <ScrollView
                  horizontal={true}
                  contentContainerStyle={{ alignItems: 'center' }}
                >
                  {this.state.imagesArray.map((item, index) => {
                    return (
                      <View
                        key={index}
                        style={{
                          height: 90,
                          width: 90,
                          marginHorizontal: 15,
                          borderRadius: 3
                        }}
                      >
                        <Image
                          source={{
                            uri: get(item, 'originalUrl', false) ? item.originalUrl : item.uri
                          }}
                          style={{ height: 90, width: 90 }}
                        />
                        <TouchableOpacity
                          onPress={() => { this.removeImage(index) }}
                          style={{
                            position: 'absolute',
                            top: 0,
                            right: 0,
                            padding: 5,
                            margin: 5,
                            borderRadius: 1,
                            backgroundColor: 'rgba(0,0,0,0.5)'
                          }}
                        >
                          <Image
                            style={{ tintColor: 'white' }}
                            source={require('../../../assets/app_icons/Icons/Close-dark.png')}
                          />
                        </TouchableOpacity>
                      </View>
                    );
                  })}
                </ScrollView>
              </View>
            }
            <View style={{ backgroundColor: 'white', padding: 15 }}>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Text
                  style={{
                    flex: 1,
                    fontFamily: style.font.family.rubikRegular,
                    color: style.color.coal
                  }}
                >
                  Public post
                </Text>
                <Switch
                  value={this.state.publicPost}
                  onValueChange={bool => this.setState({ publicPost: bool })}
                  thumbTintColor={style.color.primary}
                  onTintColor={style.color.cream}
                  style={{
                    transform: [{ scaleX: Platform.OS === 'ios' ? .7 : 1 },
                    { scaleY: Platform.OS === 'ios' ? .7 : 1 }]
                  }}
                />
              </View>
              <Text
                style={{
                  fontSize: 12,
                  color: style.color.dustyGrey,
                  paddingTop: 10
                }}
              >
                {MESSAGE}
              </Text>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = ({ universityDetails, universityPostCountryFilters, universityFeeds }) => {
  return {
    universityDetails: universityDetails,
    universityPostCountryFilters: universityPostCountryFilters,
    universityFeeds: universityFeeds.universityFeeds
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      publishPost: publishPost,
      getUniversityFeeds: getUniversityFeeds,
      setPostAudience: setPostAudience,
      uploadFile: uploadFile,
      setUniversityPostCountryFilters: setUniversityPostCountryFilters,
      setPostMajor: setPostMajor
    },
    dispatch
  )
});

export default connect(mapStateToProps, mapDispatchToProps)(
  UniversityPostCompose
);
