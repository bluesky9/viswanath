import React, { Component } from 'react';
import { View, TouchableOpacity, Text, Image } from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import MSAHeader from '../layouts/header/header';
import PeopleFilterSearch from '../people/people-filter-search';

import { filterUniversityStyles } from '../people/people-filter-styles';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  setPostMajor
} from '../../store/actions/Universities.js';

import { get, find, filter, isEmpty } from 'lodash';

// import { Spinner } from 'native-base';
interface IPeopleFilterNavParams {}
interface IPeopleFilterProps
  extends NavigationScreenProps<IPeopleFilterNavParams> {
  major: any;
  actions: any;
}
interface State {
  searchMajor;
  stateMajor;
  selectAll;
  rightTitle;
}

class MajorFilter extends Component<IPeopleFilterProps, State> {
  constructor(props) {
    super(props);
    this.state = {
      stateMajor: [],
      searchMajor: undefined,
      selectAll: false,
      rightTitle: undefined
    };
  }

  componentDidMount() {
    let stateMajor = [];
    let checkMajors = this.props.major;
    let deptMajors =  get(this.props.navigation.state.params, 'majorData', [])
    if (deptMajors.length > 0)
      deptMajors.map(item => {
        if (find(checkMajors, obj => obj === item.majorid)) {
          stateMajor.push({ majorname: item.majorname, majorid: item.majorid, status: true });
        } else {
          stateMajor.push({ majorname: item.majorname, status: false, majorid: item.majorid });
        }
      });
    if (checkMajors.length > 0) {
      this.setState({ rightTitle: 'CLEAR' });
    } else {
      this.setState({ selectAll: true });
      this.setState({ rightTitle: 'APPLY' });
    }
    this.setState({ stateMajor });
  }

  _renderItem = (obj, index) => {
    return (
      <TouchableOpacity
        onPress={() => {
          let stateMajor = this.state.stateMajor;
          if (obj.status) {
            stateMajor[index].status = false;
          } else {
            stateMajor[index].status = true;
          }

          let allSelected = filter(stateMajor, major => !major.status)
          if (allSelected.length === 0 || allSelected.length === stateMajor.length) {
            stateMajor.map((item) => {
              item.status = false
            })
            this.setState({ selectAll: true })
          } else {
            this.setState({ selectAll: false })
          }

          this.setState({ stateMajor: stateMajor, rightTitle: 'APPLY' })
          // this.setState({
          //   stateMajor: stateMajor,
          //   rightTitle: 'APPLY',
          //   selectAll: false
          // });
        }}
        key={obj.code}
        style={filterUniversityStyles.itemContainer}
      >
        {obj.status ? (
          <Image
            source={require('../../../assets/app_icons/Icons/Checkbox-active.png')}
            style={filterUniversityStyles.checkboxContainer}
            resizeMode={'stretch'}
          />
        ) : (
          <Image
            source={require('../../../assets/app_icons/Icons/Checkbox-default.png')}
            style={filterUniversityStyles.checkboxContainer}
            resizeMode={'stretch'}
          />
        )}
        <Text style={filterUniversityStyles.itemText}>
          {obj ? obj.majorname : ''}
        </Text>
      </TouchableOpacity>
    );
  }

  _handleOnApplyPress = () => {
    let majorFilters = [];

    if (this.state.rightTitle === 'APPLY') {
      filter(this.state.stateMajor, (obj) => {
        if (obj.status)
          return majorFilters.push(obj.majorid);
        else {
          return undefined;
        }
      })
    }

    this.props.actions.setPostMajor(majorFilters)
    this.props.navigation.goBack();
  };

  handleTextChange = text => {
    this.setState({ searchMajor: text });
  };

  onEverbodyPress = () => {
    let stateMajor = this.state.stateMajor;
    stateMajor.map((item) => {
      item.status = false
    })
    this.setState({ stateMajor, selectAll: true, rightTitle: 'APPLY' })
  }

  render() {
    let majors = get(this.props.navigation.state.params, 'majorData', [])
    return (
      <View style={filterUniversityStyles.container}>
        <MSAHeader
          title={'Major'}
          rightText={this.state.rightTitle}
          onPress={() => this.props.navigation.goBack()}
          onRightTextPress={() => this._handleOnApplyPress()}
        />
        <View style={filterUniversityStyles.mainContent}>
          <PeopleFilterSearch
            placeHolder={'Search major'}
            onTextChange={this.handleTextChange}
          />
        </View>
        <View style={{ marginHorizontal: 15, flex: 1 }}>
          {majors.length > 0 && (
            <KeyboardAwareScrollView>
              <TouchableOpacity
                onPress={this.onEverbodyPress}
                style={filterUniversityStyles.itemContainer}
              >
                {this.state.selectAll ? (
                  <Image
                    source={require('../../../assets/app_icons/Icons/Checkbox-active.png')}
                    style={filterUniversityStyles.checkboxContainer}
                    resizeMode={'stretch'}
                  />
                ) : (
                  <Image
                    source={require('../../../assets/app_icons/Icons/Checkbox-default.png')}
                    style={filterUniversityStyles.checkboxContainer}
                    resizeMode={'stretch'}
                  />
                )}
                <Text style={filterUniversityStyles.itemText}>All</Text>
              </TouchableOpacity>
              {this.state.stateMajor.map((item, index) => {
                if (!isEmpty(this.state.searchMajor)) {
                  if (item.majorname.startsWith(this.state.searchMajor))
                    return (
                      <View key={index}>{this._renderItem(item, index)}</View>
                    );
                  else return undefined;
                } else {
                  return (
                    <View key={index}>{this._renderItem(item, index)}</View>
                  );
                }
              })}
            </KeyboardAwareScrollView>
          )}
        </View>
      </View>
    );
  }
}

const mapStateToProps = ({ universityDetails }) => {
  return {
    major: universityDetails.postMajor
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      setPostMajor: setPostMajor
    },
    dispatch
  )
});

export default connect(mapStateToProps, mapDispatchToProps)(MajorFilter);
