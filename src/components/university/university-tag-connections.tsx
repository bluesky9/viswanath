import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  Text,
  TextInput,
  Dimensions
} from 'react-native';
// import universityStyles from './home-styles';
import universityStyles from './university-styles';
const { width } = Dimensions.get('window');
import { NavigationScreenProps } from 'react-navigation';
import MSAHeader from '../layouts/header/header';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { getUserConnections } from '../../store/actions/Feed';
import { setTaggedUsers } from '../../store/actions/Universities';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {isEmpty} from 'lodash';

interface ILoginScreenProps extends NavigationScreenProps<{}> {
  actions: any;
  postTagState: any;
  userId;
}
interface ILoginScreenState {
  searchPeople: string;
  tagStates: boolean[];
}
class PostTag extends Component<ILoginScreenProps, ILoginScreenState> {
  constructor(props) {
    super(props);
    this.state = {
      searchPeople: '',
      tagStates: new Array(200).fill(false)
    };
  }
  componentDidMount() {
    this.props.actions.getUserConnections(this.props.userId);
    if (this.props.postTagState.universityDetails.postTags) {
      this.props.postTagState.universityDetails.postTags.map(item => {
        let ind = this.props.postTagState.getConnections.connections.indexOf(
          item
        );
        this.state.tagStates[ind] = true;
      });
    }
  }
  updateTags() {
    let userArray = [];
    this.state.tagStates.map((item, index) => {
      if (item) {
        userArray.push(
          this.props.postTagState.getConnections.connections[index]
        );
      }
    });
    this.props.actions.setTaggedUsers(userArray);
    this.props.navigation.goBack();
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: 'rgb(250,251,252)' }}>
        <MSAHeader
          title={'Tag People'}
          rightText={'DONE'}
          leftText={'CANCEL'}
          onPress={() => this.props.navigation.goBack()}
          onRightTextPress={() => this.updateTags()}
        />
        <View style={universityStyles.tagSearchbarContainer}>
          <TextInput
            onChangeText={txt => this.setState({ searchPeople: txt })}
            style={universityStyles.tagInput}
            placeholder={'Search people'}
            autoCapitalize={'none'}
          />
          <View style={universityStyles.tagSearchIcon}>
            <Image
              style={{ height: 15, width: 15 }}
              source={require('../../../assets/app_icons/Icons/search.png')}
            />
          </View>
        </View>
        <View style={universityStyles.tagInstructionContainer}>
          <Text style={universityStyles.tagInst}>Your connections</Text>
        </View>
        <View style={{ flex: 6, backgroundColor: 'rgb(242,245,249)' }}>
          <KeyboardAwareScrollView>
            {this.props.postTagState.getConnections.connections.map(
              (item, index) => {
                if (
                  !isEmpty(this.props.postTagState.getConnections.connections)
                ) {
                  if (item.name.startsWith(this.state.searchPeople))
                    return (
                      <View key={index} style={universityStyles.tagItemBox}>
                        <Image
                          source={{
                            uri: item.profilePicture
                              ? item.profilePicture
                              : 'https://pixabay.com/photo-1769656/'
                          }}
                          style={universityStyles.personThumbnail}
                        />
                        <Text style={universityStyles.likesItemText}>
                          {item.name}
                        </Text>
                        <TouchableOpacity
                          onPress={() => {
                            let arr = this.state.tagStates;
                            arr[index] = !arr[index];
                            this.setState({
                              tagStates: arr
                            });
                          }}
                          style={{
                            alignSelf: 'center',
                            position: 'absolute',
                            left: width - 42
                          }}
                        >
                          <Text
                            style={[
                              universityStyles.tagButtonText,
                              {
                                color: this.state.tagStates[index]
                                  ? 'rgb(204,20,51)'
                                  : 'rgb(53,135,230)'
                              }
                            ]}
                          >
                            {this.state.tagStates[index] ? 'UNTAG' : 'TAG'}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    );
                  else return undefined;
                } else {
                  return (
                    <View key={index} style={universityStyles.tagItemBox}>
                      <Image
                        source={{
                          uri: item.profilePicture
                            ? item.profilePicture
                            : 'https://pixabay.com/photo-1769656/'
                        }}
                        style={universityStyles.personThumbnail}
                      />
                      <Text style={universityStyles.likesItemText}>
                        {item.name}
                      </Text>
                      <TouchableOpacity
                        onPress={() => {
                          let arr = this.state.tagStates;
                          arr[index] = !arr[index];
                          this.setState({ tagStates: arr });
                        }}
                        style={{
                          alignSelf: 'center',
                          position: 'absolute',
                          left: width - 42
                        }}
                      >
                        <Text
                          style={[
                            universityStyles.tagButtonText,
                            {
                              color: this.state.tagStates[index]
                                ? 'rgb(204,20,51)'
                                : 'rgb(53,135,230)'
                            }
                          ]}
                        >
                          {this.state.tagStates[index] ? 'UNTAG' : 'TAG'}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  );
                }
              }
            )}
          </KeyboardAwareScrollView>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    postTagState: state,
    userId: state.userInfoAPI.userId
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      getUserConnections: getUserConnections,
      setTaggedUsers: setTaggedUsers
    },
    dispatch
  )
});

export default connect(mapStateToProps, mapDispatchToProps)(PostTag);
