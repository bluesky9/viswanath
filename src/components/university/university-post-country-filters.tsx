import React, { Component } from 'react';
import { View, TouchableOpacity, Text, Image } from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import MSAHeader from '../layouts/header/header';
import PeopleFilterSearch from '../people/people-filter-search';

import { filterUniversityStyles } from '../people/people-filter-styles';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  getUniversitiesByCountries,
  setUniversityPostCountryFilters
} from '../../store/actions/Universities.js';

import { find, cloneDeep, filter, isEmpty } from 'lodash';

import { getCountries } from '../../store/actions/Countries.js';
// import { Spinner } from 'native-base';
interface IPeopleFilterNavParams {}
interface IPeopleFilterProps
  extends NavigationScreenProps<IPeopleFilterNavParams> {
  countries: any;
  actions: any;
}
interface State {
  checkStates: boolean[];
  countryFilters: string[];
  rightButtonText: string;
  searchCountries;
  lagBugVal: number;
  stateCountries;
  selectAll;
  rightTitle;
}

class UniversityFilter extends Component<IPeopleFilterProps, State> {
  constructor(props) {
    super(props);
    this.state = {
      checkStates: new Array(234).fill(false),
      countryFilters: [],
      rightButtonText: 'APPLY',
      lagBugVal: 0,
      stateCountries: [],
      searchCountries: undefined,
      selectAll: false,
      rightTitle: undefined
    };
  }

  componentDidMount() {
    this.props.actions.getCountries();
  }

  componentWillReceiveProps(nextProps) {
    let stateCountries = [];
    let checkStates = this.props.countries.universityPostCountryFilters.filters;
    if (nextProps.countries.countries.countries.length > 0)
      nextProps.countries.countries.countries.map(item => {
        if (find(checkStates, obj => obj === item.countryName)) {
          stateCountries.push({ countryName: item.countryName, status: true });
        } else {
          stateCountries.push({ countryName: item.countryName, status: false });
        }
      });

    if (checkStates.length > 0) {
      this.setState({ rightTitle: 'CLEAR' });
    } else {
      this.setState({ selectAll: true });
      this.setState({ rightTitle: 'APPLY' });
    }
    this.setState({ stateCountries });
  }

  _renderItem = (item, index) => {
    return (
      <TouchableOpacity
        onPress={() => {
          let stateCountries = this.state.stateCountries;
          if (item.status) {
            stateCountries[index].status = false;
          } else {
            stateCountries[index].status = true;
          }
          this.setState({
            stateCountries: stateCountries,
            rightTitle: 'APPLY',
            selectAll: false
          });
        }}
        key={item.code}
        style={filterUniversityStyles.itemContainer}
      >
        {item.status ? (
          <Image
            source={require('../../../assets/app_icons/Icons/Checkbox-active.png')}
            style={filterUniversityStyles.checkboxContainer}
            resizeMode={'stretch'}
          />
        ) : (
          <Image
            source={require('../../../assets/app_icons/Icons/Checkbox-default.png')}
            style={filterUniversityStyles.checkboxContainer}
            resizeMode={'stretch'}
          />
        )}
        <Text style={filterUniversityStyles.itemText}>
          {item ? item.countryName : ''}
        </Text>
      </TouchableOpacity>
    );
  }

  _handleOnApplyPress = () => {
    let countryFilters = cloneDeep(this.props.countries.universityPostCountryFilters.filters);

    if (this.state.rightTitle === 'CLEAR') {
      this.props.actions.setUniversityPostCountryFilters([]);
      this.props.navigation.goBack();
      return ;
    }

    countryFilters = [];
    filter(this.state.stateCountries, obj => {
      if (obj.status) return countryFilters.push(obj.countryName);
      else {
        return undefined;
      }
    });
    this.props.actions.setUniversityPostCountryFilters(countryFilters);
    // peopleFilters.myUserId = this.props.userId;
    // this.props.getPeople(peopleFilters);
    this.props.navigation.goBack();
  };

  handleTextChange = text => {
    this.setState({ searchCountries: text });
  };

  onAllPress = () => {
    let stateCountries = this.state.stateCountries
    stateCountries.map((item) => {
      item.status = false
    })
    this.setState({ stateCountries, selectAll: true })
  }

  render() {
    return (
      <View style={filterUniversityStyles.container}>
        <MSAHeader
          title={'Country'}
          rightText={this.state.rightTitle}
          onPress={() => this.props.navigation.goBack()}
          onRightTextPress={() => this._handleOnApplyPress()}
        />
        <View style={filterUniversityStyles.mainContent}>
          <PeopleFilterSearch
            placeHolder={'Search Country'}
            onTextChange={this.handleTextChange}
          />
        </View>
        <View style={{ marginHorizontal: 15, flex: 1 }}>
          {this.props.countries.countries.countries.length > 0 && (
            <KeyboardAwareScrollView>
              <TouchableOpacity style={filterUniversityStyles.itemContainer}
                onPress={this.onAllPress}
              >
                {this.state.selectAll ? (
                  <Image
                    source={require('../../../assets/app_icons/Icons/Checkbox-active.png')}
                    style={filterUniversityStyles.checkboxContainer}
                    resizeMode={'stretch'}
                  />
                ) : (
                  <Image
                    source={require('../../../assets/app_icons/Icons/Checkbox-default.png')}
                    style={filterUniversityStyles.checkboxContainer}
                    resizeMode={'stretch'}
                  />
                )}
                <Text style={filterUniversityStyles.itemText}>All</Text>
              </TouchableOpacity>
              {this.state.stateCountries.map((item, index) => {
                if (!isEmpty(this.state.searchCountries)) {
                  if (item.countryName.startsWith(this.state.searchCountries))
                    return (
                      <View key={index}>{this._renderItem(item, index)}</View>
                    );
                  else return undefined;
                } else {
                  return (
                    <View key={index}>{this._renderItem(item, index)}</View>
                  );
                }
              })}
            </KeyboardAwareScrollView>
          )}
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    countries: state
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      getCountries: getCountries,
      getUniversitiesByCountries: getUniversitiesByCountries,
      setUniversityPostCountryFilters: setUniversityPostCountryFilters
    },
    dispatch
  )
});

export default connect(mapStateToProps, mapDispatchToProps)(UniversityFilter);
