import { StyleSheet, Dimensions, Platform } from 'react-native';
const { width } = Dimensions.get('window');
import { style } from '../../styles/variables';

const universityStyles = StyleSheet.create({
  container: {
    flex: 1
  },
  followedNumberContainer: {
    paddingVertical: 15,
    paddingHorizontal: 15
  },
  listContainer: {
    borderTopWidth: 1,
    borderColor: 'rgb(233,238,241)',
    paddingBottom: 54
  },
  listItemContainer: {
    flexDirection: 'row',
    backgroundColor: 'white',
    height: 160,
    borderColor: 'rgb(233,238,241)'
  },
  itemLeftContainer: {
    flex: 1,
    paddingHorizontal: 15,
    width: width / 2,
    height: 160,
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderRightWidth: 1,
    borderColor: 'rgb(233,238,241)'
  },
  itemRightContainer: {
    flex: 1,
    paddingHorizontal: 15,
    alignItems: 'center',
    justifyContent: 'center'
  },
  universityName: {
    fontSize: 12,
    lineHeight: 14,
    color: style.color.primary,
    textAlign: 'center',
    paddingVertical: 6,
    paddingTop: 13,
    fontFamily: style.font.family.rubikRegular
  },
  countryName: {
    fontSize: 10,
    lineHeight: 12,
    color: 'rgb(128,134,145)',
    fontFamily: style.font.family.rubikRegular
  },
  followBut: {
    height: 28,
    width: 54,
    borderRadius: 3,
    backgroundColor: 'rgb(53,135,230)',
    justifyContent: 'center',
    alignItems: 'center'
  },
  tabBut: {
    flex: 1,
    paddingVertical: 15,
    justifyContent: 'center',
    alignItems: 'center'
  },
  logoStyle: {
    height: 20,
    width: 20,
    borderRadius: 10,
    marginHorizontal: 6
  },
  tabContainer: {
    backgroundColor: 'white',
    alignSelf: 'stretch',
    flexDirection: 'row'
  },
  postBoxBut: {
    marginTop: 6,
    marginBottom: 12,
    paddingHorizontal: 15,
    paddingVertical: 12,
    marginHorizontal: 15,
    shadowOffset: { width: -2, height: 2 },
    shadowOpacity: 0.1,
    elevation: 3,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderRadius: 3,
    backgroundColor: 'white'
  },
  semiLine: {
    height: 1,
    width: width - 30,
    backgroundColor: 'rgb(233,238,241)',
    position: 'absolute',
    bottom: 0,
    left: 15
  },
  universityIconContainer: {
    backgroundColor: 'white',
    padding: 10,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderRightWidth: 1,
    borderColor: 'rgb(233,238,241)'
  },
  headerContainer: {
    alignSelf: 'stretch',
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: 'rgb(233,239,247)',
    backgroundColor: 'white',
    paddingTop: 12 + style.header.height,
    paddingBottom: 12
  },
  leftIconContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 15
  },
  centerHeaderContainer: {
    flex: 4,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  headerText: {
    color: 'rgb(83,87,94)',
    fontFamily: style.font.family.rubikRegular,
    fontSize: 12,
    lineHeight: 14
  },
  rightIconContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingRight: 15
  },
  picContainer: {
    width: 98,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    paddingLeft: 15,
    paddingTop: 15
  },
  mainTextContainer: { width: width - 98, paddingTop: 15 },
  nameText: {
    fontSize: 16,
    lineHeight: 20,
    fontFamily: style.font.family.ptSerif,
    color: 'rgb(59,62,68)'
  },
  countryText: {
    color: 'rgba(83,87,94,0.8)',
    fontFamily: style.font.family.rubikRegular,
    fontSize: 12,
    lineHeight: 18,
    marginHorizontal: 6
  },
  bioContainer: {
    height: 58,
    alignSelf: 'stretch',
    paddingHorizontal: 15,
    paddingTop: 11,
    borderColor: 'rgb(233,238,241)',
    borderBottomWidth: 1,
    backgroundColor: 'white'
  },
  bioText: {
    color: 'rgb(83,87,94)',
    fontFamily: style.font.family.rubikRegular,
    fontSize: 12
  },
  educationTextContainer: {
    height: 66,
    width: width,
    justifyContent: 'flex-start',
    paddingLeft: 15,
    paddingTop: 15,
    borderColor: 'rgb(233,238,241)',
    borderBottomWidth: 1,
    backgroundColor: 'white'
  },
  statsContainer: {
    paddingVertical: 10,
    alignSelf: 'stretch',
    flexDirection: 'row',
    borderBottomWidth: 0,
    borderColor: 'rgb(233,239,247)',
    backgroundColor: 'white'
  },
  statOneContainer: {
    flex: 0.6,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 15
  },
  statNmbrText: {
    color: 'rgb(59,62,68)',
    fontFamily: style.font.family.ptSerif,
    fontSize: 16,
    lineHeight: 20
  },
  statNameText: {
    color: 'rgba(83,87,94,0.8)',
    fontFamily: style.font.family.rubikRegular,
    fontSize: 12,
    lineHeight: 18
  },
  fieldBox: {
    height: 70,
    alignSelf: 'stretch',
    flexDirection: 'column',
    borderBottomWidth: 1,
    borderColor: 'rgb(233,239,247)',
    paddingHorizontal: 15,
    alignItems: 'flex-start',
    justifyContent: 'center',
    backgroundColor: 'white'
  },
  statButtonContainer: {
    flex: 0.6,
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingRight: 15
  },
  boxHead: {
    color: 'rgb(128,134,145)',
    fontFamily: style.font.family.rubikMedium,
    fontSize: 10,
    marginVertical: 5,
    letterSpacing: 1
  },
  boxLow: {
    color: 'rgb(53,135,230)',
    fontFamily: style.font.family.rubikRegular,
    fontSize: 12,
    marginVertical: 5,
    lineHeight: 18
  },
  editBoxIcon: {
    position: 'absolute',
    top: 15,
    left: width - 35,
    height: 15,
    width: 15
  },
  fieldBigHead: {
    color: 'rgb(59,62,68)',
    fontFamily: style.font.family.rubikRegular,
    fontSize: 16,
    marginVertical: 5
  },
  fieldDarkLow: {
    color: 'rgb(83,87,94)',
    fontFamily: style.font.family.rubikRegular,
    fontSize: 12
  },
  partBorderBottom: {
    justifyContent: 'flex-start',
    width: width - 30,
    alignSelf: 'center',
    paddingHorizontal: 0
  },
  expScroll: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    paddingTop: 51
  },
  expCard: {
    height: 118,
    width: 262,
    backgroundColor: 'white',
    marginHorizontal: 5,
    shadowOffset: { height: 1, width: -1 },
    shadowOpacity: 0.1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    padding: 15,
    elevation: 3
  },
  expCardBigText: {
    fontSize: 15,
    color: 'rgb(53,71,91)',
    fontFamily: style.font.family.rubikLight,
    marginVertical: 5
  },
  expCardSmallText: {
    fontSize: 12,
    color: 'rgb(83,87,94)',
    fontFamily: style.font.family.rubikRegular,
    marginTop: 5
  },
  asheetItem: {
    fontSize: 14,
    fontFamily: style.font.family.rubikRegular,
    color: 'rgb(59,62,68)'
  },
  dualHeaderLeftText: {
    fontSize: 10,
    lineHeight: 12,
    fontFamily: style.font.family.rubikMedium,
    color: 'rgb(53,71,91)',
    letterSpacing: 0.5
  },
  dualHeaderCenterText: {
    fontSize: 14,
    fontFamily: style.font.family.rubikLight,
    color: 'rgb(53,71,91)',
    letterSpacing: 0.5
  },
  dualHeaderRightText: {
    fontSize: 10,
    lineHeight: 12,
    fontFamily: style.font.family.rubikMedium,
    color: 'rgb(53,135,230)',
    letterSpacing: 0.5
  },
  skillInp: {
    width: width - 30,
    fontSize: 12,
    fontFamily: style.font.family.rubikRegular,
    color: 'rgb(173,181,196)',
    padding: 0
  },
  tagButtonText: {
    fontSize: 10,
    lineHeight: 12,
    color: 'rgb(53,135,230)',
    fontFamily: style.font.family.rubikMedium,
    letterSpacing: 0.5
  },
  educationCollg: {
    fontSize: 12,
    fontFamily: style.font.family.rubikMedium,
    color: 'rgb(83,87,94)'
  },
  educationLows: {
    fontSize: 12,
    fontFamily: style.font.family.rubikRegular,
    color: 'rgb(83,87,94)'
  },
  fullWidthContainer: { flex: 1, width: width },
  feedErrorHeadText: {
    fontSize: 20,
    fontFamily: style.font.family.ptSerifBold,
    textAlign: 'center',
    color: 'rgb(59,62,68)'
  },
  feedErrorInstructionText: {
    fontSize: 14,
    textAlign: 'center',
    width: width - 140,
    color: 'rgb(53,71,91)',
    marginTop: 10,
    marginBottom: 15
  },
  homePageSearchBar: {
    width: width - 30,
    // height: 40,
    padding: 12,
    fontSize: 12,
    fontFamily: style.font.family.rubikRegular
  },
  statusbarBlank: {
    height: 14,
    width: width,
    alignSelf: 'stretch',
    backgroundColor: 'white'
  },
  homeHeaderWrap: {
    flex: 0.65,
    alignSelf: 'stretch',
    backgroundColor: 'white'
  },
  homeInputboxWrap: {
    height: 64,
    alignSelf: 'stretch',
    justifyContent: 'center',
    backgroundColor: 'rgb(250,251,252)',
    alignItems: 'center'
  },
  homeMainFeedWrap: {
    flex: 6,
    alignSelf: 'stretch',
    backgroundColor: 'rgb(250,251,252)'
  },
  dualHeaderContainer: {
    height: 52,
    alignSelf: 'stretch',
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: 1.5,
    borderColor: 'rgb(233,239,247)',
    backgroundColor: 'white'
  },
  audienceListItem: {
    width: width,
    height: 54,
    borderBottomWidth: 1,
    borderColor: 'rgb(233,239,247)',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'white'
  },
  audienceItemText: {
    color: 'rgb(83,87,94)',
    fontSize: 14,
    fontFamily: style.font.family.rubikRegular
  },
  newPostItemWrap: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 15,
    paddingRight: 15,
    borderBottomWidth: 1,
    borderColor: 'rgb(233,239,247)',
    backgroundColor: 'white',
    paddingVertical: 20
  },
  newPostItemHead: {
    fontSize: 12,
    lineHeight: 14,
    fontWeight: 'bold',
    color: 'rgb(83,87,94)',
    fontFamily: style.font.family.rubikMedium
  },
  newPostItemLow: {
    fontSize: 14,
    lineHeight: 17,
    marginVertical: 4,
    color: 'rgb(53,135,230)',
    fontFamily: style.font.family.rubikRegular
  },
  newPostInput: {
    alignSelf: 'stretch',
    paddingVertical: 12,
    paddingHorizontal: 12,
    borderWidth: 1,
    borderRadius: 3,
    borderColor: style.color.lighterGrey,
    fontSize: 14,
    lineHeight: 17,
    marginBottom: 15,
    height: Platform.OS === 'ios' ? undefined : 42,
    maxHeight: 125
  },
  likesContent: {
    flex: 6,
    alignSelf: 'stretch',
    backgroundColor: 'rgb(242, 245, 249)'
  },
  likesCountContainer: {
    flex: 0.8,
    alignSelf: 'stretch',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingLeft: 15
  },
  likesCountText: {
    color: 'rgb(83,87,94)',
    fontFamily: style.font.family.rubikRegular
  },
  likesListContainer: {
    flex: 9,
    alignSelf: 'stretch',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  likesListItem: {
    width: width,
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  likesItemText: {
    fontSize: 12,
    color: 'rgb(53,135,230)',
    fontFamily: style.font.family.rubikRegular
  },
  personThumbnail: {
    marginHorizontal: 15,
    height: 30,
    width: 30,
    borderRadius: 15
  },
  tagSearchbarContainer: {
    height: 61,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgb(242,245,249)'
  },
  tagInput: {
    width: width - 30,
    height: 37,
    padding: 12,
    paddingLeft: 45,
    fontSize: 12,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    backgroundColor: 'white',
    shadowOffset: {
      height: 1,
      width: -1
    },
    zIndex: 3,
    elevation: 3,
    shadowOpacity: 0.2,
    borderRadius: 3
  },
  tagSearchIcon: {
    position: 'absolute',
    height: 15,
    width: 15,
    alignSelf: 'center',
    left: 30,
    zIndex: 4,
    elevation: 4
  },
  tagInstructionContainer: {
    flex: 0.6,
    borderWidth: 1,
    borderColor: 'rgb(233,239,247)',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'rgb(242,245,249)'
  },
  tagInst: {
    marginLeft: 15,
    fontSize: 14,
    color: 'rgb(83,87,94)',
    fontFamily: style.font.family.rubikRegular
  },
  tagListContainer: {
    backgroundColor: 'rgb(242, 245, 249)',
    flex: 6,
    alignSelf: 'stretch',
    justifyContent: 'flex-start'
  },
  tagItemBox: {
    width: width,
    height: 52,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    borderColor: 'rgb(233,239,247)',
    borderBottomWidth: 1
  },
  postNoteText: {
    fontSize: 12,
    color: 'rgb(108,111,125)',
    fontFamily: style.font.family.rubikRegular,
    opacity: 0.8
  },
  postMessage: {
    fontSize: 12,
    color: 'rgb(83, 87, 94)',
    fontFamily: style.font.family.rubikRegular
  },
  postButtonBar: {
    flex: 0.8,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  postBarLikesText: {
    fontSize: 12,
    // left: 6.8,
    color: 'rgb(59,62,68)',
    fontFamily: style.font.family.rubikRegular
  },
  postBarComText: {
    fontSize: 12,
    right: 12,
    color: 'rgb(149,159,171)',
    fontFamily: style.font.family.rubikRegular
  },
  postCommentContainer: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'rgb(242, 245, 249)',
    borderTopWidth: 2,
    borderColor: 'rgb(233,239,247)',
    top: Platform.OS === 'ios' ? 0 : -10
  },
  commentText: {
    fontSize: 11,
    color: 'rgb(83,87,94)',
    fontFamily: style.font.family.rubikRegular,
    marginVertical: 2,
    width: '90%'
  },
  commentTime: {
    fontSize: 10,
    color: 'rgba(108,111,125,0.8)',
    marginVertical: 2,
    fontFamily: style.font.family.rubikRegular
  }
});
export default universityStyles;
