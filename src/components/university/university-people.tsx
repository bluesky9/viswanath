import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
  Image
} from 'react-native';
import MSAButton from '../layouts/buttons/button';
import { NavigationScreenProps } from 'react-navigation';
import MSAHeader from '../layouts/header/header';

import { peopleScreenStyles } from '../people/people-screen-styles';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { get } from 'lodash';
import { style } from '../../styles/variables';
import { UniversitiesApi, PeopleApi, DialogModal } from '../../store/actions'
import { constants } from '../../constants';

const UNFOLLOW_POST = 'Are you sure to unfollow'
const UNFOLLOW_MSG = 'You will no longer see the posts or activity in your feed.'

interface ILoginScreenProps extends NavigationScreenProps<{}> {
  departments;
  departmentsCount;
  universityDetails;
  universityActions;
  userId;
  universityId;
  universityPeopleCount;
  universityPeople;
  peopleApi;
  dialogModal;
  universityFollowers;
  departmentPeople;
  departmentPeopleCount;
  followingUniversity;
}

interface State {
  pageSize: any;
}

class UniversityPeople extends Component<ILoginScreenProps, State> {
  constructor(props) {
    super(props);
    this.state = {
      pageSize: 15
    }
  }

  handleBackPress = () => {
    this.props.navigation.goBack();
  }

  componentDidMount() {
    let forDepartment = get(this.props.navigation.state.params, 'department', undefined);
    let pageSize = 'pageSize'

    let people = {
      myUserId: this.props.userId,
      countOnly: false,
      userTypes: ['PROFESSOR', 'UNIVERSITY_STAFF'],
      univIds: [this.props.universityId]
    }

    if (forDepartment) {
      let deptid = get(forDepartment, 'deptid', undefined)
      let deptId = 'deptId'
      let followDepartment = get(forDepartment, 'loggedInUserDepartmentFollowerList', undefined)
      if (followDepartment) {
        people.userTypes.push('CURRENT_STUDENT', 'PROSPECTIVE_STUDENT', 'ALUMNI')
      }
      people[pageSize] = this.props.universityPeopleCount <= this.state.pageSize ? this.props.universityPeopleCount : this.state.pageSize;
      people[deptId] = deptid
      this.props.universityActions.getDepartmentPeople(people);
    } else {
      // let followObject = find(this.props.universityFollowers, follower => follower.profile.id === this.props.userId)
      if (this.props.followingUniversity) {
        people.userTypes.push('CURRENT_STUDENT', 'PROSPECTIVE_STUDENT', 'ALUMNI')
      }
      people[pageSize] = this.props.universityPeopleCount <= this.state.pageSize ? this.props.universityPeopleCount : this.state.pageSize;
      this.props.universityActions.getUniversityPeople(people);
    }
    // pageSize: this.props.universityPeopleCount
    // console.log(people, 'Check New Prope')
    // if (!get(this.props.navigation.state.params, 'department', undefined))
    //   this.props.universityActions.getUniversityPeople(universityPeople);
  }

  onEndReached = () => {
    let forDepartment = get(this.props.navigation.state.params, 'department', undefined);
    let pageSize = 'pageSize'

    let people = {
      myUserId: this.props.userId,
      countOnly: false,
      userTypes: ['PROFESSOR', 'UNIVERSITY_STAFF'],
      univIds: [this.props.universityId]
    }

    if (forDepartment) {
      let deptid = get(forDepartment, 'deptid', undefined)
      let deptId = 'deptId'
      let followDepartment = get(forDepartment, 'loggedInUserDepartmentFollowerList', undefined)
      if (followDepartment) {
        people.userTypes.push('CURRENT_STUDENT', 'PROSPECTIVE_STUDENT', 'ALUMNI')
      }
      people[pageSize] = this.state.pageSize + 15;
      people[deptId] = deptid
      this.props.universityActions.getDepartmentPeople(people);
    } else {
      // let followObject = find(this.props.universityFollowers, follower => follower.profile.id === this.props.userId)
      if (this.props.followingUniversity) {
        people.userTypes.push('CURRENT_STUDENT', 'PROSPECTIVE_STUDENT', 'ALUMNI')
      }
      people[pageSize] = this.state.pageSize + 15;
      this.props.universityActions.getUniversityPeople(people);
    }
    this.setState({ pageSize: this.state.pageSize + 15 })
  }

  onProfilePicPressed = (id) => {
    this.props.peopleApi.getPeopleInfo(id)
  }

  onUnfollowPress = (peopleId) => {
    let forDepartment = get(this.props.navigation.state.params, 'department', undefined);
    let page = forDepartment ? 'Department' : 'University'
    this.props.dialogModal.dialogShows
      (
      UNFOLLOW_POST, UNFOLLOW_MSG, 'action', true, () => this.props.peopleApi.unFollowPeople(peopleId, page, 'other')
      )
  }

  onFollowPress = (peopleId) => {
    let forDepartment = get(this.props.navigation.state.params, 'department', undefined);
    let page = forDepartment ? 'Department' : 'University'
    this.props.peopleApi.followPeople(peopleId, page, 'other')
  }

  renderPeople = (obj) => {
    return (
      <View style={peopleScreenStyles.peopleCardContainer}>
        <TouchableOpacity onPress={() => this.onProfilePicPressed(obj.id)}>
          <Image
            source={obj.profilePicture ? { uri: obj.profilePicture } : require('../../../assets/images/user_dummy_image.png')}
            style={peopleScreenStyles.profileIconContainer}
          />
        </TouchableOpacity>
        <View style={peopleScreenStyles.peopleDetailsConatiner}>
          <View style={[peopleScreenStyles.nameContainer, { borderBottomWidth: 0 }]}>
            <View style={{ flex: 1 }}>
              <Text onPress={() => this.props.peopleApi.getPeopleInfo(obj.id)} style={peopleScreenStyles.peopleName}>{obj.name}</Text>
              <Text style={peopleScreenStyles.peopleLocation}>
                {obj.country}
              </Text>
            </View>
            {obj.id !== this.props.userId &&
              <View style={peopleScreenStyles.followContainer}>
                {obj.following ? (
                  <MSAButton
                    onPress={() => this.onUnfollowPress(obj.id)}
                    type={'inverted'}
                    bordered={true}
                    label={'Following'}
                    containerWidth={'55%'}
                    customButtonStyle={peopleScreenStyles.followButton}
                    textSize={12}
                  />
                ) : (
                    <MSAButton
                      onPress={() => this.onFollowPress(obj.id)}
                      type={'primary'}
                      label={'Follow'}
                      containerWidth={'55%'}
                      customButtonStyle={peopleScreenStyles.followButton}
                      textSize={12}
                    />
                  )}
              </View>
            }
          </View>
          {obj.userTypes &&
            obj.userTypes.map((item, index) => {
              return (
                item.userType !== 'PROSPECTIVE_STUDENT' ?
                  <Text style={peopleScreenStyles.normalText} key={index}>
                    <Text>{constants.USERTYPES.CONTEXT[item.userType] + ' '} </Text>
                    <Text style={{ color: style.color.primary }} onPress={() => {
                      this.props.universityActions.getUniversityBySlug(item.universitySlug, true);
                    }}>
                      {item.universityName}
                    </Text>
                  </Text> :
                  undefined
              )
            }
            )
          }
          {/* <Text style={peopleScreenStyles.normalText}>
            <Text>Currently studying at </Text>
            <Text style={{ color: style.color.primary }}>
              {'Something'}
            </Text>
          </Text> */}
          {obj.interests &&
            <Text style={[peopleScreenStyles.normalText, { marginTop: 1 }]}>
              {
                obj.interests.map((item, index) => {
                  if (index < 3)
                    return <Text key={index}>
                      <Text>{item.interestArea}</Text>
                      <Text>{obj.interests.length - 1 === index ? '' : ', '}</Text>
                    </Text>
                  else
                    return <View />
                })
              }
              {obj.interests.length > 3 &&
                <Text style={{ color: style.color.primary }}>{obj.interests.length - 3}</Text>
              }
            </Text>
          }
        </View>
      </View>
    )
  }

  render() {
    // console.log(this.props.universityFollowers, this.props.departmentsCount, 'Yo')
    // let departmentList = get(this.props.departments, 'content', [])
    let peopleList = [];
    let forDepartment = get(this.props.navigation.state.params, 'department', false)
    peopleList = forDepartment ? this.props.departmentPeople : this.props.universityPeople;
    return (
      <View>
        <MSAHeader
          title={forDepartment ? 'Department People' : 'University People'}
          onPress={this.handleBackPress}
        />
        <FlatList
          data={peopleList}
          renderItem={obj => this.renderPeople(obj.item)}
          keyExtractor={item => item.id.toString()}
          contentContainerStyle={{ paddingBottom: 80 }}
          showsVerticalScrollIndicator={false}
          extraData={this.props}
          onEndReached={(this.props.universityPeopleCount > this.state.pageSize) && this.onEndReached}
          onEndReachedThreshold={0}
          ListFooterComponent={() => {
            if (this.props.universityPeopleCount > this.state.pageSize)
              return <View style={{ paddingTop: 20 }}>
                <ActivityIndicator size='large' color={style.color.primary} />
              </View>
            else return <View />
          }}
        />
      </View>
    );
  }
}

const mapStateToProps = ({ universityDetails, userInfoAPI }) => {
  return {
    universityPeople: get(universityDetails, 'universityPeople', []),
    departments: get(universityDetails, 'departments', {}),
    universityDetails: get(universityDetails, 'details', {}),
    userId: userInfoAPI.userId,
    universityId: get(universityDetails, 'details.univ_id', undefined),
    universityPeopleCount: get(universityDetails, 'universityPeopleCount', 0),
    universityFollowers: get(universityDetails, 'followers', []),
    departmentPeople: get(universityDetails, 'departmentPeople', []),
    departmentPeopleCount: get(universityDetails, 'departmentPeopleCount', 0),
    followingUniversity: get(universityDetails, 'followingUniversity', false)
    // universityFollowers: get(universityDetails, 'followers', [])
  };
};

const mapDispatchToProps = dispatch => ({
  universityActions: bindActionCreators(UniversitiesApi, dispatch),
  peopleApi: bindActionCreators(PeopleApi, dispatch),
  dialogModal: bindActionCreators(DialogModal, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(UniversityPeople);
