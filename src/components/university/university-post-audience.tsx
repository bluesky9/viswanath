import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  ScrollView,
  Image,
  Dimensions
} from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
import MSAHeader from '../layouts/header/header';
import { bindActionCreators } from 'redux';
import {
  setPostAudience,
  setUniversityPostCountryFilters,
  setPostMajor
} from '../../store/actions/Universities';
const { width } = Dimensions.get('window');
import { Icon } from 'native-base';

import { connect } from 'react-redux';
import universityStyles from './university-styles';
import { constants } from '../../constants';
import { filterUsertypeStyles, filterUniversityStyles } from '../people/people-filter-styles';
import { get, find, cloneDeep, filter } from 'lodash';

interface ILoginScreenProps extends NavigationScreenProps<{}> {
  actions: any;
  audienceState: any;
  universityDetails: any;
}

interface State {
  userTypes;
  rightTitle;
  selectAll;
  disabledCountries: boolean;
  disabledStudentTypes: boolean;
}

class HomePostAudience extends Component<ILoginScreenProps, State> {
  constructor(props) {
    super(props);
    this.state = {
      userTypes: undefined,
      rightTitle: undefined,
      selectAll: false,
      disabledCountries: (props.universityDetails.usertype === 'PROSPECTIVE_STUDENT') ? true : false,
      disabledStudentTypes: (props.universityDetails.usertype === 'UNIVERSITY_STAFF' ||
                             props.universityDetails.usertype === 'PROFESSOR') ? false : true
    };
  }

  componentDidMount() {
    let filteredUserTypes = this.props.audienceState.universityDetails.postAudience;
    let filteredMajor = this.props.universityDetails.postMajor;
    let filteredCountry = this.props.audienceState.universityPostCountryFilters.filters;

    let userTypes = [];
    Object.keys(constants.USERTYPES.PLURAL).map((item) => {
      userTypes.push({ usertype: item, name: constants.USERTYPES.PLURAL[item]})
    })
    if (filteredUserTypes.length > 0) {
      userTypes.map((item, index) => {
        if (find(filteredUserTypes, obj => obj === item.usertype)) {
          userTypes[index].status = true;
        } else {
          userTypes[index].status = false;
        }
      })
      if ((filteredUserTypes.length > 0 && !this.state.disabledStudentTypes)
          || filteredCountry.length > 0 || filteredMajor.length > 0
        ) this.setState({ rightTitle: 'CLEAR' })
      else this.setState({ rightTitle: 'DONE' })
    } else {
      this.setState({ selectAll: true, rightTitle: 'DONE' })
    }
    this.setState({ userTypes });
  }

  _handleOnApplyPress = () => {
    let userTypeFilters = cloneDeep(this.props.audienceState.universityDetails.postAudience);

    if (this.state.rightTitle === 'CLEAR') {
      if (!this.state.disabledStudentTypes) this.props.actions.setPostAudience([]);
      this.props.actions.setUniversityPostCountryFilters([]);
      this.props.actions.setPostMajor([]);
      this.props.navigation.goBack();
      return ;
    }

    userTypeFilters.userTypes = [];
    filter(this.state.userTypes, (obj) => {
      if (obj.status)
        return userTypeFilters.userTypes.push(obj.usertype);
      else {
        return undefined;
      }
    })
    this.props.actions.setPostAudience(userTypeFilters.userTypes);
    // userTypeFilters.myUserId = this.props.userId;
    // this.props.getPeople(peopleFilters);
    this.props.navigation.goBack();
  }

  _renderItem(obj, index) {
    return (
      <TouchableOpacity
        disabled={this.state.disabledStudentTypes}
        style={filterUsertypeStyles.itemContainer}
        onPress={() => {
          let userTypes = this.state.userTypes;

          if (userTypes[index].status) {
            userTypes[index].status = false
          } else {
            userTypes[index].status = true
          }

          let allSelected = filter(userTypes, userType => !userType.status)
          if (allSelected.length === 0 || allSelected.length === 5) {
            userTypes.map((item) => {
              item.status = false
            })
            this.setState({ selectAll: true })
          } else {
            this.setState({ selectAll: false })
          }

          this.setState({ userTypes: userTypes, rightTitle: 'DONE' })
        }}

        key={index}
      >
        { obj.status ?
          <Image source={require('../../../assets/app_icons/Icons/Checkbox-active.png')} style={filterUsertypeStyles.checkboxContainer}
            resizeMode='stretch'
          />
          : <Image source={require('../../../assets/app_icons/Icons/Checkbox-default.png')} style={filterUsertypeStyles.checkboxContainer}
            resizeMode='stretch'
          />
        }
        <Text style={filterUsertypeStyles.itemText}>{obj.name}</Text>
      </TouchableOpacity>
    )
  }

  onEverbodyPress = () => {
    let postAudience = this.state.userTypes;
    postAudience.map((item) => {
      item.status = false
    })
    this.setState({ userTypes: postAudience, selectAll: true, rightTitle: 'APPLY' })
  }

  render() {
    let countries = this.props.audienceState.universityPostCountryFilters.filters.length === 0 ?
      'All countries'
      : this.props.audienceState.universityPostCountryFilters.filters.map((item) => {
        return ( item + ', ');
      })

    let majorData = get(this.props.navigation.state.params, 'majorData', [])
    let departmentPostRequest = get(this.props.navigation, 'state.params.deptRequest', false)
    let selectedMajor = this.props.universityDetails.postMajor.map((item, index) => {
      let majorExists = find(majorData, majorObj => majorObj.majorid === item)
      if (majorExists) {
        let majorName = majorExists.majorname + ( this.props.universityDetails.postMajor.length - 1 === index ? '' : ', ' );
        return majorName;
      } else {
        return undefined
      }
    })

    let major = (this.props.universityDetails.postMajor.length === 0
      ||
      this.props.universityDetails.postMajor.length ===  majorData.length ) ?
      'All majors' : selectedMajor ;

    return (
      <View style={filterUsertypeStyles.container}>
        <MSAHeader
          title={'Audience'}
          rightText={this.state.rightTitle}
          onPress={() => this.props.navigation.goBack()}
          onRightTextPress={() => this._handleOnApplyPress()}
        />
        <View style={filterUsertypeStyles.mainContent}>
          <ScrollView>
            <TouchableOpacity style={[filterUniversityStyles.itemContainer, { marginHorizontal: 15 }]}
              onPress={this.onEverbodyPress}
              disabled={this.state.disabledStudentTypes}
            >
              { this.state.selectAll ? (
                <Image
                  source={require('../../../assets/app_icons/Icons/Checkbox-active.png')}
                  style={filterUniversityStyles.checkboxContainer}
                  resizeMode={'stretch'}
                />
              ) : (
                <Image
                  source={require('../../../assets/app_icons/Icons/Checkbox-default.png')}
                  style={filterUniversityStyles.checkboxContainer}
                  resizeMode={'stretch'}
                />
              )}
              <Text style={filterUniversityStyles.itemText}>
                Everybody
              </Text>
            </TouchableOpacity>
            { this.state.userTypes && this.state.userTypes.map((item, index) => {
              return <View key={index}>
                {this._renderItem(item, index)}
                </View>
            })}
            <TouchableOpacity
              disabled={this.state.disabledCountries}
              onPress={() => {
                departmentPostRequest ? this.props.navigation.navigate('UniversityPostMajorFilter', { majorData })
                : this.props.navigation.navigate('UniversityPostCountryFilter');
                this.setState({
                  rightTitle: 'DONE'
                });
              }}
              style={[universityStyles.newPostItemWrap]}
            >
              <View>
                <Text
                  style={[universityStyles.newPostItemHead, { marginTop: 15 }]}
                >
                  { departmentPostRequest ? 'MAJOR' : 'COUNTRIES'}
                </Text>
                <Text style={{ marginVertical: 15, width: width - 30 }}>
                  <Text
                    style={[
                      universityStyles.newPostItemLow,
                      { marginBottom: 15 }
                    ]}
                  >
                    { departmentPostRequest ? major : countries}
                  </Text>
                </Text>
              </View>
              <View
                style={{
                  position: 'absolute',
                  alignSelf: 'center',
                  left: width - 25
                }}
              >
                <Icon
                  style={{ fontSize: 20, color: 'rgb(197,207,219)' }}
                  name={'arrow-forward'}
                />
              </View>
            </TouchableOpacity>
          </ScrollView>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    audienceState: state,
    universityDetails: state.universityDetails
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      setPostAudience: setPostAudience,
      setUniversityPostCountryFilters: setUniversityPostCountryFilters,
      setPostMajor: setPostMajor
    },
    dispatch
  )
});

export default connect(mapStateToProps, mapDispatchToProps)(HomePostAudience);
