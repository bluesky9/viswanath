import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
  Dimensions,
  Linking,
  Platform
} from 'react-native';

const { width } = Dimensions.get('window');

import {
  Card,
  CardItem,
  Left,
  Body,
  Text,
  Spinner,
  Right,
  Icon
} from 'native-base';
import ImagePicker from 'react-native-image-crop-picker';
import { NavigationScreenProps } from 'react-navigation';
import AppButton from '../layouts/buttons/button';
import universityStyles from './university-styles';
import HomeImageIndicators from '../home/home-image-indicators';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import homeStyles from '../home/home-styles';
import { get, find, filter, findIndex, map, isEmpty } from 'lodash';
import FollowModal from '../layouts/modal/modal-follow-university';
import ActionSheet from 'react-native-actionsheet';
import { DialogModal, FeedApi, PeopleApi } from '../../store/actions';
import Autolink from 'react-native-autolink';
import LinkPreview from 'react-native-link-preview';

import profilePic from '../../../assets/images/user_dummy_image.png';
import { departmentStyles } from './department/department-styles';
import { transformForPosts } from '../../utils';

import {
  getUniversityDetails,
  universityPostLike,
  getUnivesityDepartments,
  followUniversity,
  unfollowUniversity,
  clearUniversityData,
  reportPost,
  getUniversityFeeds
} from '../../store/actions/Universities.js';
const UNFOLLOW_UNIVERSITY = 'Are you sure to unfollow';
const UNFOLLOW_MSG =
  'You will no longer see the posts or activity in your feed.';

const userOptions = ['Cancel', 'Edit post', 'Delete post'];
const options = ['Cancel', 'Report post'];
let actionsheet = undefined;
let userActionSheet = undefined;

let postsUrl = [];

interface ILoginScreenNavParams { }
interface ILoginScreenProps
  extends NavigationScreenProps<ILoginScreenNavParams> {
  uniProfileState: any;
  actions: any;
  followers: any;
  userId: number;
  dialogModal: {
    dialogShows: Function;
  };
  universityFeeds;
  universityDetails;
  details;
  feedApi;
  universityPeopleCount;
  peopleApi;
  followingUniversity;
}

type State = {
  headerStatus: number;
  followStatus: boolean;
  followModal: boolean;
  selectedPostId;
  refreshing: boolean;
  postPreview: any;
};
let postLength = undefined;
let linkPreviewCount = 0;
let linkPreviewData = 0;

class UniversityProfile extends Component<ILoginScreenProps, State> {
  listRef;
  constructor(props) {
    super(props);
    this.state = {
      headerStatus: 0,
      followStatus: false,
      followModal: false,
      selectedPostId: undefined,
      refreshing: false,
      postPreview: []
    };
    this.getLinkDetails = this.getLinkDetails.bind(this);
  }

  showActionSheet(userPost, postId) {
    this.setState({ selectedPostId: postId });
    if (userPost && userActionSheet) userActionSheet.show();
    else if (actionsheet) actionsheet.show();
  }

  onDeletePost() {
    this.props.feedApi.deletePost(this.state.selectedPostId, 'UNIVERSITY_POST');
  }

  onReportPost = reason => {
    let data = {
      postId: this.state.selectedPostId,
      reason: reason
    };
    this.props.actions.reportPost(data);
  };

  getLinkDetails(message, post, postIndex, setState = false) {
    let regex = /(http|https|Https|Http):\/\/([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?/;
    if (regex.test(message)) {
      linkPreviewCount++;
      let url = message.match(regex)[0];
      LinkPreview.getPreview(url)
        .then(data => {
          post.preview = data;
          this.state.postPreview[postIndex] = data;
          linkPreviewData++;
          if (linkPreviewData === linkPreviewCount) {
            this.forceUpdate();
          }
          if (setState) {
            this.setState({
              postPreview: this.state.postPreview
            });
          }
        })
        .catch(error => {
          console.log(error);
        });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.uniProfileState.universityFeeds.universityFeeds) {
      if (
        nextProps.uniProfileState.universityFeeds.universityFeeds.length ===
        postLength + 1
      ) {
        this.setState({ postPreview: [] });
        if (this.listRef) {
          this.listRef.scrollToIndex({ index: 0, animated: true });
        }
      }
    }
  }

  showImageModal = (images, index) => {
    this.props.dialogModal.dialogShows(
      'images',
      undefined,
      'image',
      true,
      undefined,
      undefined,
      undefined,
      undefined,
      images,
      index
    );
  };

  _handleUserActionSheetPressed = index => {
    if (index === 2) {
      this.props.dialogModal.dialogShows(
        'Delete post',
        'Do you want to delete the post',
        'action',
        true,
        () => this.onDeletePost()
      );
    } else if (index === 1) {
      this.props.navigation.navigate('UniversityPostEdit', {
        postId: this.state.selectedPostId
      });
    }
  };

  _handleActionSheetPressed = index => {
    if (index === 1) {
      this.props.dialogModal.dialogShows(
        undefined,
        undefined,
        'report',
        true,
        reason => this.onReportPost(reason)
      );
      // this.props.feedApi.getPosts(1, 20, 0);
    }
  };

  openCamera() {
    ImagePicker.openCamera({
      cropping: false,
      compressImageQuality: Platform.OS === 'ios' ? 0 : 0.5
    })
      .then(image => {
        let imageObj = {
          uri: get(image, 'path', ''),
          name: new Date().getTime().toString(),
          type: get(image, 'mime', '')
        };
        this.props.navigation.navigate('UniversityPostCompose', {
          image: imageObj
        });
      })
      .catch(e => {
        console.log(e, 'Error');
      });
  }

  renderAboutSection() {
    return (
      <View style={{ margin: 15 }}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <Text
            style={[
              universityStyles.countryText,
              { lineHeight: 18, letterSpacing: 0.2 }
            ]}
          >
            {this.props.uniProfileState.universityDetails.details
              ? this.props.uniProfileState.universityDetails.details.description
              : ''}
          </Text>
        </ScrollView>
      </View>
    );
  }

  renderDepartment(item, following) {
    return (
      <View style={{ overflow: 'hidden', width: width }}>
        <View style={{ borderWidth: 0.5, borderColor: 'rgb(233,238,241)' }}>
          <CardItem style={{ paddingTop: 15 }}>
            <Left>
              {!item.deptlogo || !/http/.test(item.deptlogo) ? (
                <Image
                  source={profilePic}
                  style={[homeStyles.personThumbnail, { marginHorizontal: 0 }]}
                />
              ) : (
                  <Image
                    source={{ uri: item.deptlogo.replace('http', 'https') }}
                    style={[homeStyles.personThumbnail, { marginHorizontal: 0 }]}
                  />
                )}
              <Body>
                <Text
                  style={[homeStyles.newPostItemLow, { marginVertical: 2 }]}
                >
                  {item.deptname ? item.deptname : 'NA'}
                </Text>
                <Text note style={homeStyles.postNoteText}>
                  {item.deptType ? item.deptType : 'NA'}
                </Text>
              </Body>
            </Left>
            <View style={universityStyles.semiLine} />
            <Right>
              <TouchableOpacity
                style={universityStyles.followBut}
                disabled={!following}
              >
                <Text
                  style={[
                    universityStyles.dualHeaderLeftText,
                    { color: 'white' }
                  ]}
                >
                  FOLLOW
                </Text>
              </TouchableOpacity>
            </Right>
          </CardItem>
          <View
            style={{
              flexDirection: 'column',
              alignItems: 'flex-start',
              backgroundColor: 'white'
            }}
          >
            <Text
              style={[
                homeStyles.postMessage,
                { marginVertical: 5, marginLeft: 50, alignSelf: 'flex-start' }
              ]}
            >
              {item.major ? map(item.major, major => major.majorname) : 'NA'}
            </Text>
            <Text
              style={[
                homeStyles.postMessage,
                { marginVertical: 5, marginLeft: 50, alignSelf: 'flex-start' }
              ]}
            >
              {item.deptdescription}
            </Text>
          </View>
        </View>
      </View>
    );
  }

  renderDepartments(following: boolean) {
    return (
      <FlatList
        contentContainerStyle={{ paddingBottom: 70 }}
        data={
          this.props.uniProfileState.universityDetails.departments.content
            ? this.props.uniProfileState.universityDetails.departments.content
            : []
        }
        renderItem={obj => this.renderDepartment(obj.item, following)}
        keyExtractor={(item, _index) => item.deptid.toString()}
        showsVerticalScrollIndicator={false}
      />
    );
  }
  renderPosts(item, postIndex) {
    let m = transformForPosts(item.postTimeStamp);
    if (postsUrl[postIndex] && postsUrl[postIndex] !== item.postMessages) {
      postsUrl[postIndex] = item.postMessages;
      this.getLinkDetails(item.postMessages, item, postIndex, true);
    } else if (!this.state.postPreview[postIndex]) {
      postsUrl[postIndex] = item.postMessages;
      this.getLinkDetails(item.postMessages, item, postIndex);
    }
    return (
      <View style={{ overflow: 'hidden', width: width }}>
        <Card style={{ transform: [{ scaleX: 1.02 }] }}>
          <CardItem style={{ paddingTop: 16, paddingBottom: 0 }}>
            {item.title ? (
              <Text style={homeStyles.title}>{item.title}</Text>
            ) : (
                undefined
              )}
          </CardItem>
          <CardItem
            button
            onPress={() =>
              this.props.navigation.navigate('UniversityPostWithComments', {
                postItem: item
              })
            }
          >
            <Left>
              <TouchableOpacity
                onPress={() => {
                  if (item.user.id === this.props.userId)
                    this.props.navigation.navigate('MyProfile');
                  else this.props.peopleApi.getPeopleInfo(item.user.id);
                }}
              >
                <Image
                  source={
                    item.user.profPicPath
                      ? { uri: item.user.profPicPath }
                      : profilePic
                  }
                  style={[homeStyles.personThumbnail, { marginHorizontal: 0 }]}
                />
              </TouchableOpacity>
              <Body>
                <Text>
                  <Text
                    style={[homeStyles.newPostItemLow, { marginVertical: 2 }]}
                    onPress={() => {
                      if (item.user.id === this.props.userId)
                        this.props.navigation.navigate('MyProfile');
                      else this.props.peopleApi.getPeopleInfo(item.user.id);
                    }}
                  >
                    {item.user.name}{' '}
                  </Text>
                  <Text
                    style={[homeStyles.newPostItemLow, { marginVertical: 2 }]}
                  >
                    {item.university && ' • ' + item.university.name}
                  </Text>
                </Text>
                <Text note style={homeStyles.postNoteText}>
                  {m}
                </Text>
              </Body>
            </Left>
          </CardItem>
          <CardItem
            button
            onPress={() =>
              this.props.navigation.navigate('UniversityPostWithComments', {
                postItem: item
              })
            }
          >
            <Text
              style={[
                homeStyles.postMessage,
                { marginBottom: 6, alignSelf: 'center' }
              ]}
            >
              <Autolink text={item.postMessages} />
            </Text>
          </CardItem>
          {this.state.postPreview[postIndex] && (
            <View style={homeStyles.linkContainer}>
              <View
                style={{
                  height: 60,
                  width: 60,
                  overflow: 'hidden',
                  marginRight: 15,
                  alignItems: 'center'
                }}
              >
                {this.state.postPreview[postIndex].images.length > 0 && (
                  <Image
                    source={{
                      uri: this.state.postPreview[postIndex].images[0]
                    }}
                    style={homeStyles.linkImage}
                  />
                )}
              </View>
              <View style={{ flex: 1, flexWrap: 'wrap' }}>
                <Text style={homeStyles.linkContainerTitle}>
                  {get(this.state.postPreview[postIndex], 'title', 'NA')}
                </Text>
                <Text style={homeStyles.linkContainerDescription}>
                  {get(this.state.postPreview[postIndex], 'description', 'NA')}
                </Text>
              </View>
            </View>
          )}
          {item.files &&
            item.files.length > 0 && (
              <HomeImageIndicators
                images={item.files}
                imageModal={this.showImageModal}
              />
            )}
          <CardItem style={{ height: 56, justifyContent: 'space-between' }}>
            <View style={{ flexDirection: 'row' }}>
              <TouchableOpacity
                onPress={() => {
                  this.props.actions.universityPostLike(item.postid);
                }}
                disabled={!this.props.followingUniversity}
              >
                {get(item, 'likes', []).length > 0 &&
                  find(
                    item.likes,
                    likedObj => likedObj.user.id === this.props.userId
                  ) ? (
                    <Icon
                      name='md-heart'
                      style={{
                        color: 'rgb(255, 97, 116)',
                        fontSize: 20,
                        width: 20
                      }}
                    />
                  ) : (
                    <Icon
                      name='md-heart-outline'
                      style={{
                        color: 'rgb(207, 208, 220)',
                        fontSize: 20,
                        width: 20
                      }}
                    />
                  )}
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate('PostLikes', {
                    likes: item.likes
                  })
                }
              >
                {get(item, 'likes', []).length > 0 &&
                  find(
                    item.likes,
                    likedObj => likedObj.user.id === this.props.userId
                  ) ? (
                    <Text style={homeStyles.postBarLikesText}>
                      {item.likes.length > 0 && item.likes.length} Likes
                  </Text>
                  ) : (
                    <Text style={homeStyles.postBarLikesTextDefault}>
                      {item.likes.length > 0 && item.likes.length} Likes
                  </Text>
                  )}
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('UniversityPostWithComments', {
                  postItem: item
                });
              }}
              style={{ flexDirection: 'row' }}
            >
              <Icon
                name='md-text'
                style={{ color: 'rgb(207, 208, 220)', fontSize: 20, width: 20 }}
              />
              <Text style={[homeStyles.postBarComText]}>
                {item.comment.length > 0 && item.comment.length} comments
              </Text>
            </TouchableOpacity>
            <View style={{ flexDirection: 'row' }}>
              <Icon
                name='md-share'
                style={{ color: 'rgb(207, 208, 220)', fontSize: 20, width: 20 }}
              />
              <Text style={[homeStyles.postBarComText]}>
                {item.share_post ? 1 : 0} Share
              </Text>
            </View>
            <TouchableOpacity
              style={{ flexDirection: 'row' }}
              onPress={() =>
                this.showActionSheet(
                  item.user.id === this.props.userId ? true : false,
                  item.postid
                )
              }
            >
              <Icon
                name='ios-more'
                style={{ color: 'rgb(207, 208, 220)', width: 20 }}
              />
            </TouchableOpacity>
          </CardItem>
        </Card>
      </View>
    );
  }
  renderPostList() {
    let checkFollowIndex = findIndex(
      this.props.followers,
      follower => follower.profile.id === this.props.userId
    );
    let checkFollowStatus = 'Follow';
    if (checkFollowIndex !== -1) {
      checkFollowStatus =
        this.props.followers[checkFollowIndex].universityConnectionStatus ===
          'PENDING'
          ? 'Pending'
          : 'Unfollow';
    }
    let feeds = this.props.uniProfileState.universityFeeds.universityFeeds
      ? this.props.uniProfileState.universityFeeds.universityFeeds
      : [];
    if (!this.props.followingUniversity) {
      feeds = filter(feeds, feed => !feed.restricted);
    }
    return (
      <View>
        {checkFollowStatus === 'Unfollow' && (
          <View style={universityStyles.postBoxBut}>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('UniversityPostCompose')
              }
            >
              <Text
                style={[
                  homeStyles.homePageSearchBar,
                  { color: 'rgba(108,111,125,0.5)' }
                ]}
              >
                Write a university post..
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.openCamera()}
              style={{ justifyContent: 'center' }}
            >
              <Image
                source={require('../../../assets/app_icons/Icons/Camera-grey.png')}
                style={{ alignSelf: 'center', width: 20, height: 20 }}
              />
            </TouchableOpacity>
          </View>
        )}
        <FlatList
          ref={ref => {
            if (!this.listRef) {
              this.listRef = ref;
            }
          }}
          contentContainerStyle={{ paddingBottom: 70 }}
          data={feeds}
          renderItem={obj => this.renderPosts(obj.item, obj.index)}
          keyExtractor={(item, index) => item.postid.toString() + index}
          showsVerticalScrollIndicator={false}
          extraData={this.props}
          refreshing={this.state.refreshing}
          onRefresh={() =>
            this.props.actions.getUniversityFeeds(
              get(
                this.props.uniProfileState,
                'universityDetails.details.univ_id',
                ''
              ),
              this.props.universityFeeds.length,
              0
            )
          }
        />
      </View>
    );
  }

  render() {
    // Check if user is following univerity
    //
    let webLink = get(this.props.details, 'webpage', '');
    if (
      this.props.uniProfileState.universityFeeds &&
      this.props.uniProfileState.universityFeeds.universityFeeds
    ) {
      postLength = this.props.uniProfileState.universityFeeds.universityFeeds
        .length;
    }
    let index = findIndex(
      this.props.followers,
      follower => follower.profile.id === this.props.userId
    );
    let followStatus = 'Follow';
    if (index !== -1) {
      followStatus =
        this.props.followers[index].universityConnectionStatus === 'PENDING'
          ? 'Pending'
          : 'Unfollow';
    }
    if (this.props.uniProfileState.universityDetails.details === undefined) {
      return (
        <View
          style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
        >
          <Spinner />
        </View>
      );
    }
    return (
      <View style={{ flex: 1 }}>
        <ActionSheet
          ref={o => (userActionSheet = o)}
          options={userOptions}
          cancelButtonIndex={0}
          destructiveButtonIndex={2}
          onPress={i => this._handleUserActionSheetPressed(i)}
        />
        <ActionSheet
          ref={o => (actionsheet = o)}
          options={options}
          cancelButtonIndex={0}
          destructiveButtonIndex={1}
          onPress={i => this._handleActionSheetPressed(i)}
        />
        <View
          style={[universityStyles.headerContainer, { borderBottomWidth: 0 }]}
        >
          <View style={universityStyles.leftIconContainer}>
            <TouchableOpacity
              onPress={() => {
                this.props.actions.clearUniversityData();
                this.props.navigation.goBack();
              }}
            >
              <Icon
                name={'md-arrow-back'}
                style={{ fontSize: 30, color: 'rgb(197, 207, 219)' }}
              />
            </TouchableOpacity>
          </View>
          <View style={universityStyles.centerHeaderContainer}>
            <Image
              source={{
                uri: this.props.uniProfileState.universityDetails.details
                  .logoURL
                  ? this.props.uniProfileState.universityDetails.details.logoURL
                  : 'https://252radio.com/wp-content/uploads/2016/11/default-user-image-300x300.png'
              }}
              style={universityStyles.logoStyle}
            />
            <Text style={universityStyles.headerText}>
              {this.props.uniProfileState.universityDetails.details.name}
            </Text>
          </View>
          <View style={universityStyles.rightIconContainer}>
            {/* <TouchableOpacity>
              <Image
                source={require('../../../assets/app_icons/Icons/bento-menu.png')}
              />
            </TouchableOpacity> */}
          </View>
        </View>
        <View
          style={{
            alignSelf: 'stretch',
            flexDirection: 'row',
            backgroundColor: 'white'
          }}
        >
          <View style={universityStyles.picContainer}>
            <Image
              source={{
                uri: this.props.uniProfileState.universityDetails.details
                  .logoURL
                  ? this.props.uniProfileState.universityDetails.details.logoURL
                  : 'https://252radio.com/wp-content/uploads/2016/11/default-user-image-300x300.png'
              }}
              style={{ height: 68, width: 68, resizeMode: 'contain' }}
            />
          </View>
          <View style={universityStyles.mainTextContainer}>
            <Text style={universityStyles.nameText}>
              {this.props.uniProfileState.universityDetails.details.name}
            </Text>
            <View
              style={{
                flexDirection: 'row',
                marginTop: 6,
                alignItems: 'center'
              }}
            >
              <Icon
                name={'pin'}
                style={{ color: 'rgb(207,208,220)', fontSize: 23, top: 2 }}
              />
              <Text style={universityStyles.countryText}>
                {this.props.uniProfileState.universityDetails.details.country}
              </Text>
            </View>
            {!isEmpty(webLink) && (
              <TouchableOpacity
                style={{
                  paddingTop: 5,
                  flexDirection: 'row',
                  paddingHorizontal: 6
                }}
                onPress={() => {
                  Linking.openURL(webLink);
                }}
              >
                <Icon name='md-link' style={departmentStyles.linkIcon} />
                <Text style={departmentStyles.linkText}>{webLink}</Text>
              </TouchableOpacity>
            )}
          </View>
        </View>
        <View style={universityStyles.statsContainer}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('UniversityPeople')}
            style={universityStyles.statOneContainer}
          >
            <Text style={universityStyles.statNmbrText}>
              {this.props.universityPeopleCount}
            </Text>
            <Text style={universityStyles.statNameText}>People</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={universityStyles.statOneContainer}
            onPress={() =>
              this.props.navigation.navigate('UniversityDepartments')
            }
          >
            <Text style={universityStyles.statNmbrText}>
              {get(
                this.props.uniProfileState,
                'universityDetails.departments.totalElements',
                0
              )}
            </Text>
            <Text style={universityStyles.statNameText}>Dept</Text>
          </TouchableOpacity>
          <View
            style={[
              universityStyles.statOneContainer,
              { flex: 2, alignItems: 'flex-end', paddingRight: 15 }
            ]}
          >
            <AppButton
              type={
                followStatus === 'Follow'
                  ? 'primary'
                  : followStatus === 'Pending'
                    ? 'grey'
                    : 'inverted'
              }
              bordered={true}
              label={followStatus}
              customButtonStyle={{ paddingTop: 7, paddingBottom: 7 }}
              containerWidth={'49%'}
              disabled={followStatus === 'Pending'}
              onPress={() =>
                index === -1
                  ? this.setState({
                    followModal: true
                  })
                  : this.props.dialogModal.dialogShows(
                    UNFOLLOW_UNIVERSITY,
                    UNFOLLOW_MSG,
                    'action',
                    true,
                    () =>
                      this.props.actions.unfollowUniversity(
                        this.props.followers[index].university.univ_id,
                        this.props.followers[index].id
                      )
                  )
              }
            />
          </View>
        </View>
        <View style={universityStyles.tabContainer}>
          <TouchableOpacity
            onPress={() =>
              this.setState({
                headerStatus: 0
              })
            }
            style={[
              universityStyles.tabBut,
              {
                borderBottomWidth: this.state.headerStatus === 0 ? 3 : 1,
                borderBottomColor:
                  this.state.headerStatus === 0
                    ? 'rgb(204,20,51)'
                    : 'rgb(233,238,241)'
              }
            ]}
          >
            <Text
              style={[
                universityStyles.dualHeaderLeftText,
                {
                  color:
                    this.state.headerStatus === 0
                      ? 'rgb(204,20,51)'
                      : 'rgb(173,181,196)'
                }
              ]}
            >
              ABOUT
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              this.setState({
                headerStatus: 1
              })
            }
            style={[
              universityStyles.tabBut,
              {
                borderBottomWidth: this.state.headerStatus === 1 ? 3 : 1,
                borderBottomColor:
                  this.state.headerStatus === 1
                    ? 'rgb(204,20,51)'
                    : 'rgb(233,238,241)'
              }
            ]}
          >
            <Text
              style={[
                universityStyles.dualHeaderLeftText,
                {
                  color:
                    this.state.headerStatus === 1
                      ? 'rgb(204,20,51)'
                      : 'rgb(173,181,196)'
                }
              ]}
            >
              POSTS
            </Text>
          </TouchableOpacity>
          {/* <TouchableOpacity
            onPress={() =>
              this.setState({
                headerStatus: 2
              })
            }
            style={[
              universityStyles.tabBut,
              {
                borderBottomWidth: this.state.headerStatus === 2 ? 3 : 1,
                borderBottomColor:
                  this.state.headerStatus === 2
                    ? 'rgb(204,20,51)'
                    : 'rgb(233,238,241)'
              }
            ]}
          >
            <Text
              style={[
                universityStyles.dualHeaderLeftText,
                {
                  color:
                    this.state.headerStatus === 2
                      ? 'rgb(204,20,51)'
                      : 'rgb(173,181,196)'
                }
              ]}
            >
              DEPARTMENTS
            </Text>
          </TouchableOpacity> */}
        </View>
        <FollowModal
          onClose={() => this.setState({ followModal: false })}
          isShown={this.state.followModal}
          domain={get(
            this.props.uniProfileState,
            'universityDetails.details.domain',
            ''
          )}
          onSubmit={data => {
            console.log('Called Once')
            if (data.universityEmail) {
              this.props.actions.followUniversity(
                get(
                  this.props.uniProfileState,
                  'universityDetails.details.univ_id',
                  ''
                ),
                data
              );
            } else {
              this.props.actions.followUniversity(
                get(
                  this.props.uniProfileState,
                  'universityDetails.details.univ_id',
                  ''
                ),
                data
              );
            }
            this.setState({ followModal: false });
          }}
        />
        <View style={{ flex: 1, backgroundColor: '#fff', paddingVertical: 10 }}>
          {this.state.headerStatus === 0
            ? this.renderAboutSection()
            : this.state.headerStatus === 1
              ? this.renderPostList()
              : this.renderDepartments(index > -1)}
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    uniProfileState: state,
    followers: state.universityDetails.followers,
    userId: state.userInfoAPI.userId,
    universityFeeds: state.universityFeeds.universityFeeds,
    universityPeopleCount: state.universityDetails.universityPeopleCount,
    followingUniversity: state.universityDetails.followingUniversity
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      getUniversityDetails: getUniversityDetails,
      universityPostLike: universityPostLike,
      getUnivesityDepartments: getUnivesityDepartments,
      followUniversity: followUniversity,
      unfollowUniversity: unfollowUniversity,
      clearUniversityData: clearUniversityData,
      reportPost: reportPost,
      getUniversityFeeds: getUniversityFeeds
    },
    dispatch
  ),
  dialogModal: bindActionCreators(DialogModal, dispatch),
  feedApi: bindActionCreators(FeedApi, dispatch),
  peopleApi: bindActionCreators(PeopleApi, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UniversityProfile);
