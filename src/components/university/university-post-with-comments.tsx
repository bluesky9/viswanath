import React, { Component } from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  Platform,
  TextInput,
  Animated,
  Keyboard,
  Linking
} from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
import { Card, CardItem, Left, Body, Text, Icon } from 'native-base';
import homeStyles from '../home/home-styles';
import AppHeader from '../layouts/header/header';
import ActionSheet from 'react-native-actionsheet';
import HomeImageIndicators from '../home/home-image-indicators';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Autolink from 'react-native-autolink';
import LinkPreview from 'react-native-link-preview';
import { urlify } from '../../validator/regx-validator';

import { style } from '../../styles/variables';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { DialogModal, FeedApi, PeopleApi } from '../../store/actions';
import { get, find, isEmpty } from 'lodash';

import {
  universityPostLike,
  updateComment,
  postComment,
  reportPost,
  deleteComment
} from '../../store/actions/Universities.js';
import { transformForPosts } from '../../utils';

// const userid = 8; // Replace After Persisting Profile Data

const CANCEL_INDEX = 0;
const userOptions = ['Cancel', 'Edit post', 'Delete post'];
const commentOptions = ['Cancel', 'Edit comment', 'Delete comment'];
const options = ['Cancel', 'Report post'];
let actionsheet = undefined;
let userActionSheet = undefined;
let commentActionSheet = undefined;
let previousUrl = '';

interface ILoginScreenProps extends NavigationScreenProps<{}> {
  navigation;
  postWithComment;
  dialog;
  universityFeeds;
  universityPostLike;
  uniPostState;
  updateComment;
  postComment;
  reportPost;
  deleteComment;
  userId;
  feedApi;
  peopleApi;
  followingUniversity;
}
interface State {
  comment: string;
  actionSheetOptions;
  selectedComment;
  editComment;
  keyboardHeight;
  postPreview: any;
}

class UniversityPostComments extends Component<ILoginScreenProps, State> {
  keyboardShowListener;
  keyboardHideListener;
  scrollView: any;
  constructor(props) {
    super(props);
    this.state = {
      comment: undefined,
      actionSheetOptions: userOptions,
      selectedComment: undefined,
      editComment: false,
      keyboardHeight: new Animated.Value(0),
      postPreview: undefined
    };
    this.getLinkDetails = this.getLinkDetails.bind(this);
  }

  componentDidMount() {
    this.keyboardShowListener = Keyboard.addListener(
      'keyboardWillShow',
      this.keyboardWillShow
    );
    this.keyboardHideListener = Keyboard.addListener(
      'keyboardWillHide',
      this.keyboardWillHide
    );
  }

  keyboardWillShow = e => {
    let keyboardHeight = e.endCoordinates.height;
    let animateDuration = e.duration;
    if (this.scrollView) this.scrollView.scrollToEnd()
    Animated.timing(this.state.keyboardHeight, {
      toValue: keyboardHeight,
      duration: animateDuration
    }).start();
  };

  keyboardWillHide = e => {
    let animateDuration = e.duration;
    Animated.timing(this.state.keyboardHeight, {
      toValue: 0,
      duration: animateDuration
    }).start();
  };

  getLinkDetails(message) {
    let regex = /(http|https|Https|Http):\/\/([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?/
    if (regex.test(message)) {
      let url = message.match(regex)[0];
      if (url !== previousUrl) {
        previousUrl = url;
        LinkPreview.getPreview(url)
          .then(data => {
            this.setState({
              postPreview: data
            })
          })
          .catch((error) => {
            console.log(error)
          });
      }
    }
  }

  componentWillUnmount() {
    this.keyboardShowListener.remove();
    this.keyboardHideListener.remove();
    previousUrl = '';
  }

  showActionSheet(userPost) {
    if (userPost) userActionSheet.show();
    else actionsheet.show();
  }

  handleTextChange = text => {
    this.setState({ comment: text });
  };

  onDeletePost() {
    this.props.navigation.goBack();
    this.props.feedApi.deletePost(this.props.postWithComment.postid, 'UNIVERSITY_POST')
  }

  showImageModal = (images, index) => {
    this.props.dialog.dialogShows(
      'images',
      undefined,
      'image',
      true,
      undefined,
      undefined,
      undefined,
      undefined,
      images,
      index
    );
  };

  _handleUserActionSheetPressed = index => {
    if (index === 2) {
      this.props.dialog.dialogShows(
        'Delete post',
        'Do you want to delete the post',
        'action',
        true,
        () => this.onDeletePost()
      );
    }
    if (index === 1) {
      this.props.navigation.navigate('UniversityPostEdit', {
        postId: this.props.postWithComment.postid
      });
    }
  };

  onSubmitComment = postId => {
    Keyboard.dismiss();
    if (!isEmpty(this.state.comment)) {
      let data = {
        postId: postId,
        comment: this.state.comment
      };
      this.props.postComment(data, 'University').then(() => {
        setTimeout(() => {
          if (this.scrollView) this.scrollView.scrollToEnd()
        }, 500)
      });
      this.setState({ comment: undefined });
    }
  };

  onEditComment = () => {
    Keyboard.dismiss();
    if (!isEmpty(this.state.comment)) {
      let data = {
        id: this.state.selectedComment,
        comments: this.state.comment
      };
      this.props.updateComment(this.props.postWithComment.postid, data, 'University');
      this.setState({ comment: undefined, editComment: false });
    }
  };

  onDeleteComment = () => {
    this.props.deleteComment(
      this.props.postWithComment.postid,
      this.state.selectedComment,
      'University'
    );
    this.setState({ comment: undefined });
  };

  handleOnLongPress = id => {
    this.setState({ selectedComment: id });
    commentActionSheet.show();
  };

  _handleCommentActionSheet = i => {
    let oldComment = find(
      this.props.postWithComment.comment,
      comment => comment.id === this.state.selectedComment
    );
    if (i === 2) {
      this.props.dialog.dialogShows(
        'Delete comment',
        'Do you want to delete the comment',
        'action',
        true,
        () => this.onDeleteComment()
      );
    }
    if (i === 1) {
      this.setState({ comment: oldComment.comments, editComment: true });
    }
  };

  onReportPost = reason => {
    let data = {
      postId: this.props.postWithComment.postid,
      reason: reason
    };
    this.props.reportPost(data, true);
  };

  _handleActionSheetPressed = index => {
    if (index === 1) {
      this.props.dialog.dialogShows(
        undefined,
        undefined,
        'report',
        true,
        reason => this.onReportPost(reason)
      );
      // this.props.feedApi.getPosts(1, 20, 0);
    }
  };

  renderTextInput = () => {
    return (
      <TextInput
        style={{
          backgroundColor: 'white',
          width: '100%',
          height: '100%',
          paddingLeft: 10,
          fontSize: 13,
          fontFamily: style.font.family.rubikRegular,
          // opacity: this.state.editComment ? 0.5 : 1,
          color: 'rgb(59,62,68)'
        }}
        underlineColorAndroid={'transparent'}
        placeholderTextColor={'rgba(108,111,125,0.5)'}
        placeholder={'Tap to write a comment'}
        onChangeText={text => this.handleTextChange(text)}
        value={this.state.comment}
        spellCheck={false}
        autoCorrect={false}
        autoCapitalize={'none'}
      />
    );
  };

  render() {
    let post = this.props.postWithComment;
    this.getLinkDetails(post.postMessages);
    let m = transformForPosts(get(post, 'postTimeStamp', 0));
    if (post)
      return (
        <View style={homeStyles.fullWidthContainer}>
          <View style={{ backgroundColor: 'white' }}>
            <AppHeader
              title={post.user.firstName + "'s " + 'post'}
              onPress={() => this.props.navigation.goBack()}
            />
          </View>
          <ActionSheet
            ref={o => (userActionSheet = o)}
            options={userOptions}
            cancelButtonIndex={CANCEL_INDEX}
            destructiveButtonIndex={2}
            onPress={i => this._handleUserActionSheetPressed(i)}
          />
          {Platform.OS === 'ios' ? (
            <View style={{ alignSelf: 'stretch', backgroundColor: 'white' }} />
          ) : (
              undefined
            )}
          <ActionSheet
            ref={o => (commentActionSheet = o)}
            options={commentOptions}
            cancelButtonIndex={CANCEL_INDEX}
            destructiveButtonIndex={2}
            onPress={i => this._handleCommentActionSheet(i)}
          />
          {Platform.OS === 'ios' ? (
            <View style={{ alignSelf: 'stretch', backgroundColor: 'white' }} />
          ) : (
              undefined
            )}
          <ActionSheet
            ref={o => (actionsheet = o)}
            options={options}
            cancelButtonIndex={CANCEL_INDEX}
            destructiveButtonIndex={1}
            onPress={i => this._handleActionSheetPressed(i)}
          />
          {Platform.OS === 'ios' ? (
            <View style={{ alignSelf: 'stretch', backgroundColor: 'white' }} />
          ) : (
              undefined
            )}
          <KeyboardAwareScrollView ref={ref => (this.scrollView = ref)}>
            <View style={{ flex: 1.45, elevation: 3 }}>
              <Card style={{ top: -6, transform: [{ scaleX: 1.02 }] }}>
                <CardItem style={{ paddingTop: 16, paddingBottom: 0 }}>
                  {post.title ?
                    <Text style={homeStyles.title}>{post.title}</Text> : undefined
                  }
                </CardItem>
                <CardItem>
                  <Left>
                    <TouchableOpacity onPress={() => {
                      if (post.user.id === this.props.userId)
                        this.props.navigation.navigate('MyProfile');
                      else
                        this.props.peopleApi.getPeopleInfo(post.user.id)
                    }
                    }>
                      <Image
                        source={
                          post.user.profPicPath
                            ? { uri: post.user.profPicPath }
                            : require('../../../assets/images/user_dummy_image.png')
                        }
                        style={[
                          homeStyles.personThumbnail,
                          { marginHorizontal: 0 }
                        ]}
                      />
                    </TouchableOpacity>
                    <Body>
                      <Text>
                        <Text
                          style={[homeStyles.newPostItemLow, { marginVertical: 2 }]}
                          onPress={() => {
                            if (post.user.id === this.props.userId)
                              this.props.navigation.navigate('MyProfile');
                            else
                              this.props.peopleApi.getPeopleInfo(post.user.id)
                          }
                          }
                        >
                          {post.user.name}
                        </Text>
                        <Text
                          style={[homeStyles.newPostItemLow, { marginVertical: 2 }]}
                        >
                          {post.university && ' • ' + post.university.name}
                        </Text>
                      </Text>
                      <Text note style={homeStyles.postNoteText}>
                        {m}
                      </Text>
                    </Body>
                  </Left>
                </CardItem>
                <CardItem>
                  <Text
                    style={[
                      homeStyles.postMessage,
                      { marginBottom: 6, alignSelf: 'center' }
                    ]}
                  >
                    <Autolink text={post.postMessages} />
                  </Text>
                </CardItem>
                {this.state.postPreview &&
                  <TouchableOpacity
                    style={homeStyles.linkContainer}
                    onPress={() => Linking.openURL(urlify(post.postMessages))
                      .catch(err => console.error('An error occurred', err))}
                  >
                    <View style={{ height: 60, width: 60, overflow: 'hidden', marginRight: 15, alignItems: 'center' }}>
                      {this.state.postPreview.images.length > 0 && <Image source={{ uri: this.state.postPreview.images[0] }}
                        style={homeStyles.linkImage} />}
                    </View>
                    <View style={{ flex: 1, flexWrap: 'wrap' }}>
                      <Text style={homeStyles.linkContainerTitle}>{get(this.state.postPreview, 'title', 'NA')}</Text>
                      <Text style={homeStyles.linkContainerDescription}>{get(this.state.postPreview, 'description', 'NA')}</Text>
                    </View>
                  </TouchableOpacity>
                }
                {post.files &&
                  post.files.length > 0 && (
                    <HomeImageIndicators
                      images={post.files}
                      imageModal={this.showImageModal}
                    />
                  )}
                <CardItem style={{ height: 56, justifyContent: 'space-between' }}>
                  <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity
                      onPress={() => {
                        this.props.universityPostLike(post.postid);
                      }}
                      disabled={!this.props.followingUniversity}
                    >
                      {post.likes && post.likes.length > 0 &&
                        find(post.likes, likedObj => likedObj.user.id === this.props.userId) ? (
                          <Icon name='md-heart' style={{ color: 'rgb(255, 97, 116)', fontSize: 20, width: 20 }} />
                        ) : (
                          <Icon name='md-heart-outline' style={{ color: 'rgb(207, 208, 220)', fontSize: 20, width: 20 }} />
                        )}
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate('PostLikes', {
                          likes: post.likes
                        })
                      }
                    >
                      {post.likes && post.likes.length > 0 &&
                        find(post.likes, likedObj => likedObj.user.id === this.props.userId) ? (
                          <Text style={homeStyles.postBarLikesText}>
                            {post.likes && post.likes.length > 0 && post.likes.length} Likes
                          </Text>
                        ) : (
                          <Text style={homeStyles.postBarLikesTextDefault}>
                            {post.likes && post.likes.length > 0 && post.likes.length} Likes
                          </Text>
                        )
                      }
                    </TouchableOpacity>
                  </View>
                  <TouchableOpacity style={{ flexDirection: 'row' }}>
                    <Icon name='md-text' style={{ color: 'rgb(207, 208, 220)', fontSize: 20, width: 20 }} />
                    <Text style={[homeStyles.postBarComText, { left: 6.8 }]}>
                      {post.comment && post.comment.length > 0 && post.comment.length} Comments
                    </Text>
                  </TouchableOpacity>
                  <View style={{ flexDirection: 'row' }}>
                    <Icon name='md-share' style={{ color: 'rgb(207, 208, 220)', fontSize: 20, width: 20 }} />
                    <Text style={[homeStyles.postBarComText, { left: 6.8 }]}>
                      Share
                    </Text>
                  </View>
                  <TouchableOpacity
                    style={{ flexDirection: 'row' }}
                    onPress={() =>
                      this.showActionSheet(post.user.id === this.props.userId ? true : false)
                    }
                  >
                    <Icon name='ios-more' style={{ color: 'rgb(207, 208, 220)', width: 20 }} />
                  </TouchableOpacity>
                </CardItem>
              </Card>
            </View>
            <View style={homeStyles.postCommentContainer}>
              {post.comment.map &&
                post.comment.map((item, index) => {
                  let time = transformForPosts(item.commentTimeStamp);
                  return (
                    <TouchableOpacity
                      style={{ flexDirection: 'row', marginVertical: 5 }}
                      key={index}
                      disabled={item.user.id !== this.props.userId}
                      onLongPress={() => this.handleOnLongPress(item.id)}
                    >
                      <View
                        style={{
                          flex: 0.7,
                          justifyContent: 'flex-start',
                          alignItems: 'center'
                        }}
                      >
                        <Image
                          source={
                            item.user.profPicPath
                              ? { uri: item.user.profPicPath }
                              : require('../../../assets/images/user_dummy_image.png')
                          }
                          style={homeStyles.personThumbnail}
                        />
                      </View>
                      <View style={{ flex: 4, justifyContent: 'flex-start' }}>
                        <Text
                          style={[
                            homeStyles.newPostItemLow,
                            { marginVertical: 2 }
                          ]}
                        >
                          {item.user.name}
                        </Text>
                        <Text style={homeStyles.commentText}>
                          {item.comments}
                        </Text>
                        <Text style={homeStyles.commentTime}>
                          {time}
                        </Text>
                      </View>
                    </TouchableOpacity>
                  );
                })}
            </View>
          </KeyboardAwareScrollView>
          {this.props.followingUniversity && <Animated.View
            style={{
              height: 54,
              backgroundColor: 'rgb(250,251,252)',
              paddingHorizontal: 15,
              // paddingVertical: 10,
              bottom: this.state.keyboardHeight
            }}
          >
            {this.state.editComment ? <View style={{ flexDirection: 'row' }}>
              <View
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  flex: 1,
                  marginRight: 15,
                  borderRadius: 5,
                  flexDirection: 'row',
                  backgroundColor: 'white'
                }}
              >
                <View style={{ flex: 1 }}>
                  {this.renderTextInput()}
                </View>
              </View>
              <TouchableOpacity
                style={{ alignItems: 'center', justifyContent: 'center' }}
                onPress={() => this.onEditComment()}>
                <Image source={require('../../../assets/app_icons/Icons/arrow-right-default.png')} style={{ height: 20, width: 20 }} />
              </TouchableOpacity>
            </View>
              : (
                <View style={{ flexDirection: 'row' }}>
                  <View
                    style={{
                      alignItems: 'center',
                      justifyContent: 'center',
                      flex: 1,
                      marginRight: 15,
                      borderRadius: 5
                    }}
                  >
                    {this.renderTextInput()}
                  </View>
                  <TouchableOpacity
                    style={{ alignItems: 'center', justifyContent: 'center' }}
                    onPress={() =>
                      this.onSubmitComment(this.props.postWithComment.postid)
                    }
                  >
                    <Image
                      source={require('../../../assets/app_icons/Icons/arrow-right-default.png')}
                      style={{ height: 20, width: 20 }}
                    />
                  </TouchableOpacity>
                </View>
              )}
          </Animated.View>
          }
        </View>
      );
    else return <View />
  }
}

const mapStateToProps = (state, navState) => {
  let navParams = navState.navigation.state.params.postItem;
  let universityFeeds = state.universityFeeds;
  return {
    uniPostState: state,
    universityFeeds: state.universityFeeds,
    postWithComment: find(
      universityFeeds.universityFeeds,
      post => post.postid === navParams.postid
    ),
    userId: state.userInfoAPI.userId,
    followingUniversity: state.universityDetails.followingUniversity
  };
};

const mapDispatchToProps = dispatch => ({
  dialog: bindActionCreators(DialogModal, dispatch),
  universityPostLike: bindActionCreators(universityPostLike, dispatch),
  updateComment: bindActionCreators(updateComment, dispatch),
  postComment: bindActionCreators(postComment, dispatch),
  reportPost: bindActionCreators(reportPost, dispatch),
  deleteComment: bindActionCreators(deleteComment, dispatch),
  feedApi: bindActionCreators(FeedApi, dispatch),
  peopleApi: bindActionCreators(PeopleApi, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(
  UniversityPostComments
);
