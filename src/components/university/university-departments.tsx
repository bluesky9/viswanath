import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  Image,
  ActivityIndicator
} from 'react-native';
import MSAButton from '../layouts/buttons/button';
import { NavigationScreenProps } from 'react-navigation';
import ActionSheet from 'react-native-actionsheet';
import PeopleHeader from '../people/people-header';

import { UniversitiesApi, DialogModal } from '../../store/actions'

import { peopleScreenStyles } from '../people/people-screen-styles';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { get, filter, isEmpty } from 'lodash';
import { style } from '../../styles/variables';

// let actionsheet = undefined;
const options = ['Cancel', 'All Departments', 'Academic', 'Non-Academic', 'Associations/Clubs', 'My Departments'];

const UNFOLLOW_POST = 'Do you want to leave this department'
const UNFOLLOW_MSG = 'You will no longer see the posts or activity in your feed.'

interface ILoginScreenProps extends NavigationScreenProps<{}> {
  departments;
  departmentsCount;
  universityDetails;
  universityActions;
  followingUniversity;
  usertype;
  dialogModal;
}

interface State {
  pageSize;
  filterType;
}

class UniversityDepartments extends Component<ILoginScreenProps, State> {
  actionSheet;
  listRef;
  constructor(props) {
    super(props);
    this.state = {
      pageSize: 20,
      filterType: undefined
    }
  }

  componentWillReceiveProps(nextProps) {
    if ((nextProps.departments.length === this.props.departments.length + 1) && this.listRef) {
      this.listRef.scrollToEnd()
    }
  }

  handleBackPress = () => {
    this.props.navigation.goBack();
  }

  onFilterPressed = () => {
    if (this.actionSheet) {
      this.actionSheet.show();
    }
  }

  onEndReached = () => {
    let pageSize = this.state.pageSize
    if (pageSize < this.props.departmentsCount && !this.state.filterType) {
      pageSize += 20;
      this.props.universityActions.getUnivesityDepartments(this.props.universityDetails.univ_id, pageSize, 0);
      this.setState({ pageSize });
    }
  }

  deptLogoPressed = (deptid) => {
    this.props.navigation.navigate('Department', { id: deptid })
  }

  onMemberPress = (obj) => {
    let deptId = get(obj, 'deptid', undefined)
    if (deptId) {
      this.props.dialogModal.dialogShows(
        UNFOLLOW_POST, UNFOLLOW_MSG, 'action', true, () => this.props.universityActions.unfollowDepartment(deptId)
      )
    }
  }

  onJoinPress = (deptid, deptAbbr, major) => {
    if (!isEmpty(major)) {
      if (!(this.props.usertype === ('CURRENT_STUDENT' || 'PROSPECTIVE_STUDENT')))
        this.props.dialogModal.dialogShows(deptAbbr, major, 'followDepartment', true, (majorId, degree) => this.join(deptid, majorId, degree))
      else {
        this.props.universityActions.followDepartment(deptid, major[0].majorid)
      }
    } else {
      this.props.universityActions.followDepartment(deptid)
    }
  }

  join = (deptid, majorId, degree) => {
    console.log(degree, 'Degree')
    this.props.universityActions.followDepartment(deptid, majorId);
  }

  handleActionSheetPress = (index) => {
    console.log(index, 'Yo')
    if (index === 1) {
      this.setState({ filterType: undefined })
    } else if (index === 4) {
      this.setState({ filterType: 'association' })
    } else if (index) {
      this.props.universityActions.getUnivesityDepartments(this.props.universityDetails.univ_id, this.props.departmentsCount, 0);
      this.setState({ filterType: options[index].toLowerCase() })
    }
  }

  renderDepartments = (obj) => {
    let major = get(obj, 'major', [])
    let followingDepartment = !isEmpty(obj.loggedInUserDepartmentFollowerList)
    return (
      <View style={peopleScreenStyles.peopleCardContainer}>
        <TouchableOpacity onPress={() => this.deptLogoPressed(get(obj, 'deptid', undefined))}>
          <Image
            source={obj && obj.deptlogo ?
              { uri: obj.deptlogo.replace('http://', 'https://') }
              : require('../../../assets/images/user_dummy_image.png')
            }
            style={peopleScreenStyles.profileIconContainer}
          />
        </TouchableOpacity>
        <View style={peopleScreenStyles.peopleDetailsConatiner}>
          <View style={[peopleScreenStyles.nameContainer, { borderBottomWidth: 0 }]}>
            <View style={{ flex: 1 }}>
              <Text onPress={() => this.deptLogoPressed(get(obj, 'deptid', undefined))}
                style={peopleScreenStyles.peopleName}>{get(obj, 'deptname', '')}
              </Text>
              <Text style={peopleScreenStyles.peopleLocation}>
                {get(obj, 'deptType', '')}
              </Text>
              {major.length > 0 && obj.deptType === 'academic' &&
                <View style={{ flexDirection: 'row', width: style.device.width - 45 - 41, flexWrap: 'wrap' }}>
                  {/* <Text style={peopleScreenStyles.normalText}> */}
                  {
                    major.map((item, index) => {
                      if (index < 3)
                        return <Text key={index}>
                          <Text style={peopleScreenStyles.normalText}>{item.majorname}</Text>
                          <Text style={peopleScreenStyles.normalText}>{major.length - 1 === index ? '' : ', '}</Text>
                        </Text>
                      else
                        return <View key={index} />
                    })
                  }
                  {major.length > 3 &&
                    <Text style={[peopleScreenStyles.normalText, { color: style.color.primary }]}>{major.length - 3 + ' More'}</Text>
                  }
                  {/* </Text> */}
                </View>
              }
            </View>
            {this.props.followingUniversity && <View style={peopleScreenStyles.followContainer}>
              {followingDepartment ? (
                <MSAButton
                  onPress={() => this.onMemberPress(obj)}
                  type={'inverted'}
                  bordered={true}
                  label={'Member'}
                  containerWidth={'51%'}
                  customButtonStyle={peopleScreenStyles.followButton}
                  disabled={!this.props.followingUniversity}
                  textSize={12}
                />
              ) : (
                  <MSAButton
                    onPress={() => this.onJoinPress(get(obj, 'deptid', undefined), get(obj, 'deptAbbr', ''), major)}
                    type={'primary'}
                    label={'Join'}
                    containerWidth={'51%'}
                    customButtonStyle={peopleScreenStyles.followButton}
                    disabled={!this.props.followingUniversity}
                    textSize={12}
                  />
                )}
            </View>}
          </View>
          {/* { obj.userTypes &&
            obj.userTypes.map((item, index) =>
            <Text style={peopleScreenStyles.normalText} key={index}>
              <Text>{constants.USERTYPES.CONTEXT[item.userType] + ' '} </Text>
              <Text style={{ color: style.color.primary }} onPress={() => console.log('University Pressed')}>
                {item.universityName}
              </Text>
            </Text>
            )
          } */}
          {/* <Text style={peopleScreenStyles.normalText}>
            <Text>Currently studying at </Text>
            <Text style={{ color: style.color.primary }}>
              {'Something'}
            </Text>
          </Text> */}
        </View>
      </View>
    )
  }

  render() {
    let departmentList = get(this.props.departments, 'content', [])
    if (this.state.filterType === 'my departments') {
      departmentList = filter(this.props.departments.content, dept => !isEmpty(dept.loggedInUserDepartmentFollowerList))
    } else if (this.state.filterType) {
      departmentList = filter(this.props.departments.content, dept => dept.deptType === this.state.filterType)
    }

    let disableAddButton = this.props.usertype === 'PROSPECTIVE_STUDENT' || this.props.usertype === 'ALUMNI'

    return (
      <View style={{ paddingBottom: 20 }}>
        <PeopleHeader
          navigation={this.props.navigation}
          onAddPress={() => this.props.navigation.navigate('DepartmentCreate')}
          title={'Departments @ ' + get(this.props.universityDetails, 'abbr', '')}
          onFilterPress={this.onFilterPressed}
          filtersApplied={this.state.filterType}
          onBackPress={this.handleBackPress}
          disableExtraButton={!this.props.usertype || disableAddButton}
        />
        <ActionSheet
          ref={o => (this.actionSheet = o)}
          options={options}
          cancelButtonIndex={0}
          onPress={i => this.handleActionSheetPress(i)}
        />
        <FlatList
          ref={ref => {
            this.listRef = ref;
          }}
          extraData={this.props}
          data={departmentList}
          renderItem={obj => this.renderDepartments(obj.item)}
          keyExtractor={item => item.deptid.toString()}
          contentContainerStyle={{ paddingBottom: 60 }}
          showsVerticalScrollIndicator={false}
          onEndReached={this.onEndReached}
          onEndReachedThreshold={0}
          ListFooterComponent={() => {
            if (this.props.departmentsCount >= this.state.pageSize && !this.state.filterType)
              return <View style={{ paddingTop: 20 }}>
                <ActivityIndicator size='large' color={style.color.primary} />
              </View>
            else return <View />
          }}
        />
      </View>
    );
  }
}

const mapStateToProps = ({ universityDetails }) => {
  return {
    departments: get(universityDetails, 'departments', {}),
    universityDetails: get(universityDetails, 'details', {}),
    departmentsCount: get(universityDetails, 'departments.totalElements', 0),
    followingUniversity: get(universityDetails, 'followingUniversity'),
    usertype: get(universityDetails, 'usertype')
  };
};

const mapDispatchToProps = dispatch => ({
  universityActions: bindActionCreators(UniversitiesApi, dispatch),
  dialogModal: bindActionCreators(DialogModal, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(UniversityDepartments);
