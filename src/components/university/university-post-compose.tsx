import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  TextInput,
  Dimensions,
  Platform,
  ScrollView,
  Image,
  Switch
} from 'react-native';
import universityStyles from './university-styles';
import ImagePicker from 'react-native-image-crop-picker';
import { Icon } from 'native-base';
const { width } = Dimensions.get('window');
import MSAHeader from '../layouts/header/header';
import { NavigationScreenProps } from 'react-navigation';
import { style } from '../../styles/variables';
import { getUniversityFeeds } from '../../store/actions/Universities.js';
import { publishPost, uploadFile } from '../../store/actions/Feed.js';
import { bindActionCreators } from 'redux';
import { get, findIndex, find, isEmpty, map } from 'lodash';
import { constants } from '../../constants';
import {
  setPostAudience,
  setUniversityPostCountryFilters,
  setPostMajor
} from '../../store/actions/Universities';

import { connect } from 'react-redux';

const IMAGE_UPLOAD_LIMIT = 5;
let MESSAGE =
  'If you turn this option ON, Visitors of this page who are not following the unversity can view this post';

interface ILoginScreenProps extends NavigationScreenProps<{}> {
  universityDetails: any;
  universityPostCountryFilters: any;
  actions: any;
  setPostAudience;
  setUniversityPostCountryFilters;
  userInfo: any;
}
interface State {
  imagesArray: any;
  title: string;
  message: string;
  publicPost: boolean;
  departmentId;
}

class UniversityPostCompose extends Component<ILoginScreenProps, State> {
  constructor(props) {
    super(props);
    this.state = {
      imagesArray: [],
      title: '',
      message: '',
      publicPost: false,
      departmentId: get(this.props.navigation.state.params, 'departmentPostCompose.deptid', undefined)
    };
  }

  componentDidMount() {
    let departmentPostRequest = get(this.props.navigation.state.params, 'departmentPostCompose', undefined)
    this.props.actions.setPostAudience([]);
    this.props.actions.setUniversityPostCountryFilters([]);
    this.props.actions.setPostMajor([]);
    // console.log(this.props.universityDetails.usertype, '121')
    if (this.props.universityDetails.usertype === 'CURRENT_STUDENT' ||
      this.props.universityDetails.usertype === 'ALUMNI'
    ) {
      if (departmentPostRequest && departmentPostRequest.deptType !== 'academic') {
        // console.log('non Academic post')
      } else {
        this.props.actions.setPostAudience([this.props.universityDetails.usertype]);
      }
    }
    if (this.props.universityDetails.usertype === 'PROSPECTIVE_STUDENT') {
      if (departmentPostRequest && departmentPostRequest.deptType !== 'academic') {
        // console.log('non Academic post')
      } else {
        this.props.actions.setPostAudience([this.props.universityDetails.usertype]);
        this.props.actions.setUniversityPostCountryFilters([this.props.userInfo.country]);
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.navigation.state.params) {
      let cameraImage = get(nextProps.navigation.state.params, 'image', undefined);
      if (cameraImage) {
        let imageIndex = findIndex(this.state.imagesArray, image => image.path === nextProps.navigation.state.params.image.path)
        if (imageIndex === -1) {
          let imagesArray = this.state.imagesArray;
          imagesArray.push(nextProps.navigation.state.params.image)
          this.setState({ imagesArray })
        }
      }
    }
  }

  openPicker() {
    if (this.state.imagesArray.length < IMAGE_UPLOAD_LIMIT)
      ImagePicker.openPicker({
        cropping: false,
        multiple: true,
        compressImageQuality: Platform.OS === 'ios' ? 0 : 0.5,
        maxFiles: IMAGE_UPLOAD_LIMIT - this.state.imagesArray.length
      })
        .then(images => {
          let arr = this.state.imagesArray;
          map(images, (image) => {
            let imageObj = {
              uri: get(image, 'path', ''),
              name: get(image, Platform.OS === 'ios' ? 'creationDate' : 'modificationDate', ''),
              type: get(image, 'mime', '')
            }
            arr.push(imageObj);
          });
          this.setState({
            imagesArray: arr
          });
        })
        .catch(Error);
  }

  openCamera() {
    if (this.state.imagesArray.length < IMAGE_UPLOAD_LIMIT)
      ImagePicker.openCamera({
        cropping: false,
        compressImageQuality: Platform.OS === 'ios' ? 0 : 0.5
      })
        .then(image => {
          let arr = this.state.imagesArray;
          let imageObj = {
            uri: get(image, 'path', ''),
            name: new Date().getTime().toString(),
            type: get(image, 'mime', '')
          }
          arr.push(imageObj);
          this.setState({
            imagesArray: arr
          });
        })
        .catch((e) => {
          console.log(e, 'Error')
        });
  }

  publishPost() {

    let majorIdsKey = 'majorIds';
    let departmentIdKey = 'departmentId'
    let departmentPostRequest = get(this.props.navigation.state.params, 'departmentPostCompose', undefined)
    let major = get(this.props.navigation.state.params, 'departmentPostCompose.major', [])
    let imagesArray = this.state.imagesArray

    // For Android specifically
    if (imagesArray.length > IMAGE_UPLOAD_LIMIT) {
      let diff = IMAGE_UPLOAD_LIMIT - imagesArray.length
      imagesArray.splice(5, diff)
    }

    let publishPostObj = {
      userTypes: this.props.universityDetails.postAudience,
      countries: this.props.universityPostCountryFilters.filters,
      title: this.state.title,
      message: this.state.message,
      postExpiry: 'MONTH',
      restricted: !this.state.publicPost,
      universityId: this.props.universityDetails.details.univ_id
    }

    if (departmentPostRequest) {
      let majorIds = [];
      if (this.props.universityDetails.postMajor.length === 0) {
        major.map((item) => {
          majorIds.push(item.majorid)
        })
      } else {
        majorIds = this.props.universityDetails.postMajor;
      }
      publishPostObj[majorIdsKey] = majorIds;
      publishPostObj[departmentIdKey] = get(this.props.navigation.state.params, 'departmentPostCompose.deptid', undefined)
    }

    if (imagesArray.length > 0) {
      this.props.actions.uploadFile(imagesArray, publishPostObj)
    } else {
      this.props.actions.publishPost(publishPostObj);
    }

  }

  render() {

    let countries = this.props.universityPostCountryFilters.filters && this.props.universityPostCountryFilters.filters.length === 0 ?
      'All countries' : this.props.universityPostCountryFilters.filters && this.props.universityPostCountryFilters.filters.map(
        (item) => {
          return (item + ', ');
        }
      )

    let majorData = get(this.props.navigation.state.params, 'departmentPostCompose.major', [])

    let selectedMajor = this.props.universityDetails.postMajor.map((item, index) => {
      let majorExists = find(majorData, majorObj => majorObj.majorid === item)
      if (majorExists) {
        let majorName = majorExists.majorname + (this.props.universityDetails.postMajor.length - 1 === index ? '' : ', ');
        return majorName;
      } else {
        return undefined
      }
    })

    let majors = (this.props.universityDetails.postMajor.length === 0
      ||
      this.props.universityDetails.postMajor.length === majorData.length) ?
      'All majors' : selectedMajor;

    let checkPost = this.state.title !== '' && (this.state.message !== '' || this.state.imagesArray.length > 0)

    return (
      <View style={{ flex: 1 }}>
        <MSAHeader
          title={'New post'}
          rightText={'PUBLISH'}
          onPress={() => this.props.navigation.goBack()}
          leftText={'CANCEL'}
          onRightTextPress={() => this.publishPost()}
          disableRightText={!checkPost}
        />
        <ScrollView>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('UniversityAudience', { majorData, deptRequest: this.state.departmentId ? true : false })}
            style={[universityStyles.newPostItemWrap]}
            disabled={this.props.universityDetails.usertype && this.props.universityDetails.usertype === 'PROSPECTIVE_STUDENT'}
          >
            <View>
              <Text
                style={universityStyles.newPostItemHead}
              >
                AUDIENCE
              </Text>
              <Text
                style={{ width: width - 50, marginTop: 5 }}
              >
                {this.props.universityDetails.postAudience ? (
                  this.props.universityDetails.postAudience
                    .length === 0 ? (
                      <Text style={universityStyles.newPostItemLow}>
                        Everybody
                    </Text>
                    ) : (
                      undefined
                    )
                ) : (
                    <Text style={universityStyles.newPostItemLow}>Everybody</Text>
                  )}
                {this.props.universityDetails.postAudience &&
                  this.props.universityDetails.postAudience.map(
                    (item, index) => {
                      return (
                        <Text
                          key={index}
                          style={universityStyles.newPostItemLow}
                        >
                          {constants.USERTYPES.PLURAL[item]}
                          {', '}
                        </Text>
                      );
                    }
                  )}
                <Text style={universityStyles.newPostItemLow}>{' from '}</Text>
                <Text style={universityStyles.newPostItemLow}>
                  {this.state.departmentId ? majors : countries}
                </Text>
              </Text>
            </View>
            <View
              style={{
                position: 'absolute',
                alignSelf: 'center',
                left: width - 25
              }}
            >
              <Icon
                style={{ fontSize: 20, color: 'rgb(197,207,219)' }}
                name={'arrow-forward'}
              />
            </View>
          </TouchableOpacity>
          <View
            style={[
              universityStyles.newPostItemWrap,
              { justifyContent: 'flex-start' }
            ]}
          >
            <Text
              style={universityStyles.newPostItemHead}
            >
              TITLE
            </Text>
            <TextInput
              onChangeText={txt =>
                this.setState({
                  title: txt
                })
              }
              style={[universityStyles.newPostInput]}
              placeholderTextColor={style.color.dustyGrey}
              placeholder={'Title is optional'}
              underlineColorAndroid={'rgba(0,0,0,0)'}
              maxLength={40}
            />
            <Text
              style={universityStyles.newPostItemHead}
            >
              POST
            </Text>
            <TextInput
              onChangeText={txt =>
                this.setState({
                  message: txt
                })
              }
              multiline={true}
              numberOfLines={5}
              style={[universityStyles.newPostInput, { marginBottom: 0, paddingTop: 12, height: undefined }]}
              placeholderTextColor={style.color.dustyGrey}
              placeholder={'Type your message..'}
              underlineColorAndroid={'rgba(0,0,0,0)'}
            />
          </View>
          <View style={{ alignSelf: 'stretch' }}>
            <View
              // onPress={() => this.openPicker()}
              style={[universityStyles.newPostItemWrap, { height: 54, flexDirection: 'row', alignItems: 'center' }]}
            >
              <TouchableOpacity
                onPress={() => this.openPicker()}
                style={{ flex: 1 }}
              >
                <Text style={universityStyles.newPostItemHead}>ADD IMAGES</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.openCamera()}
              >
                <Icon
                  style={{ fontSize: 20, color: 'rgb(197,207,219)' }}
                  name={'camera'}
                  active
                />
              </TouchableOpacity>
            </View>
            {!isEmpty(this.state.imagesArray) &&
              <View
                style={[
                  universityStyles.newPostItemWrap,
                  { height: this.state.imagesArray.length === 0 ? 0 : 120 }
                ]}
              >
                <ScrollView
                  horizontal={true}
                  contentContainerStyle={{ alignItems: 'center' }}
                >
                  {this.state.imagesArray.map((item, index) => {
                    return (
                      <View
                        key={index}
                        style={{
                          height: 90,
                          width: 90,
                          marginHorizontal: 15,
                          borderRadius: 3
                        }}
                      >
                        <Image
                          source={{
                            // uri: Platform.OS === 'ios' ? item.uri : item.path // Check For android
                            uri: Platform.OS === 'ios' ?
                              'file://' + item.uri : item.uri
                          }}
                          style={{ height: 90, width: 90 }}
                        />
                        <TouchableOpacity
                          onPress={() => {
                            let arr = this.state.imagesArray;
                            arr.splice(index, 1);
                            this.setState({ imagesArray: arr });
                          }}
                          style={{
                            position: 'absolute',
                            top: 0,
                            right: 0,
                            padding: 5,
                            margin: 5,
                            borderRadius: 1,
                            backgroundColor: 'rgba(0,0,0,0.5)'
                          }}
                        >
                          <Image
                            style={{ tintColor: 'white' }}
                            source={require('../../../assets/app_icons/Icons/Close-dark.png')}
                          />
                        </TouchableOpacity>
                      </View>
                    );
                  })}
                </ScrollView>
              </View>
            }
            <View style={{ backgroundColor: 'white', padding: 15 }}>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Text
                  style={{
                    flex: 1,
                    fontFamily: style.font.family.rubikRegular,
                    color: style.color.coal
                  }}
                >
                  Public post
                </Text>
                <Switch
                  value={this.state.publicPost}
                  onValueChange={bool => this.setState({ publicPost: bool })}
                  thumbTintColor={style.color.primary}
                  onTintColor={style.color.cream}
                  style={{
                    transform: [{ scaleX: Platform.OS === 'ios' ? .7 : 1 },
                    { scaleY: Platform.OS === 'ios' ? .7 : 1 }]
                  }}
                />
              </View>
              <Text
                style={{
                  fontSize: 12,
                  color: style.color.dustyGrey,
                  paddingTop: 10
                }}
              >
                {MESSAGE}
              </Text>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = ({ universityDetails, universityPostCountryFilters, userInfoAPI }) => {
  return {
    universityDetails: universityDetails,
    universityPostCountryFilters: universityPostCountryFilters,
    userInfo: userInfoAPI.userInfo
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      setPostAudience: setPostAudience,
      publishPost: publishPost,
      getUniversityFeeds: getUniversityFeeds,
      uploadFile: uploadFile,
      setUniversityPostCountryFilters: setUniversityPostCountryFilters,
      setPostMajor: setPostMajor
    },
    dispatch
  )
});

export default connect(mapStateToProps, mapDispatchToProps)(
  UniversityPostCompose
);
