import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  ActivityIndicator
} from 'react-native';
import { CachedImage } from 'react-native-img-cache';
import { style } from '../../styles/variables';
import { NavigationScreenProps } from 'react-navigation';
import { KeyboardAwareFlatList } from 'react-native-keyboard-aware-scroll-view';
import UniversityModal from '../layouts/modal/modal-new-university';
import PeopleHeader from '../people/people-header';
import PeopleFilterSearch from '../people/people-filter-search';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import homeStyles from '../home/home-styles';
import AppButton from '../layouts/buttons/button';
import { DrawerActions } from '../../store/actions'

import {
  getPaginatedUniversities,
  getUniversitiesFollowedByUser,
  getUniversityBySlug,
  getUniversitiesCount,
  getUniversitiesByName,
  getUniversityDetails,
  getUniversityFeeds,
  submitMissingRequest,
  getUnivesityDepartments
} from '../../store/actions/Universities.js';

import { get } from 'lodash';

import { peopleScreenStyles } from '../people/people-screen-styles';
import universityStyles from './university-styles';

interface ILoginScreenNavParams {}
interface ILoginScreenProps
  extends NavigationScreenProps<ILoginScreenNavParams> {
  uniApiState: any;
  actions: any;
  profilePic: string;
  userId: any;
}

type State = {
  showSearchBar?: boolean;
  filtersApplied?: boolean;
  dataPackCount?: number;
  firstMount?: boolean;
  searchText?: string;
  showModal?: boolean;
  refreshing?: boolean;
};
class University extends Component<ILoginScreenProps, State> {
  constructor(props) {
    super(props);
    this.state = {
      showSearchBar: false,
      filtersApplied: false,
      dataPackCount: 20,
      firstMount: true,
      searchText: '',
      showModal: false,
      refreshing: false
    };
  }
  _handleSearchPress() {
    this.setState({ showSearchBar: true });
  }

  _renderUniversityIcons() {
    return (
      <ScrollView
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ height: 72}}
      >
        {this.props.uniApiState.universitiesFollowed.universities.map(
          (item, index) => {
            return (
              item.universityConnectionStatus === 'FOLLOW' ?
              <TouchableOpacity
                onPress={() => {
                  this.props.actions.getUniversityDetails(item.university.univ_id);
                  this.props.actions.getUniversityFeeds(item.university.univ_id, 20, 0);
                  this.props.actions.getUnivesityDepartments(item.university.univ_id, 20, 0);
                  this.props.navigation.navigate('UniversityProfile');
                }}
                style={universityStyles.universityIconContainer}
                key={index}
              >
                <Image
                  source={{ uri: item.university.logoURL ?
                    item.university.logoURL : 'https://252radio.com/wp-content/uploads/2016/11/default-user-image-300x300.png' }}
                  style={{ height: 51, width: 51 }}
                />
              </TouchableOpacity> : undefined
            );
          }
        )}
      </ScrollView>
    );
  }
  componentDidMount() {
    this.props.actions.getUniversitiesCount();
    this.props.actions.getPaginatedUniversities(20, 0);
    this.props.actions.getUniversitiesFollowedByUser(this.props.userId);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.uniApiState.paginatedUniversities.universities) {
      this.setState({ refreshing: false })
    }
  }

  submitMissingRequest(data) {
    let uniData = {
      notifyMe: data.notifyMe,
      universityCountry: data.country,
      universityName: data.name,
      universityUrl: data.website
    };
    this.props.actions.submitMissingRequest(uniData, () => this.setState({ showModal: false }));
  }

  fetchMoreData() {
    if (
      this.props.uniApiState.universityCountryFilters.filters.length === 0 &&
      this.state.searchText === ''
    ) {
      this.setState(
        {
          dataPackCount: this.state.dataPackCount + 20
        },
        () => {
          this.props.actions.getPaginatedUniversities(
            this.state.dataPackCount,
            0
          );
        }
      );
    }
  }

  _handleOnProfileClick = (id) => {
    this.props.actions.getUniversityDetails(id);
    this.props.actions.getUniversityFeeds(id, 20, 0);
    this.props.actions.getUnivesityDepartments(id, 20, 0);
    this.props.navigation.navigate('UniversityProfile');
  }

  renderFlatItem(index, item) {
    return (
      <TouchableOpacity
        onPress={() => {
          // console.log(item.univ_id);
          this._handleOnProfileClick(item.univ_id)
        }}
      >
        <View key={index} style={universityStyles.itemLeftContainer}>
          <CachedImage
            source={{ uri: item.logoURL ? item.logoURL : 'https://252radio.com/wp-content/uploads/2016/11/default-user-image-300x300.png' }}
            style={{ height: 51, width: 51 }}
          />
          <Text style={universityStyles.universityName}>{item.name}</Text>
          <Text style={universityStyles.countryName}>{item.country}</Text>
        </View>
      </TouchableOpacity>
    );
  }
  showNoUniversityMessage() {
    return (
      <View style={homeStyles.fullWidthContainer}>
        <View
          style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
        >
          <Image source={require('../../../assets/emptyFeedError.png')} />
        </View>
        <View
          style={{
            flex: 1,
            justifyContent: 'flex-start',
            alignItems: 'center'
          }}
        >
          <Text style={homeStyles.feedErrorHeadText}>
            No Universities found for your country!
          </Text>
          <Text style={homeStyles.feedErrorInstructionText}>
            You can send us a request
          </Text>
          <AppButton
            type={'primary'}
            label={'Request'}
            containerWidth={'34.5%'}
            containerHeight={'45%'}
            textFamily={'Rubik-Regular'}
            textSize={13}
            onPress={() => {
              this.setState({ showModal: true });
            }}
          />
        </View>
        <View
          style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
        />
        <UniversityModal
          onClose={() => this.setState({ showModal: false })}
          isShown={this.state.showModal}
          onSubmit={data => this.submitMissingRequest(data)}
        />
      </View>
    );
  }

  renderMainList() {
    if (
      (this.props.uniApiState.universityCountryFilters.filters.length !== 0 &&
      this.props.uniApiState.universitesByCountries.universities.length === 0 &&
      this.props.uniApiState.universitesByCountries.isWaitingResponse === false)
      ||
      (this.state.searchText !== '' && this.props.uniApiState.universitiesByName.universities.length === 0)
    ) {
      return this.showNoUniversityMessage();
    }
    return (
      <KeyboardAwareFlatList
        contentContainerStyle={universityStyles.listContainer}
        numColumns={2}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        onEndReached={() => this.fetchMoreData()}
        onEndReachedThreshold={1}
        onRefresh={() => this.props.actions.getPaginatedUniversities(20, 0)}
        refreshing={this.state.refreshing}
        data={
          this.state.searchText === ''
            ? this.props.uniApiState.universityCountryFilters.filters.length !==
              0
              ? this.props.uniApiState.universitesByCountries.universities
              : this.props.uniApiState.paginatedUniversities.universities
            : this.props.uniApiState.universitiesByName.universities
        }
        renderItem={({ index, item }) => this.renderFlatItem(index, item)}
        keyExtractor={item => item.univ_id}
        ListFooterComponent={() => {
          if (
            this.props.uniApiState.universityCountryFilters.filters.length ===
              0 &&
            this.props.uniApiState.paginatedUniversities.isWaitingResponse
          )
            return (
              <View style={{ paddingTop: 20 }}>
                <ActivityIndicator size={'large'} color={style.color.primary} />
              </View>
            );
          else return <View />;
        }}
      />
    );
  }

  textChange(txt) {
    if (txt !== '') {
      this.props.actions.getUniversitiesByName(20, 0, txt);
    }
    this.setState({ searchText: txt });
  }

  render() {
    let followCount = 0;
    if (this.props.uniApiState.universitiesFollowed.universities) {
      this.props.uniApiState.universitiesFollowed.universities.forEach(university => {
        if (university.universityConnectionStatus === 'FOLLOW') {
          followCount++;
        }
      });
    }
    return (
      <View style={{ backgroundColor: 'rgb(250,251,252)', flex: 1 }}>
        {this.state.showSearchBar ? (
          <View style={peopleScreenStyles.searchBarContainer}>
            <View style={{ flex: 1 }}>
              <PeopleFilterSearch onTextChange={txt => this.textChange(txt)} />
            </View>
            <TouchableOpacity
              onPress={() =>
                this.setState({
                  showSearchBar: false
                })
              }
              style={{
                alignItems: 'flex-end',
                justifyContent: 'center',
                paddingLeft: 20
              }}
            >
              <Text style={peopleScreenStyles.cancelText}>CANCEL</Text>
            </TouchableOpacity>
          </View>
        ) : (
          <PeopleHeader
            navigation={this.props.navigation}
            onSearchPress={this._handleSearchPress.bind(this)}
            title={'Universities'}
            onFilterPress={() =>
              this.props.navigation.navigate('UniversityFilters')
            }
            filtersApplied={
              this.props.uniApiState.universityCountryFilters.filters.length !==
              0
            }
            profileIconPress={this.props.actions.drawerOpen}
            profilePic={this.props.profilePic}
          />
        )}
        <View style={[universityStyles.followedNumberContainer, {paddingBottom: 12, paddingTop: 12}]}>
          <Text style={peopleScreenStyles.peopleNumberText}>
            {followCount}{' '}
            followed
          </Text>
        </View>
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          {this._renderUniversityIcons()}
        </View>
        <View style={[universityStyles.followedNumberContainer, { paddingBottom: 17, paddingTop: 17 }]}>
          {this.state.searchText === '' ? (
            <Text style={peopleScreenStyles.peopleNumberText}>
              {this.props.uniApiState.universityCountryFilters.filters
                .length === 0
                ? ''
                : this.props.uniApiState.universitesByCountries.universities
                    .length}
              {this.props.uniApiState.universityCountryFilters.filters
                .length === 0
                ? this.props.uniApiState.universityCount.count
                : ''}{' '}
              universities
            </Text>
          ) : (
            <Text style={peopleScreenStyles.peopleNumberText}>
              {this.props.uniApiState.universitiesByName.universities.length}{' '}
              universities
            </Text>
          )}
        </View>
        {this.renderMainList()}
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    uniApiState: state,
    profilePic: get(state.userInfoAPI.userInfo, 'profilePicture', undefined),
    userId: state.userInfoAPI.userId
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      getPaginatedUniversities: getPaginatedUniversities,
      getUniversitiesFollowedByUser: getUniversitiesFollowedByUser,
      getUniversityBySlug: getUniversityBySlug,
      getUniversitiesCount: getUniversitiesCount,
      getUniversitiesByName: getUniversitiesByName,
      getUniversityDetails: getUniversityDetails,
      getUniversityFeeds: getUniversityFeeds,
      submitMissingRequest: submitMissingRequest,
      getUnivesityDepartments: getUnivesityDepartments,
      drawerOpen: DrawerActions.drawerOpen
    },
    dispatch
  )
});

export default connect(mapStateToProps, mapDispatchToProps)(University);
