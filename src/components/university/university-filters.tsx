import React, { Component } from 'react';
import { View, TouchableOpacity, Text, Image } from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import MSAHeader from '../layouts/header/header';
import PeopleFilterSearch from '../people/people-filter-search';

import { filterUniversityStyles } from '../people/people-filter-styles';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  getUniversitiesByCountries,
  setUniversityFilters
} from '../../store/actions/Universities.js';

import { isEmpty } from 'lodash';

import { getCountries } from '../../store/actions/Countries.js';
import { Spinner } from 'native-base';
interface IPeopleFilterNavParams {}
interface IPeopleFilterProps
  extends NavigationScreenProps<IPeopleFilterNavParams> {
  countries: any;
  actions: any;
}
interface State {
  checkStates: boolean[];
  countryFilters: string[];
  rightButtonText: string;
  searchCountries: string;
  lagBugVal: number;
  allState: boolean;
}

class UniversityFilter extends Component<IPeopleFilterProps, State> {
  constructor(props) {
    super(props);
    this.state = {
      checkStates: new Array(234).fill(false),
      countryFilters: [],
      rightButtonText: 'APPLY',
      searchCountries: undefined,
      lagBugVal: 0,
      allState: true
    };
  }
  _renderItem(item, index) {
    return (
      <TouchableOpacity
        onPress={() => {
          this.setState({ rightButtonText: 'APPLY', allState: false });
          if (this.state.checkStates[index] !== undefined) {
            let arr = this.state.checkStates;
            arr[index] = !arr[index];
            this.setState({ checkStates: arr });
          }
          if (this.state.checkStates[index] === undefined) {
            let arr = this.state.checkStates;
            arr[index] = true;
            this.setState({ checkStates: arr });
          }
        }}
        key={index}
        style={filterUniversityStyles.itemContainer}
      >
        <Image
          source={
            this.state.checkStates[index]
              ? require('../../../assets/app_icons/Icons/Checkbox-active.png')
              : require('../../../assets/app_icons/Icons/Checkbox-default.png')
          }
          style={filterUniversityStyles.checkboxContainer}
          resizeMode={'stretch'}
        />
        <Text style={filterUniversityStyles.itemText}>
          {item ? item.countryName : ''}
        </Text>
      </TouchableOpacity>
    );
  }
  componentDidMount() {
    if (this.props.countries.countries.countries.length === 0) {
      this.props.actions.getCountries();
    }
    this.props.countries.universityCountryFilters.filters.map(
      (item, _index) => {
        this.state.checkStates[item.id] = true;
      }
    );
    if (this.props.countries.universityCountryFilters.filters.length !== 0) {
      this.setState({ rightButtonText: 'CLEAR', allState: false });
    }
    setTimeout(() => {
      this.setState({
        lagBugVal: 254
      });
    }, 0);
  }
  applyFilters() {
    if (this.state.rightButtonText === 'APPLY') {
      let refFilters = [];
      let filters = [];
      this.state.checkStates.map((item, index) => {
        if (item === true) {
          filters.push(
            this.props.countries.countries.countries[index].countryName
          );
          refFilters.push({
            name: this.props.countries.countries.countries[index].countryName,
            id: index
          });
        }
      });
      this.props.actions.getUniversitiesByCountries(filters);
      this.props.actions.setUniversityFilters(refFilters);
      this.props.navigation.goBack();
    }
    if (this.state.rightButtonText === 'CLEAR') {
      this.props.actions.setUniversityFilters([]);
      this.props.actions.getUniversitiesByCountries([]);
      this.state.checkStates.map((_item, index) => {
        this.state.checkStates[index] = false;
      });
      this.setState({
        rightButtonText: 'APPLY',
        allState: true
      });
    }
  }

  handleTextChange = text => {
    this.setState({ searchCountries: text });
  };

  render() {
    return (
      <View style={filterUniversityStyles.container}>
        <MSAHeader
          title={'Filters'}
          rightText={this.state.rightButtonText}
          onPress={() => this.props.navigation.goBack()}
          onRightTextPress={() => this.applyFilters()}
        />
        <View style={filterUniversityStyles.mainContent}>
          <PeopleFilterSearch
            placeHolder={'Search Country'}
            onTextChange={this.handleTextChange}
          />
        </View>
        <View style={{ flex: 1 }}>
          {this.state.lagBugVal === 0 ? (
            <Spinner />
          ) : (
            <KeyboardAwareScrollView contentContainerStyle={{ paddingHorizontal: 15 }}>
              <TouchableOpacity style={filterUniversityStyles.itemContainer}>
                {this.state.allState ? (
                  <Image
                    source={require('../../../assets/app_icons/Icons/Checkbox-active.png')}
                    style={filterUniversityStyles.checkboxContainer}
                    resizeMode={'stretch'}
                  />
                ) : (
                  <Image
                    source={require('../../../assets/app_icons/Icons/Checkbox-default.png')}
                    style={filterUniversityStyles.checkboxContainer}
                    resizeMode={'stretch'}
                  />
                )}
                <Text style={filterUniversityStyles.itemText}>{'All'}</Text>
              </TouchableOpacity>
              {this.props.countries.countries.countries
                .filter((_item, index) => index < this.state.lagBugVal)
                .map((item, index) => {
                  if (!isEmpty(this.state.searchCountries)) {
                    if (item.countryName.startsWith(this.state.searchCountries))
                      return (
                        <View key={index}>{this._renderItem(item, index)}</View>
                      );
                    else return undefined;
                  } else {
                    return (
                      <View key={index}>{this._renderItem(item, index)}</View>
                    );
                  }
                })}
            </KeyboardAwareScrollView>
          )}
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    countries: state
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      getCountries: getCountries,
      getUniversitiesByCountries: getUniversitiesByCountries,
      setUniversityFilters: setUniversityFilters
    },
    dispatch
  )
});

export default connect(mapStateToProps, mapDispatchToProps)(UniversityFilter);
