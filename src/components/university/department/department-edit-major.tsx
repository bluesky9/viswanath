import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  TextInput
} from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
import { departmentCreateNewMajorStyles } from './department-styles';
import { filterUniversityStyles } from '../../people/people-filter-styles';
import { UniversitiesApi } from '../../../store/actions'
import { style } from '../../../styles/variables';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import MSAHeader from '../../layouts/header/header';

import { get, isEmpty, findIndex, filter, map, isEqual } from 'lodash';

interface ILoginScreenNavParams { }
interface ILoginScreenProps
  extends NavigationScreenProps<ILoginScreenNavParams> {
    universityActions;
}

type State = {
  majorName;
  selectedIndexes;
  levelOfStudyOffered;
};

const degreeLevel = ['BACHELORS', 'MASTERS', 'PHD', 'ASSOCIATE_DEGREE',
  'BACHELORS_ONLINE', 'MASTERS_ONLINE', 'ASSOCIATE_ONLINE', 'NON_DEGREE', 'DIPLOMA', 'CERTIFICATE', 'PROFESSIONAL']

class DepartmentEditMajor extends Component<ILoginScreenProps, State> {

  constructor(props) {
    super(props);
    this.state = {
      majorName: get(this.props, 'navigation.state.params.majorData.majorname', ''),
      selectedIndexes: [],
      levelOfStudyOffered: []
    }
  }

  componentDidMount() {
    let selectedIndexes = []
    let type = get(this.props.navigation, 'state.params.type', undefined)
    let levelOfStudyOffered = get(this.props.navigation, 'state.params.majorData.levelOfStudyOffered', [])
    if (type === 'add')
      degreeLevel.map(item => {
        selectedIndexes.push({degreeLevel: item, selected: false})
      })
    else degreeLevel.map((item) => {
      let findDegreeIndex = findIndex(levelOfStudyOffered, obj => obj.newFollowingAllowed && item === obj.levelOfStudy)
      if (findDegreeIndex === -1) selectedIndexes.push({degreeLevel: item, selected: false})
      else selectedIndexes.push({degreeLevel: item, selected: true})
    })
    this.setState({ selectedIndexes })
  }

  handleBackPress() {
    this.props.navigation.goBack()
  }

  onDeleteMajorPressed() {
    let deleteMajor = get(this.props.navigation, 'state.params.deleteMajor', undefined)
    if (deleteMajor) deleteMajor();
    this.props.navigation.goBack()
  }

  handleDonePress = () => {
    let type = get(this.props.navigation, 'state.params.type', undefined)
    let major = get(this.props.navigation, 'state.params.majorData', undefined)
    let departmentId = get(this.props.navigation, 'state.params.departmentId', undefined)
    let initialLevelOfStudy = get(this.props.navigation, 'state.params.majorData.levelOfStudyOffered', [])
    if (type === 'edit') {
      let initialName = get(this.props, 'navigation.state.params.majorData.majorname', '')
      let filterDegree = filter(initialLevelOfStudy, obj => obj.newFollowingAllowed)
      let initialLevelOfStudyDegree = map(filterDegree, dataObj => {
        return dataObj.levelOfStudy
      })
      let newLevelOfStudyDegree = []
      this.state.selectedIndexes.map(item => {
        if (item.selected) newLevelOfStudyDegree.push(item.degreeLevel)
      })
      let levelOfStudyModified = !isEqual(initialLevelOfStudyDegree.sort(), newLevelOfStudyDegree.sort())
      if ( initialName !== this.state.majorName) {
        this.props.universityActions.updateMajorName(major.majorid, this.state.majorName, !levelOfStudyModified)
      }
      if (levelOfStudyModified) {
        this.addMajorLevelOfStudy(major.majorid)
      }
      // Need to add/delete level of study flow
    } else {
      let levelOfStudyOffered = []
      this.state.selectedIndexes.map((item) => {
        if (item.selected) {
          let obj = {
            levelOfStudy: item.degreeLevel,
            newFollowingAllowed: true
          }
          levelOfStudyOffered.push(obj)
        }
      })
      this.props.universityActions.addMajor(departmentId, { majorName: this.state.majorName, levelOfStudyOffered })
    }
  }

  addMajorLevelOfStudy = (majorId) => {
    let initialLevelOfStudy = filter(get(this.props.navigation, 'state.params.majorData.levelOfStudyOffered', []), obj => obj.newFollowingAllowed )
    // console.log(initialLevelOfStudy, this.state)
    let addedLevelOfStudy = []
    let deletedLevelOfStudy = []
    this.state.selectedIndexes.map((item) => {
      if (item.selected) {
        let findLevelOfStudy = findIndex(initialLevelOfStudy, obj => obj.levelOfStudy === item.degreeLevel)
        if (findLevelOfStudy === -1) {
          addedLevelOfStudy.push(item.degreeLevel)
        }
      } else {
        let findDeletedLevelOfStudy = findIndex(initialLevelOfStudy, obj => obj.levelOfStudy === item.degreeLevel)
        if (findDeletedLevelOfStudy !== -1) {
          deletedLevelOfStudy.push(initialLevelOfStudy[findDeletedLevelOfStudy].id)
        }
      }
    })
    // console.log(addedLevelOfStudy, deletedLevelOfStudy, '123 Check')
    if (!isEmpty(addedLevelOfStudy)) {
      let levelOfStudyOffered = ''
      addedLevelOfStudy.map((item, index) => {
        if (index === 0) {
          levelOfStudyOffered = levelOfStudyOffered + item
        } else {
          levelOfStudyOffered = levelOfStudyOffered + ('&levelOfStudy=' + item)
        }
      })
      this.props.universityActions.addLevelOfStudy(majorId, levelOfStudyOffered, isEmpty(deletedLevelOfStudy))
    }
    if (!isEmpty(deletedLevelOfStudy)) {
      let deletedLevelOfStudyIds = ''
      deletedLevelOfStudy.map((item, index) => {
        if (deletedLevelOfStudy.length - 1 === index) {
          deletedLevelOfStudyIds = deletedLevelOfStudyIds + item
        } else {
          deletedLevelOfStudyIds = deletedLevelOfStudyIds + item + ','
        }
      })
      this.props.universityActions.deleteLevelOfStudy(majorId, deletedLevelOfStudyIds)
    }
  }

  deleteMajor = () => {
    let major = get(this.props.navigation, 'state.params.majorData', undefined)
    this.props.universityActions.deleteMajor(major.majorid)
  }

  renderDegrees = () => {
    let type = get(this.props.navigation, 'state.params.type', undefined)
    return (
      <ScrollView>
        { degreeLevel.map((item, index) => {
          return(
            <TouchableOpacity
              style={[filterUniversityStyles.itemContainer, { paddingHorizontal: 15 }]} key ={index}
              onPress={() => {
                let selectedIndexes = this.state.selectedIndexes
                selectedIndexes[index].selected = !this.state.selectedIndexes[index].selected
                this.setState({ selectedIndexes })
              }}
            >
              { !isEmpty(this.state.selectedIndexes) && this.state.selectedIndexes[index].selected ?
                <Image
                  source={require('../../../../assets/app_icons/Icons/Checkbox-active.png')}
                  style={filterUniversityStyles.checkboxContainer}
                  resizeMode={'stretch'}
                />
                : <Image
                source={require('../../../../assets/app_icons/Icons/Checkbox-default.png')}
                style={filterUniversityStyles.checkboxContainer}
                resizeMode={'stretch'}
              />
              }
            <Text style={filterUniversityStyles.itemText}>
              {item.replace('_', ' ')}
            </Text>
          </TouchableOpacity>
          )
        })}
        {

        }
        { type && type !== 'add' && <View style={{ paddingVertical: 20, alignItems: 'center', justifyContent: 'center' }}>
          <TouchableOpacity
            onPress={() => this.deleteMajor() }
            style={{ paddingVertical: 20 }}
          >
            <Text style={{ fontFamily: style.font.family.rubikRegular, color: '#ea1e1e' }}>Delete Major</Text>
          </TouchableOpacity>
        </View>}
      </ScrollView>
    )
  }

  render() {
    // let lengthOfLevelOfStudySelected = filter(this.state.selectedIndexes, levelOfStudy => levelOfStudy.selected).length
    // console.log(lengthOfLevelOfStudySelected, this.props, '123')
    let type = get(this.props.navigation, 'state.params.type', undefined)
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <MSAHeader
          title={type === 'add' ? 'New Major' : 'Edit Major'}
          leftText={'BACK'}
          rightText={'DONE'}
          onPress={this.handleBackPress.bind(this)}
          onRightTextPress={this.handleDonePress}
          disableRightText={this.state.majorName === '' }
        />
        <View style={{ paddingVertical: 20, paddingHorizontal: 15, borderBottomWidth: 1, borderBottomColor: style.color.lightGrey}}>
          <TextInput
            style={{ color: style.color.coal, width: '100%', lineHeight: 18 }}
            placeholder={'Major Name (Ex: Computer Science)'}
            placeholderTextColor={'rgb(173, 181, 196)'}
            underlineColorAndroid={'rgba(0,0,0,0)'}
            value={this.state.majorName}
            onChangeText={(text) => this.setState({ majorName: text })}
          />
        </View>
        <View style={departmentCreateNewMajorStyles.titleContainer}>
          <Text style={departmentCreateNewMajorStyles.title}>DEGREE LEVEL</Text>
        </View>
        {this.renderDegrees()}
      </View>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  universityActions: bindActionCreators(UniversitiesApi, dispatch)
});

export default connect(undefined, mapDispatchToProps)(DepartmentEditMajor);
