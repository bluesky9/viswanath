import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  Platform,
  TextInput,
  Dimensions
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ActionSheet from 'react-native-actionsheet';
import ImagePicker from 'react-native-image-crop-picker';
import MSAHeader from '../../layouts/header/header';
import profileStyles from '../../profile/profile-styles';
import { style } from '../../../styles/variables';
import { get, isEmpty } from 'lodash'

const { width } = Dimensions.get('window');
let actionsheet = undefined;
let profilePicOptions = ['Cancel', 'Choose an image', 'Take a picture']

type Props = {
  setDraftState: any;
  draftState: any;
  onNext: Function;
  onPrev: Function;
}

type State = {
  deptName: string;
  abbr: string;
  webAddr: string;
  desc: string;
  image: string;
};

export default class DepartmentCreateStep2 extends Component<Props, State> {

  constructor(props) {
    super(props)
    this.state = {
      deptName: '',
      abbr: '',
      webAddr: '',
      desc: '',
      image: ''
    }
  }

  handleBackPress() {
    this.props.onPrev();
  }

  onNext = () => {
    this.props.onNext()
  }

  _handleActionSheetPressed = (choosedIndex) => {
    if (choosedIndex === 1) {
      this.openGallery();
    } else if (choosedIndex === 2) {
      this.openCamera()
    }
  }

  openGallery = () => {
    ImagePicker.openPicker({
      cropping: true,
      compressImageQuality: Platform.OS === 'ios' ? 0 : 0.5
    })
      .then(image => {
        let imageObj = {
          uri: get(image, 'path', ''),
          name: get(image, Platform.OS === 'ios' ? 'creationDate' : 'modificationDate' , ''),
          type: get(image, 'mime', '')
        }
        this.props.setDraftState({ logo: get(image, 'path', ''), profilePicObj: imageObj })
      })
      .catch(Error);
  }

  openCamera = () => {
    ImagePicker.openCamera({
      cropping: true
    })
      .then(image => {
        let imageObj = {
          uri: get(image, 'path', ''),
          name: new Date().getTime().toString(),
          type: get(image, 'mime', '')
        }
        this.props.setDraftState({ logo: get(image, 'path', ''),  profilePicObj: imageObj})
      })
      .catch((e) => {
        console.log(e, 'Error')
      });
  }

  render() {
    let nextActive = (this.props.draftState.name === '' || this.props.draftState.abbreviation === '' || this.props.draftState.description === ''
     || this.props.draftState.abbreviation.length < 2) ? false : true
    console.log(nextActive, '123')
     return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <ActionSheet
          ref={o => (actionsheet = o)}
          options={profilePicOptions}
          cancelButtonIndex={0}
          onPress={i => this._handleActionSheetPressed(i)}
        />
        <MSAHeader
          title={'Create Department'}
          leftText={'BACK'}
          onPress={this.handleBackPress.bind(this)}
          rightText={'NEXT'}
          onRightTextPress={this.onNext}
          disableRightText={!nextActive}
        />
        <KeyboardAwareScrollView style={{ backgroundColor: 'rgb(250,251,252)' }}>
          <View style={{ backgroundColor: 'white' }}>
            <View
              style={[profileStyles.fieldBox, { alignItems: 'center' }]}
            >
              <Image
                source={ !isEmpty(this.props.draftState.logo) ?
                  { uri : this.props.draftState.logo }
                  : require('../../../../assets/images/user_dummy_image.png')
                }
                style={{ height: 76, width: 76, borderRadius: 38 }}
              />
              <TouchableOpacity
                style={{ paddingTop: 10, paddingBottom: 5 }}
                onPress={() => actionsheet.show()}
              >
                <Text style={[profileStyles.dualHeaderRightText, { fontSize: 12, lineHeight: 14 }]}>
                  ADD LOGO/IMAGE
                </Text>
              </TouchableOpacity>
            </View>
            <View style={[profileStyles.fieldBox, { paddingBottom: 11 }]}>
              <Text style={[profileStyles.boxHead, { color: 'rgb(83,87,94)' }]}>
                <Text>DEPARTMENT NAME</Text>
                <Text style={{ color: style.color.coal }}> *</Text>
              </Text>
              <TextInput
                style={[profileStyles.boxLow, { color: style.color.coal, width: '100%', marginTop: 15, marginBottom: 0, lineHeight: 18 }]}
                placeholder={'Department Name (Ex: Department of Economics)'}
                placeholderTextColor={'rgb(173, 181, 196)'}
                underlineColorAndroid={'rgba(0,0,0,0)'}
                value={this.props.draftState.name}
                onChangeText={(text) => this.props.setDraftState({ name: text })}
              />
            </View>
            <View style={[profileStyles.fieldBox, { paddingBottom: 11 }]}>
              <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <Text style={[profileStyles.boxHead, { color: 'rgb(83,87,94)' }]}>
                  <Text>ABBREVIATION</Text>
                  <Text style={{ color: style.color.coal }}> *</Text>
                </Text>
                <Text style={{fontSize: 12, lineHeight: 14, fontFamily: style.font.family.rubikRegular, color: '#ADB5C4'}}>
                  Min. 2 and Max. 10 characters
                </Text>
              </View>
              <TextInput
                style={[profileStyles.boxLow, { color: style.color.coal, width: width - 30, marginTop: 15, marginBottom: 0, lineHeight: 18 }]}
                placeholderTextColor={'rgb(173, 181, 196)'}
                placeholder={'Department Abbreviation (Ex: DOE)'}
                underlineColorAndroid={'rgba(0,0,0,0)'}
                value={this.props.draftState.abbreviation}
                onChangeText={(text) => this.props.setDraftState({ abbreviation: text })}
                maxLength={10}
              />
            </View>
            <View style={[profileStyles.fieldBox, { paddingBottom: 11 }]}>
              <Text
                style={[
                  profileStyles.boxHead,
                  { color: 'rgb(83,87,94)' }
                ]}
              >
                WEB ADDRESS
              </Text>
              <TextInput
                style={[profileStyles.boxLow, { color: style.color.coal, width: width - 30, marginTop: 15, marginBottom: 0, lineHeight: 18 }]}
                placeholder={'Add Department Website'}
                placeholderTextColor={'rgb(173, 181, 196)'}
                underlineColorAndroid={'rgba(0,0,0,0)'}
                value={this.props.draftState.websiteUrl}
                onChangeText={(text) => this.props.setDraftState({ websiteUrl: text })}
              />
            </View>
            <View style={[profileStyles.fieldBox, { paddingBottom: 11 }]}>
              <Text style={[profileStyles.boxHead, { color: 'rgb(83,87,94)' }]}>
                <Text>DESCRIPTION</Text>
                <Text style={{ color: style.color.coal }}> *</Text>
              </Text>
              <TextInput
                style={[profileStyles.boxLow,
                { color: style.color.coal, width: width - 30, marginBottom: 0, marginTop: 15, lineHeight: 18,  maxHeight: 105, minHeight: 105 }]}
                underlineColorAndroid={'rgba(0,0,0,0)'}
                placeholderTextColor={'rgb(173, 181, 196)'}
                placeholder={'Add a short description about this department'}
                multiline={true}
                value={this.props.draftState.description}
                onChangeText={(text) => this.props.setDraftState({ description: text })}
                numberOfLines={6}
              />
            </View>
          </View>
        </KeyboardAwareScrollView>
      </View>
    )
  }
}