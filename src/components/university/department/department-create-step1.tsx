import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import { Icon } from 'native-base';
import { departmentStyles } from './department-styles';
import MSAHeader from '../../layouts/header/header';

type Props = {
  usertype: any;
  onNext: Function;
  onPrev: Function;
  selectedDepartmentType: Function;
}

type State = {
};

export default class DepartmentCreateStep1 extends Component<Props, State> {

  handleBackPress() {
    this.props.onPrev();
  }

  onDepartmentTypeSelect = (type) => {
    this.props.selectedDepartmentType(type)
    this.props.onNext()
  }

  render() {
    let isCurrentStudent = this.props.usertype === 'CURRENT_STUDENT'
    return(
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <MSAHeader
          title={'Create Department'}
          leftText={'CANCEL'}
          onPress={this.handleBackPress.bind(this)}
        />
        { !isCurrentStudent && <TouchableOpacity onPress={() => this.onDepartmentTypeSelect('academic')}>
          <View style={departmentStyles.listContainer}>
            <Text style={departmentStyles.listTitle}>ACADEMIC DEPARTMENT</Text>
            <View style={departmentStyles.detailsContainer}>
              <View style={{ flex: 1 }}>
                <Text style={departmentStyles.listDetails}>Department that teaches classes, award degrees, and/or certificates. Ex: School
                  of Computer Engineering, College of Arts
                </Text>
              </View>
              <View style={departmentStyles.forwardArrowContainer}>
                <Icon name='ios-arrow-forward' style={departmentStyles.forwardArrow} />
              </View>
            </View>
          </View>
        </TouchableOpacity>}
       { !isCurrentStudent && <TouchableOpacity onPress={() => this.onDepartmentTypeSelect('non-academic')}>
          <View style={departmentStyles.listContainer}>
            <Text style={departmentStyles.listTitle}>NON-ACADEMIC DEPARTMENT</Text>
            <View style={departmentStyles.detailsContainer}>
              <View style={{ flex: 1 }}>
                <Text style={departmentStyles.listDetails}>Department that support in university activities
                such as Library, Sport Center, Admissions Department
                </Text>
              </View>
              <View style={departmentStyles.forwardArrowContainer}>
                <Icon name='ios-arrow-forward' style={departmentStyles.forwardArrow} />
              </View>
            </View>
          </View>
        </TouchableOpacity>}
        <TouchableOpacity onPress={() => this.onDepartmentTypeSelect('associationorclub')}>
          <View style={departmentStyles.listContainer}>
            <Text style={departmentStyles.listTitle}>ASSOCIATION/CLUB</Text>
            <View style={departmentStyles.detailsContainer}>
              <View style={{ flex: 1 }}>
                <Text style={departmentStyles.listDetails}>Organisations part of the university such as Gaming Club,
                Student Association, Cultural Organizations
                </Text>
              </View>
              <View style={departmentStyles.forwardArrowContainer}>
                <Icon name='ios-arrow-forward' style={departmentStyles.forwardArrow} />
              </View>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    )
  }
}