import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  TextInput
} from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
import { departmentCreateNewMajorStyles } from './department-styles';
import { filterUniversityStyles } from '../../people/people-filter-styles';
import { style } from '../../../styles/variables';

import MSAHeader from '../../layouts/header/header';

import { get, isEmpty, findIndex, find } from 'lodash';

interface ILoginScreenNavParams { }
interface ILoginScreenProps
  extends NavigationScreenProps<ILoginScreenNavParams> {
}

type State = {
  majorName;
  selectedIndexes;
  levelOfStudyOffered;
};

const degreeLevel = ['BACHELORS', 'MASTERS', 'PHD', 'ASSOCIATE_DEGREE',
  'BACHELORS_ONLINE', 'MASTERS_ONLINE', 'ASSOCIATE_ONLINE', 'NON_DEGREE', 'DIPLOMA', 'CERTIFICATE', 'PROFESSIONAL']

export default class DepartmentCreateNewMajor extends Component<ILoginScreenProps, State> {

  constructor(props) {
    super(props);
    this.state = {
      majorName: get(this.props, 'navigation.state.params.majorData.majorname', undefined) ?
        get(this.props, 'navigation.state.params.majorData.majorname') : get(this.props, 'navigation.state.params.majorData.majorName', '') ,
      selectedIndexes: [],
      levelOfStudyOffered: []
    }
  }

  componentDidMount() {
    let selectedIndexes = []
    let type = get(this.props.navigation, 'state.params.type', undefined)
    let levelOfStudyOffered = get(this.props.navigation, 'state.params.majorData.levelOfStudyOffered', [])
    if (type === 'add')
      degreeLevel.map(item => {
        selectedIndexes.push({degreeLevel: item, selected: false})
      })
    else degreeLevel.map((item) => {
      let findDegreeIndex = findIndex(levelOfStudyOffered, obj => item === obj.levelOfStudy)
      if (findDegreeIndex === -1) selectedIndexes.push({degreeLevel: item, selected: false})
      else selectedIndexes.push({degreeLevel: item, selected: true})
    })
    this.setState({ selectedIndexes })
  }

  handleBackPress() {
    this.props.navigation.goBack()
  }

  onDeleteMajorPressed() {
    let deleteMajor = get(this.props.navigation, 'state.params.deleteMajor', undefined)
    if (deleteMajor) deleteMajor();
    this.props.navigation.goBack()
  }

  handleDonePress = () => {
    let getMajor = get(this.props.navigation, 'state.params.getMajor', undefined)
    let updateMajor = get(this.props.navigation, 'state.params.updateMajor', undefined)
    let levelOfStudyOffered = []
    this.state.selectedIndexes.map((item) => {
      if (item.selected) {
        let obj = {
          id: 0,
          levelOfStudy: item.degreeLevel,
          newFollowingAllowed: true
        }
        levelOfStudyOffered.push(obj)
      }
    })
    if (getMajor) getMajor({ majorName: this.state.majorName, levelOfStudyOffered})
    if (updateMajor) updateMajor({ majorName: this.state.majorName, levelOfStudyOffered})
    this.props.navigation.goBack()
  }

  renderDegrees = () => {
    let type = get(this.props.navigation, 'state.params.type', undefined)
    return (
      <ScrollView>
        { degreeLevel.map((item, index) => {
          return(
            <TouchableOpacity
              style={[filterUniversityStyles.itemContainer, { paddingHorizontal: 15 }]} key ={index}
              onPress={() => {
                let selectedIndexes = this.state.selectedIndexes
                selectedIndexes[index].selected = !this.state.selectedIndexes[index].selected
                this.setState({ selectedIndexes })
              }}
            >
              { !isEmpty(this.state.selectedIndexes) && this.state.selectedIndexes[index].selected ?
                <Image
                  source={require('../../../../assets/app_icons/Icons/Checkbox-active.png')}
                  style={filterUniversityStyles.checkboxContainer}
                  resizeMode={'stretch'}
                />
                : <Image
                source={require('../../../../assets/app_icons/Icons/Checkbox-default.png')}
                style={filterUniversityStyles.checkboxContainer}
                resizeMode={'stretch'}
              />
              }
              {/* <Image
                source={require('../../../../assets/app_icons/Icons/Checkbox-default.png')}
                style={filterUniversityStyles.checkboxContainer}
                resizeMode={'stretch'}
              /> */}
            <Text style={filterUniversityStyles.itemText}>
              {item.replace('_', ' ')}
            </Text>
          </TouchableOpacity>
          )
        })}
        {

        }
        { type && type !== 'add' && <View style={{ paddingVertical: 20, alignItems: 'center', justifyContent: 'center' }}>
          <TouchableOpacity
            onPress={() => this.onDeleteMajorPressed() }
            style={{ paddingVertical: 20 }}
          >
            <Text style={{ fontFamily: style.font.family.rubikRegular, color: '#ea1e1e' }}>Delete Major</Text>
          </TouchableOpacity>
        </View>}
      </ScrollView>
    )
  }

  render() {
    let type = get(this.props.navigation, 'state.params.type', undefined)
    let checkDegreeSelect = find(this.state.selectedIndexes, obj => obj.selected)
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <MSAHeader
          title={type === 'add' ? 'New Major' : 'Edit Major'}
          leftText={'BACK'}
          rightText={'DONE'}
          onPress={this.handleBackPress.bind(this)}
          onRightTextPress={this.handleDonePress}
          disableRightText={this.state.majorName === '' || !checkDegreeSelect }
        />
        <View style={{ paddingVertical: 20, paddingHorizontal: 15, borderBottomWidth: 1, borderBottomColor: style.color.lightGrey}}>
          <TextInput
            style={{ color: style.color.coal, width: '100%', lineHeight: 18 }}
            placeholder={'Major Name (Ex: Computer Science)'}
            placeholderTextColor={'rgb(173, 181, 196)'}
            underlineColorAndroid={'rgba(0,0,0,0)'}
            value={this.state.majorName}
            onChangeText={(text) => this.setState({ majorName: text })}
          />
        </View>
        <View style={departmentCreateNewMajorStyles.titleContainer}>
          <Text style={departmentCreateNewMajorStyles.title}>DEGREE LEVEL</Text>
        </View>
        {this.renderDegrees()}
      </View>
    )
  }
}
