import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  TouchableOpacity
} from 'react-native';
import { Icon } from 'native-base';
import { NavigationScreenProps } from 'react-navigation';
import MSAHeader from '../../layouts/header/header';
import { UniversitiesApi } from '../../../store/actions'

import { peopleScreenStyles } from '../../people/people-screen-styles';
import profileStyles from '../../profile/profile-styles';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { get, find, filter, isEmpty } from 'lodash';
import { departmentStyles } from './department-styles';
// import { style } from '../../../styles/variables';

interface ILoginScreenProps extends NavigationScreenProps<{}> {
  departments;
  departmentsCount;
  universityDetails;
  universityActions;
  majors;
}

interface State {
  pageSize;
  selectedMajorIndex;
}

class DepartmentMajor extends Component<ILoginScreenProps, State> {
  constructor(props) {
    super(props);
    this.state = {
      pageSize: 20,
      selectedMajorIndex: undefined
    }
  }

  handleBackPress = () => {
    this.props.navigation.goBack();
  }

  handleEditMajorPressed = (obj, key) => {
    this.props.navigation.navigate('DepartmentEditMajor', {
      type: 'edit',
      majorData: obj,
      departmentId: get(this.props.navigation.state.params, 'departmentId', undefined)
    })
    this.setState({ selectedMajorIndex: key })
  }

  renderMajors = (obj, key) => {
    let levelOfStudyOffered = get(obj, 'levelOfStudyOffered', [])
    let showEditOption = get(this.props, 'navigation.state.params.showEditOption', false)
    return (
      <View style={[peopleScreenStyles.peopleCardContainer, { flexDirection: 'column' }]}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', flex: 1 }}>
          <Text style={peopleScreenStyles.peopleLocation}>
            {get(obj, 'majorname', '')}
          </Text>
         { showEditOption  &&
          <TouchableOpacity
            onPress={() => this.handleEditMajorPressed(obj, key)}
          >
            <Icon name='md-create' style={{ color: 'rgb(207, 208, 220)', fontSize: 20 }} />
          </TouchableOpacity>
        }
        </View>
        { !isEmpty(levelOfStudyOffered) &&
          <View style={{ paddingTop: 13, flexDirection: 'row', flexWrap: 'wrap' }}>
            {
              levelOfStudyOffered.map((item, index) => {
                if (item.newFollowingAllowed)
                  return (
                    <View style={departmentStyles.tagsBlock} key={index}>
                      <Text style={departmentStyles.tags}>{item.levelOfStudy.replace('_', ' ')}</Text>
                    </View>
                  )
                else return <View key={index}/>
              })
            }
          </View>
        }
      </View>
    )
  }

  render() {
    let departmentId = get(this.props, 'navigation.state.params.departmentId')
    let department = find(this.props.departments.content, departmentObj => departmentObj.deptid === departmentId)
    let majors = filter(this.props.majors, major => !major.deleted )
    let showEditOption = get(this.props, 'navigation.state.params.showEditOption', false)
    return (
      <View style={{ paddingBottom: 20, flex: 1 }}>
        <MSAHeader
          title={'Major @ ' +  get(department, 'deptAbbr', '')}
          onPress={this.handleBackPress}
          rightText={showEditOption && 'DONE'}
          onRightTextPress={() => this.props.navigation.goBack()}
        />
        { showEditOption &&
          <View style={[profileStyles.fieldBox, { paddingVertical: 20, flex: 0, justifyContent: 'center', backgroundColor: 'white' }]}>
            <Text style={[profileStyles.fieldDarkLow, { fontSize: 16, lineHeight: 21 }]}>
              New Major
            </Text>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('DepartmentEditMajor', {
                  type: 'add',
                  departmentId: get(this.props.navigation.state.params, 'departmentId', undefined)
                })
              }
              style={{ position: 'absolute', right: 15 }}
            >
              <Text style={profileStyles.tagButtonText}>ADD</Text>
            </TouchableOpacity>
          </View>
        }
        <FlatList
          data={majors}
          renderItem={obj => this.renderMajors(obj.item, obj.index)}
          keyExtractor={item => item.majorid.toString()}
          contentContainerStyle={{ paddingBottom: 60 }}
          showsVerticalScrollIndicator={false}
        />
      </View>
    );
  }
}

const mapStateToProps = ({ universityDetails }, navState) => {
  let navParams = navState.navigation.state.params.departmentId
  return {
    majors: find(universityDetails.departments.content, dept => dept.deptid === navParams).major,
    departments: get(universityDetails, 'departments', {} ),
    universityDetails: get(universityDetails, 'details', {}),
    departmentsCount: get(universityDetails, 'departments.totalElements', 0)
  };
};

const mapDispatchToProps = dispatch => ({
  universityActions: bindActionCreators(UniversitiesApi, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(DepartmentMajor);
