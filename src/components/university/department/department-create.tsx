import React, { Component } from 'react';
import {
  View
} from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
import Step1 from './department-create-step1';
import Step2 from './department-create-step2';
import Step3 from './department-create-step3';
import Step4 from './department-create-step4';

import { UniversitiesApi, ProfileApi } from '../../../store/actions';

import { get, isEmpty } from 'lodash';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

interface ILoginScreenNavParams { }
interface ILoginScreenProps
  extends NavigationScreenProps<ILoginScreenNavParams> {
    usertype: any;
    universitiesApi: any;
    universityId: any;
    profileApi: any;
}

type State = {
  selectedStep: number;
  departmentType: string;
  logo: string;
  name: string;
  abbreviation: string;
  websiteUrl: string;
  universityId: number;
  description: string;
  majorList: any;
  location: any;
  hoursOfOperationList: any;
  adminUserIds: any;
  profilePicObj: any;
  programList: any;
};

class DepartmentCreate extends Component<ILoginScreenProps, State> {

  constructor(props) {
    super(props);
    let location = {
      address: get(this.props, 'navigation.state.params.department.mapAddress', ''),
      latitude: get(this.props, 'navigation.state.params.department.latitude', 0),
      longitude: get(this.props, 'navigation.state.params.department.longitude', 0)
    }
    let adminUserIds = []
    if (!isEmpty(get(this.props, 'navigation.state.params.department.adminProfileIds', []))) {
      get(this.props, 'navigation.state.params.department.adminProfileIds').map((item) => {
        adminUserIds.push(item.id)
      })
    }

    this.state = {
      selectedStep: get(this.props, 'navigation.state.params.department', undefined) ? 1 : 0,
      departmentType: get(this.props, 'navigation.state.params.department.deptType', ''),
      logo: get(this.props, 'navigation.state.params.department.deptlogo', undefined),
      name: get(this.props, 'navigation.state.params.department.deptname', ''),
      abbreviation: get(this.props, 'navigation.state.params.department.deptAbbr', ''),
      websiteUrl: get(this.props, 'navigation.state.params.department.deptweblink', ''),
      universityId: this.props.universityId,
      description: get(this.props, 'navigation.state.params.department.deptdescription', ''),
      majorList: get(this.props, 'navigation.state.params.department.major', []),
      location,
      hoursOfOperationList: get(this.props, 'navigation.state.params.department.hoursOfOperations', []),
      adminUserIds: adminUserIds,
      profilePicObj: {},
      programList: get(this.props, 'navigation.state.params.department.programs', [])
    }
  }

  nextStep = () => {
    let department = get(this.props, 'navigation.state.params.department', undefined)
    if (this.state.selectedStep === 3) {
      let dataObj = {
        logo: this.state.logo ? this.state.logo : '',
        name: this.state.name,
        abbreviation: this.state.abbreviation,
        websiteUrl: this.state.websiteUrl,
        universityId: this.state.universityId,
        description: this.state.description,
        majorList: this.state.majorList,
        location: this.state.location,
        hoursOfOperationList: this.state.hoursOfOperationList,
        adminUserIds: this.state.adminUserIds,
        programList: this.state.programList
      }
      if (department) {
        let deptId = get(this.props, 'navigation.state.params.department.deptid', undefined)
        this.props.universitiesApi.updateDepartment(dataObj, deptId)
      } else {
        if (!isEmpty(this.state.profilePicObj)) {
          this.props.profileApi.uploadProfilePic(this.state.profilePicObj, undefined, dataObj, this.state.departmentType);
        } else {
          // console.log(dataObj, 'Check')
          this.props.universitiesApi.createDepartment(dataObj, this.state.departmentType)
        }
      }
    } else if (this.state.departmentType === 'academic' && this.state.selectedStep === 1 && !department)
      this.setState({ selectedStep: 2 })
    else if ( this.state.selectedStep === 1 )
      this.setState({ selectedStep: this.state.selectedStep + 2 })
    else
      this.setState({ selectedStep: this.state.selectedStep + 1 })
  }

  previousStep = () => {
    if (this.state.departmentType !== 'academic' && this.state.selectedStep === 3)
    this.setState({ selectedStep: this.state.selectedStep - 2 })
    else
      this.setState({ selectedStep: this.state.selectedStep - 1})
  }

  handleBackPress = () => {
    this.props.navigation.goBack()
  }

  render() {
    let department = get(this.props, 'navigation.state.params.department', undefined)
    return(
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        { this.state.selectedStep === 0 &&
          <Step1
            usertype={this.props.usertype}
            onNext={this.nextStep}
            onPrev={this.handleBackPress}
            selectedDepartmentType={type => {
              this.setState({ departmentType: type })
            }}
          />
        }
        { this.state.selectedStep === 1 &&
          <Step2
            onNext={this.nextStep}
            onPrev={() => department ? this.props.navigation.goBack() : this.previousStep()}
            draftState={this.state}
            setDraftState={state => this.setState(state)}
          />
        }
        { this.state.selectedStep === 2 &&
          <Step3
            onNext={this.nextStep}
            onPrev={this.previousStep}
            draftState={this.state}
            setDraftState={state => this.setState(state)}
            navigation={this.props.navigation}
          />
        }
        { this.state.selectedStep === 3 &&
          <Step4
            onNext={this.nextStep}
            onPrev={() => department ? this.setState({ selectedStep: 1 }) : this.previousStep()}
            navigation={this.props.navigation}
            draftState={this.state}
            setDraftState={state => this.setState(state)}
          />
        }
      </View>
    )
  }
}

const mapStateToProps = ({ universityDetails }) => {
  return {
    usertype: universityDetails.usertype,
    universityId: get(universityDetails, 'details.univ_id', undefined)
  };
};

const mapDispatchToProps = dispatch => ({
  universitiesApi: bindActionCreators(UniversitiesApi, dispatch),
  profileApi: bindActionCreators(ProfileApi, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(DepartmentCreate);