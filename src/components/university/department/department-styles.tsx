import { StyleSheet } from 'react-native';
import { style } from '../../../styles/variables';

export const departmentStyles = StyleSheet.create({
  container: {
    paddingTop: 12 + style.header.height,
    paddingBottom: 12,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: style.color.lightGrey,
    backgroundColor: style.color.white
  },
  leftContainer: {
    alignItems: 'flex-start'
  },
  leftText: {
    fontSize: 14,
    lineHeight: 17,
    letterSpacing: 0.5,
    fontFamily: style.font.family.rubikMedium,
    color: style.color.bluishGrey
  },
  profileIconContainer: {
    height: 30,
    width: 30,
    borderRadius: 15
  },
  rightContainer: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  bodyText: {
    color: style.color.bluishGrey,
    flex: 1,
    textAlign: 'center',
    fontFamily: style.font.family.rubikRegular,
    letterSpacing: 0.2,
    fontSize: 16,
    lineHeight: 19
  },
  activeFilter: {
    tintColor: style.color.bloodRed
  },
  listContainer: {
    paddingHorizontal: 15,
    paddingVertical: 20,
    borderBottomWidth: 1,
    borderColor: '#E9EFF7'
  },
  listTitle: {
    fontSize: 20,
    lineHeight: 24,
    color: '#707070',
    fontFamily: style.font.family.rubikRegular
  },
  detailsContainer: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    paddingTop: 6
  },
  listDetails: {
    fontSize: 14,
    lineHeight: 17,
    color: '#ADB5C4',
    fontFamily: style.font.family.rubikRegular
  },
  forwardArrowContainer: {
    paddingLeft: 15
  },
  forwardArrow: {
    fontSize: 20,
    color: '#ADB5C4'
  },
  subTitle: {
    fontSize: 12,
    lineHeight: 14,
    color: '#808691',
    fontFamily: style.font.family.rubikMedium,
    marginTop: 38
  },
  profilePic: {
    height: 41,
    width: 41,
    borderRadius: 20.5,
    resizeMode: 'cover'
  },
  name: {
    fontSize: 16,
    lineHeight: 21,
    letterSpacing: 0.2,
    fontFamily: style.font.family.rubikRegular,
    color: '#3587E6'
  },
  designation: {
    fontSize: 14,
    lineHeight: 17,
    letterSpacing: 0.2,
    fontFamily: style.font.family.rubikRegular,
    color: '#707070',
    marginTop: 2
  },
  location: {
    fontSize: 14,
    lineHeight: 21,
    letterSpacing: 0.2,
    fontFamily: style.font.family.rubikRegular,
    color: '#707070',
    marginTop: 15
  },
  timingBlock: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderBottomColor: '#E9EFF7',
    paddingVertical: 4
  },
  timing: {
    fontSize: 14,
    lineHeight: 21,
    letterSpacing: 0.2,
    fontFamily: style.font.family.rubikRegular,
    color: '#707070'
  },
  hideDivider: {
    height: 1,
    backgroundColor: '#fff',
    marginTop: -1
  },
  tagsBlock: {
    paddingVertical: 6,
    paddingHorizontal: 13,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#1E7AE3',
    backgroundColor: '#E7F2FC',
    marginRight: 6,
    marginBottom: 6
  },
  tags: {
    fontSize: 14,
    lineHeight: 17,
    letterSpacing: 0,
    fontFamily: style.font.family.rubikRegular,
    color: '#1E7AE3'
  },
  linkText: {
    fontSize: 12,
    lineHeight: 18,
    letterSpacing: 0.2,
    fontFamily: style.font.family.rubikRegular,
    color: '#3587E6'
  },
  linkIcon: {
    fontSize: 18,
    color: '#3587E6',
    marginRight: 4
  }
});

export const departmentMajorsListStyles = StyleSheet.create({
  container: {
    paddingHorizontal: 15,
    paddingVertical: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#e9eff7',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  darkText: {
    fontSize: 16,
    lineHeight: 21,
    fontFamily: style.font.family.rubikRegular,
    color: '#53575E'
  },
  blueBtn: {
    fontSize: 14,
    lineHeight: 17,
    fontFamily: style.font.family.rubikMedium,
    color: '#3587E6'
  },
  editIcon: {
    fontSize: 20,
    color: '#C5CFDB'
  },
  contentContainer: {
    paddingHorizontal: 15,
    paddingTop: 20,
    paddingBottom: 12,
    borderBottomWidth: 1,
    borderBottomColor: '#e9eff7'
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  listBlock: {
    paddingTop: 10,
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  list: {
    borderWidth: 1,
    borderColor: '#1e7ae3',
    borderRadius: 5,
    backgroundColor: '#e7f2fc',
    paddingVertical: 5,
    paddingHorizontal: 13.5,
    marginRight: 8,
    marginBottom: 8
  },
  listName: {
    fontSize: 14,
    lineHeight: 17,
    color: '#1e7ae3',
    fontFamily: style.font.family.rubikRegular
  }
});

export const departmentCreateNewMajorStyles = StyleSheet.create({
  titleContainer: {
    paddingHorizontal: 15,
    paddingTop: 32,
    paddingBottom: 6,
    borderBottomWidth: 1,
    borderBottomColor: '#e9eff7'
  },
  title: {
    fontSize: 14,
    lineHeight: 17,
    color: '#53575e',
    fontFamily: style.font.family.rubikMedium
  }
});