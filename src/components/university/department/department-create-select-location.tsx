import React, { Component } from 'react';
import {
  View
} from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { get } from 'lodash';

import MSAHeader from '../../layouts/header/header';

interface ILoginScreenNavParams { }
interface ILoginScreenProps
  extends NavigationScreenProps<ILoginScreenNavParams> {}

type State = {
  location;
};

export default class DepartmentCreateSelectLocation extends Component<ILoginScreenProps, State> {

  constructor(props) {
    super(props);
    this.state = {
      location: {
        address: '',
        latitude: 0,
        longitude: 0
      }
    }
  }

  componentDidMount() {
    let location = get(this.props, 'navigation.state.params.location')
    this.setState({ location })
  }

  onDonePressed = () => {
    let setLocation = get(this.props.navigation, 'state.params.setLocation', undefined)
    if (setLocation) setLocation(this.state.location);
    this.props.navigation.goBack()
  }

  render() {
    console.log(this.state)
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <MSAHeader
          title={'Select Location'}
          leftText={'BACK'}
          onPress={() => this.props.navigation.goBack()}
          rightText={'DONE'}
          onRightTextPress={this.onDonePressed}
        />
        <GooglePlacesAutocomplete
          placeholder='Search'
          minLength={2} // minimum length of text to search
          autoFocus={false}
          returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
          listViewDisplayed='auto'    // true/false/undefined
          fetchDetails={true}
          onPress={(data, details = undefined) => { // 'details' is provided when fetchDetails = true
            console.log(data, details);
            let location = {
              address: get(data, 'description', ''),
              latitude: get(details, 'geometry.location.lat', 0),
              longitude: get(details, 'geometry.location.lng', 0)
            }
            this.setState({ location: location })
          }}
          query={{
            key: 'AIzaSyA_fPZhE_Ex244izmvskRgxw55Hc6qzZTg',
            language: 'en' // language of the results
          }}
          styles={{
            textInputContainer: {
              width: '100%',
              height: undefined,
              backgroundColor: 'white',
              alignItems: 'center',
              paddingHorizontal: 15,
              paddingVertical: 15,
              borderTopWidth: 0,
              borderBottomWidth: 1,
              borderBottomColor: '#e9eff7'
            },
            textInput: {
              paddingLeft: 0,
              paddingRight: 0,
              paddingTop: 0,
              paddingBottom: 0,
              marginLeft: 0,
              marginRight: 0,
              marginTop: 0,
              borderRadius: 0,
              fontSize: 14
            },
            description: {
              fontWeight: 'bold'
              // paddingTop: 5 Text Style
            },
            // separator: {
            //   paddingVertical: 20
            // },
            listView: {
              marginTop: 25,
              borderTopWidth: 1,
              borderTopColor: '#e9eff7'
            },
            predefinedPlacesDescription: {
              color: '#1faadb'
            }
          }}
        />
      </View>
    )
  }
}