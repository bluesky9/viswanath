import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  // Image,
  TextInput
  // Dimensions
  // TouchableHighlight
} from 'react-native';
// import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
// import SortableListView from 'react-native-sortable-listview'
import profileStyles from '../../profile/profile-styles';
import MSAHeader from '../../layouts/header/header';

// const { width } = Dimensions.get('window');

type Props = {
  usertype: any;
  onNext: Function;
  onPrev: Function;
  selectedDepartmentType: Function
}

type State = {
};

export default class DepartmentCreateAddTags extends Component<Props, State> {

  handleBackPress() {
    this.props.onPrev();
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <MSAHeader
          title={'Add Tags'}
          leftText={'BACK'}
          rightText={'NEXT'}
          onPress={this.handleBackPress.bind(this)}
        />
        <View style={[profileStyles.fieldBox, { paddingVertical: 0, paddingTop: 20, paddingBottom: 10 }]}>
          <Text
            style={[
              profileStyles.boxHead,
              {
                color: 'rgb(83,87,94)',
                flex: 0
              }
            ]}
          >
            NEW TAG
          </Text>
          <View style={{ alignItems: 'center', flexDirection: 'row', marginTop: 10 }}>
            <TextInput
              style={[profileStyles.skillInp, { flex: 1 }]}
              placeholder={'Tag (ex: New Tag)'}
              placeholderTextColor={'rgba(108,111,125,0.5)'}
              underlineColorAndroid={'rgba(0,0,0,0)'}
              onChangeText={(text) => this.setState({ skill: text })}
              maxLength={40}
            />
            <TouchableOpacity>
              <Text style={profileStyles.tagButtonText}>ADD</Text>
            </TouchableOpacity>
          </View>
        </View>
        {/* <SortableListView style={{ flex: 1, backgroundColor: 'rgb(250,251,252)' }}
          data={data}
          renderRow={(row, sectionId, rowIndex) => {
            return <Skill removeSkill={this.removeSkill} skill={row} sectionId={sectionId} rowId={rowIndex} />
          }}
          order={this.state.order}
          onRowMoved={e => {
            this.state.order.splice(e.to, 0, this.state.order.splice(e.from, 1)[0])
            this.setState({
              order: this.state.order
            })
          }}
        >
        </SortableListView> */}
      </View>
    )
  }
}

// class Skill extends Component<{
//   sortHandlers?: any
//   removeSkill: Function;
//   skill: any;
//   sectionId?: any;
//   rowId?: any;
// }, {}> {
//   render() {
//     return <TouchableHighlight
//       style={{
//         width: '100%',
//         padding: 15,
//         backgroundColor: 'white',
//         flexDirection: 'row',
//         borderBottomWidth: 1,
//         borderColor: 'rgb(233,239,247)'
//       }}
//       // {...this.props.sortHandlers}
//     >
//       <View
//         style={{
//           flexDirection: 'row',
//           alignItems: 'center',
//           flex: 1
//         }}
//       >
//         <View style={{ flex: 1, flexDirection: 'row' }}>
//           <TouchableOpacity
//             onPress={() => this.props.removeSkill(this.props.rowId)}
//             style={{ paddingRight: 10 }}
//           >
//             <Image
//               source={require('../../../assets/app_icons/Icons/Minus.png')}
//             />
//           </TouchableOpacity>
//           <Text style={profileStyles.fieldDarkLow}>{this.props.skill ? this.props.skill.skills : ''}</Text>
//         </View>
//         <View>
//           <Image
//             source={require('../../../assets/hamburger.png')}
//             style={{ height: 14, width: 14 }}
//           />
//         </View>
//       </View>
//     </TouchableHighlight>
//   }
// }