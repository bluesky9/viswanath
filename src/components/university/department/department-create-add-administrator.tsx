import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  Dimensions
} from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
import { KeyboardAwareFlatList } from 'react-native-keyboard-aware-scroll-view';
import universityStyles from '../university-styles';
import MSAHeader from '../../layouts/header/header';

import { UniversitiesApi } from '../../../store/actions';

import { get, filter, findIndex } from 'lodash';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

const { width } = Dimensions.get('window');

interface ILoginScreenNavParams { }
interface ILoginScreenProps
  extends NavigationScreenProps<ILoginScreenNavParams> {
    universitiesApi: any;
    universityId: any;
    adminCandidate: any;
    userId: any;
}

type State = {
  adminIds: any;
  searchAdmin: any;
};

class DepartmentCreateAddAdministrator extends Component<ILoginScreenProps, State> {

  constructor(props) {
    super(props);
    this.state = {
      adminIds: [],
      searchAdmin: ''
    }
  }

  handleBackPress() {
    this.props.navigation.goBack()
  }

  componentDidMount() {
    this.props.universitiesApi.getAdminCandidate(this.props.universityId)
    let adminIds = get(this.props.navigation, 'state.params.adminIds', undefined)
    if (adminIds) this.setState({ adminIds })
  }

  onConnectionTagsPressed = (userId) => {
    let adminIds = this.state.adminIds
    let checkAdminIdIndex = findIndex(adminIds, id => id === userId )
    if (checkAdminIdIndex === -1) adminIds.push(userId)
    else adminIds.splice(checkAdminIdIndex, 1)
    this.setState({ adminIds })
  }

  onDonePressed = () => {
    let tagAdmin = get(this.props.navigation, 'state.params.tagAdmin', undefined)
    if (tagAdmin) tagAdmin(this.state.adminIds);
    console.log(this.state.adminIds, '213')
    this.props.navigation.goBack()
  }

  renderAdmin = (index, item) => {
    let checkAdminTaggedIndex = findIndex(this.state.adminIds, id => id === item.userId);
    let checkAdminTagged = (checkAdminTaggedIndex !== -1);
    return (
      <View style={universityStyles.tagItemBox} key={index}>
        <Image
          source={ item.profPicPath ? { uri: item.profPicPath } : require('../../../../assets/images/user_dummy_image.png')}
          style={universityStyles.personThumbnail}
        />
        <Text style={universityStyles.likesItemText}>
          {item.firstName + ' ' + item.lastName}
        </Text>
        <TouchableOpacity
          style={{
            alignSelf: 'center',
            position: 'absolute',
            left: width - 42
          }}
          onPress={() => this.onConnectionTagsPressed(item.userId)}
        >
          { checkAdminTagged ?
            <Text
            style={[
              universityStyles.tagButtonText,
              {
                color: 'rgb(53,135,230)'
              }
            ]}
            >
              UNTAG
            </Text>
              : <Text
              style={[
                universityStyles.tagButtonText,
                {
                  color: 'rgb(204,20,51)'
                }
              ]}
            >
              TAG
            </Text>
          }
        </TouchableOpacity>
      </View>
    )
  }

  render() {
    let adminCandidate = filter(this.props.adminCandidate, admin => admin.userId !== this.props.userId)
    let filterCandidate = filter(adminCandidate, candidate => candidate.firstName.toLowerCase().startsWith(this.state.searchAdmin))
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <MSAHeader
          title={'Add Administrators'}
          leftText={'BACK'}
          rightText={'DONE'}
          onPress={this.handleBackPress.bind(this)}
          onRightTextPress={this.onDonePressed}
        />
        <View style={universityStyles.tagSearchbarContainer}>
          <TextInput
            onChangeText={txt => this.setState({ searchAdmin: txt })}
            style={universityStyles.tagInput}
            placeholder={'Search people'}
            autoCapitalize={'none'}
          />
          <View style={universityStyles.tagSearchIcon}>
            <Image
              style={{ height: 15, width: 15 }}
              source={require('../../../../assets/app_icons/Icons/search.png')}
            />
          </View>
        </View>
        <View style={universityStyles.tagInstructionContainer}>
          <Text style={universityStyles.tagInst}>Your connections</Text>
        </View>
        <View style={{ flex: 6, backgroundColor: 'rgb(242,245,249)' }}>
          <KeyboardAwareFlatList
            data={this.state.searchAdmin === '' ? adminCandidate : filterCandidate}
            renderItem={({ index, item }) => this.renderAdmin(index, item)}
            keyExtractor={item => item.userId.toString()}
            extraData={this.state}
          />
        </View>
      </View>
    )
  }
}

const mapStateToProps = ({ universityDetails, userInfoAPI }) => {
  return {
    adminCandidate: get(universityDetails, 'adminCandidate', undefined),
    universityId: get(universityDetails, 'details.univ_id', undefined),
    userId: get(userInfoAPI, 'userId', undefined)
  };
};

const mapDispatchToProps = dispatch => ({
  universitiesApi: bindActionCreators(UniversitiesApi, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(DepartmentCreateAddAdministrator);