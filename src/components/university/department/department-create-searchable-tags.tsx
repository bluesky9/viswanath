import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  TextInput
} from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
import {get, cloneDeep} from 'lodash';
import profileStyles from '../../profile/profile-styles';
import MSAHeader from '../../layouts/header/header';

interface IPeopleFilterNavParams {
  saveExtraProfileDetails: Function;
}
interface State {
  tags: any[];
  tag: string;
}
interface IPeopleFilterProps
  extends NavigationScreenProps<IPeopleFilterNavParams> {}
export default class DepartmentCreateSearchableTags extends Component<IPeopleFilterProps, State> {
  constructor(props) {
    super(props);
    this.state = {
      tags: cloneDeep(get(this.props.navigation, 'state.params.tags', [])),
      tag: ''
    }
    this.addSkill = this.addSkill.bind(this);
    this.removeSkill = this.removeSkill.bind(this);
  }
  addSkill() {
    let tags = this.state.tags;
    tags.push(this.state.tag)
    this.setState({ tags, tag: '' })
  }

  removeSkill(index) {
    let tags = this.state.tags;
    tags.splice(index, 1)
    this.setState({ tags })
  }

  handleBackPress() {
    this.props.navigation.goBack()
  }

  onDonePressed = () => {
    let addTags = get(this.props.navigation, 'state.params.addTags', undefined)
    if (addTags) addTags(this.state.tags);
    this.props.navigation.goBack()
  }

  render() {
    return (
      <View style={{ backgroundColor: 'white', flex: 1 }}>
        <MSAHeader
          title={'Add Tags'}
          leftText={'BACK'}
          rightText={'DONE'}
          onPress={this.handleBackPress.bind(this)}
          onRightTextPress={this.onDonePressed}
        />
        <View style={[profileStyles.fieldBox, { paddingVertical: 0, paddingTop: 20, paddingBottom: 10 }]}>
          <Text
            style={[
              profileStyles.boxHead,
              {
                color: 'rgb(83,87,94)',
                flex: 0
              }
            ]}
          >
            NEW TAGS
          </Text>
          <View style={{ alignItems: 'center', flexDirection: 'row', marginTop: 10 }}>
            <TextInput
              style={[profileStyles.skillInp, {flex: 1}]}
              placeholder={'Tags (ex: Robotics)'}
              placeholderTextColor={'rgba(108,111,125,0.5)'}
              underlineColorAndroid={'rgba(0,0,0,0)'}
              value={this.state.tag}
              onChangeText={(text) => this.setState({tag: text})}
              maxLength={40}
            />
            <TouchableOpacity
              onPress={() => this.addSkill()}
              disabled={this.state.tag.trim() === ''}
            >
              <Text style={profileStyles.tagButtonText}>ADD</Text>
            </TouchableOpacity>
          </View>
        </View>
        { this.state.tags.map((item, index)  => {
          return (
            <TouchableOpacity
              style={{
                width: '100%',
                padding: 15,
                backgroundColor: 'white',
                flexDirection: 'row',
                borderBottomWidth: 1,
                borderColor: 'rgb(233,239,247)'
              }}
              key={index}
            >
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  flex: 1
                }}
              >
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <TouchableOpacity
                    onPress={() => this.removeSkill(index)}
                    style={{ paddingRight: 10 }}
                  >
                    <Image
                      source={require('../../../../assets/app_icons/Icons/Minus.png')}
                    />
                  </TouchableOpacity>
                  <Text style={profileStyles.fieldDarkLow}>{item}</Text>
                </View>
              </View>
            </TouchableOpacity>
          )
        })}
      </View>
    );
  }
}

// class Skill extends Component<{
//   sortHandlers?: any
//   removeSkill: Function;
//   skill: any;
//   sectionId?: any;
//   rowId?: any;
// }, {}> {
//   render() {
//     return <TouchableHighlight
//       style={{
//         width: '100%',
//         padding: 15,
//         backgroundColor: 'white',
//         flexDirection: 'row',
//         borderBottomWidth: 1,
//         borderColor: 'rgb(233,239,247)'
//       }}
//       {...this.props.sortHandlers}
//     >
//       <View
//         style={{
//           flexDirection: 'row',
//           alignItems: 'center',
//           flex: 1
//         }}
//       >
//         <View style={{ flex: 1, flexDirection: 'row' }}>
//           <TouchableOpacity
//             onPress={() => this.props.removeSkill(this.props.rowId)}
//             style={{ paddingRight: 10 }}
//           >
//             <Image
//               source={require('../../../assets/app_icons/Icons/Minus.png')}
//             />
//           </TouchableOpacity>
//           <Text style={profileStyles.fieldDarkLow}>{this.props.skill ? this.props.skill.skills : ''}</Text>
//         </View>
//         <View>
//           <Image
//             source={require('../../../assets/hamburger.png')}
//             style={{ height: 14, width: 14 }}
//           />
//         </View>
//       </View>
//     </TouchableHighlight>
//   }
// }
