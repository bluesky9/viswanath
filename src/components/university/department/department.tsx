import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Dimensions,
  FlatList,
  Platform,
  Linking
} from 'react-native';
const { width } = Dimensions.get('window');
import { Text, Spinner, Icon } from 'native-base';
import ImagePicker from 'react-native-image-crop-picker';
import { NavigationScreenProps } from 'react-navigation';
import AppButton from '../../layouts/buttons/button';
import universityStyles from '../university-styles';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import homeStyles from '../../home/home-styles';
import { Card, CardItem, Left, Body } from 'native-base';
import HomeImageIndicators from '../../home/home-image-indicators';

import profilePic from '../../../../assets/images/user_dummy_image.png';
import { transformForPosts } from '../../../utils';
import LinkPreview from 'react-native-link-preview';

import { get, find, isEmpty, findIndex, filter } from 'lodash';
import ActionSheet from 'react-native-actionsheet';
import { DialogModal, FeedApi, UniversitiesApi, PeopleApi } from '../../../store/actions'
import { departmentStyles } from './department-styles';

const userOptions = ['Cancel', 'Edit post', 'Delete post'];
const options = ['Cancel', 'Report post'];
const deptOptions = ['Cancel', 'Edit Department', 'Delete department']

const UNFOLLOW_POST = 'Do you want to leave this department'
const UNFOLLOW_MSG = 'You will no longer see the posts or activity in your feed.'

let actionsheet = undefined;
let userActionSheet = undefined;
let linkPreviewCount = 0;
let linkPreviewData = 0;
let deptActionSheet = undefined;

let postsUrl = [];

const days = ['MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY']

interface ILoginScreenNavParams { }
interface ILoginScreenProps
  extends NavigationScreenProps<ILoginScreenNavParams> {
  // uniProfileState: any;
  // actions: any;
  // followers: any;
  userId: number;
  dialogModal: {
    dialogShows: Function;
  }
  // universityFeeds;
  feedApi;
  // universityPeopleCount;

  department;
  universityActions;
  universityId;
  departmentPeopleCount;
  departmentPeople;
  universityDetails;
  followingUniversity;
  usertype;
  departmentFeeds;
  peopleApi;
}

type State = {
  headerStatus: number;
  followStatus: boolean;
  followModal: boolean;
  selectedPostId;
  refreshing: boolean;
  postPreview: any;
};

class Department extends Component<ILoginScreenProps, State> {
  constructor(props) {
    super(props);
    this.state = {
      headerStatus: 0,
      followStatus: false,
      followModal: false,
      selectedPostId: undefined,
      refreshing: false,
      postPreview: []
    };
    this.getLinkDetails = this.getLinkDetails.bind(this);
  }

  componentDidMount() {
    let univerityId = this.props.universityId ? [this.props.universityId] : []
    let userTypes = ['PROFESSOR', 'UNIVERSITY_STAFF'];
    let followDepartment = get(this.props.department, 'loggedInUserDepartmentFollowerList', undefined)
    if (followDepartment) userTypes.push('CURRENT_STUDENT', 'PROSPECTIVE_STUDENT', 'ALUMNI')
    let departmentPeople = {
      myUserId: this.props.userId,
      countOnly: true,
      userTypes: userTypes,
      univIds: univerityId,
      deptId: this.props.department.deptid
    }
    this.props.universityActions.getDepartmentPeople(departmentPeople)
    this.props.universityActions.getDepartmentFeeds(univerityId, this.props.department.deptid, 20, 0)
  }

  getLinkDetails(message, post, postIndex, setState = false) {
    let regex = /(http|https|Https|Http):\/\/([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?/
    if (regex.test(message)) {
      linkPreviewCount++;
      let url = message.match(regex)[0];
      LinkPreview.getPreview(url)
        .then(data => {
          console.log(data)
          post.preview = data;
          this.state.postPreview[postIndex] = data;
          linkPreviewData++;
          if (linkPreviewData === linkPreviewCount) {
            this.forceUpdate();
          }
          if (setState) {
            this.setState({
              postPreview: this.state.postPreview
            })
          }
        })
        .catch((error) => {
          console.log(error)
        });
    }
  }

  componentWillReceiveProps(nextProps) {
    let followDepartment = get(nextProps.department, 'loggedInUserDepartmentFollowerList', undefined)
    if (followDepartment && nextProps.departmentPeopleCount !== this.props.departmentPeopleCount) {
      let userTypes = ['CURRENT_STUDENT', 'PROFESSOR', 'UNIVERSITY_STAFF', 'ALUMNI', 'PROSPECTIVE_STUDENT'];
      let univerityId = this.props.universityId ? [this.props.universityId] : []
      let departmentPeople = {
        myUserId: this.props.userId,
        countOnly: true,
        userTypes: userTypes,
        univIds: univerityId,
        deptId: this.props.department.deptid
      }
      this.props.universityActions.getDepartmentPeople(departmentPeople)
    }
    if (this.props.departmentFeeds && nextProps.departmentFeeds && this.props.departmentFeeds.length + 1 === nextProps.departmentFeeds.length) {
      this.setState({ postPreview: [] })
    }
  }

  showActionSheet(userPost, postId) {
    this.setState({ selectedPostId: postId });
    if (userPost) userActionSheet.show();
    else actionsheet.show();
  }

  onDeletePost() {
    this.props.feedApi.deletePost(this.state.selectedPostId, 'DEPARTMENT_POST')
  }

  showImageModal = (images, index) => {
    this.props.dialogModal.dialogShows(
      'images',
      undefined,
      'image',
      true,
      undefined,
      undefined,
      undefined,
      undefined,
      images,
      index
    );
  };

  _handleUserActionSheetPressed = index => {
    if (index === 2) {
      this.props.dialogModal.dialogShows(
        'Delete post',
        'Do you want to delete the post',
        'action',
        true,
        () => this.onDeletePost()
      );
    } else if (index === 1) {
      this.props.navigation.navigate('UniversityPostEdit', {
        postId: this.state.selectedPostId,
        deptRequest: true,
        majors: get(this.props, 'department.major', []),
        deptId: get(this.props, 'department.deptid', undefined)
      })
    }
  };

  onDeleteDepartment = () => {
    this.props.universityActions.deleteDepartment(this.props.department.deptid, this.props.universityId)
  }

  _handleDeptActionSheet = index => {
    if (index === 2) {
      this.props.dialogModal.dialogShows(
        'Delete department',
        'Do you want to delete the department',
        'action',
        true,
        () => this.onDeleteDepartment()
      );
    } else if (index === 1) {
      this.props.navigation.navigate('DepartmentCreate', { department: this.props.department })
      console.log('Edit Department')
      // this.props.navigation.navigate('UniversityPostEdit', {
      //   postId: this.state.selectedPostId,
      //   deptRequest: true,
      //   majors: get(this.props, 'department.major', []),
      //   deptId: get(this.props, 'department.deptid', undefined)
      // })
    }
  };

  _handleActionSheetPressed = index => {
    if (index === 1) {
      this.props.dialogModal.dialogShows(
        undefined,
        undefined,
        'report',
        true,
        reason => this.onReportPost(reason)
      );
      // this.props.feedApi.getPosts(1, 20, 0);
    }
  };

  onReportPost = reason => {
    let data = {
      postId: this.state.selectedPostId,
      reason: reason
    };
    this.props.universityActions.reportPost(data);
  };

  openCamera() {
    ImagePicker.openCamera({
      cropping: false,
      compressImageQuality: Platform.OS === 'ios' ? 0 : 0.5
    })
      .then(image => {
        let imageObj = {
          uri: get(image, 'path', ''),
          name: new Date().getTime().toString(),
          type: get(image, 'mime', '')
        }
        this.props.navigation.navigate('UniversityPostCompose', { image: imageObj, departmentPostCompose: this.props.department })
        // this.props.navigation.navigate('UniversityPostCompose', { image: imageObj })
      })
      .catch((e) => {
        console.log(e, 'Error')
      });
  }

  renderDays = (newObj) => {
    return (
      <View style={departmentStyles.timingBlock}>
        <Text style={departmentStyles.timing}>{newObj.dayOfWeek}</Text>
        {newObj.closed ?
          <Text style={departmentStyles.timing}>Closed</Text>
          : <Text style={departmentStyles.timing}>{newObj.startTime} - {newObj.endTime}</Text>
        }
      </View>
    )
  }

  renderAdmins = (newObj) => {
    return (
      <View style={{ flexDirection: 'row', paddingTop: 13 }}>
        <Image source={{
          uri: get(newObj, 'user.profPicPath', undefined) ?
            newObj.user.profPicPath : 'https://252radio.com/wp-content/uploads/2016/11/default-user-image-300x300.png'
        }}
          style={departmentStyles.profilePic}
        />
        <View style={{ marginLeft: 9 }}>
          <Text style={departmentStyles.name}>{get(newObj, 'user.name', '')}</Text>
          {/* <Text style={departmentStyles.designation}>Owner</Text> */}
        </View>
      </View>
    )
  }

  renderAboutSection() {
    let programs = get(this.props.department, 'programs', [])
    let admins = get(this.props.department, 'adminProfileIds', [])
    let hoursOfOperations = get(this.props.department, 'hoursOfOperations', [])
    let location = get(this.props.department, 'mapAddress', '')
    let userProfile = get(this.props.department, 'profile', {})
    let adminIndex = findIndex(admins, admin => admin.id === userProfile.id)
    if (!isEmpty(userProfile) && (adminIndex === -1)) {
      admins.push(userProfile)
    }
    console.log(admins, '321')
    let timings = [];
    if (!isEmpty(hoursOfOperations)) {
      days.map((day) => {
        let newDayIndex = findIndex(hoursOfOperations, item => item.dayOfWeek === day)
        if (newDayIndex === -1) {
          let newDayObj = {
            dayOfWeek: day,
            closed: true
          }
          timings.push(newDayObj)
        } else {
          let newDayObj = {
            dayOfWeek: day,
            closed: false,
            startTime: hoursOfOperations[newDayIndex].startTime,
            endTime: hoursOfOperations[newDayIndex].endTime
          }
          timings.push(newDayObj)
        }
      })
    }
    return (
      <View style={{ margin: 15 }}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <Text
            style={[
              universityStyles.countryText,
              { lineHeight: 18, letterSpacing: 0.2, marginHorizontal: 0 }
            ]}
          >
            {get(this.props.department, 'deptdescription', '')}
          </Text>
          {!isEmpty(admins) && <View>
            <Text style={departmentStyles.subTitle}>OWNER/ADMINISTRATORS</Text>
            <FlatList
              data={admins}
              renderItem={obj => this.renderAdmins(obj.item)}
              keyExtractor={(item, index) => item.id + index.toString()}
              extraData={this.props}
            />
          </View>}
          {!isEmpty(location) &&
            <View>
              <Text style={departmentStyles.subTitle}>LOCATION</Text>
              <Text style={departmentStyles.location}>
                {get(this.props.department, 'mapAddress', '')}
              </Text>
            </View>
          }
          {!isEmpty(hoursOfOperations) && <View>
            <Text style={[departmentStyles.subTitle, { marginBottom: 11 }]}>HOURS/TIMINGS</Text>
            <FlatList
              data={timings}
              renderItem={obj => this.renderDays(obj.item)}
              keyExtractor={(item, index) => item.dayOfWeek + index.toString()}
              extraData={this.props}
            />
          </View>
          }
          <View style={departmentStyles.hideDivider} />
          {!isEmpty(programs) &&
            <View>
              <Text style={[departmentStyles.subTitle, { marginBottom: 11 }]}>RELEVANT TAGS</Text>
              <View style={{ paddingTop: 13, flexDirection: 'row', flexWrap: 'wrap' }}>
                {
                  programs.map((item, index) => {
                    return (
                      <View style={departmentStyles.tagsBlock} key={index}>
                        <Text style={departmentStyles.tags}>{item}</Text>
                      </View>
                    )
                  })
                }
              </View>
            </View>
          }
        </ScrollView>
      </View>
    );
  }

  renderPosts(item, postIndex) {
    let m = transformForPosts(item.postTimeStamp);
    if (postsUrl[postIndex] && postsUrl[postIndex] !== item.postMessages) {
      postsUrl[postIndex] = item.postMessages;
      this.getLinkDetails(item.postMessages, item, postIndex, true);
    } else if (!this.state.postPreview[postIndex]) {
      postsUrl[postIndex] = item.postMessages;
      this.getLinkDetails(item.postMessages, item, postIndex);
    }
    let followingDepartment = !isEmpty(get(this.props.department, 'loggedInUserDepartmentFollowerList', undefined))
    return (
      <View style={{ overflow: 'hidden', width: width }}>
        <ActionSheet
          ref={o => (userActionSheet = o)}
          options={userOptions}
          cancelButtonIndex={0}
          destructiveButtonIndex={2}
          onPress={i => this._handleUserActionSheetPressed(i)}
        />
        {Platform.OS === 'ios' ? (
          <View style={{ alignSelf: 'stretch', backgroundColor: 'white' }} />
        ) : (
            undefined
          )}
        <ActionSheet
          ref={o => (actionsheet = o)}
          options={options}
          cancelButtonIndex={0}
          destructiveButtonIndex={1}
          onPress={i => this._handleActionSheetPressed(i)}
        />
        {Platform.OS === 'ios' ? (
          <View style={{ alignSelf: 'stretch', backgroundColor: 'white' }} />
        ) : (
            undefined
          )}
        <Card style={{ transform: [{ scaleX: 1.02 }] }}>
          <CardItem
            style={{ paddingTop: 16, paddingBottom: 0 }}
          >
            {item.title ?
              <Text style={homeStyles.title}>{item.title}</Text> : undefined
            }
          </CardItem>
          <CardItem
            button
            onPress={() =>
              this.props.navigation.navigate('DepartmentPostWithComments', {
                postItem: item
              })
            }
          >
            <Left>
              <TouchableOpacity
                onPress={() => {
                  if (item.user.id === this.props.userId)
                    this.props.navigation.navigate('MyProfile');
                  else
                    this.props.peopleApi.getPeopleInfo(item.user.id);
                }}
              >
                <Image
                  source={
                    item.user.profPicPath
                      ? { uri: item.user.profPicPath }
                      : profilePic
                  }
                  style={[homeStyles.personThumbnail, { marginHorizontal: 0 }]}
                />
              </TouchableOpacity>
              <Body>
                <Text>
                  <Text
                    style={[homeStyles.newPostItemLow, { marginVertical: 2 }]}
                    onPress={() => {
                      if (item.user.id === this.props.userId)
                        this.props.navigation.navigate('MyProfile');
                      else
                        this.props.peopleApi.getPeopleInfo(item.user.id);
                    }}
                  >
                    {item.user.name}{' '}
                  </Text>
                  <Text style={[homeStyles.newPostItemLow, { marginVertical: 2 }]}>
                    {item.university && ' • ' + item.university.abbr}
                    {item.department && ' - ' + item.department.deptAbbr}
                  </Text>
                </Text>
                <Text note style={homeStyles.postNoteText}>
                  {m}
                </Text>
              </Body>
            </Left>
          </CardItem>
          <CardItem
            button
            onPress={() =>
              this.props.navigation.navigate('DepartmentPostWithComments', {
                postItem: item
              })
            }
          >
            <Text
              style={[
                homeStyles.postMessage,
                { marginBottom: 6, alignSelf: 'center' }
              ]}
            >
              {item.postMessages}
            </Text>
          </CardItem>
          {this.state.postPreview[postIndex] &&
            <View style={homeStyles.linkContainer}>
              <View style={{ height: 60, width: 60, overflow: 'hidden', marginRight: 15, alignItems: 'center' }}>
                {this.state.postPreview[postIndex].images.length > 0 && <Image source={{ uri: this.state.postPreview[postIndex].images[0] }}
                  style={homeStyles.linkImage} />}
              </View>
              <View style={{ flex: 1, flexWrap: 'wrap' }}>
                <Text style={homeStyles.linkContainerTitle}>{get(this.state.postPreview[postIndex], 'title', 'NA')}</Text>
                <Text style={homeStyles.linkContainerDescription}>{get(this.state.postPreview[postIndex], 'description', 'NA')}</Text>
              </View>
            </View>
          }
          {item.files &&
            item.files.length > 0 && (
              <HomeImageIndicators
                images={item.files}
                imageModal={this.showImageModal}
              />
            )}
          <CardItem style={{ height: 56, justifyContent: 'space-between' }}>
            <View style={{ flexDirection: 'row' }}>
              <TouchableOpacity
                onPress={() => {
                  this.props.universityActions.universityPostLike(item.postid, 'Department');
                }}
                disabled={!followingDepartment}
              >
                {get(item, 'likes', []).length > 0 &&
                  find(item.likes, likedObj => likedObj.user.id === this.props.userId) ? (
                    <Icon name='md-heart' style={{ color: 'rgb(255, 97, 116)', fontSize: 20, width: 20 }} />
                  ) : (
                    <Icon name='md-heart-outline' style={{ color: 'rgb(207, 208, 220)', fontSize: 20, width: 20 }} />
                  )}
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate('PostLikes', {
                    likes: item.likes
                  })
                }
              >
                <Text style={homeStyles.postBarLikesText}>
                  {item.likes.length > 0 && item.likes.length} Likes
                </Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('DepartmentPostWithComments', {
                  postItem: item
                });
              }}
              style={{ flexDirection: 'row' }}
            >
              <Icon name='md-text' style={{ color: 'rgb(207, 208, 220)', fontSize: 20, width: 20 }} />
              <Text style={[homeStyles.postBarComText]}>
                {item.comment.length > 0 && item.comment.length} Comments
              </Text>
            </TouchableOpacity>
            <View style={{ flexDirection: 'row' }}>
              <Icon name='md-share' style={{ color: 'rgb(207, 208, 220)', fontSize: 20, width: 20 }} />
              <Text style={[homeStyles.postBarComText]}>
                {item.share_post ? 1 : 0} Share
              </Text>
            </View>
            <TouchableOpacity
              style={{ flexDirection: 'row' }}
              onPress={() =>
                this.showActionSheet(item.user.id === this.props.userId ? true : false, item.postid)
              }
            >
              <Icon name='ios-more' style={{ color: 'rgb(207, 208, 220)', width: 20 }} />
            </TouchableOpacity>
          </CardItem>
        </Card>
      </View>
    );
  }
  renderPostList() {
    let followingDepartment = !isEmpty(get(this.props.department, 'loggedInUserDepartmentFollowerList', undefined))
    return (
      <View>
        {followingDepartment &&
          <View style={universityStyles.postBoxBut}>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('UniversityPostCompose', { departmentPostCompose: this.props.department })
              }
            >
              <Text
                style={[
                  homeStyles.homePageSearchBar,
                  { color: 'rgba(108,111,125,0.5)' }
                ]}
              >
                Write a department post..
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() =>
                this.openCamera()
              }
              style={{ justifyContent: 'center' }}
            >
              <Image
                source={require('../../../../assets/app_icons/Icons/Camera-grey.png')}
                style={{ alignSelf: 'center', width: 20, height: 20 }}
              />
            </TouchableOpacity>
          </View>
        }
        <FlatList
          contentContainerStyle={{ paddingBottom: 70 }}
          data={this.props.departmentFeeds}
          renderItem={obj => this.renderPosts(obj.item, obj.index)}
          keyExtractor={(item, index) => item.postid.toString() + index}
          showsVerticalScrollIndicator={false}
          refreshing={this.state.refreshing}
          onRefresh={() => this.props.universityActions.getDepartmentFeeds(this.props.universityId, this.props.department.deptid, 20, 0)}
        />
      </View>
    )
  }

  onJoinPress = (deptid, deptAbbr, major) => {
    console.log(major, '123')
    if (!isEmpty(major)) {
      if (!(this.props.usertype === ('CURRENT_STUDENT') || (this.props.usertype === 'PROSPECTIVE_STUDENT')))
        this.props.dialogModal.dialogShows(deptAbbr, major, 'followDepartment', true, (majorId, degree) => this.join(deptid, majorId, degree))
      else {
        this.props.universityActions.followDepartment(deptid, major[0].majorid)
      }
    } else {
      this.props.universityActions.followDepartment(deptid)
    }
  }

  join = (deptid, majorId, degree) => {
    console.log(deptid, majorId, degree, 'Degree');
    this.props.universityActions.followDepartment(deptid, majorId, degree);
  }

  onMemberPress = (obj) => {
    let deptId = get(obj, 'deptid', undefined)
    if (deptId) {
      this.props.dialogModal.dialogShows(
        UNFOLLOW_POST, UNFOLLOW_MSG, 'action', true, () => this.props.universityActions.unfollowDepartment(deptId)
      )
    }
  }

  render() {
    let department = this.props.department
    let major = get(department, 'major', [])
    let followingDepartment = !isEmpty(get(department, 'loggedInUserDepartmentFollowerList', undefined))
    let webLink = get(this.props.department, 'deptweblink', '')
    let majorLength = 0;
    let adminUserIds = get(this.props.department, 'adminUserIds', [])
    let showEditOption = false
    let userProfile = get(department, 'profile', [])
    if (!isEmpty(adminUserIds) || !isEmpty(userProfile)) {
      showEditOption = find(adminUserIds, admin => admin.id === this.props.userId) || userProfile.id === this.props.userId
    }
    if (!isEmpty(major))
      majorLength = filter(major, obj => !obj.deleted).length
    if (this.props.universityDetails.details === undefined) {
      return (
        <View
          style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
        >
          <Spinner />
        </View>
      );
    } else if (!isEmpty(this.props.department)) {
      return (
        <View style={{ flex: 1 }}>
          <ActionSheet
            ref={o => (deptActionSheet = o)}
            options={deptOptions}
            cancelButtonIndex={0}
            destructiveButtonIndex={2}
            onPress={i => this._handleDeptActionSheet(i)}
          />
          <View style={universityStyles.headerContainer}>
            <View style={universityStyles.leftIconContainer}>
              <TouchableOpacity onPress={() => {
                // this.props.actions.clearUniversityData()
                this.props.navigation.goBack()
              }}>
                <Icon name='md-arrow-back' style={{ color: 'rgb(197, 207, 219)' }} />
              </TouchableOpacity>
            </View>
            <View style={universityStyles.centerHeaderContainer}>
              <Image
                source={{
                  uri: department && department.deptlogo
                    ? department.deptlogo.replace('http://', 'https://')
                    : 'https://252radio.com/wp-content/uploads/2016/11/default-user-image-300x300.png'
                }}
                style={universityStyles.logoStyle}
              />
              <Text style={universityStyles.headerText}>
                {department.deptname}
              </Text>
            </View>
            <View style={universityStyles.rightIconContainer}>
              {showEditOption &&
                <TouchableOpacity
                  onPress={() => {
                    if (deptActionSheet) deptActionSheet.show()
                  }
                  }
                >
                  <Icon name='ios-more' style={{ color: 'rgb(197, 207, 219)' }} />
                </TouchableOpacity>
              }
            </View>
          </View>
          <View
            style={{
              alignSelf: 'stretch',
              flexDirection: 'row',
              backgroundColor: 'white'
            }}
          >
            <View style={universityStyles.picContainer}>
              <Image
                source={{
                  uri: department && department.deptlogo
                    ? department.deptlogo.replace('http://', 'https://')
                    : 'https://252radio.com/wp-content/uploads/2016/11/default-user-image-300x300.png'
                }}
                style={{ height: 68, width: 68, resizeMode: 'contain' }}
              />
            </View>
            <View style={universityStyles.mainTextContainer}>
              <Text style={[universityStyles.nameText, { paddingHorizontal: 6, letterSpacing: 0.2 }]}>
                {department.deptname}
              </Text>
              {/* <View
                style={{
                  flexDirection: 'row',
                  marginTop: 7,
                  alignItems: 'center'
                }}
              >
                <Icon
                  name={'pin'}
                  style={{ color: 'rgb(207,208,220)', fontSize: 23, top: 2 }}
                />
                <Text style={universityStyles.countryText}>
                  {this.props.uniProfileState.universityDetails.details.country}
                </Text>
              </View> */}
              <Text style={[universityStyles.countryText, { marginVertical: 5, fontSize: 14, lineHeight: 17, letterSpacing: 0.2 }]}>
                {department.deptType}
              </Text>
              {!isEmpty(webLink) &&
                <TouchableOpacity
                  style={{ paddingTop: 5, flexDirection: 'row', paddingHorizontal: 6 }}
                  onPress={() => { Linking.openURL(webLink) }}
                >
                  <Icon name='md-link' style={departmentStyles.linkIcon} />
                  <Text style={departmentStyles.linkText}>
                    {get(this.props.department, 'deptweblink', '')}
                  </Text>
                </TouchableOpacity>
              }
            </View>
          </View>
          <View style={universityStyles.statsContainer}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('UniversityPeople', { department: this.props.department })}
              style={universityStyles.statOneContainer}
            >
              <Text style={universityStyles.statNmbrText}>
                {this.props.departmentPeopleCount}
              </Text>
              <Text style={universityStyles.statNameText}>People</Text>
            </TouchableOpacity>
            {department.deptType === 'academic' &&
              <TouchableOpacity
                style={universityStyles.statOneContainer}
                onPress={() => this.props.navigation.navigate('DepartmentMajor', {
                  departmentId: this.props.department.deptid,
                  showEditOption
                })}
              >
                <Text style={universityStyles.statNmbrText}>
                  {majorLength}
                </Text>
                <Text style={universityStyles.statNameText}>Majors</Text>
              </TouchableOpacity>
            }
            <View
              style={[
                universityStyles.statOneContainer,
                { flex: 2, alignItems: 'flex-end', paddingRight: 15 }
              ]}
            >
              <AppButton
                type={followingDepartment ? 'inverted' : 'primary'}
                bordered={true}
                label={followingDepartment ? 'Member' : 'Join'}
                containerWidth={'45%'}
                customButtonStyle={{ paddingVertical: 8 }}
                disabled={!this.props.followingUniversity}
                onPress={() => followingDepartment ?
                  this.onMemberPress(department)
                  : this.onJoinPress(department.deptid, department.deptAbbr, major)
                }
              />
            </View>
          </View>
          <View style={universityStyles.tabContainer}>
            <TouchableOpacity
              onPress={() =>
                this.setState({
                  headerStatus: 0
                })
              }
              style={[
                universityStyles.tabBut,
                {
                  borderBottomWidth: this.state.headerStatus === 0 ? 3 : 1,
                  borderBottomColor:
                    this.state.headerStatus === 0
                      ? 'rgb(204,20,51)'
                      : 'rgb(233,238,241)'
                }
              ]}
            >
              <Text
                style={[
                  universityStyles.dualHeaderLeftText,
                  {
                    color:
                      this.state.headerStatus === 0
                        ? 'rgb(204,20,51)'
                        : 'rgb(173,181,196)'
                  }
                ]}
              >
                ABOUT
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() =>
                this.setState({
                  headerStatus: 1
                })
              }
              style={[
                universityStyles.tabBut,
                {
                  borderBottomWidth: this.state.headerStatus === 1 ? 3 : 1,
                  borderBottomColor:
                    this.state.headerStatus === 1
                      ? 'rgb(204,20,51)'
                      : 'rgb(233,238,241)'
                }
              ]}
            >
              <Text
                style={[
                  universityStyles.dualHeaderLeftText,
                  {
                    color:
                      this.state.headerStatus === 1
                        ? 'rgb(204,20,51)'
                        : 'rgb(173,181,196)'
                  }
                ]}
              >
                POSTS
              </Text>
            </TouchableOpacity>
          </View>
          {/* <FollowModal
            onClose={() => this.setState({ followModal: false })}
            isShown={this.state.followModal}
            onSubmit={(data) =>
              this.props.actions.followUniversity(get(this.props.uniProfileState, 'universityDetails.details.univ_id', ''), data).then(() => {
                this.setState({
                  followModal: false
                })
              })
            }
          /> */}
          <View style={{ flex: 1, backgroundColor: '#fff', paddingVertical: 10 }}>
            {this.state.headerStatus === 0
              ? this.renderAboutSection()
              : this.renderPostList()
            }
          </View>
        </View>
      );
    } else {
      return <View />
    }
  }
}

const mapStateToProps = ({ universityDetails, userInfoAPI }, navState) => {
  let navParams = navState.navigation.state.params.id
  return {
    universityDetails: universityDetails,
    userId: userInfoAPI.userId,

    department: find(universityDetails.departments.content, dept => dept.deptid === navParams),
    universityId: get(universityDetails, 'details.univ_id', undefined),
    departmentPeople: get(universityDetails, 'departmentPeople', []),
    departmentPeopleCount: get(universityDetails, 'departmentPeopleCount', 0),
    followingUniversity: get(universityDetails, 'followingUniversity', false),
    usertype: get(universityDetails, 'usertype', undefined),
    departmentFeeds: get(universityDetails, 'departmentFeeds', [])
  };
};

const mapDispatchToProps = dispatch => ({
  dialogModal: bindActionCreators(DialogModal, dispatch),
  feedApi: bindActionCreators(FeedApi, dispatch),
  universityActions: bindActionCreators(UniversitiesApi, dispatch),
  peopleApi: bindActionCreators(PeopleApi, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Department);
