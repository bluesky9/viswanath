import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import { Icon } from 'native-base';
import { departmentMajorsListStyles } from './department-styles';
import { KeyboardAwareFlatList } from 'react-native-keyboard-aware-scroll-view';
import MSAHeader from '../../layouts/header/header';
import { isEmpty } from 'lodash'

type Props = {
  setDraftState: any;
  draftState: any;
  onNext: Function;
  onPrev: Function;
  navigation: any;
}

type State = {
  selectedMajorIndex;
};

export default class DepartmentCreateMajorsList extends Component<Props, State> {

  constructor(props) {
    super(props);
    this.state = {
      selectedMajorIndex: undefined
    }
  }

  handleBackPress() {
    this.props.onPrev();
  }

  onNext = () => {
    this.props.onNext()
  }

  renderMajor = (index, item) => {
    console.log(item, '122')
    return (
      <View style={departmentMajorsListStyles.contentContainer}>
        <View style={departmentMajorsListStyles.row}>
          <Text style={departmentMajorsListStyles.darkText}>{(item.majorname ? item.majorname : item.majorName) }</Text>
          <TouchableOpacity
            onPress={() => {
                this.setState({ selectedMajorIndex: index})
                this.props.navigation.navigate('DepartmentCreateNewMajor', {
                  type: 'edit',
                  updateMajor: this.updateMajor,
                  majorData: item,
                  deleteMajor: this.deleteMajor
                })
              }
            }
          >
            <Icon name='md-create' style={departmentMajorsListStyles.editIcon} />
          </TouchableOpacity>
        </View>
        <View style={departmentMajorsListStyles.listBlock}>
        { item.levelOfStudyOffered.map((obj, key) => {
          return (
            <View style={departmentMajorsListStyles.list} key={key}>
              <Text style={departmentMajorsListStyles.listName}>{obj.levelOfStudy.replace('_', ' ')}</Text>
            </View>
          )
          })
        }
        </View>
      </View>
    )
  }

  updateMajor = (data) => {
    let majors = this.props.draftState.majorList;
    majors[this.state.selectedMajorIndex] = data
    this.props.setDraftState({ majorList: majors })
    this.setState({ selectedMajorIndex: undefined })
  }

  getMajor = (data) => {
    let majors = this.props.draftState.majorList;
    majors.push(data);
    this.props.setDraftState({ majorList: majors })
  }

  deleteMajor = () => {
    console.log('called', this.state.selectedMajorIndex)
    let majors = this.props.draftState.majorList;
    majors.splice(this.state.selectedMajorIndex, 1)
    this.props.setDraftState({ majorList: majors })
    this.setState({ selectedMajorIndex: undefined })
  }

  render() {
    console.log(this.props.draftState.majorList, '123')
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <MSAHeader
          title={'Create Department'}
          leftText={'BACK'}
          rightText={'NEXT'}
          onPress={this.handleBackPress.bind(this)}
          onRightTextPress={this.onNext}
          disableRightText={isEmpty(this.props.draftState.majorList)}
        />
        <View style={departmentMajorsListStyles.container}>
          <Text style={departmentMajorsListStyles.darkText}>New Major</Text>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('DepartmentCreateNewMajor', {
              type: 'add',
              getMajor: this.getMajor
            })}
          >
            <Text style={departmentMajorsListStyles.blueBtn}>ADD</Text>
          </TouchableOpacity>
        </View>
        { !isEmpty(this.props.draftState.majorList) &&
          <KeyboardAwareFlatList
            keyExtractor={item => item.majorname ? item.majorname : item.majorName}
            data={this.props.draftState.majorList}
            renderItem={({ index, item }) => this.renderMajor(index, item)}
            extraData={this.props}
          />
        }
      </View>
    )
  }
}