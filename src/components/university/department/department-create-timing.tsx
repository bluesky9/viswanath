import React, { Component } from 'react';
import {
  View,
  Text,
  Platform,
  Switch,
  FlatList
} from 'react-native';
import DatePicker from 'react-native-datepicker'
import { NavigationScreenProps } from 'react-navigation';
import profileStyles from '../../profile/profile-styles';
import { departmentCreateNewMajorStyles } from './department-styles';
import MSAHeader from '../../layouts/header/header';

import { style } from '../../../styles/variables';

import { isEmpty, get, findIndex } from 'lodash';

import { connect } from 'react-redux';

const days = ['MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY']

interface ILoginScreenNavParams { }
interface ILoginScreenProps
  extends NavigationScreenProps<ILoginScreenNavParams> {
}

type State = {
  hoursOfOperationList;
};

class DepartmentCreateTiming extends Component<ILoginScreenProps, State> {

  constructor(props) {
    super(props);
    this.state = {
      hoursOfOperationList: []
    }
  }

  handleBackPress() {
    this.props.navigation.goBack()
  }

  componentDidMount() {
    console.log(this.props.navigation)
    let data = get(this.props.navigation, 'state.params.data', [])
    console.log(data, 'data')
    let hoursOfOperationList = this.state.hoursOfOperationList
    if (isEmpty(data)) {
      days.map((item) => {
        let dayObj = {
          dayOfWeek: item,
          endTime: '12:30 am',
          startTime: '12:00 am',
          closed: false
        }
        hoursOfOperationList.push(dayObj)
      })
    } else {
      days.map((item) => {
        let findDay = findIndex(data, dataObj => dataObj.dayOfWeek === item)
        if (findDay !== -1) {
          let dayObj = {
            dayOfWeek: item,
            endTime: data[findDay].endTime,
            startTime: data[findDay].startTime,
            closed: false
          }
          hoursOfOperationList.push(dayObj)
        } else {
          let dayObj = {
            dayOfWeek: item,
            endTime: '12:30 am',
            startTime: '12:00 am',
            closed: true
          }
          hoursOfOperationList.push(dayObj)
        }
      })
    }
    this.setState({ hoursOfOperationList })
  }

  onDonePressed = () => {
    let hoursOfOperationList = this.state.hoursOfOperationList
    let newList = []
    hoursOfOperationList.map((item) => {
      if (!item.closed) {
        let newObj = {
          dayOfWeek: item.dayOfWeek,
          endTime: item.endTime,
          startTime: item.startTime
        }
        newList.push(newObj)
      }
    })
    let setTime = get(this.props.navigation, 'state.params.setTime', undefined)
    if (setTime) setTime(newList);
    this.props.navigation.goBack()
  }

  renderDays = (item, index) => {
    return (
      <View>
        <View style={departmentCreateNewMajorStyles.titleContainer}>
          <Text style={departmentCreateNewMajorStyles.title}>{item.dayOfWeek}</Text>
        </View>
        <View
          style={[
            profileStyles.fieldBox,
            { flexDirection: 'row', borderBottomWidth: 0, paddingBottom: 9 }
          ]}
        >
          <View
            style={{
              justifyContent: 'flex-start',
              alignItems: 'flex-start',
              flex: 1
            }}
          >
            <Text
              style={[profileStyles.dualHeaderLeftText, { marginBottom: 9, fontSize: 12, lineHeight: 14 }]}
            >
              FROM
              </Text>
            <DatePicker
              style={{ width: 80 }}
              date={item.startTime}
              mode={'time'}
              format={'hh:mm a'}
              confirmBtnText={'Confirm'}
              cancelBtnText={'Cancel'}
              showIcon={false}
              onDateChange={(date) => {
                let hoursOfOperationList = this.state.hoursOfOperationList
                hoursOfOperationList[index].startTime = date
                this.setState({hoursOfOperationList})
              }}
              customStyles={{
                dateInput: {
                  height: 28,
                  // width: 80,
                  borderColor: 'rgb(53,135,230)',
                  borderRadius: 3
                },
                dateText: { color: 'rgb(53,135,230)', fontSize: 14, lineHeight: 18 }
              }}
              disabled = {item.closed}
            />
          </View>
          <View
            style={{
              justifyContent: 'flex-start',
              alignItems: 'flex-start',
              flex: 1
            }}
          >
            <Text
              style={[profileStyles.dualHeaderLeftText, { marginBottom: 9, fontSize: 12, lineHeight: 14 }]}
            >
              TO
              </Text>
            {/* {this.state.education.present ? */}
              {/* <View style={{ height: 40, justifyContent: 'center' }}>
                <Text style={[profileStyles.presentText, { fontSize: 14 }]}>Present</Text>
              </View> */}
              {/* :  */}
              <DatePicker
                style={{ width: 80 }}
                format={'hh:mm a'}
                mode={'time'}
                date={item.endTime}
                confirmBtnText={'Confirm'}
                cancelBtnText={'Cancel'}
                showIcon={false}
                onDateChange={(date) => {
                  let hoursOfOperationList = this.state.hoursOfOperationList
                  hoursOfOperationList[index].endTime = date
                  this.setState({hoursOfOperationList})
                }}
                customStyles={{
                  dateInput: {
                    height: 28,
                    // width: 80,
                    borderColor: 'rgb(53,135,230)',
                    borderRadius: 3
                  },
                  dateText: { color: 'rgb(53,135,230)', fontSize: 14, lineHeight: 18 }
                }}
                disabled = {item.closed}
              />
            {/* } */}
          </View>
        </View>
        <View style={profileStyles.presentBtnContainer}>
          <View style={{ flex: 1 }} />
          <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start' }}>
            <Switch
              onValueChange={(bool) => {
                let hoursOfOperationList = this.state.hoursOfOperationList;
                hoursOfOperationList[index].closed = bool;
                this.setState({ hoursOfOperationList })
              }}
              value={item.closed}
              thumbTintColor={style.color.primary}
              onTintColor={style.color.cream}
              style={{
                transform: [{ scaleX: Platform.OS === 'ios' ? .7 : 1 },
                { scaleY: Platform.OS === 'ios' ? .7 : 1 }]
              }}
            />
            <Text style={profileStyles.presentText}>Closed</Text>
          </View>
        </View>
      </View>
    )
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <MSAHeader
          title={'Add Hours/Timings'}
          leftText={'BACK'}
          rightText={'DONE'}
          onPress={this.handleBackPress.bind(this)}
          onRightTextPress={this.onDonePressed}
        />
        { !isEmpty(this.state.hoursOfOperationList) &&
          <FlatList
            data={this.state.hoursOfOperationList}
            renderItem={obj => this.renderDays(obj.item, obj.index)}
            keyExtractor={(item, index) => item.dayOfWeek + index}
            showsVerticalScrollIndicator={false}
            extraData={this.state}
          />
        }
      </View>
    )
  }
}

const mapStateToProps = ({ universityDetails }) => {
  return {
    usertype: universityDetails.usertype
  };
};

export default connect(mapStateToProps, undefined)(DepartmentCreateTiming);