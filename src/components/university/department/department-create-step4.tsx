import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import { Icon } from 'native-base';
import MSAHeader from '../../layouts/header/header';
import settingsStyles from '../../settings/settings-styles';
import { style } from '../../../styles/variables';

import { isEmpty, map } from 'lodash';

type Props = {
  onNext: Function;
  onPrev: Function;
  navigation: any;
  setDraftState: any;
  draftState: any;
}

type State = {
};

export default class DepartmentCreateStep3 extends Component<Props, State> {

  handleBackPress() {
    this.props.onPrev();
  }

  onNext = () => {
    this.props.onNext()
  }

  setLocation = (location) => {
    this.props.setDraftState({ location })
  }

  setTime = (time) => {
    if (!isEmpty(time)) this.props.setDraftState({  hoursOfOperationList : time })
  }

  tagAdmin = (ids) => {
     this.props.setDraftState({  adminUserIds : ids })
  }

  addTags = (tags) => {
    this.props.setDraftState({  programList : tags })
  }

  render() {
    let programList = '';
    if (!isEmpty(this.props.draftState.programList)) {
      map(this.props.draftState.programList, (program, index) => {
        if (index !== this.props.draftState.programList.length - 1)
          programList = programList + program + ', '
        else programList = programList + program
      });
    }
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <MSAHeader
          title={'Create Department'}
          leftText={'BACK'}
          onPress={this.handleBackPress.bind(this)}
          rightText={'NEXT'}
          onRightTextPress={this.onNext}
        />
        <TouchableOpacity>
          <View style={settingsStyles.optionListBlock}>
            <Text style={settingsStyles.settingListItem}>Academic Department - Optional fields</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('DepartmentCreateAddAdministrator', {
            tagAdmin: this.tagAdmin,
            adminIds: this.props.draftState.adminUserIds
          })}
        >
          <View style={settingsStyles.optionListBlock}>
            <Text style={settingsStyles.settingListItem}>SELECT ADMINISTRATORS</Text>
            <Icon name='ios-arrow-forward' style={{fontSize: 20, color: '#adb5c4'}} />
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('DepartmentCreateSelectLocation', {
            setLocation: this.setLocation,
            location: this.props.draftState.location
          })}
        >
          <View style={{ borderBottomWidth: 1, borderColor: 'rgb(233, 239, 247)', paddingBottom: 8 }}>
            <View style={[settingsStyles.optionListBlock, { borderBottomWidth: 0, paddingTop: 18, paddingBottom: 10 }]}>
              <Text style={settingsStyles.settingListItem}>ADD ADDRESS/LOCATION</Text>
              <Icon name='ios-arrow-forward' style={{fontSize: 20, color: '#adb5c4'}} />
            </View>
            { this.props.draftState.location.address !== '' &&
              <Text style={{ color: style.color.primary, paddingHorizontal: 15 }}>{this.props.draftState.location.address}</Text>
            }
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('DepartmentCreateTiming', {
            setTime: this.setTime,
            data: this.props.draftState.hoursOfOperationList
          })}
          >
          <View style={settingsStyles.optionListBlock}>
            <Text style={settingsStyles.settingListItem}>
              ADD TIMING/HOURS
            </Text>
            <Icon name='ios-arrow-forward' style={{fontSize: 20, color: '#adb5c4'}} />
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('DepartmentCreateSearchableTags', {
            addTags: this.addTags,
            tags: this.props.draftState.programList
          })}
        >
          <View style={{ borderBottomWidth: 1, borderColor: 'rgb(233, 239, 247)', paddingBottom: 8 }}>
            <View style={[settingsStyles.optionListBlock, { borderBottomWidth: 0, paddingTop: 18, paddingBottom: 10 }]}>
              <Text style={settingsStyles.settingListItem}>ADD SEARCHABLE TAGS</Text>
              <Icon name='ios-arrow-forward' style={{fontSize: 20, color: '#adb5c4'}} />
            </View>
            { programList !== '' &&
              <Text style={{ color: style.color.primary, paddingHorizontal: 15 }}>{programList}</Text>
            }
          </View>
        </TouchableOpacity>
      </View>
    )
  }
}