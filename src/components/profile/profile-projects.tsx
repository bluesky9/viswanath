import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
import { Icon } from 'native-base';
import MSAHeader from '../layouts/header/header';
import { get, map, findIndex, cloneDeep, isEmpty } from 'lodash';
import { DialogModal } from '../../store/actions';
import moment from 'moment';
import profileStyles from './profile-styles';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { transformDate } from '../../utils';

const DELETE_PROJECT_TITLE = 'Delete Project'
const DELETE_PROJECT_MSG = 'The project will be deleted permanently'

interface State {
  projects: any[]
}
interface IPeopleFilterNavParams {
  saveExtraProfileDetails: Function;
  deleteProject: Function;
}
interface IPeopleFilterProps
  extends NavigationScreenProps<IPeopleFilterNavParams> {
    dialogModal: {
      dialogShows: Function;
    };
  }
class ProfileProjects extends Component<IPeopleFilterProps, State> {
  constructor(props) {
    super(props);
    this.state = {
        projects: cloneDeep(get(this.props.navigation, 'state.params.project', []))
    }
    this.addProject = this.addProject.bind(this);
    this.deleteProject = this.deleteProject.bind(this);
  }
  addProject(project: any) {
    this.props.navigation.state.params.saveExtraProfileDetails(project, 'research').then((profileDetails) => {
      if (project.id) {
        let index = findIndex(this.state.projects, data => data.researchId === project.id);
        if (index > -1) {
          project.researchId = project.id;
          this.state.projects[index] = project;
          this.setState({
            projects: this.state.projects
          })
        }
      } else if (profileDetails && profileDetails.research) {
        this.setState({
          projects: profileDetails.research
        })
      }
    })
  }

  deleteConfirmationModal = (id: string) => {
    this.props.dialogModal.dialogShows(
      DELETE_PROJECT_TITLE, DELETE_PROJECT_MSG, 'action', true, () => this.deleteProject(id)
    )
  }

  deleteProject = (id: string) => {
    let index = findIndex(this.state.projects, data => data.researchId === id);
    this.props.navigation.state.params.deleteProject(id, 'research')
    if (index > -1) {
      this.state.projects.splice(index, 1);
    }
    this.setState({
      projects: this.state.projects
    })
  }
  render() {
    return (
      <View style={{ backgroundColor: 'rgb(250,251,252)', flex: 1 }}>
        <MSAHeader
          title={'Projects & Research'}
          rightText={'DONE'}
          leftText={'CANCEL'}
          onPress={() => this.props.navigation.goBack()}
          onRightTextPress={() => this.props.navigation.goBack()}
        />
        <View style={[profileStyles.fieldBox, { justifyContent: 'center', backgroundColor: 'white' }]}>
          <Text style={[profileStyles.fieldDarkLow, { fontSize: 16, lineHeight: 21 }]}>
            Add projects
          </Text>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('ProfileNewProject', {addProject: this.addProject, project: {}})
            }
            style={{ position: 'absolute', right: 15 }}
          >
            <Text style={profileStyles.tagButtonText}>ADD</Text>
          </TouchableOpacity>
        </View>
        <ScrollView>
          {
            map(this.state.projects, (project, index) => {
              if ( typeof project.startDate === 'string') {
                project.startDate = parseInt(moment(transformDate(project.startDate)).format('x'), 10)
              }
              if ( typeof project.endDate === 'string') {
                project.endDate = parseInt(moment( transformDate(project.endDate)).format('x'), 10)
              }
              return (
                <View style={[profileStyles.fieldBox, { backgroundColor: 'white' }]} key={index}>
                  <View style={{ width: '100%', flexDirection: 'row' }}>
                    <Text style={[profileStyles.educationCollg, { marginVertical: 2, flex: 1 }]}>
                      {project.title}
                    </Text>
                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate('ProfileNewProject', {
                          addProject: this.addProject, project, deleteProject: this.deleteConfirmationModal
                        })
                      }
                    >
                      <Icon name='md-create' style={{ color: 'rgb(207, 208, 220)', fontSize: 20 }} />
                    </TouchableOpacity>
                  </View>
                  {!isEmpty(project.projectUrl) &&
                    <Text style={[profileStyles.educationLows, { marginVertical: 2 }]}>
                      {project.projectUrl}
                    </Text>
                  }
                  <Text style={[profileStyles.educationLows, { marginVertical: 2 }]}>
                  { moment(new Date(project.startDate)).format('MMM Y') + '-' + moment(new Date(project.endDate)).format('MMM Y')}
                  </Text>
                  {!isEmpty(project.description) &&
                    <Text style={[profileStyles.educationLows, { marginVertical: 2 }]}>
                      {project.description}
                    </Text>
                  }
                </View>
              );
            })
          }
        </ScrollView>
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  dialogModal: bindActionCreators(DialogModal, dispatch)
});

export default connect(undefined, mapDispatchToProps)(ProfileProjects);