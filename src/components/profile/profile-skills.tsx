import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  TouchableHighlight,
  TextInput
} from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
import MSAHeader from '../layouts/header/header';
import {get, map, cloneDeep} from 'lodash';
import profileStyles from './profile-styles';
import SortableListView from 'react-native-sortable-listview'

interface IPeopleFilterNavParams {
  saveExtraProfileDetails: Function;
}
interface State {
  skills: any[];
  skill: string;
  order: any;
}
interface IPeopleFilterProps
  extends NavigationScreenProps<IPeopleFilterNavParams> {}
export default class ProfileSkills extends Component<IPeopleFilterProps, State> {
  constructor(props) {
    super(props);
    this.state = {
      skills: cloneDeep(get(this.props.navigation, 'state.params.skills', [])),
      skill: '',
      order: Object.keys(get(this.props.navigation, 'state.params.skills', []))
    }
    this.addSkill = this.addSkill.bind(this);
    this.removeSkill = this.removeSkill.bind(this);
  }
  addSkill() {
    this.state.skills.push({skills: this.state.skill.trim()});
    this.state.order.push((this.state.skills.length - 1).toString())
    this.setState({skills: this.state.skills, skill: '', order: this.state.order})
  }

  removeSkill(index) {
    this.state.skills.splice(index, 1);
    this.state.order.splice(this.state.order.indexOf(index), 1);
    const data = [];
    map(this.state.order, (order) => {
      if (order > index) {
        order--;
      }
      data.push(order.toString());
    });
    this.setState({skills: this.state.skills, order: data})
  }
  render() {
    return (
      <View style={{ backgroundColor: 'white', flex: 1 }}>
        <MSAHeader
          title={'Skills'}
          rightText={'SAVE'}
          leftText={'CANCEL'}
          onPress={() => this.props.navigation.goBack()}
          onRightTextPress={() => {
            const data = cloneDeep(this.state.skills);
            map(this.state.order, (order, index) => {
              data[index] = this.state.skills[order];
            });
            this.props.navigation.state.params.saveExtraProfileDetails(data, 'skillset')
          }}
        />
        <View style={[profileStyles.fieldBox, { paddingVertical: 0, paddingTop: 20, paddingBottom: 10 }]}>
          <Text
            style={[
              profileStyles.boxHead,
              {
                color: 'rgb(83,87,94)',
                flex: 0
              }
            ]}
          >
            NEW SKILL
          </Text>
          <View style={{ alignItems: 'center', flexDirection: 'row', marginTop: 10 }}>
            <TextInput
              style={[profileStyles.skillInp, {flex: 1}]}
              placeholder={'Skill (ex: Data Analysis)'}
              placeholderTextColor={'rgba(108,111,125,0.5)'}
              underlineColorAndroid={'rgba(0,0,0,0)'}
              value={this.state.skill}
              onChangeText={(text) => this.setState({skill: text})}
              maxLength={40}
            />
            <TouchableOpacity
              onPress={() => this.addSkill()}
              disabled={this.state.skill.trim() === ''}
            >
              <Text style={profileStyles.tagButtonText}>ADD</Text>
            </TouchableOpacity>
          </View>
        </View>
          <SortableListView style={{ flex: 1, backgroundColor: 'rgb(250,251,252)' }}
            data={this.state.skills}
            renderRow={(row, sectionId, rowIndex) => {
              return <Skill removeSkill={this.removeSkill} skill={row} sectionId={sectionId} rowId={rowIndex}/>
            }}
            order={this.state.order}
            onRowMoved={e => {
              this.state.order.splice(e.to, 0, this.state.order.splice(e.from, 1)[0])
              this.setState({
                order: this.state.order
              })
            }}
          >
          </SortableListView>
        </View>
    );
  }
}

class Skill extends Component<{
  sortHandlers?: any
  removeSkill: Function;
  skill: any;
  sectionId?: any;
  rowId?: any;
}, {}> {
  render() {
    return <TouchableHighlight
      style={{
        width: '100%',
        padding: 15,
        backgroundColor: 'white',
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderColor: 'rgb(233,239,247)'
      }}
      {...this.props.sortHandlers}
    >
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          flex: 1
        }}
      >
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <TouchableOpacity
            onPress={() => this.props.removeSkill(this.props.rowId)}
            style={{ paddingRight: 10 }}
          >
            <Image
              source={require('../../../assets/app_icons/Icons/Minus.png')}
            />
          </TouchableOpacity>
          <Text style={profileStyles.fieldDarkLow}>{this.props.skill ? this.props.skill.skills : ''}</Text>
        </View>
        <View>
          <Image
            source={require('../../../assets/hamburger.png')}
            style={{ height: 14, width: 14 }}
          />
        </View>
      </View>
    </TouchableHighlight>
  }
}
