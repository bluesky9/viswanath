import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  ScrollView,
  TouchableOpacity,
  Linking,
  Dimensions
} from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
import { Icon } from 'native-base';
import AppButton from '../layouts/buttons/button';
import profileStyles from './profile-styles';
import ActionSheet from 'react-native-actionsheet';
import { connect } from 'react-redux';
import { get, map, find, isEmpty, filter } from 'lodash';
import { DialogModal, PeopleApi, NotificationsActions } from '../../store/actions';
import { constants } from '../../constants'
import Autolink from 'react-native-autolink';
import { departmentStyles } from '../university/department/department-styles';
import { getUniversityBySlug } from '../../store/actions/Universities';

import { bindActionCreators } from 'redux';
import moment from 'moment';

import { getExtraProfileDetails, saveBasicProfileDetails, saveExtraProfileDetails, deleteExtraProfileDetails } from '../../store/actions/Profile';

const { width } = Dimensions.get('window');

// const userId = 14
const UNFOLLOW_POST = 'Are you sure to unfollow'
const UNFOLLOW_MSG = 'You will no longer see the posts or activity in your feed.'
const DISCONNECT_POST = 'Are you sure to disconnect'
const DISCONNECT_MSG = 'You are now disconnected and cannot message each other.'
const BLOCK_POST = 'Block user'
const BLOCK_MSG = 'The user will no longer be able to connect woth you. You can unblock users later in Settings.'
const UNBLOCK_POST = 'Do you want to unblock this user'
const UNBLOCK_MSG = 'After you unblock, the user will be able to see your posts.'

const CANCEL_INDEX = 0;
const optionsBlock = ['Cancel', 'Block user'];
const optionsUnblock = ['Cancel', 'Unblock user'];
let actionsheet = undefined;
interface IPeopleFilterNavParams {
  id: string;
}
interface IPeopleFilterProps
  extends NavigationScreenProps<IPeopleFilterNavParams> {
  userInfo: {
    profilePic: string;
  };
  getExtraProfileDetails: Function;
  saveUserInfo: Function;
  saveExtraProfileDetails: Function;
  deleteExtraProfileDetails: Function;
  peopleInfo;
  dialogModal;
  peopleApi;
  userId;
  getUniversityBySlug;
  notificationsActions;
}
class ProfilePeople extends Component<IPeopleFilterProps> {
  constructor(props) {
    super(props);
    this.saveUserInfo = this.saveUserInfo.bind(this);
    this.saveExtraProfileDetails = this.saveExtraProfileDetails.bind(this);
    this.deleteExtraProfileDetails = this.deleteExtraProfileDetails.bind(this);
  }
  showActionSheet() {
    actionsheet.show();
  }
  saveUserInfo(userData: any) {
    this.props.saveUserInfo(userData);
  }
  saveExtraProfileDetails(userData: any, type: string) {
    this.props.saveExtraProfileDetails(userData, type);
  }
  deleteExtraProfileDetails(id: string, type: string) {
    this.props.deleteExtraProfileDetails(id, type);
  }
  onProfilePicPressed = (image) => {
    let images = [];
    images.push({ originalUrl: image })
    this.props.dialogModal.dialogShows(
      'images',
      undefined,
      'image',
      true,
      undefined,
      undefined,
      undefined,
      undefined,
      images
    );
  }
  _onConnectionsPressed(connected, myRequest, userBlocked) {
    if (userBlocked) {
      this.props.peopleApi.unBlockPeople(this.props.peopleInfo.id)
      this.props.dialogModal.dialogShows('Connection request', this.props.peopleInfo.id, 'connect')
    } else {
      if (connected) {
        if (connected === 'Pending' && !myRequest) {
          this.props.notificationsActions.acceptConnection(this.props.peopleInfo.id, 'PeoplePage')
        } else {
          this.props.dialogModal.dialogShows(
            DISCONNECT_POST, DISCONNECT_MSG, 'action', true, () => this.props.peopleApi.disconnectPeople(this.props.peopleInfo.id))
        }
      } else {
        this.props.dialogModal.dialogShows('Connection request', this.props.peopleInfo.id, 'connect')
      }
    }
  }

  _onFollowingPressed = (following, userBlocked) => {
    if (userBlocked) {
      this.props.peopleApi.unBlockPeople(this.props.peopleInfo.id)
    }
    if (following) {
      this.props.dialogModal.dialogShows(
        UNFOLLOW_POST, UNFOLLOW_MSG, 'action', true, () => this.props.peopleApi.unFollowPeople(this.props.peopleInfo.id, 'Profile', 'other'))
    } else {
      this.props.peopleApi.followPeople(this.props.peopleInfo.id, 'Profile', 'other')
    }
  }

  onEllipsePressed = (connections) => {
    if (connections === 'Blocked') {
      this.props.dialogModal.dialogShows(
        UNBLOCK_POST, UNBLOCK_MSG, 'action', true, () => this.props.peopleApi.unBlockPeople(this.props.peopleInfo.id)
      )
    } else {
      this.props.dialogModal.dialogShows(
        BLOCK_POST, BLOCK_MSG, 'action', true, () => this.props.peopleApi.blockPeople(this.props.peopleInfo.id)
      )
    }
  }

  render() {
    let user = get(this.props.peopleInfo, 'user', undefined);
    let skills = get(this.props.peopleInfo, 'skills', []);
    // map(get(this.props.peopleInfo, 'skills', []), (item) => {
    //   skills = skills + item.skills + ' ,'
    // });
    let following = find(this.props.peopleInfo.followers, follower => follower.id === this.props.userId)
    let connections = get(this.props.peopleInfo, 'connectionStatus', '');
    let userTypes = get(this.props.peopleInfo, 'userTypes', []);
    if (!isEmpty(userTypes)) {
      userTypes = filter(userTypes, userTypeObj => userTypeObj.userType !== 'PROSPECTIVE_STUDENT')
    }
    let connectionObj = {
      status: '',
      buttonType: ''
    }
    if (connections) {
      if (connections === 'Blocked') {
        connectionObj = {
          status: 'Connect',
          buttonType: 'primary'
        }
      } else if (this.props.peopleInfo.myRequest) {
        connectionObj = {
          status: connections === 'Pending' ? 'Requested' : 'Connected',
          buttonType: connections === 'Pending' ? 'primary' : 'inverted'
        }
      } else {
        connectionObj = {
          status: connections === 'Pending' ? 'Approve' : 'Connected',
          buttonType: connections === 'Pending' ? 'bloodRed' : 'inverted'
        }
      }
    } else {
      connectionObj = {
        status: 'Connect',
        buttonType: 'primary'
      }
    }
    let interests = get(this.props.peopleInfo, 'areasOfInterest', []);
    // map(get(this.props.peopleInfo, 'areasOfInterest', []), (interest, index) => {
    //   if (index !== get(this.props.peopleInfo, 'areasOfInterest.length', 0) - 1)
    //     interests = interests + interest.interestArea + ', '
    //   else interests = interests + interest.interestArea
    // });
    const educations = get(this.props.peopleInfo, 'education', []);
    const researches = get(this.props.peopleInfo, 'research', []);
    const awards = get(this.props.peopleInfo, 'achievement', []);
    const volunteerings = get(this.props.peopleInfo, 'volunteerWork', []);
    const website = get(this.props.peopleInfo, 'website', []);
    const workExps = get(this.props.peopleInfo, 'workExps', []);
    const memberships = get(this.props.peopleInfo, 'membership', []);

    return (
      <View style={{ flex: 1, backgroundColor: 'rgb(250,251,252)' }}>
        <ActionSheet
          ref={o => (actionsheet = o)}
          options={connections === 'Blocked' ? optionsUnblock : optionsBlock}
          cancelButtonIndex={CANCEL_INDEX}
          destructiveButtonIndex={1}
          onPress={i => {
            i === 1
              ? this.onEllipsePressed(connections)
              : console.log(i);
          }}
        />
        <View style={profileStyles.headerContainer}>
          <View style={profileStyles.leftIconContainer}>
            <TouchableOpacity style={profileStyles.leftIconBtn} onPress={() => this.props.navigation.goBack()}>
              <Icon name='md-arrow-back' style={{ color: 'rgb(207, 208, 220)', fontSize: 30 }} />
            </TouchableOpacity>
          </View>
          <View style={profileStyles.centerHeaderContainer}>
            <Image
              source={user && user.profPicPath && connections !== 'Blocked' ?
                {
                  uri: user.profPicPath
                }
                : require('../../../assets/images/user_dummy_image.png')
              }
              style={{ height: 20, width: 20, marginHorizontal: 6, borderRadius: 10 }}
            />
            <Text style={profileStyles.headerText} numberOfLines={1}>
              {connections === 'Blocked' ? 'Unknown User' : get(user, 'firstName', 'NA') + ' ' + get(user, 'lastName', '')}
            </Text>
          </View>
          <View style={profileStyles.rightIconContainer}>
            {this.props.userId !== this.props.peopleInfo.id &&
              <TouchableOpacity style={profileStyles.rightIconBtn} onPress={() => this.showActionSheet()}>
                <Icon name='ios-more' style={{ color: 'rgb(207, 208, 220)', fontSize: 30 }} />
              </TouchableOpacity>
            }
          </View>
        </View>
        <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: 'white' }}>
          <View style={{ paddingHorizontal: 12, borderColor: 'rgb(233,238,241)', borderBottomWidth: 1, paddingVertical: 14 }}>
            <View style={{ flexDirection: 'row' }}>
              <TouchableOpacity
                onPress={() => this.onProfilePicPressed(get(user, 'profPicPath', undefined))}
              >
                <Image
                  source={user && user.profPicPath && connections !== 'Blocked' ?
                    {
                      uri: user.profPicPath
                    }
                    : require('../../../assets/images/user_dummy_image.png')
                  }
                  style={{ height: 76, width: 76, borderRadius: 38, marginRight: 12 }}
                />
              </TouchableOpacity>
              <View style={{ flexWrap: 'wrap', width: width - 100 }}>
                <Text style={profileStyles.nameText}>
                  {connections === 'Blocked' ? 'Unknown User' : get(user, 'firstName', 'NA') + ' ' + get(user, 'lastName', '')}
                </Text>
                {connections !== 'Blocked' && <View
                  style={{
                    flexDirection: 'row',
                    marginTop: 10,
                    alignItems: 'center'
                  }}
                >
                  <Icon
                    name={'pin'}
                    style={{ color: 'rgb(207,208,220)', fontSize: 23 }}
                  />
                  <Text style={profileStyles.countryText}>
                    {get(user, 'country', 'NA')}
                  </Text>
                </View>
                }
              </View>
            </View>
            {connections !== 'Blocked' &&
              <Text style={[profileStyles.bioText, { paddingTop: 11 }]}>
                {get(this.props.peopleInfo, 'shortDesc', 'NA')}
              </Text>
            }
          </View>
          {!isEmpty(userTypes) && connections !== 'Blocked' && <View style={profileStyles.educationTextContainer}>
            {
              map(userTypes, (item, index) => {
                return (
                  <Text style={[profileStyles.bioText, { color: 'rgb(59, 62, 68)', lineHeight: 17 }]} key={index}>
                    <Text>{constants.USERTYPES.CONTEXT[item.userType] + ' '}</Text>
                    <Text
                      style={{ color: 'rgb(53, 135, 230)' }}
                      onPress={() => {
                        this.props.getUniversityBySlug(item.universitySlug)
                        this.props.navigation.navigate('UniversityProfile')
                      }}
                    >
                      {item.universityName}
                    </Text>
                  </Text>
                )
              })
            }
          </View>}
          {connections !== 'Blocked' &&
            <View style={[profileStyles.statsContainer]}>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('PeopleFollowerList', {
                    title: 'Followers',
                    blocked: connections === 'Blocked' ? true : false,
                    userType: 'other',
                    source: 'otherProfile',
                    id: this.props.navigation.state.params.id
                  })
                }
                }
                style={[profileStyles.statOneContainer, { flex: 0.20 }]}
              >
                <Text style={profileStyles.statNmbrText}>
                  {get(this.props.peopleInfo, 'followers', []).length}
                </Text>
                <Text style={profileStyles.statNameText}>Followers</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('PeopleFollowerList',
                    {
                      title: 'Following',
                      blocked: connections === 'Blocked' ? true : false,
                      userType: 'other',
                      source: 'otherProfile',
                      id: this.props.navigation.state.params.id
                    }
                  )
                }
                }
                style={[profileStyles.statOneContainer, { flex: 0.20 }]}
              >
                <Text style={profileStyles.statNmbrText}>
                  {get(this.props.peopleInfo, 'following', []).length}
                </Text>
                <Text style={profileStyles.statNameText}>Following</Text>
              </TouchableOpacity>
              <View style={[profileStyles.statButtonContainer, { flex: 0.34 }]}>
                {this.props.userId !== this.props.peopleInfo.id &&
                  <AppButton
                    onPress={() => this._onFollowingPressed(following, connections === 'Blocked' ? true : false)}
                    type={following ? 'inverted' : 'primary'}
                    bordered={true}
                    label={following ? 'Following' : 'Follow'}
                    customButtonStyle={{ marginVertical: 0, paddingHorizontal: 8, paddingVertical: 7 }}
                    textSize={12}
                    containerWidth={'90%'}
                  />
                }
              </View>
              <View style={[profileStyles.statButtonContainer, { flex: 0.30 }]}>
                {this.props.userId !== this.props.peopleInfo.id &&
                  <AppButton
                    onPress={() =>
                      this._onConnectionsPressed(connections, this.props.peopleInfo.myRequest, connections === 'Blocked' ? true : false)
                    }
                    type={connectionObj.buttonType}
                    bordered={true}
                    label={connectionObj.status}
                    customButtonStyle={{ marginVertical: 0, paddingHorizontal: 8, paddingVertical: 7 }}
                    textSize={12}
                    containerWidth={'90%'}
                    disabled={connectionObj.status === 'Requested' ? true : false}
                  />
                }
              </View>
            </View>
          }
          {!isEmpty(website) && connections !== 'Blocked' &&
            <View style={profileStyles.fieldBox}>
              <Text style={profileStyles.boxHead}>WEBSITE</Text>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Icon name='md-link' style={{ color: '#2E7CE2', fontSize: 20, marginTop: 15, marginRight: 10 }} />
                <Autolink text={website} style={profileStyles.boxLow} />
              </View>
            </View>
          }
          {!isEmpty(skills) && connections !== 'Blocked' &&
            <View style={profileStyles.fieldBox}>
              <Text style={profileStyles.boxHead}>SKILLS</Text>
              <View style={{ paddingTop: 13, flexDirection: 'row', flexWrap: 'wrap' }}>
                {
                  skills.map((item, index) => {
                    return (
                      <View style={departmentStyles.tagsBlock} key={index}>
                        <Text style={departmentStyles.tags}>{item.skills}</Text>
                      </View>
                    )
                  })
                }
              </View>
            </View>
          }
          {!isEmpty(interests) && connections !== 'Blocked' &&
            <View style={profileStyles.fieldBox}>
              <Text style={profileStyles.boxHead}>INTERESTS</Text>
              <View style={{ paddingTop: 13, flexDirection: 'row', flexWrap: 'wrap' }}>
                {
                  interests.map((item, index) => {
                    return (
                      <View style={departmentStyles.tagsBlock} key={index}>
                        <Text style={departmentStyles.tags}>{item.interestArea}</Text>
                      </View>
                    )
                  })
                }
              </View>
            </View>
          }
          {!isEmpty(educations) && connections !== 'Blocked' &&
            <View style={[profileStyles.fieldBox, { paddingVertical: 0, paddingTop: 15 }]}>
              <Text style={profileStyles.boxHead}>
                EDUCATION
              </Text>
              {
                map(educations, (education, index) => {
                  return (
                    <View style={[profileStyles.educationFieldBox, { borderBottomWidth: (educations.length - 1 === index) ? 0 : 1 }]} key={index}>
                      <Text style={profileStyles.fieldBigHead}>
                        {education.universityName}
                      </Text>
                      <Text style={profileStyles.fieldDarkLow}>
                        {education.major + '-' + education.degree}
                      </Text>
                      <Text style={[profileStyles.fieldDarkLow, { paddingBottom: 10 }]}>
                        {moment(education.startDate).format('MMM Y') + '-' +
                          (education.present ? 'Present' : moment(education.endDate).format('MMM Y'))
                        }
                      </Text>
                    </View>
                  )
                })
              }
            </View>
          }
          {!isEmpty(workExps) && connections !== 'Blocked' &&
            <View style={profileStyles.fieldBox}>
              <View>
                <Text style={profileStyles.boxHead}>
                  EXPERIENCE
                </Text>
              </View>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                contentContainerStyle={[profileStyles.expScroll]}
              >
                {map(workExps, (workExp, index) => {
                  return (
                    <View style={profileStyles.expCard} key={index}>
                      <Text style={profileStyles.expCardBigText}>{workExp.organization}</Text>
                      <Text style={[profileStyles.expCardSmallText]}>
                        {workExp.designation}
                      </Text>
                      <Text style={[profileStyles.expCardSmallText]}>
                        {moment(workExp.startDate).format('MMM Y') + '-' +
                          (workExp.present ? 'Present' : moment(workExp.endDate).format('MMM Y'))
                        }
                      </Text>
                      {!isEmpty(workExp.description) &&
                        <Text style={[profileStyles.expCardSmallText]}>
                          {workExp.description}
                        </Text>
                      }
                    </View>
                  );
                })}
              </ScrollView>
            </View>
          }
          {!isEmpty(memberships) && connections !== 'Blocked' &&
            <View style={profileStyles.fieldBox}>
              <Text style={profileStyles.boxHead}>
                MEMBERSHIPS & AFFILIATIONS
              </Text>
              {
                map(memberships, (membership, index) => {
                  return (
                    <View key={index}>
                      <Text style={[profileStyles.expCardSmallText, { color: 'rgb(153,46,46)', marginTop: 15 }]}>
                        {membership.organization}
                      </Text>
                      <Text style={[profileStyles.expCardSmallText]}>
                        {moment(membership.startDate).format('MMM Y') + '-' +
                          (membership.present ? 'Present' : moment(membership.endDate).format('MMM Y'))
                        }
                      </Text>
                      {!isEmpty(membership.description) &&
                        <Text style={[profileStyles.expCardSmallText]}>
                          {membership.description}
                        </Text>
                      }
                    </View>
                  );
                })
              }
            </View>
          }
          {!isEmpty(researches) && connections !== 'Blocked' &&
            <View style={[profileStyles.fieldBox]}>
              <Text style={profileStyles.boxHead}>PROJECTS & RESEARCH</Text>
              {
                map(researches, (research, index) => {
                  return (
                    <View style={[profileStyles.educationFieldBox, {
                      borderBottomWidth: (researches.length - 1 === index) ? 0 : 1,
                      paddingTop: 0, paddingBottom: (researches.length - 1 === index) ? 0 : 15
                    }]} key={index}>
                      <TouchableOpacity
                        onPress={() => {
                          if (research.projectUrl) {
                            Linking.openURL(research.projectUrl).catch(() => alert('Unable to open link'))
                          }
                        }
                        }
                      >
                        <Text style={[profileStyles.boxLow, { color: 'rgb(153,46,46)' }]}>
                          {research.title}
                        </Text>
                        <Text style={profileStyles.expCardSmallText}>
                          {moment(research.startDate).format('MMM Y') + '-' + moment(research.endDate).format('MMM Y')}
                        </Text>
                        {!isEmpty(research.description) &&
                          <Text style={profileStyles.expCardSmallText}>
                            {research.description}
                          </Text>
                        }
                      </TouchableOpacity>
                    </View>
                  )
                })
              }
            </View>
          }
          {!isEmpty(awards) && connections !== 'Blocked' &&
            <View style={profileStyles.fieldBox}>
              <Text style={profileStyles.boxHead}>AWARDS & RECOGNIZATIONS</Text>
              {
                map(awards, (award, index) => {
                  return (
                    <View style={[profileStyles.educationFieldBox, {
                      borderBottomWidth: (awards.length - 1 === index) ? 0 : 1,
                      paddingBottom: (awards.length - 1 === index) ? 0 : 15
                    }]} key={index}>
                      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={[profileStyles.fieldBigHead]}>{award.award}</Text>
                      </View>
                      <Text style={[profileStyles.fieldDarkLow]}>
                        {award.description}
                      </Text>
                      <Text style={[profileStyles.fieldDarkLow, { top: 5 }]}>
                        {(award.date) ? moment(award.date).format('Y') : 'NA'}
                      </Text>
                    </View>
                  )
                })
              }
            </View>
          }
          {!isEmpty(volunteerings) && connections !== 'Blocked' &&
            <View style={profileStyles.fieldBox}>
              <Text style={profileStyles.boxHead}>VOLUNTEERING</Text>
              {
                map(volunteerings, (volunteer, index) => {
                  return (
                    <View style={[profileStyles.educationFieldBox, {
                      borderBottomWidth: (volunteerings.length - 1 === index) ? 0 : 1,
                      paddingBottom: (volunteerings.length - 1 === index) ? 0 : 15,
                      paddingTop: 0
                    }]} key={index}>
                      <Text style={[profileStyles.boxLow, { color: 'rgb(153,46,46)' }]}>
                        {volunteer.organization}
                      </Text>
                      <Text style={profileStyles.expCardSmallText}>
                        {moment(volunteer.startDate).format('MMM Y') + '-' +
                          (volunteer.present ? 'Present' : moment(volunteer.endDate).format('MMM Y'))
                        }
                      </Text>
                      {!isEmpty(volunteer.description) &&
                        <Text style={profileStyles.expCardSmallText}>
                          {volunteer.description}
                        </Text>
                      }
                    </View>
                  )
                })
              }
            </View>
          }
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    userInfo: state.userInfoAPI.userInfo,
    peopleInfo: state.peopleInfo.peopleInfo[props.navigation.state.params.id],
    userId: state.userInfoAPI.userId
  };
};

const mapDispatchToProps = dispatch => ({
  getExtraProfileDetails: bindActionCreators(getExtraProfileDetails, dispatch),
  saveUserInfo: bindActionCreators(saveBasicProfileDetails, dispatch),
  saveExtraProfileDetails: bindActionCreators(saveExtraProfileDetails, dispatch),
  deleteExtraProfileDetails: bindActionCreators(deleteExtraProfileDetails, dispatch),
  dialogModal: bindActionCreators(DialogModal, dispatch),
  peopleApi: bindActionCreators(PeopleApi, dispatch),
  getUniversityBySlug: bindActionCreators(getUniversityBySlug, dispatch),
  notificationsActions: bindActionCreators(NotificationsActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePeople);
