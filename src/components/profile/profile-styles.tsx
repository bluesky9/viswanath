import { StyleSheet, Dimensions } from 'react-native';
import { style } from '../../styles/variables';
const { width } = Dimensions.get('window');

const profileStyles = StyleSheet.create({
  loaderHolder : {
    width: '100%',
    height: '100%',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.3)',
    position: 'absolute'
  },
  headerContainer: {
    paddingTop: 12 + style.header.height,
    paddingBottom: 12,
    paddingHorizontal: 15,
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: 'rgb(233,239,247)',
    backgroundColor: 'white'
  },
  leftIconContainer: {
    flex: 0.25,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  leftIconBtn: {
    paddingLeft: 0,
    paddingRight: 15
  },
  centerHeaderContainer: {
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  headerText: {
    color: 'rgb(83,87,94)',
    fontFamily: style.font.family.rubikRegular,
    fontSize: 16,
    lineHeight: 19,
    width: '89%'
  },
  rightIconContainer: {
    flex: 0.25,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  rightIconBtn: {
    paddingLeft: 15,
    paddingRight: 0,
    paddingVertical: 5
  },
  nameText: {
    fontSize: 16,
    lineHeight: 20,
    fontFamily: style.font.family.ptSerifBold,
    color: 'rgb(59,62,68)'
  },
  countryText: {
    color: 'rgba(83,87,94,0.8)',
    fontFamily: style.font.family.rubikRegular,
    fontSize: 14,
    lineHeight: 17,
    marginHorizontal: 6
  },
  bioText: {
    color: 'rgb(83,87,94)',
    fontFamily: style.font.family.rubikRegular,
    fontSize: 14,
    lineHeight: 21
  },
  educationTextContainer: {
    paddingHorizontal: 15,
    paddingVertical: 16,
    borderColor: 'rgb(233,238,241)',
    borderBottomWidth: 1
  },
  statsContainer: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: 'rgb(233,239,247)',
    padding: 15
  },
  statOneContainer: {
    flex: 0.6,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  statOneContainerProfile: {
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  statNmbrText: {
    color: 'rgb(59,62,68)',
    fontFamily: style.font.family.ptSerif,
    fontSize: 16,
    lineHeight: 20
  },
  statNameText: {
    paddingTop: 1,
    color: 'rgb(83,87,94)',
    fontFamily: style.font.family.rubikRegular,
    fontSize: 12,
    lineHeight: 18
  },
  fieldBox: {
    borderBottomWidth: 1,
    borderColor: 'rgb(233,239,247)',
    padding: 15
  },
  educationFieldBox: {
    borderBottomWidth: 1,
    borderColor: 'rgb(233,239,247)',
    paddingVertical: 15
    // paddingTop: 15
  },
  statButtonContainer: {
    flex: 0.6,
    justifyContent: 'center',
    alignItems: 'center'
  },
  statButtonContainerProfile: {
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  boxHead: {
    color: 'rgb(128,134,145)',
    fontFamily: style.font.family.rubikMedium,
    fontSize: 12,
    lineHeight: 14,
    letterSpacing: 1,
    flex: 1
  },
  boxLow: {
    color: 'rgb(53,135,230)',
    fontFamily: style.font.family.rubikRegular,
    fontSize: 14,
    marginTop: 15,
    lineHeight: 17
  },
  editBoxIcon: {
    height: 15,
    width: 15
  },
  fieldBigHead: {
    color: 'rgb(59,62,68)',
    fontFamily: style.font.family.rubikRegular,
    fontSize: 16,
    lineHeight: 20,
    marginTop: 0,
    marginBottom: 5
  },
  fieldDarkLow: {
    color: 'rgb(83,87,94)',
    fontFamily: style.font.family.rubikRegular,
    fontSize: 14,
    lineHeight: 17
  },
  partBorderBottom: {
    justifyContent: 'flex-start',
    width: width - 30,
    alignSelf: 'center',
    paddingHorizontal: 0
  },
  expScroll: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    paddingVertical: 25,
    paddingBottom: 15
  },
  expCard: {
    height: '100%',
    width: 262,
    backgroundColor: 'white',
    marginHorizontal: 5,
    shadowOffset: { height: 1, width: -1 },
    shadowOpacity: 0.1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    padding: 15,
    elevation: 3
  },
  expCardBigText: {
    fontSize: 15,
    color: 'rgb(53,71,91)',
    fontFamily: style.font.family.rubikLight,
    marginVertical: 5
  },
  expCardSmallText: {
    fontSize: 14,
    lineHeight: 17,
    color: 'rgb(83,87,94)',
    fontFamily: style.font.family.rubikRegular,
    marginTop: 5
  },
  asheetItem: {
    fontSize: 16,
    lineHeight: 21,
    fontFamily: style.font.family.rubikRegular,
    color: 'rgb(59,62,68)'
  },
  dualHeaderLeftText: {
    fontSize: 14,
    lineHeight: 17,
    fontFamily: style.font.family.rubikMedium,
    color: 'rgb(53,71,91)',
    letterSpacing: 0.5
  },
  dualHeaderCenterText: {
    fontSize: 16,
    lineHeight: 19,
    fontFamily: style.font.family.rubikRegular,
    color: 'rgb(53,71,91)',
    letterSpacing: 0.2
  },
  dualHeaderRightText: {
    fontSize: 14,
    lineHeight: 17,
    fontFamily: style.font.family.rubikMedium,
    color: 'rgb(53,135,230)',
    letterSpacing: 0.5
  },
  skillInp: {
    fontSize: 14,
    lineHeight: 18,
    fontFamily: style.font.family.rubikRegular,
    color: style.color.coal,
    padding: 0
  },
  tagButtonText: {
    fontSize: 14,
    lineHeight: 17,
    color: 'rgb(53,135,230)',
    fontFamily: style.font.family.rubikMedium,
    letterSpacing: 0.5
  },
  educationCollg: {
    fontSize: 14,
    lineHeight: 17,
    fontFamily: style.font.family.rubikMedium,
    color: 'rgb(83,87,94)'
  },
  educationLows: {
    fontSize: 14,
    lineHeight: 17,
    fontFamily: style.font.family.rubikRegular,
    color: 'rgb(83,87,94)'
  },
  presentBtnContainer: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: 'rgb(233,239,247)',
    paddingBottom: 15
  },
  presentText: {
    color: 'rgb(83,87,94)',
    fontFamily: style.font.family.rubikRegular,
    fontSize: 14,
    lineHeight: 18
  }
});
export default profileStyles;
