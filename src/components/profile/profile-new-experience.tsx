import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Alert,
  TextInput,
  Switch,
  Platform
} from 'react-native';
import DatePicker from 'react-native-datepicker';
import { NavigationScreenProps } from 'react-navigation';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import MSAHeader from '../layouts/header/header';
import moment from 'moment';
import { style } from '../../styles/variables';
import profileStyles from './profile-styles';
import { transformDate } from '../../utils';
interface State {
experience: Experience;
}
interface Experience {
  id?: string,
  workId?: string,
  organization: string,
  designation: string,
  location: string,
  description: string,
  startDate: string;
  endDate: string;
  startMonth: number;
  startYear: number;
  endMonth: number;
  endYear: number;
  department: string;
  present: boolean;
}
interface IPeopleFilterNavParams {
  addExperience?: Function;
  experience?: Experience;
  deleteExps?: Function;
}
interface IPeopleFilterProps
  extends NavigationScreenProps<IPeopleFilterNavParams> {
  }
export default class ProfileNewExperience extends Component<IPeopleFilterProps, State> {
  constructor(props: IPeopleFilterProps) {
    super(props);
    const {
      workId,
      organization,
      designation,
      location,
      description,
      startDate,
      endDate,
      present
    } = props.navigation.state.params.experience;
    this.state = {
      experience: {
        id: (workId) ? workId : '',
        organization: (organization) ? organization : '',
        designation: (designation) ? designation : '',
        location: (location) ? location : '',
        description: (description) ? description : '',
        department: 'Engineering',
        startDate: (startDate) ? moment(startDate).format('MMM Y') : moment().format('MMM Y'),
        endDate: (endDate) ? moment(endDate).format('MMM Y') : moment().format('MMM Y'),
        startMonth: (startDate) ? moment(startDate).month() + 1 : moment().month() + 1,
        startYear: (startDate) ? moment(startDate).year() : moment().year(),
        endMonth: (endDate) ? moment(endDate).month() + 1 : moment().month() + 1,
        endYear: (endDate) ? moment(endDate).year() : moment().year(),
        present: (present) ? present : false
      }
    }
  }
  onChange(text: string, field: string) {
    let newText = text.split(' ')
    let temp = this.state.experience[field];
    this.state.experience[field] = text;
    if (field === 'startDate' || field === 'endDate') {
      if (moment(transformDate(this.state.experience.endDate)) <
          moment(transformDate(this.state.experience.startDate))) {
        this.state.experience[field] = temp;
        setTimeout(() => {
          Alert.alert('To date should be greater than From date');
        }, 500);
      } else {
        if (field === 'startDate') {
          this.state.experience.startMonth = parseInt(moment().month(newText[0]).format('M'), 10)
          this.state.experience.startYear = parseInt(newText[1], 10)
        } else if (field === 'endDate') {
          this.state.experience.endMonth = parseInt(moment().month(newText[0]).format('M'), 10)
          this.state.experience.endYear = parseInt(newText[1], 10)
        }
        this.setState({
          experience: this.state.experience
        })
      }
    } else {
      this.setState({
        experience: this.state.experience
      })
    }
  }
  render() {
    return (
      <View style={{ backgroundColor: 'rgb(250,251,252)', flex: 1 }}>
        <MSAHeader
          title={'New Experience'}
          rightText={'SAVE'}
          leftText={'CANCEL'}
          onPress={() => this.props.navigation.goBack()}
          onRightTextPress={() => this.props.navigation.state.params.addExperience(this.state.experience)}
        />
        <KeyboardAwareScrollView>
          <View style={{ backgroundColor: 'white' }}>
            <View style={profileStyles.fieldBox}>
              <Text
                style={[profileStyles.dualHeaderLeftText, { marginBottom: 15, fontSize: 12, lineHeight: 14 }]}
              >
                ORGANIZATION / COMPANY
              </Text>
              <TextInput
                style={[profileStyles.skillInp, { color: 'rgb(83,87,94)' }]}
                placeholder={'Ex. Google'}
                placeholderTextColor={'rgba(108,111,125,0.5)'}
                underlineColorAndroid={'rgba(0,0,0,0)'}
                value={this.state.experience.organization}
                onChangeText={(text) => this.onChange(text, 'organization')}
              />
            </View>
            <View style={profileStyles.fieldBox}>
              <Text
                style={[profileStyles.dualHeaderLeftText, { marginBottom: 15, fontSize: 12, lineHeight: 14 }]}
              >
                TITLE
              </Text>
              <TextInput
                style={[profileStyles.skillInp, { color: 'rgb(83,87,94)' }]}
                placeholder={'Ex. Social Media Expert'}
                placeholderTextColor={'rgba(108,111,125,0.5)'}
                underlineColorAndroid={'rgba(0,0,0,0)'}
                value={this.state.experience.designation}
                onChangeText={(text) => this.onChange(text, 'designation')}
              />
            </View>
            <View style={profileStyles.fieldBox}>
              <Text
                style={[profileStyles.dualHeaderLeftText, { marginBottom: 15, fontSize: 12, lineHeight: 14 }]}
              >
                LOCATION
              </Text>
              <TextInput
                style={[profileStyles.skillInp, { color: 'rgb(83,87,94)' }]}
                placeholder={'Ex. Oklahoma'}
                placeholderTextColor={'rgba(108,111,125,0.5)'}
                underlineColorAndroid={'rgba(0,0,0,0)'}
                value={this.state.experience.location}
                onChangeText={(text) => this.onChange(text, 'location')}
              />
            </View>
            <View style={profileStyles.fieldBox}>
              <Text
                style={[profileStyles.dualHeaderLeftText, { marginBottom: 15, fontSize: 12, lineHeight: 14 }]}
              >
                DESCRIPTION (optional)
              </Text>
              <TextInput
                style={[profileStyles.skillInp, { color: 'rgb(83,87,94)' }]}
                placeholder={'Ex. Share what you did for the company'}
                placeholderTextColor={'rgba(108,111,125,0.5)'}
                underlineColorAndroid={'rgba(0,0,0,0)'}
                value={this.state.experience.description}
                onChangeText={(text) => this.onChange(text, 'description')}
                maxLength={2000}
                multiline={true}
                numberOfLines={5}
              />
            </View>
            <View style={[profileStyles.fieldBox, { flexDirection: 'row', borderBottomWidth: 0, paddingBottom: 9 }]}>
              <View
                style={{
                  justifyContent: 'flex-start',
                  alignItems: 'flex-start',
                  flex: 1
                }}
              >
                <Text
                  style={[profileStyles.dualHeaderLeftText, { marginBottom: 15, fontSize: 12, lineHeight: 14 }]}
                >
                  FROM
                </Text>
                <DatePicker
                  style={{ width: 80 }}
                  date={this.state.experience.startDate}
                  mode={'date'}
                  format={'MMM Y'}
                  confirmBtnText={'Confirm'}
                  cancelBtnText={'Cancel'}
                  showIcon={false}
                  onDateChange={(date) => this.onChange(date, 'startDate')}
                  customStyles={{
                    dateInput: {
                      height: 28,
                      width: 80,
                      borderColor: 'rgb(53,135,230)',
                      borderRadius: 5
                    },
                    dateText: { color: 'rgb(53,135,230)', fontSize: 14, lineHeight: 18 }
                  }}
                />
              </View>
              <View
                style={{ flex: 1 }}
              >
                <Text
                  style={[profileStyles.dualHeaderLeftText, { marginBottom: 15, fontSize: 12, lineHeight: 14 }]}
                >
                  TO
                </Text>
                { this.state.experience.present ?
                  <View style={{ height: 40, justifyContent: 'center' }}>
                    <Text style={[profileStyles.presentText, { fontSize: 14 }]}>Present</Text>
                  </View>
                  :
                  <DatePicker
                    style={{ width: 80 }}
                    mode={'date'}
                    date={this.state.experience.endDate}
                    format={'MMM Y'}
                    confirmBtnText={'Confirm'}
                    cancelBtnText={'Cancel'}
                    showIcon={false}
                    onDateChange={(date) => this.onChange(date, 'endDate')}
                    customStyles={{
                      dateInput: {
                        height: 28,
                        width: 80,
                        borderColor: 'rgb(53,135,230)',
                        borderRadius: 5
                      },
                      dateText: { color: 'rgb(53,135,230)', fontSize: 14, lineHeight: 18 }
                    }}
                  />
                }
              </View>
            </View>
            <View style={profileStyles.presentBtnContainer}>
              <View style={{flex: 1}} />
              <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start'}}>
                <Switch
                  onValueChange={(bool) => {
                    let experience = this.state.experience;
                    experience.present = bool;
                    this.setState({ experience })
                  }}
                  value={this.state.experience.present}
                  thumbTintColor={style.color.primary}
                  onTintColor={style.color.cream}
                  style={{
                    transform: [{ scaleX: Platform.OS === 'ios' ? .7 : 1 },
                      { scaleY: Platform.OS === 'ios' ? .7 : 1 }] }}
                />
                <Text style={profileStyles.presentText}>Present</Text>
              </View>
            </View>
          </View>
          {this.state.experience.id ? <TouchableOpacity
            style={{ width: '100%', alignItems: 'center', padding: 10, backgroundColor: 'white' }}
            onPress={() => {
              // this.props.navigation.goBack()
              this.props.navigation.state.params.deleteExps(this.state.experience.id)}}
              >
            <Text style={{ color: '#CC1433' }}>Delete</Text>
          </TouchableOpacity> : undefined}
        </KeyboardAwareScrollView>
      </View>
    );
  }
}
