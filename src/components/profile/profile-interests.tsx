import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  TextInput
} from 'react-native';
import { Icon } from 'native-base';
import { NavigationScreenProps } from 'react-navigation';
import MSAHeader from '../layouts/header/header';
import {map, get, cloneDeep} from 'lodash';
import profileStyles from './profile-styles';
import SortableListView from 'react-native-sortable-listview'

interface IPeopleFilterNavParams {
  saveExtraProfileDetails: Function;
}
interface State {
  interests: any[];
  interest: string;
  order: any;
}
interface IPeopleFilterProps
  extends NavigationScreenProps<IPeopleFilterNavParams> {}
export default class ProfileInterests extends Component<IPeopleFilterProps, State> {
  constructor(props) {
    super(props);
    this.state = {
      interests: cloneDeep(get(this.props.navigation, 'state.params.interests', [])),
      interest: '',
      order: Object.keys(get(this.props.navigation, 'state.params.interests', []))
    }
    this.addInterest = this.addInterest.bind(this);
    this.removeInterest = this.removeInterest.bind(this);
  }

  addInterest(interest: any) {
    this.state.interests.push(interest);
    this.state.order.push((this.state.interests.length - 1).toString())
    this.setState({interests: this.state.interests, interest: '', order: this.state.order})
  }
  removeInterest(index) {
    this.state.interests.splice(index, 1);
    this.state.order.splice(this.state.order.indexOf(index), 1);
    const data = [];
    map(this.state.order, (order) => {
      if (order > index) {
        order--;
      }
      data.push(order.toString());
    });
    this.setState({interests: this.state.interests, order: data})
  }
  render() {
    return (
      <View style={{ backgroundColor: 'white', flex: 1 }}>
        <MSAHeader
          title={'Interests'}
          rightText={'SAVE'}
          leftText={'CANCEL'}
          onPress={() => this.props.navigation.goBack()}
          onRightTextPress={() => {
            const data = cloneDeep(this.state.interests);
            map(this.state.order, (order, index) => {
              data[index] = this.state.interests[order];
            });
            this.props.navigation.state.params.saveExtraProfileDetails(data, 'interests')}
          }
        />
        <View style={[profileStyles.fieldBox, { paddingVertical: 0, paddingTop: 20, paddingBottom: 10, flex: 0 }]}>
          <Text
            style={[
              profileStyles.boxHead,
              {
                color: 'rgb(83,87,94)',
                flex: 0
              }
            ]}
          >
            NEW INTEREST
          </Text>
          <View style={{ alignItems: 'center', flexDirection: 'row', marginTop: 10 }}>
            <TextInput
              style={[profileStyles.skillInp, { flex: 1 }]}
              placeholder={'Interest (ex: Economics)'}
              placeholderTextColor={'rgba(108,111,125,0.5)'}
              underlineColorAndroid={'rgba(0,0,0,0)'}
              value={this.state.interest}
              onChangeText={(text) => this.setState({interest: text})}
              maxLength={40}
            />
            <TouchableOpacity
              onPress={() => {this.addInterest({interestArea: this.state.interest})}}
              disabled={this.state.interest.trim() === ''}
            >
              <Text style={profileStyles.tagButtonText}>ADD</Text>
            </TouchableOpacity>
          </View>
        </View>
        {/* <View
          style={{
            height: height - 146,
            width: width,
            backgroundColor: 'rgb(250,251,252)'
          }}
        > */}
          <SortableListView style={{ flex: 1, backgroundColor: 'rgb(250,251,252)' }}
            data={this.state.interests}
            renderRow={(row, sectionId, rowIndex) => {
              return <Interest removeInterest={this.removeInterest} interest={row} sectionId={sectionId} rowId={rowIndex}/>
            }}
            order={this.state.order}
            onRowMoved={e => {
              this.state.order.splice(e.to, 0, this.state.order.splice(e.from, 1)[0])
              this.setState({
                order: this.state.order
              })
            }}
          >
          </SortableListView>
        </View>
      // </View>
    );
  }
}

class Interest extends Component<{
  sortHandlers?: any
  removeInterest: Function;
  interest: any;
  sectionId?: any;
  rowId?: any;
}, {}> {
  render() {
    return <TouchableHighlight
      style={{
        width: '100%',
        padding: 15,
        backgroundColor: 'white',
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderColor: 'rgb(233,239,247)'
      }}
      {...this.props.sortHandlers}
    >
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          flex: 1
        }}
      >
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <TouchableOpacity
            onPress={() => this.props.removeInterest(this.props.rowId)}
            style={{ paddingRight: 10 }}
          >
            <Icon name='md-remove-circle' style={{ color: '#C5132D', fontSize: 16 }} />
          </TouchableOpacity>
          <Text style={profileStyles.fieldDarkLow}>{this.props.interest ? this.props.interest.interestArea : ''}</Text>
        </View>
        <View>
          <Icon name='md-reorder' style={{ color: 'rgb(207, 208, 220)', fontSize: 20 }} />
        </View>
      </View>
    </TouchableHighlight>
  }
}
