import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  ScrollView,
  TouchableOpacity,
  Linking,
  Dimensions,
  ActivityIndicator
} from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
import { Icon } from 'native-base';
import AppButton from '../layouts/buttons/button';
import profileStyles from './profile-styles';
import ActionSheet from 'react-native-actionsheet';
import { connect } from 'react-redux';
import { get, map, isEmpty, filter } from 'lodash';
import { bindActionCreators } from 'redux';
import { departmentStyles } from '../university/department/department-styles';
import moment from 'moment';

import { style } from '../../styles/variables';

import { constants } from '../../constants'

import {
  getExtraProfileDetails,
  saveBasicProfileDetails,
  saveExtraProfileDetails,
  deleteExtraProfileDetails,
  uploadProfilePic
} from '../../store/actions/Profile';
import { DialogModal } from '../../store/actions';

import { getUniversityBySlug } from '../../store/actions/Universities';

const { width } = Dimensions.get('window');

const CANCEL_INDEX = 0;
const options = ['Cancel', 'Edit Profile', 'Settings', 'Notifications'];
let actionsheet = undefined;
const REMOVE_EDUCATION = 'Remove Education'
const REMOVE_EDUCATION_DESC = 'Are you sure to remove the education?'

const REMOVE_EXPERIENCE = 'Remove Experience'
const REMOVE_EXPERIENCE_DESC = 'Are you sure to remove the experience?'

interface IPeopleFilterNavParams {}
interface IPeopleFilterProps
  extends NavigationScreenProps<IPeopleFilterNavParams> {
    userInfo: {
      profilePic: string;
      firstName: string;
    };
    dialogModal: any;
    getExtraProfileDetails: Function;
    saveUserInfo: Function;
    saveExtraProfileDetails: Function;
    deleteExtraProfileDetails: Function;
    uploadProfilePic: Function;
    getUniversityBySlug;
  }
class MyProfile extends Component<IPeopleFilterProps> {
  scrollRef;
  constructor(props) {
    super(props);
    this.saveUserInfo = this.saveUserInfo.bind(this);
    this.saveExtraProfileDetails = this.saveExtraProfileDetails.bind(this);
    this.deleteExtraProfileDetails = this.deleteExtraProfileDetails.bind(this);
    this.confirmModal = this.confirmModal.bind(this);
  }
  showActionSheet() {
    actionsheet.show();
  }
  componentDidMount () {
    this.props.getExtraProfileDetails(get(this.props.userInfo, 'id', undefined));
  }
  saveUserInfo(userData: any) {
    this.props.saveUserInfo(userData);
  }
  saveProfileImage = (profilePic: any, userdata: any) => {
    console.log(profilePic, '1122')
    this.props.uploadProfilePic(profilePic, userdata)
  }
  saveExtraProfileDetails(userData: any, type: string) {
    return this.props.saveExtraProfileDetails(userData, type);
  }
  deleteExtraProfileDetails(id: string, type: string) {
    this.props.deleteExtraProfileDetails(id, type);
  }
  onProfilePicPressed = (image) => {
    let images = [];
    images.push({ originalUrl: image })
    this.props.dialogModal.dialogShows(
      'images',
      undefined,
      'image',
      true,
      undefined,
      undefined,
      undefined,
      undefined,
      images
    );
  }
  confirmModal(type: string, callback) {
    switch (type) {
      case 'education':
              this.props.dialogModal.dialogShows( REMOVE_EDUCATION, REMOVE_EDUCATION_DESC, 'action', true, () => callback());
              break;
      case 'workExperience':
              this.props.dialogModal.dialogShows( REMOVE_EXPERIENCE, REMOVE_EXPERIENCE_DESC, 'action', true, () => callback());
              break;
      default: break;
    }
  }

  scrollToPageEnd = () => {
    if (this.scrollRef) {
      // console.log(this.scrollRef, '121')
      this.scrollRef.scrollToEnd();
    }
  }

  render() {
    let skills = get(this.props.userInfo, 'skills', []);
    let interests = get(this.props.userInfo, 'areasOfInterest', []);
    let userTypes = get(this.props.userInfo, 'userTypes', []);
    if (!isEmpty(userTypes)) {
      userTypes = filter(userTypes, userTypeObj => userTypeObj.userType !== 'PROSPECTIVE_STUDENT')
    }
    const educations = get(this.props.userInfo, 'education', []);
    const researches = get(this.props.userInfo, 'research', []);
    const awards = get(this.props.userInfo, 'achievement', []);
    const volunteerings = get(this.props.userInfo, 'volunteerWork', []);
    return (
      <View style={{ flex: 1, backgroundColor: 'rgb(250,251,252)' }}>
        <ActionSheet
          ref={o => (actionsheet = o)}
          options={options}
          cancelButtonIndex={CANCEL_INDEX}
          onPress={i => {
            i === 2
              ? this.props.navigation.navigate('SettingsMain')
              : console.log(i);
            i === 1
              ? this.props.navigation.navigate('ProfileEdit',
                { userInfo: get(this.props, 'userInfo', {}), saveUserInfo: this.saveUserInfo, saveProfileImage: this.saveProfileImage}
              )
              : console.log(i);
            i === 3
              ? this.props.navigation.navigate('Notifications')
              : console.log(i);
          }}
        />
        <View style={profileStyles.headerContainer}>
          <View style={profileStyles.leftIconContainer}>
            <TouchableOpacity style={profileStyles.leftIconBtn} onPress={() => this.props.navigation.goBack()}>
              <Icon name='md-arrow-back' style={{ color: 'rgb(207, 208, 220)', fontSize: 30 }} />
            </TouchableOpacity>
          </View>
          <View style={profileStyles.centerHeaderContainer}>
            <Image
              source={
                {
                  uri: get(this.props.userInfo, 'profilePicture', undefined ) ?
                    get(this.props.userInfo, 'profilePicture')
                    : 'https://252radio.com/wp-content/uploads/2016/11/default-user-image-300x300.png'
                }
              }
              style={{ height: 20, width: 20, marginHorizontal: 6, borderRadius: 10 }}
            />
            <Text style={profileStyles.headerText} numberOfLines={1}>
              {get(this.props.userInfo, 'firstName', 'NA') + ' ' + get(this.props.userInfo, 'lastName', '')}
            </Text>
          </View>
          <View style={profileStyles.rightIconContainer}>
            <TouchableOpacity style={profileStyles.rightIconBtn} onPress={() => this.showActionSheet()}>
              <Icon name='ios-more' style={{ color: 'rgb(207, 208, 220)', fontSize: 30 }} />
            </TouchableOpacity>
          </View>
        </View>
        <ScrollView
          ref = {(r) => this.scrollRef ? undefined : this.scrollRef = r}
          showsVerticalScrollIndicator={false}
          style={{ backgroundColor: 'white' }}
        >
          <View style={{ paddingHorizontal: 15, borderColor: 'rgb(233,238,241)', borderBottomWidth: 1, paddingVertical: 15 }}>
            <View style={{ flexDirection: 'row' }}>
              <TouchableOpacity
                onPress={() => this.onProfilePicPressed(get(this.props.userInfo, 'profilePicture', undefined ))}
              >
                <Image
                  source={{
                    uri: get(this.props.userInfo, 'profilePicture', undefined ) ?
                    get(this.props.userInfo, 'profilePicture')
                    : 'https://252radio.com/wp-content/uploads/2016/11/default-user-image-300x300.png'
                  }}
                  style={{ height: 76, width: 76, borderRadius: 38, marginRight: 15 }}
                />
              </TouchableOpacity>
              <View style={{flexWrap: 'wrap', width: width - 106}}>
                <Text style={profileStyles.nameText}>
                  {get(this.props.userInfo, 'firstName', 'NA') + ' ' + get(this.props.userInfo, 'lastName', '')}
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    marginTop: 7,
                    alignItems: 'center'
                  }}
                >
                  <Icon
                    name={'pin'}
                    style={{ color: 'rgb(207,208,220)', fontSize: 23, top: 2 }}
                  />
                  <Text style={profileStyles.countryText}>
                    {get(this.props.userInfo, 'country', 'NA') }
                  </Text>
                </View>
              </View>
            </View>
            <Text style={[profileStyles.bioText, { paddingTop: 11 }]}>
              {get(this.props.userInfo, 'bio', 'NA')}
            </Text>
          </View>
          { !isEmpty(userTypes) &&
            <View style={profileStyles.educationTextContainer}>
              { map(userTypes, (item, index) => {
                return (
                  <Text style={[ profileStyles.bioText, { color: 'rgb(59, 62, 68)'} ]} key={index}>
                    <Text>{constants.USERTYPES.CONTEXT[item.userType] + ' '}</Text>
                    <Text
                      style={{ color: 'rgb(53, 135, 230)' }}
                      onPress={() => {
                        this.props.getUniversityBySlug(item.universitySlug)
                        this.props.navigation.navigate('UniversityProfile')
                      }}
                    >
                      {item.universityName}
                    </Text>
                  </Text>
                )
              }
              )}
            </View>
          }
          <View style={[profileStyles.statsContainer, {flexDirection: 'row', justifyContent: 'space-between'} ]}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('PeopleFollowerList', {
                  title: 'Followers',
                  userType: 'loggedInUser'
                  }
                )}
              }
            >
              <Text style={profileStyles.statNmbrText}>
              {get(this.props.userInfo, 'followers', []).length}
              </Text>
              <Text style={profileStyles.statNameText}>Followers</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('PeopleFollowerList', {
                  userType: 'loggedInUser',
                  title: 'following'
                  }
                )}
              }
            >
              <Text style={profileStyles.statNmbrText}>
              {get(this.props.userInfo, 'following', []).length}
              </Text>
              <Text style={profileStyles.statNameText}>Following</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('PeopleFollowerList', {
                  userType: 'loggedInUser',
                  title: 'connections'
                  }
                )}
              }
            >
              <Text style={profileStyles.statNmbrText}>
              {get(this.props.userInfo, 'connections', []).length}
              </Text>
              <Text style={profileStyles.statNameText}>Connections</Text>
            </TouchableOpacity>
            <View style={{
              width: 60
            }}>
              <AppButton
                onPress={() =>
                  this.props.navigation.navigate(
                    'ProfileEdit',
                    { userInfo: get(this.props, 'userInfo', {}), saveUserInfo: this.saveUserInfo, saveProfileImage: this.saveProfileImage}
                  )
                }
                type={'inverted'}
                bordered={true}
                label={'Edit'}
                customButtonStyle={{ marginVertical: 0, paddingHorizontal: 12, paddingVertical: 7, flexDirection: 'row' }}
                textFamily={'Rubik-Regular'}
                textSize={12}
                containerWidth={'100%'}
                enableImage={true}
              />
              {/* <Image
                source={require('../../../assets/app_icons/Icons/arrowhead-down-default.png')}
                style={{ position: 'absolute', right: 5, tintColor: 'rgb(53, 135, 230)' }}
              /> */}
            </View>
          </View>
          <View style={profileStyles.fieldBox}>
            <View style={{ flexDirection: 'row' }}>
              <Text style={profileStyles.boxHead}>WEBSITE</Text>
            </View>
            {get(this.props.userInfo, 'website.length', 0) !== 0 &&
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Icon name='md-link' style={{ color: '#2E7CE2', fontSize: 20, marginTop: 16, marginRight: 10 }} />
                <Text style={profileStyles.boxLow} onPress={() => Linking.openURL(get(this.props.userInfo, 'website', 'NA'))
                .catch(err => console.error('An error occurred', err))}>
                {get(this.props.userInfo, 'website', 'NA')}
                </Text>
              </View>
            }
          </View>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('ProfileSkills',
                {skills: get(this.props.userInfo, 'skills', []), saveExtraProfileDetails: this.saveExtraProfileDetails})}
            style={profileStyles.fieldBox}
          >
            <View style={{ flexDirection: 'row' }}>
              <Text style={profileStyles.boxHead}>SKILLS</Text>
              <Icon name='md-add' style={{ color: 'rgb(207, 208, 220)', fontSize: 23 }} />
            </View>
            <View style={{ paddingTop: 13, flexDirection: 'row', flexWrap: 'wrap' }}>
            {
              skills.map((item, index) => {
                return (
                  <View style={departmentStyles.tagsBlock} key={index}>
                    <Text style={departmentStyles.tags}>{item.skills}</Text>
                  </View>
                )
              })
            }
          </View>
          </TouchableOpacity>
          <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('ProfileInterests',
                {interests: get(this.props.userInfo, 'areasOfInterest', []), saveExtraProfileDetails: this.saveExtraProfileDetails})}
              style={profileStyles.fieldBox}
          >
            <View style={{ flexDirection: 'row' }}>
              <Text style={profileStyles.boxHead}>INTERESTS</Text>
              <Icon name='md-add' style={{ color: 'rgb(207, 208, 220)', fontSize: 23 }} />
            </View>
            <View style={{ paddingTop: 13, flexDirection: 'row', flexWrap: 'wrap' }}>
              {
                interests.map((item, index) => {
                  return (
                    <View style={departmentStyles.tagsBlock} key={index}>
                      <Text style={departmentStyles.tags}>{item.interestArea}</Text>
                    </View>
                  )
                })
              }
            </View>
          </TouchableOpacity>
          <View style={[profileStyles.fieldBox]}>
            <View style={{ flexDirection: 'row' }}>
              <Text style={profileStyles.boxHead}>
                EDUCATION
              </Text>
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate('ProfileEducationHist',
                    {education: get(this.props.userInfo, 'education', []),
                      saveExtraProfileDetails:
                      this.saveExtraProfileDetails, deleteHistory: this.deleteExtraProfileDetails, confirmModal: this.confirmModal})
                }
              >
                <Icon name='md-add' style={{ color: 'rgb(207, 208, 220)', fontSize: 23 }} />
              </TouchableOpacity>
            </View>
            {
              map(educations, (education, index) => {
                return (
                  <View style={[profileStyles.educationFieldBox, { borderBottomWidth: (educations.length - 1 === index) ? 0 : 1 },
                    {paddingBottom: (educations.length - 1 === index) ? 0 : 15}]} key={index}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                      <Text style={profileStyles.fieldBigHead}>
                        {education.universityName}
                      </Text>
                    </View>
                    <Text style={profileStyles.fieldDarkLow}>
                      {education.major + '-' + education.degree}
                    </Text>
                    <Text style={[profileStyles.fieldDarkLow, { marginTop: 4 }]}>
                      { moment(education.startDate).format('MMM Y') + '-' +
                        (education.present ? 'Present' : moment(education.endDate).format('MMM Y'))
                      }
                    </Text>
                  </View>
                )
              })
            }
          </View>
          <View style={profileStyles.fieldBox}>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('ProfileExperience',
                  {experience: get(this.props.userInfo, 'workExps', []),
                    saveExtraProfileDetails:
                    this.saveExtraProfileDetails, deleteExps: this.deleteExtraProfileDetails, confirmModal: this.confirmModal})}
            >
              <View style={{ flexDirection: 'row' }}>
                <Text style={profileStyles.boxHead}>
                  EXPERIENCE
                </Text>
                <Icon name='md-add' style={{ color: 'rgb(207, 208, 220)', fontSize: 23 }} />
              </View>
            </TouchableOpacity>
            { get(this.props.userInfo, 'workExps.length', 0) !== 0 &&
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                contentContainerStyle={profileStyles.expScroll}
              >
              {map(get(this.props.userInfo, 'workExps', []), (workExp, index) => {
                return (
                  <View style={profileStyles.expCard} key={index}>
                    <Text style={profileStyles.expCardBigText}>{workExp.organization}</Text>
                    <Text style={[profileStyles.expCardSmallText]}>
                      {workExp.designation}
                    </Text>
                    {!isEmpty(workExp.location) &&
                      <Text style={[profileStyles.expCardSmallText]}>
                        {workExp.location}
                      </Text>
                    }
                    <Text style={[profileStyles.expCardSmallText]}>
                      {moment(workExp.startDate).format('MMM Y') + '-' +
                        (workExp.present ? 'Present' : moment(workExp.endDate).format('MMM Y'))
                      }
                    </Text>
                    {!isEmpty(workExp.description) &&
                      <Text style={[profileStyles.expCardSmallText]}>
                        {workExp.description}
                      </Text>
                    }
                  </View>
                );
              })}
              </ScrollView>
            }
          </View>
          <View style={profileStyles.fieldBox}>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('ProfileMembership',
                  {membership: get(this.props.userInfo, 'membership', []), type: 'Membership',
                    saveExtraProfileDetails: this.saveExtraProfileDetails, deleteMembership: this.deleteExtraProfileDetails})}
            >
              <View style={{ flexDirection: 'row' }}>
                <Text style={profileStyles.boxHead}>
                  MEMBERSHIPS & AFFILIATIONS
                </Text>
                <Icon name='md-add' style={{ color: 'rgb(207, 208, 220)', fontSize: 23 }} />
              </View>
            </TouchableOpacity>
            {
              map(get(this.props.userInfo, 'membership', []), (membership, index) => {
                return (
                  <View key={index}>
                    <Text style={[profileStyles.boxLow, { color: 'rgb(153,46,46)' }]}>
                      {membership.organization}
                    </Text>
                    <Text style={profileStyles.expCardSmallText}>
                      {membership.title}
                    </Text>
                    <Text style={[profileStyles.expCardSmallText]}>
                      {moment(membership.startDate).format('MMM Y') + '-' +
                        (membership.present ? 'Present' : moment(membership.endDate).format('MMM Y'))
                      }
                    </Text>
                    {!isEmpty(membership.description) &&
                      <Text style={[profileStyles.expCardSmallText]}>
                        {membership.description}
                      </Text>
                    }
                  </View>
                );
              })
            }
          </View>
          <View style={profileStyles.fieldBox}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('ProfileProject',
                {project: get(this.props.userInfo, 'research', []),
                  saveExtraProfileDetails: this.saveExtraProfileDetails, deleteProject: this.deleteExtraProfileDetails})}
            >
              <View style={{ flexDirection: 'row' }}>
                <Text style={profileStyles.boxHead}>PROJECTS & RESEARCH</Text>
                <Icon name='md-add' style={{ color: 'rgb(207, 208, 220)', fontSize: 23 }} />
              </View>
            </TouchableOpacity>
              {
                map(researches, (research, index) => {
                  return (
                    <View style={[profileStyles.educationFieldBox,
                      { borderBottomWidth: (researches.length - 1 === index) ? 0 : 1 },
                      { paddingBottom: (researches.length - 1 === index) ? 0 : 15 },
                      {paddingTop: 0}]} key={index}>
                      <TouchableOpacity
                          onPress={() => {
                            if (research.projectUrl) {
                              Linking.openURL(research.projectUrl).catch(() => alert('Unable to open link'))}
                            }
                        }
                      >
                        <Text style={[profileStyles.boxLow, { color: 'rgb(153,46,46)' }]}>
                          {research.title}
                        </Text>
                        <Text style={profileStyles.expCardSmallText}>
                        {moment(research.startDate).format('MMM Y') + '-' + moment(research.endDate).format('MMM Y')}
                        </Text>
                        {!isEmpty(research.description) &&
                          <Text style={profileStyles.expCardSmallText}>
                            {research.description}
                          </Text>
                        }
                      </TouchableOpacity>
                    </View>
                  )
                })
              }
            </View>
            <View style={profileStyles.fieldBox}>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('ProfileAward',
                  {awards: get(this.props.userInfo, 'achievement', []),
                    saveExtraProfileDetails: this.saveExtraProfileDetails, deleteAward: this.deleteExtraProfileDetails})}
              >
                <View style={{ flexDirection: 'row' }}>
                  <Text style={profileStyles.boxHead}>AWARDS & RECOGNIZATIONS</Text>
                  <Icon name='md-add' style={{ color: 'rgb(207, 208, 220)', fontSize: 23 }} />
                </View>
              </TouchableOpacity>
            {
              map(awards, (award, index) => {
                return (
                  <View style={[profileStyles.educationFieldBox,
                    { borderBottomWidth: (awards.length - 1 === index) ? 0 : 1 },
                    { paddingBottom: (awards.length - 1 === index) ? 0 : 15 }]} key={index}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                      <Text style={[profileStyles.fieldBigHead]}>{award.award}</Text>
                    </View>
                    {!isEmpty(award.description) &&
                      <Text style={[profileStyles.fieldDarkLow, {marginBottom: 5}]}>
                        {award.description}
                      </Text>
                    }
                    <Text style={[profileStyles.fieldDarkLow]}>
                      {(award.date) ? moment(award.date).format('Y') : 'NA'}
                    </Text>
                  </View>
                )
              })
            }
          </View>
          <View style={profileStyles.fieldBox}>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('ProfileMembership',
                  {membership: get(this.props.userInfo, 'volunteerWork', []), type: 'volunteer',
                    saveExtraProfileDetails: this.saveExtraProfileDetails, deleteMembership: this.deleteExtraProfileDetails,
                    scrollToBottom: () => this.scrollToPageEnd()
                  })}
            >
              <View style={{ flexDirection: 'row' }}>
                <Text style={profileStyles.boxHead}>VOLUNTEERING</Text>
                <Icon name='md-add' style={{ color: 'rgb(207, 208, 220)', fontSize: 23 }} />
              </View>
            </TouchableOpacity>
            {
              map(volunteerings, (volunteer, index) => {
                return (
                  <View style={[profileStyles.educationFieldBox,
                    { borderBottomWidth: (volunteerings.length - 1 === index) ? 0 : 1, paddingTop: 0 },
                    { paddingBottom: (volunteerings.length - 1 === index) ? 0 : 15 }]} key={index}>
                    <Text style={[profileStyles.boxLow, { color: 'rgb(153,46,46)', marginBottom: 4 }]}>
                      {volunteer.organization}
                    </Text>
                    <Text style={[profileStyles.fieldDarkLow, {marginBottom: 4}]}>
                      {volunteer.title}
                    </Text>
                    <Text style={[profileStyles.fieldDarkLow, {marginTop: 1, marginBottom: 4}]}>
                      {moment(volunteer.startDate).format('MMM Y') + '-' +
                        (volunteer.present ? 'Present' : moment(volunteer.endDate).format('MMM Y'))
                      }
                    </Text>
                    {!isEmpty(volunteer.description) &&
                      <Text style={profileStyles.fieldDarkLow}>
                        {volunteer.description}
                      </Text>
                    }
                  </View>
                )
              })
            }
          </View>
        </ScrollView>
        {!this.props.userInfo.firstName &&
          <View style={profileStyles.loaderHolder}>
            <ActivityIndicator animating={!this.props.userInfo.firstName} size='large' color={style.color.lightGrey}/>
          </View>
        }
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    userInfo: state.userInfoAPI.userInfo
  };
};

const mapDispatchToProps = dispatch => ({
  getExtraProfileDetails: bindActionCreators(getExtraProfileDetails, dispatch),
  saveUserInfo: bindActionCreators(saveBasicProfileDetails, dispatch),
  saveExtraProfileDetails: bindActionCreators(saveExtraProfileDetails, dispatch),
  deleteExtraProfileDetails: bindActionCreators(deleteExtraProfileDetails, dispatch),
  uploadProfilePic: bindActionCreators(uploadProfilePic, dispatch),
  getUniversityBySlug: bindActionCreators(getUniversityBySlug, dispatch),
  dialogModal: bindActionCreators(DialogModal, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(MyProfile);
