import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Alert,
  TextInput,
  Switch,
  Platform
} from 'react-native';
import DatePicker from 'react-native-datepicker';
import { NavigationScreenProps } from 'react-navigation';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import MSAHeader from '../layouts/header/header';
import { get } from 'lodash';
import moment from 'moment';
import profileStyles from './profile-styles';
import { style } from '../../styles/variables';
import { transformDate } from '../../utils';
interface State {
  membership: Membership;
}
interface Membership {
  id?: string,
  membershipId?: string;
  volunteerId?: string;
  organization: string,
  role: string,
  description: string,
  startDate: string;
  endDate: string;
  startMonth: number;
  startYear: number;
  endMonth: number;
  endYear: number;
  title: string;
  present: boolean;
}
interface IPeopleFilterNavParams {
  addMembership?: Function
  membership?: Membership
  deleteMembership: Function
}
interface IPeopleFilterProps
  extends NavigationScreenProps<IPeopleFilterNavParams> {
  }
export default class ProfileNewMembership extends Component<IPeopleFilterProps, State> {
  constructor(props: IPeopleFilterProps) {
    super(props);
    const {
      volunteerId,
      membershipId,
      organization,
      description,
      title,
      startDate,
      endDate,
      present
    } = props.navigation.state.params.membership;
    const type = get(this.props.navigation.state.params, 'type', 'Membership')
    this.state = {
      membership: {
        id: (type === 'Membership') ? ((membershipId) ? membershipId : '') : (volunteerId) ? volunteerId : '',
        organization: (organization) ? organization : '',
        role: (title) ? title : '',
        title: (title) ? title : '',
        description: (description) ? description : '',
        present: (present) ? present : false,
        startDate: (startDate) ? moment(startDate).format('MMM Y') : moment().format('MMM Y'),
        endDate: (endDate) ? moment(endDate).format('MMM Y') : moment().format('MMM Y'),
        startMonth: (startDate) ? moment(startDate).month() + 1 : moment().month() + 1,
        startYear: (startDate) ? moment(startDate).year() : moment().year(),
        endMonth: (endDate) ? moment(endDate).month() + 1 : moment().month() + 1,
        endYear: (endDate) ? moment(endDate).year() : moment().year()
      }
    }
  }
  onChange(text: string, field: string) {
    let newText = text.split(' ')
    if (field === 'role') {
      this.state.membership.title = text;
    }
    let temp = this.state.membership[field];
    this.state.membership[field] = text;
    if (field === 'startDate' || field === 'endDate') {
      if (moment(transformDate(this.state.membership.endDate)) <
          moment(transformDate(this.state.membership.startDate))) {
        this.state.membership[field] = temp;
        setTimeout(() => {
          Alert.alert('To date should be greater than From date');
        }, 500);
      } else {
        if (field === 'startDate') {
          this.state.membership.startMonth = parseInt(moment().month(newText[0]).format('M'), 10)
          this.state.membership.startYear = parseInt(newText[1], 10)
        } else if (field === 'endDate') {
          this.state.membership.endMonth = parseInt(moment().month(newText[0]).format('M'), 10)
          this.state.membership.endYear = parseInt(newText[1], 10)
        }
        this.setState({
          membership: this.state.membership
        })
      }
    } else {
      this.setState({
        membership: this.state.membership
      })
    }
  }
  render() {
    const type = get(this.props.navigation.state.params, 'type', 'Membership');
    return (
      <View style={{ backgroundColor: 'rgb(250,251,252)', flex: 1 }}>
        <MSAHeader
          title={type === 'Membership' ? 'New Membership' : 'Volunteering'}
          rightText={'SAVE'}
          leftText={'CANCEL'}
          onPress={() => this.props.navigation.goBack()}
          onRightTextPress={() => this.props.navigation.state.params.addMembership(this.state.membership)}
        />
        <KeyboardAwareScrollView>
          <View style={{ backgroundColor: 'white' }}>
            <View style={profileStyles.fieldBox}>
              <Text
                style={[profileStyles.dualHeaderLeftText, { marginBottom: 15, fontSize: 12, lineHeight: 14 }]}
              >
                ORGANIZATION / COMPANY
              </Text>
              <TextInput
                style={[profileStyles.skillInp, { color: 'rgb(83,87,94)' }]}
                placeholder={type === 'Membership' ? 'Ex: Google' : 'UNESCO'}
                placeholderTextColor={'rgba(108,111,125,0.5)'}
                underlineColorAndroid={'rgba(0,0,0,0)'}
                value={this.state.membership.organization}
                onChangeText={(text) => this.onChange(text, 'organization')}
                maxLength={128}
              />
            </View>
            <View style={profileStyles.fieldBox}>
              <Text
                style={[profileStyles.dualHeaderLeftText, { marginBottom: 15, fontSize: 12, lineHeight: 14 }]}
              >
                ROLE
              </Text>
              <TextInput
                style={[profileStyles.skillInp, { color: 'rgb(83,87,94)' }]}
                placeholder={type === 'Membership' ? 'Ex: Member, Treasurer' : 'Volunteer'}
                placeholderTextColor={'rgba(108,111,125,0.5)'}
                underlineColorAndroid={'rgba(0,0,0,0)'}
                value={this.state.membership.role}
                onChangeText={(text) => this.onChange(text, 'role')}
                maxLength={128}
              />
            </View>
            <View style={profileStyles.fieldBox}>
              <Text
                style={[profileStyles.dualHeaderLeftText, { marginBottom: 15, fontSize: 12, lineHeight: 14 }]}
              >
                DESCRIPTION
              </Text>
              <TextInput
                style={[profileStyles.skillInp, { color: 'rgb(83,87,94)' }]}
                placeholder={type === 'Membership' ? 'Ex: Briefly describe your association or activities' : 'Describe to explain more'}
                placeholderTextColor={'rgba(108,111,125,0.5)'}
                underlineColorAndroid={'rgba(0,0,0,0)'}
                value={this.state.membership.description}
                onChangeText={(text) => this.onChange(text, 'description')}
                multiline={true}
                numberOfLines={5}
              />
            </View>
            <View style={[ profileStyles.fieldBox, { flexDirection: 'row' }]}>
              <View style={{ flex: 1 }}>
                <Text
                  style={[profileStyles.dualHeaderLeftText, { marginBottom: 15, fontSize: 12, lineHeight: 14 }]}
                >
                  FROM
                </Text>
                <DatePicker
                  style={{ width: 80 }}
                  date={this.state.membership.startDate}
                  mode={'date'}
                  format={'MMM Y'}
                  confirmBtnText={'Confirm'}
                  cancelBtnText={'Cancel'}
                  showIcon={false}
                  onDateChange={(date) => this.onChange(date, 'startDate')}
                  customStyles={{
                    dateInput: {
                      height: 28,
                      width: 80,
                      borderColor: 'rgb(53,135,230)',
                      borderRadius: 5
                    },
                    dateText: { color: 'rgb(53,135,230)', fontSize: 14, lineHeight: 18 }
                  }}
                />
              </View>
              <View style={{ flex: 1 }}>
                <Text
                  style={[profileStyles.dualHeaderLeftText, { marginBottom: 15, fontSize: 12, lineHeight: 14 }]}
                >
                  TO
                </Text>
                { this.state.membership.present ?
                  <View style={{ height: 40, justifyContent: 'center' }}>
                    <Text style={[profileStyles.presentText, { fontSize: 14 }]}>Present</Text>
                  </View>
                  : <DatePicker
                    style={{ width: 80 }}
                    mode={'date'}
                    date={this.state.membership.endDate}
                    format={'MMM Y'}
                    confirmBtnText={'Confirm'}
                    cancelBtnText={'Cancel'}
                    showIcon={false}
                    onDateChange={(date) => this.onChange(date, 'endDate')}
                    customStyles={{
                      dateInput: {
                        height: 28,
                        width: 80,
                        borderColor: 'rgb(53,135,230)',
                        borderRadius: 5
                      },
                      dateText: { color: 'rgb(53,135,230)', fontSize: 14, lineHeight: 18 }
                    }}
                  />
                }
              </View>
            </View>
            <View style={profileStyles.presentBtnContainer}>
              <View style={{flex: 1}} />
              <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start'}}>
                <Switch
                  onValueChange={(bool) => {
                    let membership = this.state.membership;
                    membership.present = bool;
                    this.setState({ membership })
                  }}
                  value={this.state.membership.present}
                  thumbTintColor={style.color.primary}
                  onTintColor={style.color.cream}
                  style={{
                    transform: [{ scaleX: Platform.OS === 'ios' ? .7 : 1 },
                      { scaleY: Platform.OS === 'ios' ? .7 : 1 }] }}
                />
                <Text style={profileStyles.presentText}>Present</Text>
              </View>
            </View>
          </View>
          {this.state.membership.id ? <TouchableOpacity
            style={{ width: '100%', alignItems: 'center', padding: 10, backgroundColor: 'white' }}
            onPress={() => {
              // this.props.navigation.goBack()
              this.props.navigation.state.params.deleteMembership(this.state.membership.id)}}
              >
            <Text style={{ color: '#CC1433' }}>Delete</Text>
          </TouchableOpacity> : undefined}
        </KeyboardAwareScrollView>
      </View>
    );
  }
}
