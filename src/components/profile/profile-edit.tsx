import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  TextInput,
  Platform,
  Dimensions
} from 'react-native';
import MSAHeader from '../layouts/header/header';
import { NavigationScreenProps } from 'react-navigation';
import ActionSheet from 'react-native-actionsheet';
import ImagePicker from 'react-native-image-crop-picker';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { get, isEmpty } from 'lodash';
import profileStyles from './profile-styles';
import { style } from '../../styles/variables';
import { cloneDeep, isEqual } from 'lodash';

const { width } = Dimensions.get('window');
let actionsheet = undefined;
let profilePicOptions = ['Cancel', 'Choose an image', 'Take a picture'];

interface IPeopleFilterState {
  firstName: string;
  lastName: string;
  website: string;
  bio: string;
  country: string;
  profilePicture: string;
  profilePic: any;
}
interface IPeopleFilterNavParams {
  saveUserInfo: Function;
  saveProfileImage: Function;
}
interface IPeopleFilterProps
  extends NavigationScreenProps<IPeopleFilterNavParams> {}

let prevDetails = undefined;
export default class HomeFeedsEmptyState extends Component<
  IPeopleFilterProps,
  IPeopleFilterState
> {
  prevDetails;
  constructor(props: IPeopleFilterProps) {
    super(props);
    this.state = {
      firstName: get(props.navigation.state.params, 'userInfo.firstName', ''),
      lastName: get(props.navigation.state.params, 'userInfo.lastName', ''),
      bio: get(props.navigation.state.params, 'userInfo.bio', ''),
      country: get(props.navigation.state.params, 'userInfo.country', ''),
      website: get(props.navigation.state.params, 'userInfo.website', ''),
      profilePicture: get(
        props.navigation.state.params,
        'userInfo.profilePicture',
        ''
      ),
      profilePic: undefined
    };
    prevDetails = cloneDeep(this.state);
  }

  // componentDidMount() {
  //   console.log(this.state, 'State')
  //   this.prevDetails = this.state
  // }

  changedText(text, field) {
    this.setState({ [field]: text });
  }

  onSavePressed = () => {
    let userInfo = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      bio: this.state.bio,
      country: this.state.country,
      website: this.state.website ? this.state.website.trim() : ''
    };
    if (this.state.profilePic) {
      this.props.navigation.state.params.saveProfileImage(
        this.state.profilePic,
        userInfo
      );
    } else {
      this.props.navigation.state.params.saveUserInfo(userInfo);
    }
  };

  openGallery = () => {
    ImagePicker.openPicker({
      cropping: true,
      compressImageQuality: Platform.OS === 'ios' ? 0 : 1
    })
      .then(image => {
        let imageObj = {
          uri: get(image, 'path', ''),
          name: get(
            image,
            Platform.OS === 'ios' ? 'creationDate' : 'modificationDate',
            ''
          ),
          type: get(image, 'mime', '')
        };
        this.setState({
          profilePic: imageObj,
          profilePicture: get(image, 'path', '')
        });
      })
      .catch(Error);
  };

  openCamera = () => {
    ImagePicker.openCamera({
      cropping: true
    })
      .then(image => {
        let imageObj = {
          uri: get(image, 'path', ''),
          name: new Date().getTime().toString(),
          type: get(image, 'mime', '')
        };
        this.setState({
          profilePic: imageObj,
          profilePicture: get(image, 'path', '')
        });
      })
      .catch(e => {
        console.log(e, 'Error');
      });
  };

  _handleActionSheetPressed = choosedIndex => {
    if (choosedIndex === 1) {
      this.openGallery();
    } else if (choosedIndex === 2) {
      this.openCamera();
    }
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ActionSheet
          ref={o => (actionsheet = o)}
          options={profilePicOptions}
          cancelButtonIndex={0}
          onPress={i => this._handleActionSheetPressed(i)}
        />
        <MSAHeader
          title={'Edit Profile'}
          onPress={() => this.props.navigation.goBack()}
          rightText={'SAVE'}
          onRightTextPress={this.onSavePressed}
          disableRightText={isEqual(this.state, prevDetails)}
          rightTextColor={
            isEqual(this.state, prevDetails)
              ? style.color.bluishGrey
              : style.color.primary
          }
        />
        <KeyboardAwareScrollView
          style={{ backgroundColor: 'rgb(250,251,252)' }}
        >
          {Platform.OS === 'ios' ? (
            <View
              style={{
                height: 14,
                alignSelf: 'stretch',
                backgroundColor: 'white'
              }}
            />
          ) : (
            undefined
          )}
          <View style={{ backgroundColor: 'white' }}>
            <View style={[profileStyles.fieldBox, { alignItems: 'center' }]}>
              <Image
                source={
                  !isEmpty(this.state.profilePicture)
                    ? { uri: this.state.profilePicture }
                    : require('../../../assets/images/user_dummy_image.png')
                }
                style={{ height: 76, width: 76, borderRadius: 38 }}
              />
              <TouchableOpacity
                style={{ paddingTop: 10, paddingBottom: 5 }}
                onPress={() => actionsheet.show()}
              >
                <Text
                  style={[
                    profileStyles.dualHeaderRightText,
                    { fontSize: 12, lineHeight: 14 }
                  ]}
                >
                  CHANGE PICTURE
                </Text>
              </TouchableOpacity>
            </View>
            <View style={[profileStyles.fieldBox, { paddingBottom: 11 }]}>
              <Text style={[profileStyles.boxHead, { color: 'rgb(83,87,94)' }]}>
                FIRST NAME
              </Text>
              <TextInput
                style={[
                  profileStyles.boxLow,
                  {
                    color: 'rgb(83,87,94)',
                    width: '100%',
                    marginTop: 15,
                    marginBottom: 0,
                    lineHeight: 18
                  }
                ]}
                placeholder={'Jasbir'}
                value={this.state.firstName}
                onChangeText={text => this.changedText(text, 'firstName')}
                maxLength={25}
              />
            </View>
            <View style={[profileStyles.fieldBox, { paddingBottom: 11 }]}>
              <Text style={[profileStyles.boxHead, { color: 'rgb(83,87,94)' }]}>
                LAST NAME
              </Text>
              <TextInput
                style={[
                  profileStyles.boxLow,
                  {
                    color: 'rgb(83,87,94)',
                    width: width - 30,
                    marginTop: 15,
                    marginBottom: 0,
                    lineHeight: 18
                  }
                ]}
                placeholder={'Shergill'}
                value={this.state.lastName}
                onChangeText={text => this.changedText(text, 'lastName')}
                maxLength={25}
              />
            </View>
            <View style={[profileStyles.fieldBox, { paddingBottom: 11 }]}>
              <Text style={[profileStyles.boxHead, { color: 'rgb(83,87,94)' }]}>
                BIO
              </Text>
              <TextInput
                style={[
                  profileStyles.skillInp,
                  {
                    color: 'rgb(83,87,94)',
                    marginTop: 15,
                    marginBottom: 0,
                    lineHeight: 18,
                    maxHeight: 105
                  }
                ]}
                placeholder={'Tap to enter text'}
                value={this.state.bio ? this.state.bio : ''}
                onChangeText={text => this.changedText(text, 'bio')}
                placeholderTextColor={'rgb(173,181,196)'}
                underlineColorAndroid={'rgba(0,0,0,0)'}
                maxLength={400}
                multiline={true}
                numberOfLines={6}
              />
            </View>
            <View style={[profileStyles.fieldBox, { paddingBottom: 11 }]}>
              <Text style={[profileStyles.boxHead, { color: 'rgb(83,87,94)' }]}>
                WEBSITE
              </Text>
              <TextInput
                style={[
                  profileStyles.boxLow,
                  {
                    color: 'rgb(83,87,94)',
                    width: width - 30,
                    marginBottom: 0,
                    marginTop: 15
                  }
                ]}
                placeholder={
                  'https://app.zeplin.io/project/5a8dbd86642280fb4a5bfea1/screen/5a8dbf13f06c0ea64a945f6a'
                }
                value={this.state.website}
                onFocus={() => {
                  if (this.state.website === '') {
                    this.changedText('https://', 'website');
                  }
                }}
                onChangeText={text => this.changedText(text, 'website')}
                autoCorrect={false}
                autoCapitalize={'none'}
              />
            </View>
            <View style={[profileStyles.fieldBox, { paddingBottom: 11 }]}>
              <Text style={[profileStyles.boxHead, { color: 'rgb(83,87,94)' }]}>
                COUNTRY
              </Text>
              <Text
                style={[
                  profileStyles.boxLow,
                  {
                    color: 'rgb(83,87,94)',
                    width: width - 30,
                    marginTop: 15,
                    marginBottom: 0,
                    lineHeight: 18
                  }
                ]}
              >
                {this.state.country}
              </Text>
            </View>
          </View>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}
