import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
import MSAHeader from '../layouts/header/header';
import { get, map, findIndex, cloneDeep, isEmpty } from 'lodash';
import { transformDate } from '../../utils';
import moment from 'moment';
import profileStyles from './profile-styles';
interface State {
  experiences: any[]
}
interface IPeopleFilterNavParams {
  saveExtraProfileDetails: Function;
  deleteExps: Function;
  confirmModal: Function;
}
interface IPeopleFilterProps
  extends NavigationScreenProps<IPeopleFilterNavParams> {}
export default class ProfilExperience extends Component<IPeopleFilterProps, State> {
  constructor(props) {
    super(props);
    this.state = {
        experiences: cloneDeep(get(this.props.navigation, 'state.params.experience', []))
    }
    this.addExperience = this.addExperience.bind(this);
    this.deleteExps = this.deleteExps.bind(this);
  }
  addExperience(experience: any) {
    this.props.navigation.state.params.saveExtraProfileDetails(experience, 'workExperience').then((profileDetails) => {
      if (experience.id) {
        let index = findIndex(this.state.experiences, data => data.workId === experience.id);
        if (index > -1) {
          experience.workId = experience.id;
          this.state.experiences[index] = experience;
          this.setState({
            experiences: this.state.experiences
          })
        }
      } else if (profileDetails && profileDetails.workExps) {
        this.setState({
          experiences: profileDetails.workExps
        })
      }
    })
    .catch(e => {
      console.log(e, '!!!!')
    })
  }
  deleteExps(id: string) {
    this.props.navigation.state.params.confirmModal('workExperience', () => {
      let index = findIndex(this.state.experiences, data => data.workId === id);
      this.props.navigation.state.params.deleteExps(id, 'workExperience')
      if (index > -1) {
        this.state.experiences.splice(index, 1);
      }
      this.setState({
        experiences: this.state.experiences
      })
    })
  }
  render() {
    return (
      <View style={{ backgroundColor: 'white', flex: 1 }}>
        <MSAHeader
          title={'Experience'}
          rightText={'DONE'}
          leftText={'CANCEL'}
          onPress={() => this.props.navigation.goBack()}
          onRightTextPress={() => this.props.navigation.goBack()}
        />
        <View style={[profileStyles.fieldBox, { justifyContent: 'center' }]}>
          <Text style={[profileStyles.fieldDarkLow, { fontSize: 16, lineHeight: 21 }]}>
            Add Experience
          </Text>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('ProfileNewExperience', {addExperience: this.addExperience, experience: {}})
            }
            style={{ position: 'absolute', right: 15 }}
          >
            <Text style={profileStyles.tagButtonText}>ADD</Text>
          </TouchableOpacity>
        </View>
        <ScrollView style={{ backgroundColor: 'rgb(250,251,252)' }}>
          {
            map(this.state.experiences, (experience, index) => {
              if ( typeof experience.startDate === 'string') {
                experience.startDate = parseInt(moment(transformDate(experience.startDate)).format('x'), 10)
              }
              if ( typeof experience.endDate === 'string') {
                experience.endDate = parseInt(moment(transformDate(experience.endDate)).format('x'), 10)
              }
              return (
                <View style={[profileStyles.fieldBox, { backgroundColor: 'white' }]} key={index}>
                  <View style={{ flexDirection: 'row', alignItems: 'center', width: '100%' }}>
                    <Text style={[profileStyles.educationCollg, { flex: 1 }]}>
                      {experience.designation}
                    </Text>
                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate('ProfileNewExperience',
                          {addExperience: this.addExperience, experience, deleteExps: this.deleteExps})
                      }
                    >
                      <Image
                        source={require('../../../assets/app_icons/Icons/edit-default.png')}
                        style={{
                          height: 16,
                          width: 16
                        }}
                      />
                    </TouchableOpacity>
                  </View>
                  <Text style={[profileStyles.educationLows]}>
                    {experience.organization}
                  </Text>
                  {!isEmpty(experience.location) &&
                    <Text style={[profileStyles.educationLows]}>
                      {experience.location}
                    </Text>
                  }
                  <Text style={[profileStyles.educationLows]}>
                  { moment(new Date(experience.startDate)).format('MMM Y') + '-' +
                    (experience.present ? 'Present' : moment(new Date(experience.endDate)).format('MMM Y'))
                  }
                  </Text>
                  {!isEmpty(experience.description) &&
                    <Text style={[profileStyles.educationLows]}>
                      {experience.description}
                    </Text>
                  }
                </View>
              );
            })
          }
        </ScrollView>
      </View>
    );
  }
}
