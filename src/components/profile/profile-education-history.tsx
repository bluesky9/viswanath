import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import { Icon } from 'native-base';
import { NavigationScreenProps } from 'react-navigation';
import MSAHeader from '../layouts/header/header';
import { get, map, findIndex, cloneDeep } from 'lodash';
import moment from 'moment';
import { transformDate } from '../../utils';
import profileStyles from './profile-styles';
interface State {
  history: any[]
}
interface IPeopleFilterNavParams {
  saveExtraProfileDetails: Function;
  deleteHistory: Function;
  confirmModal: Function;
}
interface IPeopleFilterProps
  extends NavigationScreenProps<IPeopleFilterNavParams> {}
export default class ProfileSkills extends Component<IPeopleFilterProps, State> {
  constructor(props) {
    super(props);
    this.state = {
      history: cloneDeep(get(this.props.navigation, 'state.params.education', []))
    }
    this.addHistory = this.addHistory.bind(this);
    this.deleteHistory = this.deleteHistory.bind(this);
  }
  addHistory(history: any) {
    this.props.navigation.state.params.saveExtraProfileDetails(history, 'education').then((profileDetails) => {
      if (history.id) {
        let index = findIndex(this.state.history, data => data.eduId === history.id);
        history.eduId = history.id;
        if (index > -1) {
          this.state.history[index] = history;
          this.setState({
            history: this.state.history
          })
        }
      } else if (profileDetails && profileDetails.education) {
        this.setState({
          history: profileDetails.education
        })
      }
    })
    .catch(e => {
      console.log(e, '!!!!')
    })
  }
  deleteHistory(id: string) {
    this.props.navigation.state.params.confirmModal('education', () => {
      let index = findIndex(this.state.history, data => data.eduId === id);
      this.props.navigation.state.params.deleteHistory(id, 'education')
      if (index > -1) {
        this.state.history.splice(index, 1);
      }
      this.setState({
        history: this.state.history
      })
    })
  }
  render() {
    return (
      <View style={{ backgroundColor: 'white', flex: 1 }}>
        <MSAHeader
          title={'Education History'}
          rightText={'DONE'}
          leftText={'CANCEL'}
          onPress={() => this.props.navigation.goBack()}
          onRightTextPress={() => this.props.navigation.goBack()}
        />
        <View style={[profileStyles.fieldBox, { paddingVertical: 20, flex: 0, justifyContent: 'center' }]}>
          <Text style={[profileStyles.fieldDarkLow, { fontSize: 16, lineHeight: 21 }]}>
            New Education
          </Text>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('ProfileNewEducation', {addHistory: this.addHistory, history: {}})
            }
            style={{ position: 'absolute', right: 15 }}
          >
            <Text style={profileStyles.tagButtonText}>ADD</Text>
          </TouchableOpacity>
        </View>
        <ScrollView style={{ flex: 1, backgroundColor: 'rgb(250,251,252)' }}>
        {
          map(this.state.history, (history, index) => {
            if ( typeof history.startDate === 'string') {
              history.startDate = parseInt(moment(transformDate(history.startDate)).format('x'), 10)
            }
            if ( typeof history.endDate === 'string') {
              history.endDate = parseInt(moment(transformDate(history.endDate)).format('x'), 10)
            }
            return (
              <View style={[profileStyles.fieldBox, { backgroundColor: 'white'}]} key={index}>
                <View style={{ flexDirection: 'row', alignItems: 'center', width: '100%' }}>
                  <Text style={[profileStyles.educationCollg, { flex: 1 }]}>
                    {history.universityName}
                  </Text>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate('ProfileNewEducation',
                        {addHistory: this.addHistory, history: history, deleteHistory: this.deleteHistory})}
                  >
                    <Icon name='md-create' style={{ color: 'rgb(207, 208, 220)', fontSize: 20 }} />
                  </TouchableOpacity>
                </View>
                <Text style={[profileStyles.educationLows, { marginTop: 15 }]}>
                  {history.major + '-' + history.degree}
                </Text>
                <Text style={[profileStyles.educationLows, { marginTop: 15 }]}>
                { moment(new Date(history.startDate)).format('MMM Y') + '-' +
                  (history.present ? 'Present' : moment(new Date(history.endDate)).format('MMM Y'))
                }
                </Text>
              </View>
            );
          })
        }
        </ScrollView>
      </View>
    );
  }
}
