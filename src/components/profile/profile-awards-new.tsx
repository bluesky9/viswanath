import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput
} from 'react-native';
import DatePicker from 'react-native-datepicker';
import { NavigationScreenProps } from 'react-navigation';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import MSAHeader from '../layouts/header/header';
import moment from 'moment';
import profileStyles from './profile-styles';
interface State {
  award: Award
}

interface Award {
  id?: string;
  achieveId?: string;
  award: string,
  description: string,
  date: string;
  organization: string;
  month: number;
  year: number;
}
interface IPeopleFilterNavParams {
  addAward?: Function
  award: Award
  deleteAward: Function
}
interface IPeopleFilterProps
  extends NavigationScreenProps<IPeopleFilterNavParams> {
  }
export default class ProfileNewAward extends Component<IPeopleFilterProps, State> {
  constructor(props: IPeopleFilterProps) {
    super(props);
    const {
      achieveId,
      award,
      organization,
      description,
      date,
      month,
      year
    } = this.props.navigation.state.params.award
    this.state = {
      award: {
        id: (achieveId) ? achieveId : '',
        award: (award) ? award : '',
        organization: (organization) ? organization : 'mySkool',
        description: (description) ? description : '',
        date: (date) ? moment(date).format('MMM Y') : moment().format('MMM Y'),
        month: month ? month : moment().month() + 1,
        year: year ? year : moment().year()
      }
    }
  }
  onChange(text: string, field: string) {
    let newText = text.split(' ')
    if (field === 'date') {
      this.state.award.month = parseInt(moment().month(newText[0]).format('M'), 10)
      this.state.award.year = parseInt(newText[1], 10)
    }
    this.state.award[field] = text;
    this.setState({
        award: this.state.award
    })
  }
  render() {
    console.log(this.state.award.date, '1')
    return (
      <View style={{ backgroundColor: 'rgb(250,251,252)', flex: 1 }}>
        <MSAHeader
          title={'Awards & Recognitions'}
          rightText={'SAVE'}
          leftText={'CANCEL'}
          onPress={() => this.props.navigation.goBack()}
          onRightTextPress={() => this.props.navigation.state.params.addAward(this.state.award)}
        />
        <KeyboardAwareScrollView>
          <View style={{ backgroundColor: 'white' }}>
            <View style={profileStyles.fieldBox}>
            <Text
              style={[profileStyles.dualHeaderLeftText, { marginBottom: 15, fontSize: 12, lineHeight: 14 }]}
            >
              Award Title
            </Text>
            <TextInput
              style={[profileStyles.skillInp, { color: 'rgb(83,87,94)' }]}
              placeholder={'Ex: Best Performer of the year'}
              placeholderTextColor={'rgba(108,111,125,0.5)'}
              underlineColorAndroid={'rgba(0,0,0,0)'}
              value={this.state.award.award}
              onChangeText={(text) => this.onChange(text, 'award')}
              maxLength={128}
            />
          </View>
          <View style={[ profileStyles.fieldBox, { flexDirection: 'row' }]}>
              <View style={{ flex: 1 }}>
              <Text
                style={[profileStyles.dualHeaderLeftText, { marginBottom: 15, fontSize: 12, lineHeight: 14 }]}
              >
                Awarded On
              </Text>
              <DatePicker
                style={{ width: 80 }}
                mode={'date'}
                date={this.state.award.date}
                format={'MMM Y'}
                confirmBtnText={'Confirm'}
                cancelBtnText={'Cancel'}
                showIcon={false}
                onDateChange={(date) => this.onChange(date, 'date')}
                customStyles={{
                  dateInput: {
                    height: 28,
                    width: 80,
                    borderColor: 'rgb(53,135,230)',
                    borderRadius: 5
                  },
                  dateText: { color: 'rgb(53,135,230)', fontSize: 14, lineHeight: 18 }
                }}
              />
            </View>
          </View>
          <View
            style={profileStyles.fieldBox}
          >
            <Text
              style={[profileStyles.dualHeaderLeftText, { marginBottom: 15, fontSize: 12, lineHeight: 14 }]}
            >
              DESCRIPTION
            </Text>
            <TextInput
              style={[profileStyles.skillInp, { color: 'rgb(83,87,94)' }]}
              placeholder={'Ex: Briefly describe what earnt you this award/recognition'}
              placeholderTextColor={'rgba(108,111,125,0.5)'}
              underlineColorAndroid={'rgba(0,0,0,0)'}
              value={this.state.award.description}
              onChangeText={(text) => this.onChange(text, 'description')}
              maxLength={128}
            />
          </View>
        </View>
          {this.state.award.id ? <TouchableOpacity
            style={{ width: '100%', alignItems: 'center', padding: 10, backgroundColor: 'white' }}
            onPress={() => {
              // this.props.navigation.goBack()
              this.props.navigation.state.params.deleteAward(this.state.award.id)}}
          >
            <Text style={{ color: '#CC1433' }}>Delete</Text>
          </TouchableOpacity> : undefined}
        </KeyboardAwareScrollView>
      </View>
    );
  }
}
