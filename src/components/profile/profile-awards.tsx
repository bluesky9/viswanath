import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import { Icon } from 'native-base';
import { NavigationScreenProps } from 'react-navigation';
import { get, map, findIndex, cloneDeep } from 'lodash';
import MSAHeader from '../layouts/header/header';
import { DialogModal } from '../../store/actions';
import moment from 'moment';
import profileStyles from './profile-styles';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

const DELETE_AWARD_TITLE = 'Delete Award'
const DELETE_AWARD_MSG = 'The Award/Recognition will be deleted permanently'

interface State {
  awards: any[]
}
interface IPeopleFilterNavParams {
  saveExtraProfileDetails: Function;
  deleteAward: Function;
}
interface IPeopleFilterProps
  extends NavigationScreenProps<IPeopleFilterNavParams> {
    dialogModal: {
      dialogShows: Function;
    };
  }
class ProfileAward extends Component<IPeopleFilterProps, State> {
  constructor(props) {
    super(props);
    this.state = {
        awards: cloneDeep(get(this.props.navigation, 'state.params.awards', []))
    }
    this.addAward = this.addAward.bind(this);
    this.deleteAward = this.deleteAward.bind(this);
  }

  deleteConfirmationModal = (id: string) => {
    this.props.dialogModal.dialogShows(
      DELETE_AWARD_TITLE, DELETE_AWARD_MSG, 'action', true, () => this.deleteAward(id)
    )
  }

  addAward(award: any) {
    this.props.navigation.state.params.saveExtraProfileDetails(award, 'achievement').then((profileDetails) => {
      if (award.id) {
        let index = findIndex(this.state.awards, data => data.achieveId === award.id);
        if (index > -1) {
          award.achieveId = award.id;
          this.state.awards[index] = award;
          this.setState({
            awards: this.state.awards
          })
        }
      } else if ( profileDetails && profileDetails.achievement ) {
        this.setState({
          awards: profileDetails.achievement
        })
      }
    })
  }

  deleteAward(id: string) {
    let index = findIndex(this.state.awards, data => data.achieveId === id);
    this.props.navigation.state.params.deleteAward(id, 'achievement')
    if (index > -1) {
      this.state.awards.splice(index, 1);
    }
    this.setState({
      awards: this.state.awards
    })
  }
  render() {
    return (
      <View style={{ backgroundColor: 'rgb(250,251,252)', flex: 1 }}>
        <MSAHeader
          title={'Awards & Recognitions'}
          rightText={'DONE'}
          leftText={'CANCEL'}
          onPress={() => this.props.navigation.goBack()}
          onRightTextPress={() => this.props.navigation.goBack()}
        />
        <View style={[profileStyles.fieldBox, { justifyContent: 'center', backgroundColor: 'white' }]}>
          <Text style={[profileStyles.fieldDarkLow, { fontSize: 16, lineHeight: 21 }]}>
            New Award
          </Text>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('ProfileNewAward', {addAward: this.addAward, award: {}})
            }
            style={{ position: 'absolute', right: 15 }}
          >
            <Text style={profileStyles.tagButtonText}>ADD</Text>
          </TouchableOpacity>
        </View>
        <ScrollView>
          {
            map(this.state.awards, (award, index) => {
              if ( typeof award.date === 'string') {
                award.date = parseInt(moment(new Date(award.date)).format('x'), 10)
              }
              return (
                <View style={[profileStyles.fieldBox, { backgroundColor: 'white' }]} key={index}>
                  <View style={{ width: '100%', flexDirection: 'row' }}>
                    <Text style={[profileStyles.educationCollg, { marginVertical: 2, flex: 1 }]}>
                      {award.award}
                    </Text>
                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate('ProfileNewAward', {addAward: this.addAward, award, deleteAward: this.deleteConfirmationModal})
                      }
                    >
                      <Icon name='md-create' style={{ color: 'rgb(207, 208, 220)', fontSize: 16 }} />
                    </TouchableOpacity>
                  </View>
                  <Text style={[profileStyles.educationLows, { marginVertical: 2 }]}>
                    {moment(new Date(award.date)).format('MMM Y')}
                  </Text>
                  {award.description ? <Text style={[profileStyles.educationLows, { marginVertical: 2 }]}>
                      {award.description}
                  </Text> : undefined}
                </View>
              );
            })
          }
        </ScrollView>
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  dialogModal: bindActionCreators(DialogModal, dispatch)
});

export default connect(undefined, mapDispatchToProps)(ProfileAward);
