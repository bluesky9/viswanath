import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import { Icon } from 'native-base';
import { NavigationScreenProps } from 'react-navigation';
import { get, map, findIndex, cloneDeep, isEmpty } from 'lodash';
import MSAHeader from '../layouts/header/header';
import { DialogModal } from '../../store/actions';
import moment from 'moment';
import profileStyles from './profile-styles';
import { transformDate } from '../../utils';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

const DELETE_VOLUNTEERING_TITLE = 'Delete Volunteering'
const DELETE_VOLUNTEERING_MSG = 'The Volunteering experience will be deleted permanently'
const DELETE_MEMBERSHIP_TITLE = 'Delete Membership'
const DELETE_MEMBERSHIP_MSG = 'The membership will be deleted permanently'

interface State {
  memberships: any[]
}
interface IPeopleFilterNavParams {
  saveExtraProfileDetails: Function;
  deleteMembership: Function;
  type: string;
}
interface IPeopleFilterProps
  extends NavigationScreenProps<IPeopleFilterNavParams> {
    dialogModal: {
      dialogShows: Function;
    };
  }
class ProfilMembership extends Component<IPeopleFilterProps, State> {
  constructor(props) {
    super(props);
    this.state = {
        memberships: cloneDeep(get(this.props.navigation, 'state.params.membership', []))
    }
    this.addMembership = this.addMembership.bind(this);
    this.deleteMembership = this.deleteMembership.bind(this);
  }
  addMembership(membership: any) {
    const type = get(this.props.navigation.state.params, 'type', 'Membership');
    this.props.navigation.state.params.saveExtraProfileDetails(membership,
      this.props.navigation.state.params.type !== 'volunteer' ? 'membership' : 'volunteerWork').then((profileDetails) => {
        if (membership.id) {
          let index = findIndex(this.state.memberships, data => ((type === 'Membership') ? data.membershipId : data.volunteerId) === membership.id);
          if (index > -1) {
            membership[(type === 'Membership') ? 'membershipId' : 'volunteerId'] = membership.id;
            this.state.memberships[index] = membership;
            this.setState({
              memberships: this.state.memberships
            })
          }
        } else if (profileDetails && (profileDetails.membership || profileDetails.volunteer)) {
          this.setState({
            memberships: profileDetails[type !== 'volunteer' ? 'membership' : 'volunteerWork']
          })
        }
      })
  }

  deleteConfirmationModal = (id: string) => {
    let messageTitle = '';
    let message = '';
    if (get(this.props.navigation.state.params, 'type', 'Membership') === 'Membership') {
      messageTitle = DELETE_MEMBERSHIP_TITLE;
      message = DELETE_MEMBERSHIP_MSG;
    } else {
      messageTitle = DELETE_VOLUNTEERING_TITLE;
      message = DELETE_VOLUNTEERING_MSG;
    }
    this.props.dialogModal.dialogShows(
      messageTitle, message, 'action', true, () => this.deleteMembership(id)
    )
  }

  deleteMembership (id: string) {
    const type = get(this.props.navigation.state.params, 'type', 'Membership');
    let index = findIndex(this.state.memberships, data => (type === 'Membership') ? data.membershipId : data.volunteerId === id);
    this.props.navigation.state.params.deleteMembership(id, (type === 'Membership') ? 'membership' : 'volunteerWork')
    if (index > -1) {
      this.state.memberships.splice(index, 1);
    }
    this.setState({
      memberships: this.state.memberships
    })
  }
  render() {
    const type = get(this.props.navigation.state.params, 'type', 'Membership');
    const scrollToBottom = get(this.props.navigation.state.params, 'scrollToBottom', undefined);
    // console.log(scrollToBottom, '123')
    return (
      <View style={{ backgroundColor: 'white', flex: 1 }}>
        <MSAHeader
          title={(type === 'Membership') ? 'Memberships' : 'Volunteering'}
          rightText={'DONE'}
          leftText={'CANCEL'}
          onPress={() => this.props.navigation.goBack()}
          onRightTextPress={() => {
            this.props.navigation.goBack();
            if (type !== 'Membership' && scrollToBottom) {
              scrollToBottom()
            }
          }}
        />
        <View style={[profileStyles.fieldBox, { justifyContent: 'center' }]}>
          <Text style={[profileStyles.fieldDarkLow, { fontSize: 16, lineHeight: 21 }]}>
            {(type === 'Membership') ? 'New Membership' : 'Add Volunteering'}
          </Text>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('ProfileNewMembership', {addMembership: this.addMembership, type: type, membership: {}})
            }
            style={{ position: 'absolute', right: 15 }}
          >
            <Text style={profileStyles.tagButtonText}>ADD</Text>
          </TouchableOpacity>
        </View>
        <ScrollView style={{ backgroundColor: 'rgb(250,251,252)' }}>
          {
            map(this.state.memberships, (membership, index) => {
              if ( typeof membership.startDate === 'string') {
                membership.startDate = parseInt(moment(transformDate(membership.startDate)).format('x'), 10)
              }
              if ( typeof membership.endDate === 'string') {
                membership.endDate = parseInt(moment( transformDate(membership.endDate)).format('x'), 10)
              }
              return (
                <View style={[profileStyles.fieldBox, { backgroundColor: 'white' }]} key={index}>
                  <View style={{  width: '100%', flexDirection: 'row' }}>
                    <Text style={[profileStyles.educationCollg, { marginVertical: 2, flex: 1 }]}>
                      {membership.title}
                    </Text>
                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate('ProfileNewMembership',
                          {addMembership: this.addMembership, membership, type, deleteMembership: this.deleteConfirmationModal})}
                    >
                      <Icon name='md-create' style={{ color: 'rgb(207, 208, 220)', fontSize: 16 }} />
                    </TouchableOpacity>
                  </View>
                  <Text style={[profileStyles.educationLows, { marginVertical: 2 }]}>
                    {membership.organization}
                  </Text>
                  <Text style={[profileStyles.educationLows, { marginVertical: 2 }]}>
                  { moment(new Date(membership.startDate)).format('MMM Y') + '-' +
                    (membership.present ? 'Present' : moment(new Date(membership.endDate)).format('MMM Y'))
                  }
                  </Text>
                  {!isEmpty(membership.description) &&
                    <Text style={[profileStyles.educationLows, { marginVertical: 2 }]}>
                      {membership.description}
                    </Text>
                  }
                </View>
              );
            })
          }
        </ScrollView>
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  dialogModal: bindActionCreators(DialogModal, dispatch)
});

export default connect(undefined, mapDispatchToProps)(ProfilMembership);
