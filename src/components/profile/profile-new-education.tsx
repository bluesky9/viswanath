import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Alert,
  TextInput,
  Switch,
  Platform
} from 'react-native';
import DatePicker from 'react-native-datepicker';
import { NavigationScreenProps } from 'react-navigation';
import MSAHeader from '../layouts/header/header';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import moment from 'moment';
import { transformDate } from '../../utils';
import profileStyles from './profile-styles';
import { style } from '../../styles/variables';
interface State {
  education: Education
}

interface Education {
  id?: string;
  eduId?: string;
  universityName: string;
  degree: string;
  major: string;
  startDate: string;
  endDate: string;
  startMonth: number;
  startYear: number;
  endMonth: number;
  endYear: number;
  present: boolean;
}
interface IPeopleFilterNavParams {
  addHistory?: Function;
  history?: Education;
  deleteHistory?: Function;
}
interface IPeopleFilterProps
  extends NavigationScreenProps<IPeopleFilterNavParams> {
  }
export default class ProfileSkills extends Component<IPeopleFilterProps, State> {
  constructor(props: IPeopleFilterProps) {
    super(props);
    const {
      eduId,
      universityName,
      degree,
      major,
      startDate,
      endDate,
      present
    } = props.navigation.state.params.history;
    this.state = {
      education: {
        id: (eduId) ? eduId : '',
        universityName: universityName ? universityName : '',
        degree: degree ? degree : '',
        major: major ? major : '',
        startDate: (startDate) ? moment(new Date(startDate)).format('MMM Y') : moment().format('MMM Y'),
        endDate: (endDate) ? moment(new Date(endDate)).format('MMM Y') : moment().format('MMM Y'),
        startMonth: (startDate) ? moment(startDate).month() + 1 : moment().month() + 1,
        startYear: (startDate) ? moment(startDate).year() : moment().year(),
        endMonth: (endDate) ? moment(endDate).month() + 1 : moment().month() + 1,
        endYear: (endDate) ? moment(endDate).year() : moment().year(),
        present: (present) ? present : false
      }
    }
  }
  onChange(text: string, field: string) {
    let newText = text.split(' ')
    let temp = this.state.education[field];
    this.state.education[field] = text;
    if (field === 'startDate' || field === 'endDate') {
      if (moment(transformDate(this.state.education.endDate)) <
          moment(transformDate(this.state.education.startDate))) {
        this.state.education[field] = temp;
        setTimeout(() => {
          Alert.alert('To date should be greater than From date');
        }, 500);
      } else {
        if (field === 'startDate') {
          this.state.education.startMonth = parseInt(moment().month(newText[0]).format('M'), 10)
          this.state.education.startYear = parseInt(newText[1], 10)
        } else if (field === 'endDate') {
          this.state.education.endMonth = parseInt(moment().month(newText[0]).format('M'), 10)
          this.state.education.endYear = parseInt(newText[1], 10)
        }
        this.setState({
          education: this.state.education
        })
      }
    } else {
      this.setState({
        education: this.state.education
      })
    }
  }
  render() {
    // console.log(this.state.education, '112')
    return (
      <View style={{ backgroundColor: 'rgb(250,251,252)', flex: 1 }}>
        <MSAHeader
          title={'New Education'}
          rightText={'SAVE'}
          leftText={'CANCEL'}
          onPress={() => this.props.navigation.goBack()}
          onRightTextPress={() => this.props.navigation.state.params.addHistory(this.state.education)}
        />
        <KeyboardAwareScrollView style={{ flex: 1 }}>
          <View style ={{ backgroundColor: 'white'}}>
          <View style={[profileStyles.fieldBox]}>
            <Text
              style={[profileStyles.dualHeaderLeftText, { marginBottom: 15, fontSize: 12, lineHeight: 14 }]}
            >
              SCHOOL / UNIVERSITY
            </Text>
            <TextInput
              style={[profileStyles.skillInp, { color: 'rgb(83,87,94)' }]}
              placeholder={'Ex: New York University'}
              placeholderTextColor={'rgba(108,111,125,0.5)'}
              underlineColorAndroid={'rgba(0,0,0,0)'}
              value={this.state.education.universityName}
              onChangeText={(text) => this.onChange(text, 'universityName')}
              maxLength={128}
            />
          </View>
          <View style={profileStyles.fieldBox}>
            <Text
              style={[profileStyles.dualHeaderLeftText, { marginBottom: 15, fontSize: 12, lineHeight: 14 }]}
            >
              DEGREE / LEVEL
            </Text>
            <TextInput
              style={[profileStyles.skillInp, { color: 'rgb(83,87,94)' }]}
              placeholder={'Ex: Bachelors, Masters'}
              placeholderTextColor={'rgba(108,111,125,0.5)'}
              underlineColorAndroid={'rgba(0,0,0,0)'}
              value={this.state.education.degree}
              onChangeText={(text) => this.onChange(text, 'degree')}
              maxLength={128}
            />
          </View>
          <View style={profileStyles.fieldBox}>
            <Text
              style={[profileStyles.dualHeaderLeftText, { marginBottom: 15, fontSize: 12, lineHeight: 14 }]}
            >
              FIELD OF STUDY
            </Text>
            <TextInput
              style={[profileStyles.skillInp, { color: 'rgb(83,87,94)' }]}
              placeholder={'Ex: Arts, Engineering'}
              placeholderTextColor={'rgba(108,111,125,0.5)'}
              underlineColorAndroid={'rgba(0,0,0,0)'}
              value={this.state.education.major}
              onChangeText={(text) => this.onChange(text, 'major')}
              maxLength={128}
            />
          </View>
          <View
            style={[
              profileStyles.fieldBox,
              { flexDirection: 'row', borderBottomWidth: 0, paddingBottom: 9 }
            ]}
          >
            <View
              style={{
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                flex: 1
              }}
            >
              <Text
                style={[profileStyles.dualHeaderLeftText, { marginBottom: 9, fontSize: 12, lineHeight: 14 }]}
              >
                FROM
              </Text>
              <DatePicker
                style={{ width: 80 }}
                date={this.state.education.startDate}
                mode={'date'}
                format={'MMM Y'}
                confirmBtnText={'Confirm'}
                cancelBtnText={'Cancel'}
                showIcon={false}
                onDateChange={(date) => this.onChange(date, 'startDate')}
                customStyles={{
                  dateInput: {
                    height: 28,
                    // width: 80,
                    borderColor: 'rgb(53,135,230)',
                    borderRadius: 3
                  },
                  dateText: { color: 'rgb(53,135,230)', fontSize: 14, lineHeight: 18 }
                }}
              />
            </View>
            <View
              style={{
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
                flex: 1
              }}
            >
              <Text
                style={[profileStyles.dualHeaderLeftText, { marginBottom: 9, fontSize: 12, lineHeight: 14 }]}
              >
                TO
              </Text>
              { this.state.education.present ?
                <View style={{ height: 40, justifyContent: 'center' }}>
                  <Text style={[profileStyles.presentText, { fontSize: 14 }]}>Present</Text>
                </View>
                : <DatePicker
                style={{ width: 80 }}
                mode={'date'}
                date={this.state.education.endDate}
                format={'MMM Y'}
                confirmBtnText={'Confirm'}
                cancelBtnText={'Cancel'}
                showIcon={false}
                onDateChange={(date) => this.onChange(date, 'endDate')}
                customStyles={{
                  dateInput: {
                    height: 28,
                    // width: 80,
                    borderColor: 'rgb(53,135,230)',
                    borderRadius: 3
                  },
                  dateText: { color: 'rgb(53,135,230)', fontSize: 14, lineHeight: 18 }
                }}
              />
              }
            </View>
          </View>
          <View style={profileStyles.presentBtnContainer}>
            <View style={{flex: 1}} />
            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start'}}>
              <Switch
                onValueChange={(bool) => {
                  let education = this.state.education;
                  education.present = bool;
                  this.setState({ education })
                }}
                value={this.state.education.present}
                thumbTintColor={style.color.primary}
                onTintColor={style.color.cream}
                style={{
                  transform: [{ scaleX: Platform.OS === 'ios' ? .7 : 1 },
                    { scaleY: Platform.OS === 'ios' ? .7 : 1 }] }}
              />
              <Text style={profileStyles.presentText}>Present</Text>
            </View>
          </View>
          {this.state.education.id ? <TouchableOpacity
            style={{ width: '100%', alignItems: 'center', padding: 10 }}
            onPress={() => {
              // this.props.navigation.goBack()
              this.props.navigation.state.params.deleteHistory(this.state.education.id)}}
          >
            <Text style={{ color: '#CC1433' }}>Delete</Text>
          </TouchableOpacity> : undefined}
          </View>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}
