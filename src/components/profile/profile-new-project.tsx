import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Alert,
  TextInput
} from 'react-native';
import DatePicker from 'react-native-datepicker';
import { NavigationScreenProps } from 'react-navigation';
import MSAHeader from '../layouts/header/header';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import moment from 'moment';
import profileStyles from './profile-styles';
import { transformDate } from '../../utils';
interface State {
project: Project;
}
interface Project {
  id?: string;
  researchId?: string;
  title: string,
  projectUrl: string,
  description: string,
  startDate: string;
  endDate: string;
  startMonth: number;
  startYear: number;
  endMonth: number;
  endYear: number;
}
interface IPeopleFilterNavParams {
  addProject?: Function
  project?: Project
  deleteProject?: Function
}
interface IPeopleFilterProps
  extends NavigationScreenProps<IPeopleFilterNavParams> {
  }
export default class ProfileNewProject extends Component<IPeopleFilterProps, State> {
  constructor(props: IPeopleFilterProps) {
    super(props);
    const {
      researchId,
      projectUrl,
      description,
      title,
      startDate,
      endDate
    } = props.navigation.state.params.project;
    this.state = {
      project: {
        id: (researchId) ? researchId : '',
        title: (title) ? title : '',
        projectUrl: (projectUrl) ? projectUrl : '',
        description: (description) ? description : '',
        startDate: (startDate) ? moment(startDate).format('MMM Y') : moment().format('MMM Y'),
        endDate: (endDate) ? moment(endDate).format('MMM Y') : moment().format('MMM Y'),
        startMonth: (startDate) ? moment(startDate).month() + 1 : moment().month() + 1,
        startYear: (startDate) ? moment(startDate).year() : moment().year(),
        endMonth: (endDate) ? moment(endDate).month() + 1 : moment().month() + 1,
        endYear: (endDate) ? moment(endDate).year() : moment().year()
      }
    }
  }
  onChange(text: string, field: string) {
    let newText = text.split(' ')
    let temp = this.state.project[field];
    this.state.project[field] = text;
    if (field === 'startDate' || field === 'endDate') {
      if (moment(transformDate(this.state.project.endDate)) <
          moment(transformDate(this.state.project.startDate))) {
        this.state.project[field] = temp;
        setTimeout(() => {
          Alert.alert('To date should be greater than From date');
        }, 500);
      } else {
        if (field === 'startDate') {
          this.state.project.startMonth = parseInt(moment().month(newText[0]).format('M'), 10)
          this.state.project.startYear = parseInt(newText[1], 10)
        } else if (field === 'endDate') {
          this.state.project.endMonth = parseInt(moment().month(newText[0]).format('M'), 10)
          this.state.project.endYear = parseInt(newText[1], 10)
        }
        this.setState({
          project: this.state.project
        })
      }
    } else {
      this.setState({
        project: this.state.project
      })
    }
  }
  render() {
    return (
      <View style={{ backgroundColor: 'rgb(250,251,252)', flex: 1 }}>
        <MSAHeader
          title={'Projects & Research'}
          rightText={'SAVE'}
          leftText={'CANCEL'}
          onPress={() => this.props.navigation.goBack()}
          onRightTextPress={() => this.props.navigation.state.params.addProject(this.state.project)}
        />
        <KeyboardAwareScrollView>
          <View style={{ backgroundColor: 'white' }}>
            <View style={profileStyles.fieldBox}>
            <Text
              style={[profileStyles.dualHeaderLeftText, { marginBottom: 12, fontSize: 12, lineHeight: 14 }]}
            >
              PROJECT TITLE
            </Text>
            <TextInput
              style={[profileStyles.skillInp, { color: 'rgb(83,87,94)' }]}
              placeholder={'Ex: Tesing cars to run on water'}
              placeholderTextColor={'rgba(108,111,125,0.5)'}
              underlineColorAndroid={'rgba(0,0,0,0)'}
              value={this.state.project.title}
              onChangeText={(text) => this.onChange(text, 'title')}
              maxLength={128}
            />
          </View>
          <View style={profileStyles.fieldBox}>
            <Text
              style={[profileStyles.dualHeaderLeftText, { marginBottom: 12, fontSize: 12, lineHeight: 14 }]}
            >
              PROJECT URL
            </Text>
            <TextInput
              style={[profileStyles.skillInp, { color: 'rgb(83,87,94)' }]}
              placeholder={'Ex: www.sciencejournal.com/caronwater'}
              placeholderTextColor={'rgba(108,111,125,0.5)'}
              underlineColorAndroid={'rgba(0,0,0,0)'}
              onFocus={() => {
                if (this.state.project.projectUrl === '') {
                  this.onChange('https://', 'projectUrl')
                }
              }}
              value={this.state.project.projectUrl}
              onChangeText={(text) => this.onChange(text, 'projectUrl')}
              maxLength={128}
            />
          </View>
          <View style={[ profileStyles.fieldBox, { flexDirection: 'row' }]}>
            <View style={{ flex: 1}}>
              <Text
                style={[profileStyles.dualHeaderLeftText, { marginBottom: 12, fontSize: 12, lineHeight: 14 }]}
              >
                FROM
              </Text>
              <DatePicker
                style={{ width: 80 }}
                date={this.state.project.startDate}
                mode={'date'}
                format={'MMM Y'}
                confirmBtnText={'Confirm'}
                cancelBtnText={'Cancel'}
                showIcon={false}
                onDateChange={(date) => this.onChange(date, 'startDate')}
                customStyles={{
                  dateInput: {
                    height: 28,
                    width: 80,
                    borderColor: 'rgb(53,135,230)',
                    borderRadius: 5
                  },
                  dateText: { color: 'rgb(53,135,230)', fontSize: 14, lineHeight: 18 }
                }}
              />
            </View>
            <View style={{ flex: 1}}>
              <Text
                style={[profileStyles.dualHeaderLeftText, { marginBottom: 12, fontSize: 12, lineHeight: 14 }]}
              >
                TO
              </Text>
              <DatePicker
                style={{ width: 80 }}
                mode={'date'}
                date={this.state.project.endDate}
                format={'MMM Y'}
                confirmBtnText={'Confirm'}
                cancelBtnText={'Cancel'}
                showIcon={false}
                onDateChange={(date) => this.onChange(date, 'endDate')}
                customStyles={{
                  dateInput: {
                    height: 28,
                    width: 80,
                    borderColor: 'rgb(53,135,230)',
                    borderRadius: 5
                  },
                  dateText: { color: 'rgb(53,135,230)', fontSize: 14, lineHeight: 18 }
                }}
              />
            </View>
          </View>
            <View style={profileStyles.fieldBox}>
              <Text
                  style={[profileStyles.dualHeaderLeftText, { marginBottom: 12, fontSize: 12, lineHeight: 14 }]}
              >
                  DESCRIPTION
              </Text>
              <TextInput
                  style={[profileStyles.skillInp, { color: 'rgb(83,87,94)', minHeight: 34 }]}
                  placeholder={'Ex: Briefly describe your project/research and your involvement'}
                  placeholderTextColor={'rgba(108,111,125,0.5)'}
                  underlineColorAndroid={'rgba(0,0,0,0)'}
                  value={this.state.project.description}
                  onChangeText={(text) => this.onChange(text, 'description')}
                  maxLength={128}
                  multiline={true}
              />
              </View>
            </View>
          {this.state.project.id ? <TouchableOpacity
            style={{ width: '100%', alignItems: 'center', padding: 10, backgroundColor: 'white' }}
            onPress={() => {
              // this.props.navigation.goBack()
              this.props.navigation.state.params.deleteProject(this.state.project.id)}}
              >
            <Text style={{ color: '#CC1433' }}>Delete</Text>
          </TouchableOpacity> : undefined}
        </KeyboardAwareScrollView>
      </View>
    );
  }
}
