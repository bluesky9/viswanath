import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { KeyboardAwareFlatList } from 'react-native-keyboard-aware-scroll-view';
import { NavigationScreenProps } from 'react-navigation';
import MSAHeader from '../layouts/header/header';
import Contacts from 'react-native-contacts';
import PeopleFilterSearch from '../people/people-filter-search';

import { isEmpty, filter, findIndex, get } from 'lodash';
import { style } from '../../styles/variables';

import { InviteActions } from '../../store/actions';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

interface ILoginScreenProps extends NavigationScreenProps<{}> {
  inviteActions: any;
}

type State = {
  contacts: any;
  inviteSentIndex: any;
  searchContact: string;
}

class InviteContacts extends Component<ILoginScreenProps, State> {
  constructor(props) {
    super(props);
    this.state = {
      inviteSentIndex: [],
      contacts: [],
      searchContact: ''
    }
  }

  componentDidMount() {
    Contacts.checkPermission((err, permission) => {
      if (err) console.log(err)
      if (permission === 'undefined') {
        Contacts.requestPermission((error, permissions) => {
          if (error) console.log(error)
          if (permissions === 'authorized') {
            Contacts.getAll((errorObj, contacts) => {
              if (errorObj) console.log(errorObj, 'Error')
              else {
                let contactsWithEmail = filter(contacts, contactObj => !isEmpty(contactObj.emailAddresses))
                this.setState({ contacts: contactsWithEmail })
              }
            })
          }
        })
      }
      if (permission === 'authorized') {
        Contacts.getAll((errorObj, contacts) => {
          if (errorObj) console.log(errorObj, 'Error')
          else {
            let contactsWithEmail = filter(contacts, contactObj => !isEmpty(contactObj.emailAddresses))
            this.setState({ contacts: contactsWithEmail })
          }
        })
      }
      if (permission === 'denied') {
        console.log(permission, '123')
      }
    })
  }

  onSendPress = (email, index) => {
    let inviteSentIndex = this.state.inviteSentIndex;
    inviteSentIndex.push(index)
    this.props.inviteActions.invitePeople(email)
    this.setState({ inviteSentIndex })
  }

  renderContacts = (index, item) => {
    let inviteSent = false;
    let checkIndex = findIndex(this.state.inviteSentIndex, obj => obj === index )
    if (checkIndex !== -1) inviteSent = true;
    let phoneNumer = get(item, 'phoneNumbers[0].number', undefined)
    return (
      <View
        style={{ paddingVertical: 10, marginHorizontal: 15,
          flexDirection: 'row', borderBottomWidth: 2, borderBottomColor: style.color.lightGrey }}
      >
        <Image
          style={{ height: 41, width: 41, borderRadius: 20.5 }}
          source={  item.hasThumbnail
            ? { uri: item.thumbnailPath }
            : require('../../../assets/images/user_dummy_image.png')
          }
        />
        <View style={{ justifyContent: 'center', flex: 1 }}>
          <View style={{ marginLeft: 10, justifyContent: 'space-between', flexDirection: 'row' }}>
            <Text style={{ fontFamily: style.font.family.rubikRegular, color: style.color.primary,
              fontSize: 16, lineHeight: 21 }}>{item.givenName}</Text>
            <TouchableOpacity
              style={{ justifyContent: 'center' }}
              onPress={() => this.onSendPress(item.emailAddresses[0].email, index)}
              disabled={inviteSent}
            >
            { inviteSent ?
              <Text style={{ color: '#3587e6', fontFamily: style.font.family.rubikMedium, fontSize: 16, letterSpacing: 0.5, lineHeight: 21 }}>
                SENT
              </Text>
              : <Text style={{ color: '#3587e6', fontFamily: style.font.family.rubikMedium, fontSize: 16, letterSpacing: 0.5, lineHeight: 21 }}>
                INVITE
              </Text>
            }
            </TouchableOpacity>
          </View>
          <Text style={{ marginLeft: 10, paddingVertical: 4, color: '#808691', fontFamily: style.font.family.rubikRegular  }}>
            {item.emailAddresses[0].email}
          </Text>
          {
            phoneNumer && <Text style={{ marginLeft: 10, color: '#808691', fontFamily: style.font.family.rubikRegular  }}>
            {phoneNumer}
            </Text>
          }
        </View>
      </View>
    )
  }

  handleTextChange = (text) => {
    this.setState({ searchContact: text.toLowerCase() })
  }

  render() {
    let contacts = this.state.contacts
    let filterContacts = filter(contacts, contact => contact.givenName.toLowerCase().startsWith(this.state.searchContact))
    return (
      <View style={{ flex: 1, backgroundColor: style.color.whiteCream }}>
        <MSAHeader
          title={'Invite from contacts'}
          onPress={() => this.props.navigation.goBack()}
        />
        <View style={{ flex: 1 }}>
          <View style={{ marginVertical: 10, marginHorizontal: 15 }}>
            <PeopleFilterSearch
              placeHolder={'Search people'}
              onTextChange={this.handleTextChange}
            />
          </View>
          <KeyboardAwareFlatList
            data={this.state.searchContact === '' ? contacts : filterContacts}
            renderItem={({ index, item }) => this.renderContacts(index, item)}
            keyExtractor={item => item.recordID}
            extraData={this.state}
          />
        </View>
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  inviteActions: bindActionCreators(InviteActions, dispatch)
});

export default connect(undefined, mapDispatchToProps)(InviteContacts);
