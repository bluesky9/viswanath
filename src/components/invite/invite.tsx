import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, Share } from 'react-native';
import { Icon } from 'native-base';
import { NavigationScreenProps } from 'react-navigation';
import MSAHeader from '../layouts/header/header';
import validator from '../../validator/regx-validator';

import { style } from '../../styles/variables';

import { InviteActions } from '../../store/actions';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

interface ILoginScreenProps extends NavigationScreenProps<{}> {
  inviteActions: any;
}

type State = {
  email: string;
}

class Invite extends Component<ILoginScreenProps, State> {
  constructor(props) {
    super(props);
    this.state = {
      email: ''
    }
  }

  onSendPress = () => {
    this.props.inviteActions.invitePeople(this.state.email)
    this.setState({ email: ''})
  }

  handleTextChange = (text) => {
    this.setState({ email: text})
  }

  render() {
    let content = {
      message: 'UPLE is where universities meet their people. Join me on UPLE and discover your university. Click on the link https://www.uple.co',
      title: 'Uple App'
    }
    return (
      <View style={{ backgroundColor: 'white', flex: 1 }}>
        <MSAHeader
          title={'Invite people'}
          onPress={() => this.props.navigation.goBack()}
          rightText={'SEND'}
          disableRightText={!validator('Email', this.state.email) ? false : true}
          rightTextColor={!validator('Email', this.state.email) ? style.color.primary : style.color.dustyGrey}
          onRightTextPress={this.onSendPress}
        />
        <View>
          <View style={{ paddingTop: 29, paddingBottom: 32, paddingHorizontal: 15}}>
            <Text style={{ fontFamily: style.font.family.ptSerif , fontSize: 24, lineHeight: 30, textAlign: 'center', color: style.color.nero }}>
              Invite via e-mail
            </Text>
          </View>
          <View style={{ backgroundColor: 'white', marginVertical: 15, paddingVertical: 10, marginHorizontal: 15,
              borderWidth: 1, flexDirection: 'row', justifyContent: 'center', borderColor: 'rgb(233,238,241)' }}>
            <Icon name={'md-mail'} style={{ color: 'rgb(197, 207, 219)', fontSize: 20, marginLeft: 12 }}/>
            <TextInput
              placeholder={'Type an email address to invite'}
              onChangeText={this.handleTextChange}
              value={this.state.email}
              autoCapitalize={'none'}
              underlineColorAndroid={'rgba(0,0,0,0)'}
              autoCorrect={false}
              placeholderTextColor={'rgb(153,153,153)'}
              style={{ paddingLeft: 10, flex: 1, fontSize: 12, fontFamily: style.font.family.rubikRegular }}
            />
          </View>
          <Text style={{paddingTop: 20, paddingBottom: 31, textAlign: 'center', fontFamily: style.font.family.rubikRegular,
            lineHeight: 18, letterSpacing: 0.2, fontSize: 12, color: '#53575e'}}>
            or
          </Text>
          <View style={{ marginVertical: 15, backgroundColor: 'white' }}>
            <TouchableOpacity
              style={{ flexDirection: 'row', paddingHorizontal: 15, paddingVertical: 18, borderTopWidth: 2,
                borderBottomWidth: 2, justifyContent: 'space-between', borderColor: '#fafbfc'
              }}
              onPress={() => this.props.navigation.navigate('InviteContacts')}
            >
              <View style={{ alignItems: 'center' }}>
                <Text style={{ fontFamily: style.font.family.rubikRegular, color: style.color.coal }}>Invite from Contacts list</Text>
              </View>
              <Icon name={'ios-arrow-forward'} style={{ color: 'rgb(197, 207, 219)', fontSize: 20 }}/>
            </TouchableOpacity>
            <TouchableOpacity
              style={{ flexDirection: 'row', paddingHorizontal: 15,
                paddingVertical: 18, justifyContent: 'space-between', borderBottomWidth: 2, borderBottomColor: '#fafbfc'
              }}
              onPress={() => Share.share(content)}
            >
              <View style={{ alignItems: 'center' }}>
                <Text style={{ fontFamily: style.font.family.rubikRegular, color: style.color.coal }}>Share an invite link</Text>
              </View>
              <Icon name={'ios-arrow-forward'} style={{ color: 'rgb(197, 207, 219)', fontSize: 20 }}/>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  inviteActions: bindActionCreators(InviteActions, dispatch)
});

export default connect(undefined, mapDispatchToProps)(Invite);
