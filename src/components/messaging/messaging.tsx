import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, FlatList } from 'react-native';
import { NavigationScreenProps } from 'react-navigation';
import { SwipeRow, Icon } from 'native-base';

import PeopleFilterSearch from '../people/people-filter-search';
import MSAHeader from '../layouts/header/header';

import { messagingStyles } from './messaging-styles';
import { style } from '../../styles/variables';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { get, map, filter } from 'lodash';
import { transformForConversations } from '../../utils';
import { DialogModal } from '../../store/actions';

const CustomSwipeRow = SwipeRow as any;
const DELETE_MSG = 'Are you sure to delete'
const DELETE_DESC = 'You will no longer see the messages from this conversation'

import { getAllConversations, muteConversation, deleteConversation } from '../../store/actions/Messaging';

interface IMessagingFilterNavParams {}
interface IMessagingFilterProps
  extends NavigationScreenProps<IMessagingFilterNavParams> {
    getAllConversations: Function;
    conversations: any;
    muteConversation: Function;
    userId: string;
    deleteConversation: Function;
    dialogModal: any;
  }

interface State {
  filter: string;
  refreshing: boolean;
}

class Messaging extends Component<IMessagingFilterProps, State> {
  swipeRefs = [];
  constructor(props) {
    super(props);
    this.state = {
      filter: '',
      refreshing: false
    }
  }
  componentDidMount() {
    this.props.getAllConversations();
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.conversations) {
      this.setState({ refreshing: false })
    }
  }
  _renderChatBody(item, muted, conversationIndex, messageSeen) {
    let user = undefined;
    map(item.memberInfos, (member) => {
      if (member.users.id !== this.props.userId) {
        user = member.users;
      }
    })
    if (!user && item.memberInfos && item.memberInfos[0]) {
      user = item.memberInfos[0].users;
    }
    if (user) {
    return (
        <TouchableOpacity
          style={[messagingStyles.swiperBodyContainer, {paddingTop: 0, paddingBottom: 0, height: 55, width: '100%'}, {
            backgroundColor: 'white'
          }]}
          onPress={() => this.props.navigation.navigate('MessagingSingle', {messages: item.messages, conversationId: item.id,
              name: item.conversationType === 'ONE_TO_ONE' ? user.name : item.name, muted, conversationIndex})}
        >
          {item.conversationType === 'GROUP' ?
            <Image
              source={require('../../../assets/images/user-group-512.png')}
              style={messagingStyles.profileContainer}
            /> :
            <Image
              source={user.profPicPath ? {uri: user.profPicPath }
                : require('../../../assets/images/user_dummy_image.png')}
              style={messagingStyles.profileContainer}
            />
          }
          <View style={messagingStyles.userDetails}>
            <Text style={messagingStyles.userNameText} numberOfLines={1}>{item.conversationType === 'GROUP' ? item.name : user.name}</Text>
            {item.unreadCount > 0 ? (
              <Text style={messagingStyles.unreadMessageText}>
                {item.unreadCount + ' unread messages'}
              </Text>
            ) : (
              <Text
                style={[
                  messagingStyles.unreadMessageText,
                  { color: style.color.nero }
                ]}
                numberOfLines={1}
              >
                {(item.messages && item.messages[item.messages.length - 1]) ? item.messages[item.messages.length - 1].message : 'No Messages'}
              </Text>
            )}
          </View>
          <View style={[messagingStyles.rightItemContainer, {alignItems: 'flex-end', position: 'relative'}]}>
            {muted ? (
            <Icon name='md-volume-off' style={{ color: 'rgb(207, 208, 220)', fontSize: 18, alignSelf: 'flex-end' }} />
            ) : (
              <Text style={messagingStyles.timeText}>{transformForConversations(item.messages && item.messages[item.messages.length - 1] ?
                item.messages[item.messages.length - 1].createdDate : item.updatedDate)}</Text>
            )}
            {/* <View style={messagingStyles.menuIcon}>
              <Image
                source={require('../../../assets/app_icons/Icons/bento-menu.png')}
              />
            </View> */}
            {messageSeen ? undefined
            :
            <View
              style={{
                height: 10,
                width: 10,
                borderRadius: 5,
                backgroundColor: '#297AE6',
                position: 'absolute',
                right: 5,
                bottom: 10
              }}
            />}
          </View>
        </TouchableOpacity>
    )} else {return <View />}
  }

  _renderItem(item, index) {
    let muted = false;
    item.memberInfos.forEach((member) => {
      if (member.users.id === this.props.userId) {
        muted = member.muted;
      }
    })
    let messageSeen = false;
    map(item.messages, (message, innerIndex) => {
      if (innerIndex === item.messages.length - 1) {
        map(message.messageMemberInfoList, (member) => {
          if (member.users.id === this.props.userId) {
            messageSeen = member.messageSeen;
          }
        })
      }
    })
    return (
      <CustomSwipeRow
        style={{
          backgroundColor: 'white'
        }}
        // leftOpenValue={65}
        rightOpenValue={-158}
        ref={(component) => {this.swipeRefs[index] = component}}
        body={this._renderChatBody(item, muted, index, messageSeen)}
        right={this._renderMute(item, muted, index)}
        onRowOpen={() => {
          this.swipeRefs.forEach((swipeRef, swipeIndex) => {
            if (swipeRef && swipeIndex !== index) {
              swipeRef._root.closeRow();
            }
          });
        }}
        // left={this._renderDelete(item, index)}
      />
    );
  }

  _renderMute(obj, muted, swipeIndex) {
    return (
      <View style={{flexDirection: 'row', flex: 1}}>
        <TouchableOpacity
          onPress={() => {this.props.muteConversation(obj.id, this.props.userId, (muted ? 'unmute' : 'mute'));
          if (this.swipeRefs[swipeIndex]) {
            this.swipeRefs[swipeIndex]._root.closeRow();
          }
        }}
          style={messagingStyles.muteSwiper}
        >
          <Icon name='md-volume-off' style={{ color: '#fff', fontSize: 22 }} />
          <Text style={messagingStyles.muteText}>{(muted) ? 'UNMUTE' : 'MUTE'}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            this.props.dialogModal.dialogShows(DELETE_MSG, DELETE_DESC, 'action', true, () =>
              this.props.deleteConversation(obj.id, this.props.userId, true))
            if (this.swipeRefs[swipeIndex]) {
              this.swipeRefs[swipeIndex]._root.closeRow();
            }
          }}
          style={messagingStyles.deleteSwiper}
        >
          <Icon name='md-trash' style={{ color: '#fff', fontSize: 22 }} />
          <Text style={messagingStyles.muteText}>{'DELETE'}</Text>
        </TouchableOpacity>
      </View>
    );
  }

  // _renderDelete(obj, swipeIndex) {
  //   return (
  //     <TouchableOpacity
  //       onPress={() => {
  //       this.props.dialogModal.dialogShows( DELETE_MSG, DELETE_DESC, 'action', true, () =>
  //         this.props.deleteConversation(obj.id, this.props.userId, true))
  //       if (this.swipeRefs[swipeIndex]) {
  //         this.swipeRefs[swipeIndex]._root.closeRow();
  //       }
  //     }}
  //       style={messagingStyles.rightSwiper}
  //     >
  //       <Image
  //         source={require('../../../assets/app_icons/Icons/Muted.png')}
  //         style={messagingStyles.muteIcon}
  //       />
  //       <Text style={messagingStyles.muteText}>{'DELETE'}</Text>
  //     </TouchableOpacity>
  //   );
  // }

  render() {
    return (
      <View style={messagingStyles.container}>
        <MSAHeader
          title={'Messages'}
          leftIcon={'add'}
          onLeftIconPress={() =>
            this.props.navigation.navigate('MessagingAddPeople', {users: []})
          }
        />
        <View style={messagingStyles.searchBarContainer}>
          <PeopleFilterSearch placeHolder={'Search'} onTextChange={(text) => this.setState({filter: text})}/>
        </View>
        <View style={{flex: 1, paddingBottom: 54}}>
          <FlatList
            data={filter(this.props.conversations, (conversation) => {
              return RegExp(this.state.filter).test(conversation.name) ||
              filter(conversation.memberInfos, (member) => RegExp(this.state.filter).test(member.users.name)).length > 0
            })}
            renderItem={obj => this._renderItem(obj.item, obj.index)}
            keyExtractor={item => item.id.toString()}
            refreshing={this.state.refreshing}
            onRefresh={() => this.props.getAllConversations()}
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    conversations: state.messagingStore.allConversations,
    // userInfo: state.userInfoAPI.userInfo,
    userId: get(state.userInfoAPI.userInfo, 'id', undefined)
  };
};

const mapDispatchToProps = dispatch => ({
  getAllConversations: bindActionCreators(getAllConversations, dispatch),
  muteConversation: bindActionCreators(muteConversation, dispatch),
  dialogModal: bindActionCreators(DialogModal, dispatch),
  deleteConversation: bindActionCreators(deleteConversation, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Messaging);
