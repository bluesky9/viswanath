import React, { Component } from 'react';
import { View } from 'react-native';
import { NavigationScreenProps } from 'react-navigation';

import { GiftedChat } from 'react-native-gifted-chat'
import MSAHeader from '../layouts/header/header';
import { getSocketConnection } from '../../App';

import { get, map } from 'lodash';
import moment from 'moment';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { findIndex } from 'lodash';
import uuidv4 from 'uuid';

import { getMessagesForConverstation, sendMessage, deleteMessage, getAllConversations, markMessageAsSeen } from '../../store/actions/Messaging';

interface IMessagingFilterNavParams {
  messages: any;
  name:  string;
  conversationIndex: string;
  muted: boolean;
}
interface IMessagingFilterProps
  extends NavigationScreenProps<IMessagingFilterNavParams> {
    contentSize: any;
    getMessages: Function;
    sendMessage: Function;
    messages: any;
    userId: string;
    deleteMessage: Function;
    conversations: any;
    conversationId: number;
    getAllConversations: Function;
    markMessageAsSeen: Function;
  }

type State = {
  messages: Array<Object>;
  isLoadingEarlier: boolean;
  conversationId: string;
}

function getFormattedMessages(messages: any) {
  const transformedMessages = [];
  messages.map((message) => {
    transformedMessages.push({
      _id: message.id,
      text: message.message,
      createdAt: moment(message.createdDate).toString(),
      user: {
        _id: message.messageMemberInfoList[0].users.id,
        name: message.messageMemberInfoList[0].users.name,
        avatar: message.messageMemberInfoList[0].users.profPicPath
      }
    })
  });
  return transformedMessages;
}

class MessagingSingle extends Component<IMessagingFilterProps, State > {
  socket = getSocketConnection();
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      isLoadingEarlier: false,
      conversationId: get(this.props.navigation.state.params, 'conversationId', '')
    }
    this.loadNextMessages = this.loadNextMessages.bind(this);
    this.handleNotifications = this.handleNotifications.bind(this);
    this.clearCurrentMessages = this.clearCurrentMessages.bind(this);
  }

  componentDidMount() {
    // Fetch messages
    this.props.getMessages(this.state.conversationId, 0);
    this.socket.emit('joinRoom', { roomName: this.state.conversationId})
    this.socket.on('chatevent', (data) => {
      this.handleNotifications(data)
    })
    this.socket.on('sendToRoom', (data) => {
      this.handleNotifications(data)
    })
  }

  clearCurrentMessages() {
    this.setState({
      messages: []
    })
  }

  componentWillUnmount() {
    this.socket.removeListener('chatevent');
    this.socket.removeListener('sendToRoom')
  }

  handleNotifications(data) {
    let messages = [{text: '', user: {_id : undefined, name: '', avatar: ''}, createdAt: Date.now(), _id: uuidv4()}];
    if (data.conversationID === this.state.conversationId.toString() && this.props.userId.toString() !== data.sendFrom.id) {
      messages[0].text = data.message;
      messages[0].user = {
        _id: parseInt(data.sendFrom.id, 10),
        name: data.sendFrom.firstName + ' ' + data.sendFrom.lastName,
        avatar: data.sendFrom.profPicPath
      }
      this.setState(previousState => ({
        messages: GiftedChat.append(previousState.messages, messages)
      }));
      this.props.markMessageAsSeen(this.state.conversationId);
    }
  }

  loadNextMessages() {
    this.props.getMessages(this.state.conversationId, this.props.messages.number);
  }

  onSend(messages = []) {
    let userIds = [];
    let userDetails = {
      firstName: '',
      lastName: '',
      profPicPath: ''
    }
    let conversationIndex = this.props.navigation.state.params.conversationIndex;
    if (this.props.conversations[conversationIndex]
      && this.state.conversationId === this.props.conversations[conversationIndex].id) {
        let conversationItem = this.props.conversations[conversationIndex];
        map(conversationItem.memberInfos, (member) => {
          if (member.users.id === this.props.userId) {
            userDetails.firstName = member.users.firstName;
            userDetails.lastName = member.users.lastName;
            userDetails.profPicPath = member.users.profPicPath;
          }
          userIds.push(member.users.id)
        });
    }
    // let sendTo = { roomName: this.state.conversationId};
    let sendFromObj = {
        id: this.props.userId,
        firstName : userDetails.firstName ,
        lastName: userDetails.lastName,
        profPicPath: userDetails.profPicPath
    };
    let socketData = {conversationID: this.state.conversationId, sendFrom : sendFromObj ,
      sendTo: this.state.conversationId, message: messages[0].text};
    this.socket.emit('sendToRoom', socketData);
    if (messages[0].text) {
      this.props.sendMessage(this.state.conversationId, {message: messages[0].text, messageType: 'INFO', senderUserId: this.props.userId});
      this.setState(previousState => ({
        messages: GiftedChat.append(previousState.messages, messages)
      }))
    }
  }

  onLongPress(context: any, message: any) {
        if (message.user._id === this.props.userId) {
          const options = [
            'Delete',
            'Cancel'
          ];
          const cancelButtonIndex = options.length - 1;
          context.actionSheet().showActionSheetWithOptions({
            options,
            cancelButtonIndex
          },
          (buttonIndex) => {
            switch (buttonIndex) {
              case 0:
                let index = findIndex(this.state.messages.concat(getFormattedMessages(this.props.messages.content)),
                  (messageObj) => messageObj._id === message._id)
                if (index > -1) {
                  if (this.state.messages.length <= index) {
                    this.state.messages.splice(index, 1);
                    this.setState({messages: this.state.messages});
                  }
                  this.props.deleteMessage(this.state.conversationId, message._id)
                }
                break;
              default: break;
            }
          });
        }
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <MSAHeader
          title={(this.props.navigation.state.params.name) ? this.props.navigation.state.params.name : 'NA'}
          onPress={() => {
            this.props.getAllConversations();
            this.props.navigation.goBack()}
          }
          rightIcon={'menu'}
          onRightIconPress={() => this.props.navigation.navigate('MessagingChatInformation', {name: this.props.navigation.state.params.name,
              members: this.props.messages.content.length > 0 ? this.props.messages.content[0].messageMemberInfoList : [],
                conversationId: this.state.conversationId, clearCurrentMessages: this.clearCurrentMessages,
                conversationIndex: this.props.navigation.state.params.conversationIndex })}
        />
        <GiftedChat
          messages={this.state.messages.concat(getFormattedMessages(this.props.messages.content))}
          loadEarlier={!this.props.messages.last}
          onLoadEarlier={this.loadNextMessages}
          onSend={messages => this.onSend(messages)}
          onLongPress={(context, message) => this.onLongPress(context, message)}
          user={{
            _id: this.props.userId
          }}
        />
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    messages: state.messagingStore.messages,
    conversations: state.messagingStore.allConversations,
    // userInfo: state.userInfoAPI.userInfo,
    userId: get(state.userInfoAPI.userInfo, 'id', undefined)
  };
};

const mapDispatchToProps = dispatch => ({
  getAllConversations: bindActionCreators(getAllConversations, dispatch),
  getMessages: bindActionCreators(getMessagesForConverstation, dispatch),
  markMessageAsSeen: bindActionCreators(markMessageAsSeen, dispatch),
  sendMessage: bindActionCreators(sendMessage, dispatch),
  deleteMessage: bindActionCreators(deleteMessage, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(MessagingSingle);
