import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, ScrollView, FlatList } from 'react-native';
import { NavigationScreenProps } from 'react-navigation';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import PeopleFilterSearch from '../people/people-filter-search';
import MSAHeader from '../layouts/header/header';

import { messagingStyles, addPeopleStyles } from './messaging-styles';
import { getPeopleByName, getPeopleInfo } from '../../store/actions/People.js'
import { addPersonToConversation, removePersonFromConversation, createConversation } from '../../store/actions/Messaging';
import { DialogModal } from '../../store/actions';

import { map, findIndex, cloneDeep, isEmpty, get } from 'lodash';

import { style } from '../../styles/variables';
const REMOVE_USER = 'Remove user from conversation'
const REMOVE_USER_DESC = 'Are you sure to remove the user from conversation?'

interface IMessagingFilterNavParams {
  users: any;
  conversationId: any;
  clearCurrentMessages: Function;
}
interface IMessagingFilterProps
  extends NavigationScreenProps<IMessagingFilterNavParams> {
    peopleData?: any;
    getPeopleByName?: any;
    peopleApi?: any;
    userId?; any;
    connections: any;
    addPersonToConversation: Function;
    removePersonFromConversation: Function;
    createConversation: Function;
    conversations: any;
    dialogModal: any;
  }

type State = {
    peopleSearch;
    myUserId;
    searchTerm: string;
    peopleData: any;
    userIds: any;
};

class MessagingAddPeople extends Component<IMessagingFilterProps, State> {

  constructor(props) {
    super(props);
    let users = [];
    let userIds = [];
    if (this.props.navigation.state.params.users) {
      map(this.props.navigation.state.params.users, (member) => {
        if (member.users.id !== props.userId) {
          users.push(member.users);
          userIds.push(member.users.id);
        } else {
          member.users.owner = true;
          users.push(member.users);
          userIds.push(member.users.id);
        }
      })
    }
    this.state = {
        searchTerm: '',
        myUserId: undefined,
        peopleSearch: {
          userTypes: [],
          countries: [],
          pageSize: 15,
          pageNo: 0,
          univIds: [],
          userIds: [],
          countOnly: false
      },
      userIds,
      peopleData: users
    }
  }

  componentDidMount() {
    let peopleSearch = cloneDeep(this.state.peopleSearch)
    peopleSearch.myUserId = this.props.userId
  }

  _handleOnTextChange = (text) => {
    this.setState({searchTerm: text});
  }

  addUser(user: any) {
    let index = findIndex(this.state.peopleData, (people) => people.id === user.id);
    if (index === -1) {
      if (this.props.navigation.state.params.conversationId) {
        this.props.addPersonToConversation(this.props.navigation.state.params.conversationId, user.id)
        if (this.props.navigation.state.params.clearCurrentMessages) {
          this.props.navigation.state.params.clearCurrentMessages();
        }
      }
      this.state.peopleData.push(user);
      this.state.userIds.push(user.id);
      this.setState({
        userIds: this.state.userIds,
        peopleData: this.state.peopleData
      })
    }
  }

  removeUser(index) {
    if (this.props.navigation.state.params.conversationId) {
      if (this.state.peopleData.length < 3 ) {
        alert('There must exist atleast 2 users')
      } else {
        if (index > -1) {
          this.props.removePersonFromConversation(this.props.navigation.state.params.conversationId, this.state.peopleData[index].id)
          if (this.props.navigation.state.params.clearCurrentMessages) {
            this.props.navigation.state.params.clearCurrentMessages();
          }
          this.state.peopleData.splice(index, 1);
          this.state.userIds.splice(index, 1);
          this.setState({
            userIds: this.state.userIds,
            peopleData: this.state.peopleData
          })
        }
      }
    } else {
      if (index > -1) {
        this.state.userIds.splice(index, 1);
        this.state.peopleData.splice(index, 1);
        this.setState({
          userIds: this.state.userIds,
          peopleData: this.state.peopleData
        })
      }
    }
  }

  // renderSearchResult() {
  //   return(
  //     <View>
  //       <View style={addPeopleStyles.listContainer}>
  //         <Text style={addPeopleStyles.listTitleText}>Search Results</Text>
  //       </View>
  //       {(!isEmpty(this.props.peopleData) && this.props.peopleData.length > 0) ?
  //       <FlatList
  //         data={this.props.peopleData}
  //         renderItem={obj => <View style={addPeopleStyles.itemContainer}>
  //                     <TouchableOpacity
  //                       onPress={() => obj.item.id === this.props.userId ? this.props.navigation.navigate('MyProfile') :
  //                       this.props.peopleApi(obj.item.id)}
  //                     >
  //                     <Image
  //                       source={obj.item.profPicPath ? { uri: obj.item.profPicPath } : require('../../../assets/images/user_dummy_image.png')}
  //                       style={addPeopleStyles.profileContainer}
  //                     />
  //                     </TouchableOpacity>
  //                     <Text style={addPeopleStyles.userNameText}>{obj.item.firstName + ' ' + obj.item.lastName}</Text>
  //                     <TouchableOpacity style={addPeopleStyles.removeButtonContainer} onPress={() => this.addUser(obj.item)}>
  //                       <Text style={[addPeopleStyles.removeButtonText, {color: style.color.primary}]}>
  //                         {(this.state.userIds.indexOf(obj.item.id) === -1) ? 'Add' : 'Added'}
  //                       </Text>
  //                     </TouchableOpacity>
  //                     </View>
  //         }
  //         keyExtractor={item => item.id.toString()}
  //         contentContainerStyle={{ paddingBottom: 60 }}
  //         showsVerticalScrollIndicator={false}
  //       /> :
  //       <Text> 0 Results</Text>
  //       }
  //     </View>
  //   )
  // }

  renderAddedPeople(obj) {
    return <View style={[addPeopleStyles.itemContainer, { flex: 1}]}>
              <TouchableOpacity
                onPress={() => obj.item.owner ? this.props.navigation.navigate('MyProfile') :
                this.props.peopleApi(obj.item.id)}
                style={{flexDirection: 'row', flex: 1, alignItems: 'center'}}
              >
              {
                obj.item.profPicPath ?
                <Image
                  source={obj.item.profPicPath ? { uri: obj.item.profPicPath } : require('../../../assets/images/user_dummy_image.png')}
                  style={addPeopleStyles.profileContainer}
                /> :
                <Image
                  source={obj.item.profilePicture ? { uri: obj.item.profilePicture } : require('../../../assets/images/user_dummy_image.png')}
                  style={addPeopleStyles.profileContainer}
                />
              }
                <Text numberOfLines = {1} style={[addPeopleStyles.userNameText, { flex: 1 }]}>
                  {obj.item.name ? obj.item.name : (obj.item.firstName + ' ' + obj.item.lastName)}
                </Text>
              </TouchableOpacity>
              <View style={{ flex: 0 }}>
                {!obj.item.owner && <TouchableOpacity style={{ flex: 0 }}onPress={() => {
                  this.props.dialogModal.dialogShows( REMOVE_USER, REMOVE_USER_DESC, 'action', true, () =>
                    this.removeUser(obj.index))}}>
                  <Text style={addPeopleStyles.removeButtonText}>REMOVE</Text>
                </TouchableOpacity>}
              </View>
            </View>
  }

  _renderPeople() {
    return(
      <View>
        <View style={addPeopleStyles.listContainer}>
          <Text style={addPeopleStyles.listTitleText}>People in conversation</Text>
        </View>
        {(!isEmpty(this.state.peopleData) && this.state.peopleData.length > 0) &&
          <FlatList
            data={this.state.peopleData}
            renderItem={(obj) => this.renderAddedPeople(obj)}
            keyExtractor={item => item.id.toString()}
            contentContainerStyle={{ paddingBottom: 60 }}
            showsVerticalScrollIndicator={false}
          />
        }
      </View>
    )
  }

  checkForExistingConversation() {
    let otherConversationFound = false;
   map(get(this.props, 'conversations', []), (conversation, index) => {
     if (conversation.memberInfos && conversation.memberInfos.length === 2) {
      let foundCount = 0;
      let otherUser = undefined;
        map(conversation.memberInfos, (member) => {
          if (this.state.userIds.indexOf(member.users.id) !== -1) {
            foundCount++;
          }
          if (member.users.id !== this.props.userId) {
            otherUser = member.users;
          }
        })
        if (foundCount === 1 && conversation.conversationType === 'ONE_TO_ONE') {
          otherConversationFound = true;
          const navigation = this.props.navigation as any;
          navigation.replace('MessagingSingle', {messages: conversation.messages, conversationId: conversation.id,
            name: conversation.conversationType === 'ONE_TO_ONE' ? otherUser.name : conversation.name, muted: false, conversationIndex: index});
        }
     }
   })
   if (!otherConversationFound) {
      this.props.createConversation({
        conversationType: 'ONE_TO_ONE',
        logoURL: 'test.jpg',
        memberUserIds: map(this.state.peopleData, 'id'),
        name: 'not-blank',
        ownerOrRequesterUserId: this.props.userId
      })
    }
  }

  _renderConnections() {
    return (
      <View>
        <View style={addPeopleStyles.listContainer}>
          <Text style={addPeopleStyles.listTitleText}>Your connections</Text>
        </View>
        { this.props.connections.map((item, index) => {
            if (this.state.searchTerm) {
              if (RegExp(this.state.searchTerm).test(item.name)) {
                return (
                  <View style={addPeopleStyles.itemContainer} key={index}>
                    <TouchableOpacity
                            onPress={() => item.id === this.props.userId ? this.props.navigation.navigate('MyProfile') :
                            this.props.peopleApi(item.id)}
                          >
                      <Image
                        source={item.profilePicture ? { uri: item.profilePicture } : require('../../../assets/images/user_dummy_image.png')}
                        style={addPeopleStyles.profileContainer}
                      />
                    </TouchableOpacity>
                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                      <Text numberOfLines = {1}style={[addPeopleStyles.userNameText, { flex: 1 }]}>{item.name}</Text>
                      <TouchableOpacity style={{ alignItems: 'flex-end'}} onPress={() => this.addUser(item)}>
                        <Text style={[addPeopleStyles.removeButtonText, {color: style.color.primary}]}>
                        {(this.state.userIds.indexOf(item.id) === -1) ? 'ADD' : 'ADDED'}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                )
              } else {
                return undefined;
              }
            } else {
              return (
                <View style={addPeopleStyles.itemContainer} key={index}>
                  <TouchableOpacity
                          onPress={() => item.id === this.props.userId ? this.props.navigation.navigate('MyProfile') :
                          this.props.peopleApi(item.id)}
                        >
                    <Image
                      source={item.profilePicture ? { uri: item.profilePicture } : require('../../../assets/images/user_dummy_image.png')}
                      style={addPeopleStyles.profileContainer}
                    />
                  </TouchableOpacity>
                  <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                    <Text numberOfLines = {1}style={[addPeopleStyles.userNameText, { flex: 1 }]}>{item.name}</Text>
                    <TouchableOpacity style={{ alignItems: 'flex-end'}} onPress={() => this.addUser(item)}>
                      <Text style={[addPeopleStyles.removeButtonText, {color: style.color.primary}]}>
                      {(this.state.userIds.indexOf(item.id) === -1) ? 'ADD' : 'ADDED'}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              )
            }
          })
        }
      </View>
    )
  }

  render() {
    return (
      <View style={messagingStyles.container}>
        <MSAHeader
          title={'Add people'}
          rightText={'DONE'}
          onRightTextPress={() => (this.state.peopleData.length > 0) ?
              (!this.props.navigation.state.params.conversationId) ?
                (this.state.peopleData.length === 1) ?
                  this.checkForExistingConversation() :
                this.props.navigation.navigate('MessagingCreateGroup', {users: this.state.peopleData, userId: this.props.userId})
                : this.props.navigation.goBack()
              : alert('Add some users')}
          onPress={() => this.props.navigation.goBack() }
        />
        <View style={messagingStyles.searchBarContainer}>
          <PeopleFilterSearch placeHolder={'Search People'} onTextChange={this._handleOnTextChange}/>
        </View>
        <ScrollView style={addPeopleStyles.mainContent}>
          {this._renderPeople()}
          {this._renderConnections()}
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return ({
    conversations: state.messagingStore.allConversations,
    peopleData: state.peopleByName.people,
    connections: state.userInfoAPI.userInfo.connections,
    userId: state.userInfoAPI.userId
  })
}

const mapDispatchToProps = dispatch => ({
    getPeopleByName: bindActionCreators(getPeopleByName, dispatch),
    removePersonFromConversation: bindActionCreators(removePersonFromConversation, dispatch),
    addPersonToConversation: bindActionCreators(addPersonToConversation, dispatch),
    createConversation: bindActionCreators(createConversation, dispatch),
    peopleApi: bindActionCreators(getPeopleInfo, dispatch),
    dialogModal: bindActionCreators(DialogModal, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MessagingAddPeople)
