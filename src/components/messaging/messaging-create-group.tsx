import React, { Component } from 'react';
import { View, Text, TextInput } from 'react-native';
import { NavigationScreenProps } from 'react-navigation';

import MSAHeader from '../layouts/header/header';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { map } from 'lodash';

import { addGroupStyles } from './messaging-styles';
import { style } from '../../styles/variables';
import { createConversation, renameConversation } from '../../store/actions/Messaging.js';

interface IMessagingFilterNavParams {
  userId: string;
  users: any;
  name: any;
  conversationId: string;
}
interface IMessagingFilterProps
  extends NavigationScreenProps<IMessagingFilterNavParams> {
    createConversation: Function;
    renameConversation: Function;
  }

interface State {
  groupName: string;
}

class MessagingCreateGroup extends Component<IMessagingFilterProps, State > {
  constructor(props) {
    super(props);
    this.state = {
      groupName: (this.props.navigation.state.params.name) ? this.props.navigation.state.params.name : ''
    }
  }
  render() {
    return (
      <View style={addGroupStyles.container}>
        <MSAHeader
          title={this.props.navigation.state.params.conversationId ? 'Update group name' : 'New group name'}
          rightText={'SAVE'}
          leftText={'CANCEL'}
          onRightTextPress={() => {
            if (this.state.groupName && this.props.navigation.state.params.users && this.state.groupName.length >= 2) {
                this.props.createConversation({
                conversationType: 'GROUP',
                logoURL: 'test.jpg',
                memberUserIds: map(this.props.navigation.state.params.users, 'id'),
                name: this.state.groupName,
                ownerOrRequesterUserId: this.props.navigation.state.params.userId
              })
              } else if (this.state.groupName.length > 2) {
                this.props.renameConversation(this.props.navigation.state.params.conversationId, this.state.groupName)
              }
            }
            }
          onPress={() =>  this.props.navigation.goBack()}
        />
        <View style={addGroupStyles.mainContent}>
        <Text style={addGroupStyles.itemTitle}>GROUP NAME</Text>
        <TextInput
          value={this.state.groupName}
          onChangeText={(text) => this.setState({groupName: text})}
          style={addGroupStyles.textField}
          maxLength = {20}
          placeholder={'Tap to enter new group name...'}
          placeholderTextColor={style.color.grey}
        />
        </View>
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => ({
    createConversation: bindActionCreators(createConversation, dispatch),
    renameConversation: bindActionCreators(renameConversation, dispatch)
})

export default connect(
  undefined,
  mapDispatchToProps
)(MessagingCreateGroup)
