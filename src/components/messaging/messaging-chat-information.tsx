import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Switch } from 'react-native';
import { Icon } from 'native-base';
import { NavigationScreenProps } from 'react-navigation';

import MSAHeader from '../layouts/header/header';

import { messagingStyles } from './messaging-styles';
import { style } from '../../styles/variables';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { get } from 'lodash';

import { leaveConversation, clearConversation, muteConversation, deleteConversation } from '../../store/actions/Messaging';
import { DialogModal } from '../../store/actions';

const LEAVE_MSG = 'Are you sure to leave'
const LEAVE_DESC = 'You will no longer see the messages from this conversation'
const DELETE_MSG = 'Are you sure to delete'
const DELETE_DESC = 'You will no longer see the messages from this conversation'
const CLEAR_MSG = 'Are you sure to clear'
const CLEAR_DESC = 'You will no longer see the messages from this conversation'

interface IMessagingFilterNavParams {
  name: string;
  members: any;
  conversationId: string;
  muted: boolean;
  conversationIndex: number;
  clearCurrentMessages: Function;
}
interface IMessagingFilterProps
  extends NavigationScreenProps<IMessagingFilterNavParams> {
    userId: string;
    dialogModal?: any;
    leaveConversation: Function;
    clearMessages: Function;
    messages: any;
    conversations: any;
    muteConversation: Function;
    deleteConversation: Function;
  }

interface State {
  notifications: boolean;
  type: string;
}

class MessagingChatInformation extends Component<IMessagingFilterProps, State > {

  constructor(props) {
    super(props);
    let muted = false;
    let type = 'ONE_TO_ONE';
    let item = props.conversations[props.navigation.state.params.conversationIndex];
    if (item && item.id === props.navigation.state.params.conversationId) {
      type = item.conversationType;
      item.memberInfos.forEach((member) => {
        if (member.users.id === props.userId) {
          muted = member.muted;
        }
      })
    }
    this.state = {
      notifications: !muted,
      type: type
    }
  }

  render() {
    let item = this.props.conversations[this.props.navigation.state.params.conversationIndex];
    let members = [];
    if (item && item.id === this.props.navigation.state.params.conversationId) {
      members = item.memberInfos;
    } else {
      // Updated List
      item = this.props.conversations[0];
      if (item && item.id === this.props.navigation.state.params.conversationId) {
        members = item.memberInfos;
      }
    }
    return (
      <View style={{ backgroundColor: style.color.whiteCream }}>
        <MSAHeader title={'Chat information'}onPress={() => this.props.navigation.goBack()}/>
        <TouchableOpacity style={[messagingStyles.chatItemView, { alignItems: 'center' }]}>
          <Text style={messagingStyles.chatItemText}>Notifications</Text>
          <View style={{ alignItems: 'flex-end' }}>
            <Switch value={this.state.notifications} onValueChange={() => {
              this.props.muteConversation(this.props.navigation.state.params.conversationId,
                this.props.userId, (this.state.notifications ? 'mute' : 'unmute'))
              this.setState({notifications: !this.state.notifications})}}
              style={messagingStyles.switch}
              />
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={messagingStyles.chatItemView} onPress={() =>
            this.props.navigation.navigate('MessagingAddPeople',
            {users: members,
            clearCurrentMessages: this.props.navigation.state.params.clearCurrentMessages,
            conversationId: this.props.navigation.state.params.conversationId})}>
          <Text style={messagingStyles.chatItemText}>
            {members.length} Members
          </Text>
          <Icon name='md-arrow-forward' style={{ color: 'rgb(207, 208, 220)', fontSize: 20, alignSelf: 'flex-end' }} />
        </TouchableOpacity>
        <TouchableOpacity style={messagingStyles.chatItemView} onPress={() =>
            this.props.navigation.navigate('MessagingCreateGroup',
            {userId: this.props.userId, name: this.props.navigation.state.params.name,
            conversationId: this.props.navigation.state.params.conversationId})}>
          <Text style={messagingStyles.chatItemText}>{this.props.navigation.state.params.name}</Text>
          <Icon name='md-arrow-forward' style={{ color: 'rgb(207, 208, 220)', fontSize: 20, alignSelf: 'flex-end' }} />
        </TouchableOpacity>
        <TouchableOpacity style={messagingStyles.chatItemView} onPress={() =>
        this.props.dialogModal.dialogShows( CLEAR_MSG, CLEAR_DESC, 'action', true, () => {
            this.props.navigation.state.params.clearCurrentMessages();
            this.props.clearMessages(this.props.navigation.state.params.conversationId, this.props.userId)})
          }>
          <Text style={messagingStyles.chatItemText}>Clear conversation</Text>
        </TouchableOpacity>
        <TouchableOpacity style={messagingStyles.chatItemView} onPress={() =>
          this.props.dialogModal.dialogShows( DELETE_MSG, DELETE_DESC, 'action', true, () =>
          this.props.deleteConversation(this.props.navigation.state.params.conversationId, this.props.userId))}>
          <Text style={[messagingStyles.chatItemText, { color: 'rgb(204,20,51)'}]}>Delete conversation</Text>
        </TouchableOpacity>
        {this.state.type === 'GROUP' && <TouchableOpacity style={messagingStyles.chatItemView} onPress={() =>
          this.props.dialogModal.dialogShows( LEAVE_MSG, LEAVE_DESC, 'action', true, () =>
          this.props.leaveConversation(this.props.navigation.state.params.conversationId, this.props.userId))}>
          <Text style={[messagingStyles.chatItemText, { color: 'rgb(204,20,51)'}]}>Leave conversation</Text>
        </TouchableOpacity>}
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    messages: state.messagingStore.messages,
    conversations: state.messagingStore.allConversations,
    // userInfo: state.userInfoAPI.userInfo,
    userId: get(state.userInfoAPI.userInfo, 'id', undefined)
  };
};

const mapDispatchToProps = dispatch => ({
  leaveConversation: bindActionCreators(leaveConversation, dispatch),
  dialogModal: bindActionCreators(DialogModal, dispatch),
  clearMessages: bindActionCreators(clearConversation, dispatch),
  muteConversation: bindActionCreators(muteConversation, dispatch),
  deleteConversation: bindActionCreators(deleteConversation, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(MessagingChatInformation);
