import { StyleSheet } from 'react-native';

import { style } from '../../styles/variables';

export const messagingStyles = StyleSheet.create({
  container: {
    backgroundColor: style.color.whiteCream,
    flex: 1
    // paddingBottom: 54
  },
  searchBarContainer: {
    paddingHorizontal: 15,
    paddingVertical: 12
  },
  swiper: {
    paddingTop: 0,
    paddingBottom: 0,
    borderBottomWidth: 0.5,
    borderBottomColor: style.color.cream
  },
  swiperBodyContainer: {
    backgroundColor: style.color.white,
    paddingLeft: 15,
    paddingVertical: 14,
    flexDirection: 'row'
  },
  profileContainer: {
    height: 50,
    width: 50,
    borderRadius: 25
  },
  userDetails: {
    paddingHorizontal: 15,
    paddingTop: 4,
    flex: 1
  },
  userNameText: {
    fontFamily: style.font.family.rubikMedium,
    fontSize: 16,
    lineHeight: 19,
    letterSpacing: 0.2,
    color: style.color.bluishGrey
  },
  unreadMessageText: {
    fontSize: 14,
    lineHeight: 17,
    fontFamily: style.font.family.rubikRegular,
    color: style.color.primary,
    paddingTop: 7
  },
  rightItemContainer: {
    paddingTop: 4
  },
  timeText: {
    fontFamily: style.font.family.rubikRegular,
    fontSize: 12,
    lineHeight: 14,
    color: style.color.nero,
    paddingRight: 4
  },
  menuIcon: {
    paddingTop: 12,
    paddingBottom: 12,
    paddingLeft: 12,
    alignSelf: 'flex-end'
  },
  muteSwiper: {
    backgroundColor: 'orange',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10
  },
  deleteSwiper: {
    backgroundColor: 'rgb(255,42,78)',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10
  },
  muteIcon: {
    tintColor: style.color.white,
    height: 19,
    width: 19,
    resizeMode: 'contain'
  },
  muteText: {
    fontSize: 10,
    lineHeight: 12,
    fontFamily: style.font.family.rubikMedium,
    letterSpacing: 0.5,
    color: style.color.white,
    paddingTop: 10
  },
  chatItemView: {
    paddingVertical: 18,
    paddingHorizontal: 15,
    flexDirection: 'row',
    backgroundColor: style.color.white,
    borderBottomWidth: 1,
    borderColor: style.color.lightGrey
  },
  chatItemText: {
    flex: 1,
    fontSize: 16,
    lineHeight: 19,
    fontFamily: style.font.family.rubikRegular,
    color: style.color.coal
  },
  switch: {
    // transform: [{ scaleX: .61 }, { scaleY: .61 }],
    height: 30
  }
});

export const addPeopleStyles = StyleSheet.create({
  mainContent: {
    borderTopWidth: 1,
    paddingHorizontal: 15,
    borderColor: style.color.lightGrey
  },
  listContainer: {
    paddingVertical: 15,
    borderBottomWidth: 1,
    borderColor: style.color.lightGrey
  },
  listTitleText: {
    color: style.color.coal,
    fontFamily: style.font.family.rubikRegular,
    fontSize: 14,
    lineHeight: 17
  },
  itemContainer: {
    paddingVertical: 15,
    borderBottomWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: style.color.lightGrey
  },
  profileContainer: {
    height: 41,
    width: 41,
    borderRadius: 20.5
  },
  userNameText: {
    paddingLeft: 9,
    // flex: 1,
    fontSize: 16,
    lineHeight: 21,
    fontFamily: style.font.family.rubikRegular,
    color: style.color.primary
  },
  removeButtonContainer: {
    alignItems: 'flex-end'
  },
  removeButtonText: {
    fontFamily: style.font.family.rubikMedium,
    fontSize: 14,
    lineHeight: 17,
    paddingVertical: 7,
    paddingLeft: 10,
    letterSpacing: 0.5,
    color: 'rgb(204,20,51)'
  }
})

export const addGroupStyles = StyleSheet.create({
  container: {
    backgroundColor: style.color.whiteCream,
    flex: 1
  },
  mainContent: {
    paddingHorizontal: 15,
    paddingTop: 20,
    backgroundColor: style.color.white,
    borderBottomWidth: 1,
    borderBottomColor: style.color.lightGrey
  },
  itemTitle: {
    letterSpacing: 1,
    color: style.color.bluishGrey,
    fontFamily: style.font.family.rubikMedium,
    fontSize: 12,
    lineHeight: 14
  },
  textField: {
    paddingVertical: 10,
    fontSize: 14,
    lineHeight: 18,
    fontFamily: style.font.family.rubikRegular
  }
})