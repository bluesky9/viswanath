import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  ScrollView,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
  Dimensions
} from 'react-native';
import { style } from '../../styles/variables';
import { bindActionCreators } from 'redux';
import notificationStyles from './notification-styles';
import MSAHeader from '../layouts/header/header';
import { connect } from 'react-redux';
import {
  getAllNotifications,
  getConnectionNotifications,
  getNotificationsUnreadCount,
  markAllAsSeen,
  acceptConnection,
  dismissConnection
} from '../../store/actions/Notifications.js';
import { getPost } from '../../store/actions/Feed.js';
import { getPeopleInfo } from '../../store/actions/People.js';
import { transformForPosts } from '../../utils';
import { get, isEmpty } from 'lodash';

import { NavigationScreenProps } from 'react-navigation';
import { PeopleApi } from '../../store/actions'
// import notificationStyles from '../profile/profile-styles';
interface IPeopleFilterNavParams {
  displayNotif: boolean
}
interface IPeopleFilterProps
  extends NavigationScreenProps<IPeopleFilterNavParams> {
  actions: any;
  notificationState: any;
  peopleApi: {
    getPeopleInfo: Function;
  };
  userId: number;
}
interface NotificationsState {
  index: number;
  showModal: boolean;
  notiCount: number;
  originalCount: number;
}
const { width } = Dimensions.get('window');
class Notifications extends Component<IPeopleFilterProps, NotificationsState> {
  constructor(props) {
    super(props);
    this.state = {
      index: (get(this.props.navigation.state, 'params.displayNotif', false)) ?
        1 : 0, notiCount: 10, showModal: false, originalCount: 0
    };
  }
  componentDidMount() {
    this.props.actions.getAllNotifications(this.state.notiCount, 0);
    this.props.actions.getConnectionNotifications();
  }
  componentWillUnmount() {
    this.props.actions.markAllAsSeen();
    this.props.actions.getAllNotifications(this.state.notiCount, 0);
    this.props.actions.getNotificationsUnreadCount();
  }
  renderGeneralNotification(item, index) {
    if (item.notificationType === 'POST') {
      return this.renderPostNotification(item, index);
    }
    if (item.notificationType === 'TAG') {
      return this.renderTagNotification(item, index);
    }
    if (item.notificationType === 'LIKE') {
      return this.renderLikeNotification(item, index);
    }
    if (item.notificationType === 'COMMENT') {
      return this.renderCommentNotification(item, index);
    }
    if (item.notificationType === 'SHARE') {
      return this.renderShareNotification(item, index);
    }
    if (item.notificationType === 'FOLLOW') {
      return this.renderFollowNotification(item, index);
    }
    return undefined;
  }

  renderFollowNotification(data, index) {
    let m = transformForPosts(data.notificationTime);
    return (
      <TouchableOpacity
        onPress={() => this.props.actions.getPeopleInfo(data.userFollowNotification.requesterUser.id)}
        key={index}
        style={[
          notificationStyles.dispView,
          {
            backgroundColor: data.seennotification
              ? 'white'
              : 'rgb(232, 234, 237)'
          }
        ]}
      >
        <Image
          source={{
            uri: data.userFollowNotification.requesterUser.user.profPicPath
              ? data.userFollowNotification.requesterUser.user.profPicPath
              : 'https://www.buira.org/assets/images/shared/default-profile.png'
          }}
          style={notificationStyles.picDisp}
        />
        <View
          style={{
            marginLeft: 11,
            justifyContent: 'flex-start'
          }}
        >
          <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
            <Text style={[notificationStyles.boxLow,
            { width: width - 82 }]}>
              {data.userFollowNotification.requesterUser.user.name}
              <Text
                style={[notificationStyles.boxLow,
                { color: 'rgb(83,87,94)', fontSize: 14, lineHeight: 17, fontFamily: style.font.family.rubikRegular }]}
              >
                {' '}
                started following you
              </Text>
            </Text>
          </View>
          <Text
            style={[
              notificationStyles.boxLow,
              { color: 'rgb(173,181,196)', marginTop: 5, fontFamily: style.font.family.rubikRegular }
            ]}
          >
            {m}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

  renderCommentNotification(data, index) {
    let m = transformForPosts(data.notificationTime);
    return (
      <TouchableOpacity
        onPress={() => this.props.actions.getPost(data.commentspostNotification.post.postid)}
        key={index}
        style={[
          notificationStyles.dispView,
          {
            backgroundColor: data.seennotification
              ? 'white'
              : 'rgb(232, 234, 237)'
          }
        ]}
      >
        <Image
          source={{
            uri: data.commentspostNotification.user.profPicPath
              ? data.commentspostNotification.user.profPicPath
              : 'https://www.buira.org/assets/images/shared/default-profile.png'
          }}
          style={notificationStyles.picDisp}
        />
        <View
          style={{
            marginLeft: 11,
            justifyContent: 'flex-start'
          }}
        >
          <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
            <Text style={[notificationStyles.boxLow,
            { width: width - 82 }]}>
              {data.commentspostNotification.user.name}
              <Text
                style={[notificationStyles.boxLow,
                { color: 'rgb(83,87,94)', fontSize: 14, lineHeight: 17, fontFamily: style.font.family.rubikRegular }]}
              >
                {' '}
                commented on a post
                {' '}
              </Text>
              <Text
                style={[
                  notificationStyles.boxLow,
                  { color: 'rgb(83,87,94)', marginLeft: 3 }
                ]}
              >
                {data.commentspostNotification.post.title}
              </Text>
            </Text>
          </View>
          <Text
            style={[
              notificationStyles.boxLow,
              { color: 'rgb(173,181,196)', marginTop: 5, fontFamily: style.font.family.rubikRegular }
            ]}
          >
            {m}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

  renderShareNotification(data, index) {
    let m = transformForPosts(data.notificationTime);
    return (
      <TouchableOpacity
        key={index}
        style={[
          notificationStyles.dispView,
          {
            backgroundColor: data.seennotification
              ? 'white'
              : 'rgb(232, 234, 237)'
          }
        ]}
      >
        <Image
          source={{
            uri: data.sharepostNotification.user.profPicPath
              ? data.sharepostNotification.user.profPicPath
              : 'https://www.buira.org/assets/images/shared/default-profile.png'
          }}
          style={notificationStyles.picDisp}
        />
        <View
          style={{
            marginLeft: 11,
            justifyContent: 'flex-start'
          }}
        >
          <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
            <Text style={[notificationStyles.boxLow,
            { width: width - 82 }]}>
              {data.sharepostNotification.user.name}
              <Text
                style={[notificationStyles.boxLow,
                { color: 'rgb(83,87,94)', fontSize: 14, lineHeight: 17, fontFamily: style.font.family.rubikRegular }]}
              >
                {' '}
                shared a post
                {' '}
              </Text>
              <Text
                style={[
                  notificationStyles.boxLow,
                  { color: 'rgb(83,87,94)', marginLeft: 3 }
                ]}
              >
                {data.sharepostNotification.title}
              </Text>
            </Text>
          </View>
          <Text
            style={[
              notificationStyles.boxLow,
              { color: 'rgb(173,181,196)', marginTop: 5, fontFamily: style.font.family.rubikRegular }
            ]}
          >
            {m}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

  renderLikeNotification(data, index) {
    let m = transformForPosts(data.notificationTime);
    return (
      <TouchableOpacity
        onPress={() => this.props.actions.getPost(data.likepostNotification.post.postid)}
        key={index}
        style={[
          notificationStyles.dispView,
          {
            backgroundColor: data.seennotification
              ? 'white'
              : 'rgb(232, 234, 237)'
          }
        ]}
      >
        <Image
          source={{
            uri: data.likepostNotification.user.profPicPath
              ? data.likepostNotification.user.profPicPath
              : 'https://www.buira.org/assets/images/shared/default-profile.png'
          }}
          style={notificationStyles.picDisp}
        />
        <View
          style={{
            marginLeft: 11,
            justifyContent: 'flex-start'
          }}
        >
          <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
            <Text style={[notificationStyles.boxLow,
            { width: width - 82 }]}>
              {data.likepostNotification.user.name}
              <Text
                style={[notificationStyles.boxLow,
                { color: 'rgb(83,87,94)', fontSize: 14, lineHeight: 17, fontFamily: style.font.family.rubikRegular }]}
              >
                {' '}
                liked a post
                {' '}
              </Text>
              <Text
                style={[
                  notificationStyles.boxLow,
                  { color: 'rgb(83,87,94)', marginLeft: 3 }
                ]}
              >
                {data.likepostNotification.post.title}
              </Text>
            </Text>
          </View>
          <Text
            style={[
              notificationStyles.boxLow,
              { color: 'rgb(173,181,196)', marginTop: 5, fontFamily: style.font.family.rubikRegular }
            ]}
          >
            {m}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

  renderPostNotification(data, index) {
    let m = transformForPosts(data.notificationTime);
    return (
      <TouchableOpacity
        onPress={() => this.props.actions.getPost(data.relatedPost.postid)}
        key={index}
        style={[
          notificationStyles.dispView,
          {
            backgroundColor: data.seennotification
              ? 'white'
              : 'rgb(232, 234, 237)'
          }
        ]}
      >
        <Image
          source={{
            uri: data.relatedPost.user.profPicPath
              ? data.relatedPost.user.profPicPath
              : 'https://www.buira.org/assets/images/shared/default-profile.png'
          }}
          style={notificationStyles.picDisp}
        />
        <View
          style={{
            marginLeft: 11,
            justifyContent: 'flex-start'
          }}
        >
          <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
            <Text style={[notificationStyles.boxLow,
            { width: width - 82 }]}>
              {data.relatedPost.user.name}
              <Text
                style={[notificationStyles.boxLow,
                { color: 'rgb(83,87,94)', fontSize: 14, lineHeight: 17, fontFamily: style.font.family.rubikRegular }]}
              >
                {' '}
                shared a post
                {' '}
              </Text>
              <Text
                style={[
                  notificationStyles.boxLow,
                  { color: 'rgb(83,87,94)', marginLeft: 3 }
                ]}
              >
                {data.relatedPost.title
                  ? data.relatedPost.title.length > 60
                    ? data.relatedPost.title.substring(0, 61).concat('...')
                    : data.relatedPost.title
                  : ''}
              </Text>
            </Text>
          </View>
          <Text
            style={[
              notificationStyles.boxLow,
              { color: 'rgb(173,181,196)', marginTop: 5, fontFamily: style.font.family.rubikRegular }
            ]}
          >
            {m}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
  renderTagNotification(data, index) {
    let m = transformForPosts(data.notificationTime);
    return (
      <View
        key={index}
        style={[
          notificationStyles.dispView,
          {
            backgroundColor: data.seennotification
              ? 'white'
              : 'rgb(232, 234, 237)'
          }
        ]}
      >
        <Image
          source={{
            uri: data.tagpostNotification.user.profPicPath
              ? data.tagpostNotification.user.profPicPath
              : 'https://www.buira.org/assets/images/shared/default-profile.png'
          }}
          style={notificationStyles.picDisp}
        />
        <View
          style={{
            marginLeft: 11,
            justifyContent: 'flex-start'
          }}
        >
          <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
            <Text style={[notificationStyles.boxLow,
            { width: width - 82 }]}>
              {data.tagpostNotification.user.name}
              <Text
                style={[notificationStyles.boxLow,
                { color: 'rgb(83,87,94)', fontSize: 14, lineHeight: 17, fontFamily: style.font.family.rubikRegular }]}
              >
                {' '}
                was tagged in a post
                {' '}
              </Text>
              <Text
                style={[
                  notificationStyles.boxLow,
                  { color: 'rgb(83,87,94)', marginLeft: 3 }
                ]}
              >
                {data.tagpostNotification.title
                  ? data.tagpostNotification.title.length > 60
                    ? data.tagpostNotification.title.substring(0, 61).concat('...')
                    : data.tagpostNotification.title
                  : ''}
              </Text>
            </Text>
          </View>
          <Text
            style={[
              notificationStyles.boxLow,
              {
                color: 'rgb(173,181,196)', marginTop: 5,
                fontFamily: style.font.family.rubikRegular,
                fontSize: 14, lineHeight: 17
              }
            ]}
          >
            {m}
          </Text>
        </View>
      </View>
    );
  }

  fetchMoreNotifications() {
    if (this.state.notiCount >= 10 && !this.props.notificationState.notificationState.isWaitingResponse
      && this.state.notiCount !== this.state.originalCount) {
      this.props.actions.getAllNotifications(this.state.notiCount + 10, 0).then(() => {
        this.setState({
          originalCount: this.state.notiCount,
          notiCount: this.state.notiCount + 10
        })
      });
    }
  }

  renderGeneral() {
    let generalNotifications = this.props.notificationState.notificationState.allNotifications
    if (isEmpty(generalNotifications)) {
      return (
        <View style={{ padding: 15 }}>
          <Text style={{ textAlign: 'center', color: 'rgb(173, 181, 196)', fontFamily: 'rubik-regular' }}>
            There are no new notifications. Make a post or connect with others on UPLE to receive notification
          </Text>
        </View>
      )
    } else {
      return (
        <FlatList
          contentContainerStyle={{ paddingBottom: 54 }}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          onEndReached={() => {
            if (generalNotifications.length % 10 === 0) {
              this.fetchMoreNotifications()
            }
          }}
          onEndReachedThreshold={1}
          data={this.props.notificationState.notificationState.allNotifications}
          renderItem={({ index, item }) =>
            this.renderGeneralNotification(item, index)
          }
          keyExtractor={item => item.id.toString()}
          ListFooterComponent={() => {
            return (
              (this.props.notificationState.notificationState.isWaitingResponse) ?
                <View style={{ paddingVertical: 15 }}>
                  <ActivityIndicator size={'large'} color={style.color.primary} />
                </View> : <View></View>
            );
          }}
        />
      );
    }
  }
  renderConnectionNotification(item, index) {
    let notifUser = undefined;
    if (item.userConnectionNotification.requesterUser.id !== this.props.userId) {
      notifUser = item.userConnectionNotification.requesterUser;
    } else if (item.userConnectionNotification.requesteeUser.id !== this.props.userId) {
      notifUser = item.userConnectionNotification.requesteeUser;
    }
    let m = transformForPosts(item.notificationTime);
    if (item.userConnectionNotification.connectionStatus === 'ACCEPTED') {
      return (
        <TouchableOpacity onPress={() => this.props.peopleApi.getPeopleInfo(notifUser.id)}
          key={index}
          style={[
            notificationStyles.dispView,
            { height: undefined, flexDirection: 'row' }
          ]}
        >
          <Image
            source={notifUser.user.profPicPath ?
              { uri: notifUser.user.profPicPath }
              : require('../../../assets/images/user_dummy_image.png')}
            style={notificationStyles.picDisp}
          />
          <View
            style={{
              marginLeft: 11,
              justifyContent: 'flex-start'
            }}
          >
            <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
              <Text style={[notificationStyles.boxLow,
              { width: width - 82 }]}>
                {notifUser.user.name}
                <Text
                  style={[
                    notificationStyles.boxLow,
                    { color: 'rgb(83,87,94)', fontSize: 14, lineHeight: 17, fontFamily: style.font.family.rubikRegular }
                  ]}
                >
                  {' '}and you are now connected.
                </Text>
              </Text>
            </View>
            <Text
              style={[
                notificationStyles.boxLow,
                { color: 'rgb(173,181,196)', lineHeight: 17, fontFamily: style.font.family.rubikRegular, marginTop: 5 }
              ]}
            >
              {m}
            </Text>
          </View>
        </TouchableOpacity>
      );
    }
    return (
      <View
        key={index}
        style={[
          notificationStyles.dispView,
          { height: undefined, flexDirection: 'row' }
        ]}
      >
        <TouchableOpacity
          onPress={() => this.props.peopleApi.getPeopleInfo(item.userConnectionNotification.requesterUser.user.id)}
        >
          <Image
            source={{
              uri: item.userConnectionNotification.requesterUser.user
                .profPicPath
                ? item.userConnectionNotification.requesterUser.user.profPicPath
                : 'https://www.buira.org/assets/images/shared/default-profile.png'
            }}
            style={notificationStyles.picDisp}
          />
        </TouchableOpacity>
        <View
          style={{
            marginLeft: 11,
            flex: 1,
            justifyContent: 'flex-start'
          }}
        >
          <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
            <Text
              onPress={() => this.props.peopleApi.getPeopleInfo(item.userConnectionNotification.requesterUser.user.id)}
              style={notificationStyles.boxLow}>
              {item.userConnectionNotification.requesterUser.user.name}
            </Text>
          </View>
          <Text
            style={[
              notificationStyles.boxLow,
              { color: 'rgb(83,87,94)', marginTop: 5, lineHeight: 18 }
            ]}
          >
            {item.userConnectionNotification.requestmessage}
          </Text>
          <Text
            style={[
              notificationStyles.boxLow,
              { color: 'rgb(173,181,196)', marginTop: 5, fontFamily: style.font.family.rubikRegular }
            ]}
          >
            {m}
          </Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'flex-end',
              paddingTop: 20
            }}
          >
            <TouchableOpacity
              onPress={() => {
                this.props.actions.dismissConnection(item.userConnectionNotification.requesterUser.id);
              }}
              style={notificationStyles.dismissBut}
            >
              <Text
                style={[
                  notificationStyles.boxLow,
                  { color: 'rgb(173,181,196)', fontSize: 13, lineHeight: 15 }
                ]}
              >
                Dismiss
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.props.actions.acceptConnection(item.userConnectionNotification.requesterUser.id);
              }}
              style={notificationStyles.acceptBut}
            >
              <Text style={[notificationStyles.boxLow,
              { color: 'white', fontSize: 13, lineHeight: 15 }]}>
                Accept
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
  renderConnections() {
    let connections = this.props.notificationState.notificationState.connectionNotifications
    return (
      <ScrollView
        contentContainerStyle={{ paddingBottom: 54 }}
        showsVerticalScrollIndicator={false}
      >
        {isEmpty(connections) ?
          <View style={{ padding: 15 }}>
            <Text style={{ textAlign: 'center', color: 'rgb(173, 181, 196)', fontFamily: 'rubik-regular' }}>
              There are no connections. Invite your friends to join UPLE to receive connection notifications
            </Text>
          </View>
          : connections.map(
            (item, index) => {
              return this.renderConnectionNotification(item, index);
            }
          )}
      </ScrollView>
    );
  }
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <MSAHeader
          title={'Notifications'}
          disableLeftIcon={true}
        />
        <View
          style={[
            notificationStyles.fieldBox,
            { flexDirection: 'row', paddingHorizontal: 0 }
          ]}
        >
          <TouchableOpacity
            onPress={() =>
              this.setState({
                index: 0
              })
            }
            style={{
              flex: 1,
              borderBottomWidth: this.state.index === 0 ? 2 : 0,
              borderColor:
                this.state.index === 0 ? 'rgb(204,20,51)' : 'transparent',
              paddingVertical: 21,
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Text
              style={[
                notificationStyles.dualHeaderLeftText,
                {
                  color:
                    this.state.index === 0
                      ? 'rgb(204,20,51)'
                      : 'rgb(173,181,196)'
                }
              ]}
            >
              GENERAL
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              this.setState({
                index: 1
              })
            }
            style={{
              flex: 1,
              borderBottomWidth: this.state.index === 1 ? 2 : 0,
              borderColor:
                this.state.index === 1 ? 'rgb(204,20,51)' : 'transparent',
              paddingVertical: 21,
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Text
              style={[
                notificationStyles.dualHeaderLeftText,
                {
                  color:
                    this.state.index === 1
                      ? 'rgb(204,20,51)'
                      : 'rgb(173,181,196)'
                }
              ]}
            >
              CONNECTIONS
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{ flex: 1 }}>
          {this.state.index === 0
            ? this.renderGeneral()
            : this.renderConnections()}
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    notificationState: state,
    userId: state.userInfoAPI.userId
  };
};
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      getAllNotifications: getAllNotifications,
      getConnectionNotifications: getConnectionNotifications,
      getNotificationsUnreadCount: getNotificationsUnreadCount,
      markAllAsSeen: markAllAsSeen,
      acceptConnection: acceptConnection,
      dismissConnection: dismissConnection,
      getPost: getPost,
      getPeopleInfo: getPeopleInfo
    },
    dispatch
  ),
  peopleApi: bindActionCreators(PeopleApi, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Notifications);
