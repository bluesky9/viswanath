import { StyleSheet, Dimensions } from 'react-native';
// import { style } from '../../styles/variables';
const { width } = Dimensions.get('window');
import { style } from '../../styles/variables';

const notificationStyles = StyleSheet.create({
  dispView: {
    flexDirection: 'row',
    // alignSelf: 'stretch',
    borderBottomWidth: 1,
    borderColor: 'rgb(233,239,247)',
    paddingHorizontal: 21,
    paddingVertical: 22,
    alignItems: 'flex-start',
    // justifyContent: 'center',
    backgroundColor: 'white'
  },
  picDisp: { height: 41, width: 41, borderRadius: 20.5 },
  dismissBut: {
    paddingVertical: 7,
    paddingHorizontal: 9,
    borderWidth: 1,
    borderColor: 'rgb(173,181,196)',
    borderRadius: 3,
    justifyContent: 'center',
    alignItems: 'center'
  },
  acceptBut: {
    paddingVertical: 7,
    paddingHorizontal: 9,
    backgroundColor: 'rgb(53,135,230)',
    borderRadius: 3,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 10
  },
  headerContainer: {
    paddingTop: style.header.height + 17,
    paddingBottom: 17,
    alignSelf: 'stretch',
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: 'rgb(233,239,247)',
    backgroundColor: 'white'
  },
  leftIconContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 15
  },
  centerHeaderContainer: {
    flex: 4,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  headerText: {
    color: 'rgb(83,87,94)',
    fontFamily: style.font.family.rubikRegular,
    fontSize: 12
  },
  rightIconContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingRight: 15
  },
  picContainer: {
    width: 106,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    paddingLeft: 15,
    paddingTop: 15
  },
  mainTextContainer: { width: width - 106, paddingTop: 15 },
  nameText: {
    fontSize: 16,
    fontFamily: style.font.family.ptSerif,
    color: 'rgb(59,62,68)'
  },
  countryText: {
    color: 'rgba(83,87,94,0.8)',
    fontFamily: style.font.family.rubikRegular,
    fontSize: 12,
    marginHorizontal: 6
  },
  bioContainer: {
    height: 58,
    alignSelf: 'stretch',
    paddingHorizontal: 15,
    paddingTop: 11,
    borderColor: 'rgb(233,238,241)',
    borderBottomWidth: 1,
    backgroundColor: 'white'
  },
  bioText: {
    color: 'rgb(83,87,94)',
    fontFamily: style.font.family.rubikRegular,
    fontSize: 12
  },
  educationTextContainer: {
    height: 66,
    width: width,
    justifyContent: 'flex-start',
    paddingLeft: 15,
    paddingTop: 15,
    borderColor: 'rgb(233,238,241)',
    borderBottomWidth: 1,
    backgroundColor: 'white'
  },
  statsContainer: {
    height: 69,
    alignSelf: 'stretch',
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: 'rgb(233,239,247)',
    backgroundColor: 'white'
  },
  statOneContainer: {
    flex: 0.6,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 15
  },
  statNmbrText: {
    color: 'rgb(59,62,68)',
    fontFamily: style.font.family.ptSerif,
    fontSize: 16
  },
  statNameText: {
    color: 'rgba(83,87,94,0.8)',
    fontFamily: style.font.family.rubikRegular,
    fontSize: 12
  },
  fieldBox: {
    // height: 70,
    alignSelf: 'stretch',
    flexDirection: 'column',
    borderBottomWidth: 1,
    borderColor: 'rgb(233,239,247)',
    paddingHorizontal: 15,
    alignItems: 'flex-start',
    justifyContent: 'center',
    backgroundColor: 'white'
  },
  statButtonContainer: {
    flex: 0.6,
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingRight: 15
  },
  boxHead: {
    color: 'rgb(128,134,145)',
    fontFamily: style.font.family.rubikMedium,
    fontSize: 10,
    marginVertical: 5,
    letterSpacing: 1
  },
  boxLow: {
    color: 'rgb(53,135,230)',
    fontFamily: style.font.family.rubikMedium,
    fontSize: 14,
    // marginVertical: 5,
    lineHeight: 17,
    letterSpacing: 0.2
  },
  // boxLowTall: {
  //   color: 'rgb(53,135,230)',
  //   fontFamily: style.font.family.rubikRegular,
  //   fontSize: 12,
  //   // marginVertical: 5,
  //   lineHeight: 18,
  //   letterSpacing: 0.2
  // },
  editBoxIcon: {
    position: 'absolute',
    top: 15,
    left: width - 35,
    height: 15,
    width: 15
  },
  fieldBigHead: {
    color: 'rgb(59,62,68)',
    fontFamily: style.font.family.rubikRegular,
    fontSize: 16,
    marginVertical: 5
  },
  fieldDarkLow: {
    color: 'rgb(83,87,94)',
    fontFamily: style.font.family.rubikRegular,
    fontSize: 12
  },
  partBorderBottom: {
    justifyContent: 'flex-start',
    width: width - 30,
    alignSelf: 'center',
    paddingHorizontal: 0
  },
  expScroll: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    paddingTop: 51
  },
  expCard: {
    height: 118,
    width: 262,
    backgroundColor: 'white',
    marginHorizontal: 5,
    shadowOffset: { height: 1, width: -1 },
    shadowOpacity: 0.1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    padding: 15,
    elevation: 3
  },
  expCardBigText: {
    fontSize: 15,
    color: 'rgb(53,71,91)',
    fontFamily: style.font.family.rubikLight,
    marginVertical: 5
  },
  expCardSmallText: {
    fontSize: 12,
    color: 'rgb(83,87,94)',
    fontFamily: style.font.family.rubikRegular,
    marginTop: 5
  },
  asheetItem: {
    fontSize: 14,
    fontFamily: style.font.family.rubikRegular,
    color: 'rgb(59,62,68)'
  },
  dualHeaderLeftText: {
    fontSize: 14,
    lineHeight: 17,
    fontFamily: style.font.family.rubikMedium,
    color: 'rgb(53,71,91)',
    letterSpacing: 0.5
  },
  dualHeaderCenterText: {
    fontSize: 16,
    lineHeight: 19,
    fontFamily: style.font.family.rubikRegular,
    color: 'rgb(53,71,91)',
    letterSpacing: 0.2
  },
  dualHeaderRightText: {
    fontSize: 10,
    fontFamily: style.font.family.rubikMedium,
    color: 'rgb(53,135,230)',
    letterSpacing: 0.5
  },
  skillInp: {
    width: width - 30,
    fontSize: 12,
    fontFamily: style.font.family.rubikRegular,
    color: 'rgb(173,181,196)',
    padding: 0
  },
  tagButtonText: {
    fontSize: 10,
    color: 'rgb(53,135,230)',
    fontFamily: style.font.family.rubikMedium,
    letterSpacing: 0.5
  }
});

export default notificationStyles;
