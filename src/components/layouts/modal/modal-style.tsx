import { StyleSheet, Dimensions, Platform } from 'react-native';
const { width } = Dimensions.get('window');
import { style } from '../../../styles/variables';

export const modalErrorStyle = StyleSheet.create({
  title: {
    color: style.color.nero,
    fontSize: 24,
    lineHeight: 31,
    fontFamily: style.font.family.ptSerif,
    marginBottom: 20,
    textAlign: 'center'
  },
  errorDescription: {
    fontFamily: style.font.family.rubikRegular,
    fontSize: style.font.size.normal,
    lineHeight: style.font.lineHeight.normal.normal,
    color: style.color.nero,
    textAlign: 'center'
  },
  errorInstruction: {
    fontFamily: style.font.family.rubikRegular,
    fontSize: style.font.size.normal,
    lineHeight: style.font.lineHeight.normal.normal,
    color: style.color.nero,
    textAlign: 'center'
  },
  inpHeader: {
    color: 'rgb(83,87,94)',
    fontSize: 12,
    lineHeight: 14,
    fontFamily: style.font.family.rubikMedium,
    letterSpacing: 1,
    alignSelf: 'flex-start'
  },
  textInp: {
    width: width - 30,
    height: 110,
    paddingHorizontal: 15,
    paddingTop: 15,
    fontFamily: style.font.family.rubikRegular,
    fontSize: 14,
    lineHeight: 17,
    color: 'rgb(83,87,94)'
  },
  sendBtnTxt: {
    color: 'white',
    fontFamily: style.font.family.rubikRegular,
    fontSize: 13,
    lineHeight: 15
  },
  inpDesc: {
    fontSize: 11,
    lineHeight: 21,
    fontFamily: style.font.family.rubikRegular,
    color: 'rgb(197,207,219)',
    alignSelf: 'flex-end',
    paddingRight: 15,
    paddingBottom: 3
  },
  button: {
    marginTop: 33,
    paddingVertical: 13,
    paddingHorizontal: 26,
    shadowOpacity: 0.12,
    shadowOffset: { height: 2, width: 0 },
    backgroundColor: 'white',
    borderRadius: 5,
    elevation: 4,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    color: style.color.primary,
    fontSize: 14,
    lineHeight: 17,
    fontFamily: style.font.family.rubikRegular
  },
  crossButton: {
    position: 'absolute',
    top: Platform.OS === 'ios' ? 20 : 0,
    right: 0,
    paddingVertical: 10,
    paddingHorizontal: 20
  },
  crossIcon: {
    fontSize: 20
  }
});

export const modalActionStyle = StyleSheet.create({
  title: {
    color: style.color.nero,
    fontSize: 24,
    lineHeight: 31,
    fontFamily: style.font.family.ptSerif,
    paddingBottom: 20,
    textAlign: 'center'
  },
  actionDescription: {
    fontFamily: style.font.family.rubikRegular,
    fontSize: 14,
    lineHeight: 17,
    paddingHorizontal: 20,
    color: style.color.nero,
    textAlign: 'center'
  },
  noButtonText: {
    color: style.color.primary,
    fontSize: 14,
    lineHeight: 17,
    fontFamily: style.font.family.rubikRegular
  },
  noButton: {
    marginTop: 33,
    paddingVertical: 15,
    paddingHorizontal: 40,
    shadowOpacity: 0.12,
    shadowOffset: { height: 2, width: 0 },
    elevation: 1,
    backgroundColor: 'white',
    borderRadius: 5,
    marginRight: 15
  },
  yesButtonText: {
    color: style.color.bloodRed,
    fontSize: 14,
    lineHeight: 17,
    fontFamily: style.font.family.rubikRegular
  },
  yesButton: {
    marginTop: 33,
    paddingVertical: 14,
    paddingHorizontal: 40,
    borderWidth: 1,
    borderColor: style.color.bloodRed,
    backgroundColor: 'white',
    borderRadius: 5,
    marginLeft: 15
  },
  fieldBox: {
    borderBottomWidth: 1,
    borderColor: 'rgb(233,238,241)',
    alignSelf: 'stretch'
  },
  boxHead: {
    color: 'rgb(83,87,94)',
    fontFamily: style.font.family.rubikMedium,
    fontSize: 10,
    letterSpacing: 1,
    flex: 1
  },
  skillInp: {
    fontSize: 12,
    fontFamily: style.font.family.rubikRegular,
    color: 'rgb(173,181,196)'
  },
  educationLows: {
    fontSize: 14,
    fontFamily: style.font.family.rubikRegular,
    color: 'rgb(83,87,94)'
  },
  crossButton: {
    position: 'absolute',
    top: Platform.OS === 'ios' ? 20 : 0,
    right: 0,
    paddingVertical: 10,
    paddingHorizontal: 20
  },
  crossIcon: {
    fontSize: 20
  }
});

export const modalImageStyle = StyleSheet.create({
  image: {
    flex: 1,
    width: width,
    resizeMode: 'contain'
  },
  crossButton: {
    position: 'absolute',
    top: Platform.OS === 'ios' ? 20 : 0,
    right: 0,
    padding: 20
  }
});
