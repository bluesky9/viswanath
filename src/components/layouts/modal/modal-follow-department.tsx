import React, { Component } from 'react';
import {
  View,
  Modal,
  Dimensions
} from 'react-native';
const { width } = Dimensions.get('window');
import { Picker, Form } from 'native-base';

import MSAHeader from '../header/header';

import { dialogHides } from '../../../store/actions/ErrorModal.js'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { isEmpty, find, get } from 'lodash';

const Item = Picker.Item;

type Props = {
  onPress?: Function,
  type?: string,
  isShown?: boolean,
  title?: string,
  message?: any,
  dialogType?: string,
  closable?: boolean,
  dialogHides?: any,
  onConfirm?;
  onCancel?;
};

type state = {
  selectedPick: string;
  selectedDegree: string;
};

class MSAModalFollowDepartment extends Component<Props, state> {
  constructor(props) {
    super(props);
    this.state = {
      selectedPick: 'key0',
      selectedDegree: 'key1'
    };
  }
  handlePicker = (val)  => {
    this.setState({ selectedPick: val });
  }

  handleDegreePicker = (val) => {
    this.setState({ selectedDegree: val})
  }

  onCancelPress = () => {
    this.props.dialogHides()
  }

  closeModal = () => {
    this.props.dialogHides()
  }

  onJoinPress = (levelOfStudy) => {
    if (( isEmpty(levelOfStudy) || this.state.selectedDegree !== 'key1')  && this.state.selectedPick !== 'key0') {
      this.props.onConfirm(this.state.selectedPick, isEmpty(levelOfStudy) ? undefined : this.state.selectedDegree);
      this.props.dialogHides()
    }
  }

  makeList = (list, header) => {
    let d = [];
    d.push(<Item label={header} value={'key0'} key= {-1} />);
    list.map((data, i) => {
      d.push(<Picker.Item label={data.majorname} value={data.majorid} key={i}/>)
    })
    return d;
  }

  makeLevelOfStudyList = (list, header) => {
    let d = [];
    d.push(<Item label={header} value={'key1'} key= {-1} />);
    list.map((data, i) => {
      d.push(<Picker.Item label={data.levelOfStudy.replace('_', ' ')} value={data.id} key={i}/>)
    })
    return d;
  }

  render() {
    let levelOfStudyOffered = get(find(this.props.message, major => major.majorid === this.state.selectedPick), 'levelOfStudyOffered', undefined)
    return (
      <View>
        <Modal
          visible={this.props.isShown}
          animationType={'slide'}
          onRequestClose={() => this.closeModal()}
        >
          <MSAHeader
            title={'Join ' + this.props.title}
            rightText={'JOIN'}
            leftText={'CANCEL'}
            onPress={this.onCancelPress}
            onRightTextPress={() => this.onJoinPress(levelOfStudyOffered)}
          />
          <View style={{ flex: 1, alignItems: 'center', paddingTop: 60 }}>
            <Form>
              <Picker
                iosHeader={'Select a Major'}
                style={{
                  borderWidth: 1,
                  borderColor: 'rgb(233,238,241)',
                  width: width - 30,
                  marginTop: 15
                }}
                mode={'dropdown'}
                selectedValue={this.state.selectedPick}
                onValueChange={val => {
                  this.handlePicker(val)}
                }
              >
                { this.makeList(this.props.message, 'Select a Major')}
              </Picker>
              </Form>
              { !isEmpty(levelOfStudyOffered) &&
              <Form>
                <Picker
                  iosHeader={'Select a Degree/Level of Study'}
                  style={{
                    borderWidth: 1,
                    borderColor: 'rgb(233,238,241)',
                    width: width - 30,
                    marginTop: 15
                  }}
                  mode={'dropdown'}
                  selectedValue={this.state.selectedDegree}
                  onValueChange={val => this.handleDegreePicker(val)}
                >
                  {this.makeLevelOfStudyList(levelOfStudyOffered, 'Select a Degree/Level of Study')}
                  {/* <Item label={'Select a Degree/Level of Study'} value={'key1'} />
                  <Item label={'Bachelors'} value={'BACHELORS'} /> */}
                </Picker>
              </Form>
            }
          </View>
        </Modal>
      </View>
    );
  }
}

const mapStateToProps = ({ errorModal }) => {
  return {
    isShown: errorModal.isShown,
    title: errorModal.title,
    message: errorModal.message,
    dialogType: errorModal.dialogType,
    closable: errorModal.closable,
    onConfirm: errorModal.onConfirm,
    onCancel: errorModal.onCancel
  };
}

const mapDispatchToProps = dispatch => ({
  dialogHides: bindActionCreators(dialogHides, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(MSAModalFollowDepartment);
