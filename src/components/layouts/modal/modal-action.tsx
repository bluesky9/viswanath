import React, { Component } from 'react';
import { View, TouchableOpacity, Text, Modal } from 'react-native';
import { Icon } from 'native-base';

import { modalActionStyle } from './modal-style';

import { dialogHides } from '../../../store/actions/ErrorModal.js'

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

type Props = {
  onPress?: Function,
  type?: string,
  isShown?: boolean,
  title?: string,
  message?: string,
  dialogType?: string,
  closable?: boolean,
  dialogHides?: any,
  onConfirm?;
  onCancel?;
};

type state = {
  modalVisible
}

class MSAModalAction extends Component<Props, state> {
  constructor(props) {
    super(props);
    // this._onPressButton = this._onPressButton.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  _cancelPressed = () => {
    if (this.props.onCancel) {
      this.props.onCancel();
    }
    this.props.dialogHides();
  }

  _confirmPressed = () => {
    if (this.props.onConfirm) {
      this.props.onConfirm();
    }
    this.props.dialogHides();
  }

  closeModal() {
    this.props.dialogHides();
  }

  render() {
    console.log(this.props.isShown, this.props.title, this.props.message, this.props.dialogType, this.props.closable, 'Modals' );
    return <View>
        <Modal
          visible={this.props.isShown}
          animationType={'slide'}
          onRequestClose={() => this.closeModal()}
        >
          <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, paddingHorizontal: 40 }}>
            <Text style={modalActionStyle.title}>{this.props.title + '?' }</Text>
            <Text style={modalActionStyle.actionDescription}>{this.props.message}</Text>
            <View style={{ flexDirection: 'row' }}>
              <TouchableOpacity style={modalActionStyle.noButton} onPress={this._cancelPressed}>
                <Text style={modalActionStyle.noButtonText}>No</Text>
              </TouchableOpacity>
              <TouchableOpacity style={modalActionStyle.yesButton} onPress={this._confirmPressed}>
                <Text style={modalActionStyle.yesButtonText}>Yes</Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity style={modalActionStyle.crossButton} onPress={() => this.closeModal()}>
              <Icon
                style={modalActionStyle.crossIcon}
                name={'md-close'}
              />
            </TouchableOpacity>
          </View>
        </Modal>
      </View>;
  }
}

const mapStateToProps = ({ errorModal }) => {
  return {
    isShown: errorModal.isShown,
    title: errorModal.title,
    message: errorModal.message,
    dialogType: errorModal.dialogType,
    closable: errorModal.closable,
    onConfirm: errorModal.onConfirm,
    onCancel: errorModal.onCancel
  };
}

const mapDispatchToProps = dispatch => ({
  dialogHides: bindActionCreators(dialogHides, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(MSAModalAction);

// Example
{/* <MSAModalAction
  onPress={() => this._handleErrorPress.bind(this)}
  type='Refresh'
  action='Logout'
  onClose={() => this.setState({ showModal: false})}
  showModal = {this.state.showModal}
  actionDescription = {'You will stop recieveing notifications and new updates'}
/> */}