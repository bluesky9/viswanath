import React, { Component } from 'react';
import { View, TouchableOpacity, Text, Modal, TextInput, Platform } from 'react-native';
import { Icon } from 'native-base';

import MSAButton from '../buttons/button';
import { dialogHides } from '../../../store/actions/ErrorModal.js'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import { modalActionStyle } from './modal-style';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';

import { style } from '../../../styles/variables';

type Props = {
  onPress?: Function,
  type?: string,
  isShown?: boolean,
  title?: string,
  message?: string,
  dialogType?: string,
  closable?: boolean,
  dialogHides?: any,
  onConfirm?;
  onCancel?;
};

type state = {
  selectedCategory;
  reason;
}

const category = ['Copyright infringement', 'Racial abuse', 'Spamming', 'False accusations', 'Other'];

class MSAModalReport extends Component<Props, state> {
  constructor(props) {
    super(props);
    this.state = {
      selectedCategory: 0,
      reason: undefined
    }
    this.closeModal = this.closeModal.bind(this);
  }

  _cancelPressed = () => {
    if (this.props.onCancel) {
      this.props.onCancel();
    }
    this.props.dialogHides();
  }

  _confirmPressed = () => {
    if (this.props.onConfirm) {
      let reason = this.state.selectedCategory === 4 ? this.state.reason : category[this.state.selectedCategory];
      if (!isEmpty(reason))
        this.props.onConfirm(reason);
      else return;
    }
    this.props.dialogHides();
  }

  closeModal() {
    this.props.dialogHides();
  }

  _onCategoryPressed = (index) => {
    this.setState({ selectedCategory: index })
  }

  handleOnTextChange = (text) => {
    this.setState({ reason: text })
  }

  render() {
    console.log(this.props.isShown, this.props.title, this.props.message, this.props.dialogType, this.props.closable, 'Modals');
    return <View style={{ flex: 1 }}>
      <Modal
        visible={this.props.isShown}
        animationType={'slide'}
        onRequestClose={() => this.closeModal()}
      >
        <View style={{ flex: 1 }}>
          <KeyboardAwareScrollView contentContainerStyle={{
            flex: Platform.OS === 'ios' ? 1 : undefined
          }}>
            <View style={{ paddingHorizontal: 40, paddingTop: 10, paddingBottom: 10, flex: 1, alignItems: 'center', justifyContent: 'center' }}>
              <Text style={modalActionStyle.title}>{'Report Post'}</Text>
              <Text style={modalActionStyle.actionDescription}>{'Please choose a category of violation'}</Text>
              <View style={{ paddingTop: 28 }}>
                {category.map((item, index) => {
                  if (this.state.selectedCategory === index)
                    return (
                      <MSAButton
                        onPress={() => this._onCategoryPressed(index)}
                        type={'primary'}
                        label={item}
                        textFamily={style.font.family.rubikRegular}
                        customButtonStyle={{ backgroundColor: 'rgb(47,120, 204)', marginVertical: 5 }}
                        key={index}
                      />
                    )
                  else
                    return (
                      <MSAButton
                        onPress={() => this._onCategoryPressed(index)}
                        type={'inverted'}
                        label={item}
                        bordered={true}
                        textFamily={style.font.family.rubikRegular}
                        customButtonStyle={{ marginVertical: 5 }}
                        key={index}
                      />
                    )
                })}
              </View>
              {this.state.selectedCategory === 4 &&
                <View style={{ width: '100%', borderWidth: 1, borderColor: style.color.cream, marginTop: 5 }}>
                  <TextInput
                    style={{
                      paddingHorizontal: 12,
                      paddingVertical: 14,
                      paddingTop: 12,
                      fontFamily: style.font.family.rubikRegular,
                      fontSize: 14,
                      lineHeight: 17,
                      borderBottomWidth: 0,
                      maxHeight: Platform.OS === 'ios' ? 73 : 100
                    }}
                    placeholderTextColor={style.color.coal}
                    underlineColorAndroid={'rgba(0,0,0,0)'}
                    numberOfLines={5}
                    multiline={true}
                    maxLength={200}
                    onChangeText={(text) => this.handleOnTextChange(text)}
                  />
                  <Text style={{
                    textAlign: 'right', paddingRight: 12, lineHeight: 21, fontSize: 11, letterSpacing: 0.2, color: 'rgb(197,207,219)'
                  }}>
                    Charachter limit: 200
                      </Text>
                </View>
              }
            </View>
            <View style={{ paddingVertical: 37, paddingHorizontal: 40, backgroundColor: 'rgb(245,247,250)' }}>
              <MSAButton
                onPress={this._confirmPressed}
                type={'primary'}
                label={'Confirm and send'}
                enableShadow={true}
                textFamily={style.font.family.rubikRegular}
                textSize={style.font.size.large}
                textLineHeight={style.font.lineHeight.normal.large}
              />
            </View>
          </KeyboardAwareScrollView>
        </View>
        <TouchableOpacity style={modalActionStyle.crossButton} onPress={() => this.closeModal()}>
          <Icon
            style={modalActionStyle.crossIcon}
            name={'md-close'}
          />
        </TouchableOpacity>
      </Modal>
    </View>;
  }
}

const mapStateToProps = ({ errorModal }) => {
  return {
    isShown: errorModal.isShown,
    title: errorModal.title,
    message: errorModal.message,
    dialogType: errorModal.dialogType,
    closable: errorModal.closable,
    onConfirm: errorModal.onConfirm,
    onCancel: errorModal.onCancel
  };
}

const mapDispatchToProps = dispatch => ({
  dialogHides: bindActionCreators(dialogHides, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(MSAModalReport);

// Example
{/* <MSAModalAction
  onPress={() => this._handleErrorPress.bind(this)}
  type='Refresh'
  action='Logout'
  onClose={() => this.setState({ showModal: false})}
  showModal = {this.state.showModal}
  actionDescription = {'You will stop recieveing notifications and new updates'}
/> */}