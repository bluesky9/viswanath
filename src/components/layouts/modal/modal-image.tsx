import React, { Component } from 'react';
import {
  View,
  Modal,
  Image,
  ScrollView,
  TouchableOpacity,
  Dimensions
} from 'react-native';

import { dialogHides } from '../../../store/actions/ErrorModal.js';
import GestureRecognizer from 'react-native-swipe-gestures';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { modalImageStyle } from './modal-style';
import { isEmpty, get } from 'lodash';

type Props = {
  onPress?: Function;
  type?: string;
  isShown?: boolean;
  title?: string;
  message?: string;
  dialogType?: string;
  closable?: boolean;
  dialogHides?: any;
  onConfirm?;
  onCancel?;
  images?;
  index: number;
};

type state = {
  selectedCategory;
  reason;
};

const category = [
  'Copyright infringement',
  'Racial abuse',
  'Spamming',
  'False accusations',
  'Other'
];

class MSAModalImage extends Component<Props, state> {
  listRef: any;
  constructor(props) {
    super(props);
    this.state = {
      selectedCategory: 0,
      reason: undefined
    };
    this.closeModal = this.closeModal.bind(this);
  }

  componentDidMount() {
    if (this.listRef && this.props.index) {
      this.listRef.scrollTo({x: this.props.index * Dimensions.get('window').width, y: 0})
    }
  }

  _cancelPressed = () => {
    if (this.props.onCancel) {
      this.props.onCancel();
    }
    this.props.dialogHides();
  };

  _confirmPressed = () => {
    if (this.props.onConfirm) {
      let reason =
        this.state.selectedCategory === 4
          ? this.state.reason
          : category[this.state.selectedCategory];
      if (!isEmpty(reason)) this.props.onConfirm(reason);
    }
    this.props.dialogHides();
  };

  closeModal() {
    console.log('yo')
    this.props.dialogHides();
  }

  _onCategoryPressed = index => {
    this.setState({ selectedCategory: index });
  };

  handleOnTextChange = text => {
    this.setState({ reason: text });
  };

  render() {
    console.log(
      this.props.isShown,
      this.props.title,
      this.props.message,
      this.props.dialogType,
      this.props.closable,
      this.props.images,
      this.props.index,
      'Modals'
    );
    return (
      <GestureRecognizer
      // onSwipe = { (data) => console.log(data, 'Check Swipe Down')}
      onSwipeDown={() => this.closeModal()}
      style={{ flex: 1 }}
      >
      <View>
        <Modal visible={this.props.isShown} animationType={'slide'} onRequestClose={() => this.closeModal()}>
          <ScrollView
            ref={ref => {
              this.listRef = ref;
            }}
            horizontal={true}
            pagingEnabled={true}
          >
            {this.props.images.length > 0 && this.props.images.map(
                (item, index) => {
                  return (
                    <View
                      key={index}
                      style={{
                        alignItems: 'center',
                        justifyContent: 'center'
                      }}
                    >
                      <Image
                        source={ get(item, 'originalUrl', undefined) ? { uri: get(item, 'originalUrl', '') }
                          : require('../../../../assets/images/user_dummy_image.png')
                        }
                        style={modalImageStyle.image}
                      />
                    </View>
                  );
                }
              )}
          </ScrollView>
          <TouchableOpacity style={modalImageStyle.crossButton} onPress={() => this.closeModal()}>
            <Image
            source={require('../../../../assets/app_icons/Icons/Close-dark.png')}
          />
          </TouchableOpacity>
        </Modal>
      </View>
          </GestureRecognizer>
    )
  }
}

const mapStateToProps = ({ errorModal }) => {
  return {
    isShown: errorModal.isShown,
    title: errorModal.title,
    message: errorModal.message,
    dialogType: errorModal.dialogType,
    closable: errorModal.closable,
    onConfirm: errorModal.onConfirm,
    onCancel: errorModal.onCancel,
    images: errorModal.images,
    index: errorModal.index
  };
};

const mapDispatchToProps = dispatch => ({
  dialogHides: bindActionCreators(dialogHides, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(MSAModalImage);

// Example
{
  /* <MSAModalAction
  onPress={() => this._handleErrorPress.bind(this)}
  type='Refresh'
  action='Logout'
  onClose={() => this.setState({ showModal: false})}
  showModal = {this.state.showModal}
  actionDescription = {'You will stop recieveing notifications and new updates'}
/> */
}
