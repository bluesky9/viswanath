import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  Modal,
  Image,
  Dimensions
} from 'react-native';
const { width } = Dimensions.get('window');
import { modalActionStyle, modalErrorStyle } from './modal-style';
import MSAButton from '../buttons/button';
import JInput from '../input/input';
import { Picker, Form, Icon } from 'native-base';
import { constants } from '../../../constants';

let date = new Date();
let presentYear = date.getFullYear();

const Item = Picker.Item;

type Props = {
  onSubmit?: Function;
  onClose?: Function;
  isShown?: boolean;
  domain?: any;
};

type state = {
  selectedPick: string;
  titleMessage: string;
  placeholder: string;
  showInput: boolean;
  inputVal: string;
  emailErr: string;
};

class MSAModalReport extends Component<Props, state> {
  constructor(props) {
    super(props);
    this.state = {
      selectedPick: 'key0',
      titleMessage:
        'Confirm your membership by providing your university e-mail address',
      placeholder: 'email@university.com',
      showInput: false,
      inputVal: '',
      emailErr: ''
    };
  }

  onSubmitPress = () => {
    if (
      this.state.selectedPick === 'CURRENT_STUDENT' ||
      this.state.selectedPick === 'PROFESSOR' ||
      this.state.selectedPick === 'UNIVERSITY_STAFF'
    ) {
      // console.log(this.state.inputVal)
      if (/\S+@\S+\.\S+/.test(this.state.inputVal)) {
        if (RegExp(this.props.domain).test(this.state.inputVal)) {
          this.props.onClose();
          this.props.onSubmit({
            userType: this.state.selectedPick,
            universityEmail: this.state.inputVal
          });
        } else {
          this.setState({
            emailErr: 'Domain is not associated with current university'
          });
        }
      } else {
        this.setState({ emailErr: 'Invalid email' });
      }
    } else {
      if (this.state.selectedPick === 'PROSPECTIVE_STUDENT') {
        console.log(presentYear, 'YEAR', this.state.inputVal);
        if (parseInt(this.state.inputVal, 10) >= presentYear) {
          this.props.onClose();
          this.props.onSubmit({
            userType: this.state.selectedPick,
            intendedOrGraduationYear: this.state.inputVal
          });
        } else {
          this.setState({
            emailErr: 'Future date should be ' + presentYear + ' onwards'
          });
        }
      } else if (this.state.selectedPick === 'ALUMNI') {
        this.props.onClose();
        this.props.onSubmit({
          userType: this.state.selectedPick,
          intendedOrGraduationYear: this.state.inputVal
        });
      }
    }
  };
  handlePicker(val) {
    switch (val) {
      case 'key0':
        this.setState({ showInput: false });
        break;
      case 'PROSPECTIVE_STUDENT':
        this.setState({
          titleMessage: 'Intended year of entry',
          placeholder: 'future year...',
          showInput: true,
          inputVal: ''
        });
        break;
      case 'ALUMNI':
        this.setState({
          titleMessage: 'Graduation Year / End of Service',
          placeholder: 'past year...',
          showInput: true,
          inputVal: ''
        });
        break;
      default:
        this.setState({
          titleMessage:
            'Confirm your membership by providing your university e-mail address',
          placeholder: 'email@' + this.props.domain,
          showInput: true,
          inputVal: ''
        });
    }
    this.setState({ selectedPick: val });
  }
  render() {
    let keyboardType =
      this.state.selectedPick === 'PROSPECTIVE_STUDENT' ||
      this.state.selectedPick === 'ALUMNI'
        ? 'numeric'
        : 'email-address';
    return (
      <View>
        <Modal visible={this.props.isShown} animationType={'slide'}>
          <View style={{ flex: 1, alignItems: 'center' }}>
            <Image
              style={{ height: 30, width: 97, marginTop: 41 }}
              source={require('../../../../assets/images/main_icon.png')}
            />
            <Text
              style={[
                modalActionStyle.title,
                { marginTop: 32, width: 290, textAlign: 'center' }
              ]}
            >
              {'Follow the university'}
            </Text>
            <Form>
              <Picker
                iosHeader={'Select one'}
                style={{
                  borderWidth: 1,
                  borderColor: 'rgb(233,238,241)',
                  width: width - 10,
                  marginTop: 15
                }}
                mode={'dropdown'}
                selectedValue={this.state.selectedPick}
                onValueChange={val => this.handlePicker(val)}
              >
                <Item label={'Select user-type'} value={'key0'} />
                <Item
                  label={constants.USERTYPES.SIGULAR.CURRENT_STUDENT}
                  value={'CURRENT_STUDENT'}
                />
                <Item
                  label={constants.USERTYPES.SIGULAR.PROSPECTIVE_STUDENT}
                  value={'PROSPECTIVE_STUDENT'}
                />
                <Item
                  label={constants.USERTYPES.SIGULAR.PROFESSOR}
                  value={'PROFESSOR'}
                />
                <Item
                  label={constants.USERTYPES.SIGULAR.UNIVERSITY_STAFF}
                  value={'UNIVERSITY_STAFF'}
                />
                <Item
                  label={constants.USERTYPES.SIGULAR.ALUMNI}
                  value={'ALUMNI'}
                />
              </Picker>
            </Form>
            {this.state.showInput && (
              <View
                style={{
                  transform: [{ scaleY: this.state.showInput ? 1 : 0 }],
                  width: '100%',
                  paddingHorizontal: 15
                }}
              >
                <Text
                  style={[
                    modalActionStyle.educationLows,
                    { width: width - 60, marginVertical: 30 }
                  ]}
                >
                  {this.state.titleMessage}
                </Text>
                <JInput
                  placeholder={this.state.placeholder}
                  changeText={txt =>
                    this.setState({ inputVal: txt, emailErr: '' })
                  }
                  keyboard={keyboardType}
                  error={this.state.emailErr}
                  style={{ alignSelf: 'stretch' }}
                />
              </View>
            )}
            <View style={{ margin: 15, marginTop: 30 }}>
              <MSAButton
                type={'primary'}
                label={'Follow'}
                enableShadow={true}
                disabled={this.state.inputVal === ''}
                onPress={() => this.onSubmitPress()}
              />
            </View>
            <TouchableOpacity
              style={modalErrorStyle.crossButton}
              onPress={() => this.props.onClose()}
            >
              <Icon style={modalErrorStyle.crossIcon} name={'md-close'} />
            </TouchableOpacity>
          </View>
        </Modal>
      </View>
    );
  }
}

export default MSAModalReport;
