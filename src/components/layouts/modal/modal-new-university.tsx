import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  Modal,
  Image,
  TextInput,
  Dimensions
} from 'react-native';
const { width } = Dimensions.get('window');
import Picker from 'react-native-picker';
import { map } from 'lodash';

import MSAButton from '../buttons/button';
import { modalActionStyle, modalErrorStyle } from './modal-style';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { getCountries } from '../../../store/actions/Countries.js'

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// import { style } from '../../../styles/variables';

type Props = {
  onSubmit?: Function;
  onClose?: Function;
  isShown?: boolean;
  getCountries;
  countries;
};

type state = {
  checkStatus: boolean;
  name: string;
  website: string;
  country: string;
};

class MSAModalUniversityRequest extends Component<Props, state> {
  constructor(props) {
    super(props);
    this.state = {
      checkStatus: true,
      website: '',
      country: '',
      name: ''
    };
  }

  componentDidMount() {
    this.props.getCountries()
  }

  onSelectUniversity(countries) {
    Picker.init({
      pickerData: countries,
      pickerTitleText: 'Select university',
      pickerCancelBtnText: 'Cancel',
      pickerConfirmBtnText: 'Confirm',
      selectedValue: [],
      onPickerConfirm: data => {
          this.setState({country: data[0]});
          Picker.hide();
      },
      onPickerCancel: () => {
          Picker.hide();
      }
    });
    Picker.show();
  }

  render() {
    return (
      <View>
        <Modal visible={this.props.isShown} animationType={'slide'}>
          <View style={{ flex: 1, alignItems: 'center' }}>
            <Image
              style={{ height: 30, width: 97, marginTop: 35 }}
              source={require('../../../../assets/images/main_icon.png')}
            />
            <KeyboardAwareScrollView
              contentContainerStyle={{
                alignItems: 'center',
                paddingBottom: 60
              }}
              showsVerticalScrollIndicator={false}
            >
              <Text
                style={[
                  modalActionStyle.title,
                  { marginTop: 32, width: 290, textAlign: 'center' }
                ]}
              >
                {'Add your university or college'}
              </Text>
              <Image
                style={{ marginTop: 23 }}
                source={require('../../../../assets/newUniversity.png')}
              />
              <View
                style={[
                  modalActionStyle.fieldBox,
                  { height: 71, padding: 15, marginTop: 25, borderTopWidth: 1 }
                ]}
              >
                <Text style={[modalActionStyle.boxHead]}>NAME</Text>
                <TextInput
                  style={[modalActionStyle.skillInp, { color: '#53575e'}]}
                  placeholder={'Name'}
                  placeholderTextColor={'rgba(108,111,125,0.5)'}
                  underlineColorAndroid={'rgba(0,0,0,0)'}
                  onChangeText={txt => this.setState({ name: txt })}
                />
              </View>
              <View
                style={[modalActionStyle.fieldBox, { height: 71, padding: 15 }]}
              >
                <Text style={[modalActionStyle.boxHead]}>
                  WEBSITE (optional)
                </Text>
                <TextInput
                  style={[modalActionStyle.skillInp, { color: '#53575e'}]}
                  placeholder={'Ex. www.chuckles.com'}
                  placeholderTextColor={'rgba(108,111,125,0.5)'}
                  underlineColorAndroid={'rgba(0,0,0,0)'}
                  onChangeText={txt => this.setState({ website: txt })}
                />
              </View>
              <View
                style={[modalActionStyle.fieldBox, { height: 71, padding: 15 }]}
              >
                <Text style={[modalActionStyle.boxHead]}>COUNTRY</Text>
                <TouchableOpacity
                  onPress={() => this.onSelectUniversity(this.props.countries)}
                >
                  <Text style={{ color: '#53575e'}}>{this.state.country}</Text>
                </TouchableOpacity>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 22 }}>
                <TouchableOpacity
                  onPress={() =>
                    this.setState({
                      checkStatus: !this.state.checkStatus
                    })
                  }
                >
                  <Image
                    source={
                      this.state.checkStatus
                        ? require('../../../../assets/app_icons/Icons/Checkbox-active.png')
                        : require('../../../../assets/app_icons/Icons/Checkbox-default.png')
                    }
                    style={{ marginHorizontal: 15, marginTop: 5 }}
                  />
                </TouchableOpacity>
                <Text
                  style={[
                    modalActionStyle.educationLows,
                    { width: width - 60 }
                  ]}
                >
                  Send me an update when this university is on uple
                </Text>
              </View>
              <View style={{ paddingHorizontal: 15, marginTop: 40 }}>
                <MSAButton
                  onPress={() => {
                    let data = {
                      notifyMe: this.state.checkStatus,
                      name: this.state.name,
                      country: this.state.country,
                      website: this.state.website
                    };
                    console.log(data);
                    this.props.onSubmit(data);
                  }}
                  disabled={this.state.name === '' || this.state.country === ''}
                  type={'primary'}
                  label={'Send Request'}
                  enableShadow={true}
                />
              </View>
            </KeyboardAwareScrollView>
            <TouchableOpacity
              style={modalErrorStyle.crossButton}
              onPress={() => this.props.onClose()}
            >
              <Image
                source={require('../../../../assets/app_icons/Icons/Close-dark.png')}
              />
            </TouchableOpacity>
          </View>
        </Modal>
      </View>
    );
  }
}

const mapStateToProps = ({ countries }) => {
  return {
    countries: map(countries.countries, ('countryName'))
  };
};

const mapDispatchToProps = dispatch => ({
  getCountries: bindActionCreators(getCountries, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(MSAModalUniversityRequest)