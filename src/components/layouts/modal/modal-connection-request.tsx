import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  Modal,
  Image,
  Dimensions,
  TextInput
} from 'react-native';
import {Icon} from 'native-base';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
const { width } = Dimensions.get('window');

import { dialogHides } from '../../../store/actions/ErrorModal.js'
import { PeopleApi } from '../../../store/actions'

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { modalErrorStyle } from './modal-style';

type Props = {
  onPress?: Function,
  type?: string,
  isShown?: boolean,
  title?: string,
  message?: string,
  dialogType?: string,
  closable?: boolean,
  dialogHides?: any,
  onConfirm?;
  onCancel?;
  peopleInfo;
  peopleApi;
  userInfo: any;
  peopleId: number;
};

type state = {
  connectMessage;
};

class MSAModalConnect extends Component<Props, state> {
  constructor(props) {
    super(props);
    this.state = {
      connectMessage: 'Hi, my name is ' + this.props.userInfo.firstName + ' ' + this.props.userInfo.lastName + '. I would like to connect with you.'
    }
    this.closeModal = this.closeModal.bind(this);
  }

  closeModal() {
    this.props.dialogHides();
  }

  _confirmPressed = () => {
    // if (this.props.onConfirm) {
    //   let reason = this.state.selectedCategory === 4 ? this.state.reason : category[this.state.selectedCategory];
    //   if (!_.isEmpty(reason))
    //     this.props.onConfirm(reason);
    // }
    this.props.peopleApi.connectPeople(this.props.peopleInfo.id, this.state.connectMessage)
    this.props.dialogHides();
  }

  _handleTextChange = (text) => {
    this.setState({ connectMessage: text })
  }

  render() {
    console.log(this.props.peopleInfo, 'People Info')
    return (
      <View>
        <Modal
          visible={this.props.isShown}
          animationType={'slide'}
          onRequestClose={() => this.closeModal()}
        >
          <KeyboardAwareScrollView
            style={{
              flex: 1
            }}
          >
            <View style={{
              paddingHorizontal: 15, alignItems: 'center',
              justifyContent: 'flex-start', flex: 1}}>
              <Text style={[modalErrorStyle.title, { paddingTop: 69 }]}>
                {this.props.title}
              </Text>
              <Image
                style={{
                  height: 91,
                  width: 91,
                  borderRadius: 91 / 2,
                  paddingTop: 93 / 2 + 17,
                  marginBottom: 10
                }}
                source={ this.props.peopleInfo.user.profPicPath ?
                  {uri : this.props.peopleInfo.user.profPicPath}
                  : require('../../../../assets/images/user_dummy_image.png')}
              />
              <Text style={[modalErrorStyle.buttonText]}>
                {this.props.peopleInfo.user.firstName + ' ' + this.props.peopleInfo.user.lastName}
              </Text>
              <View
                style={{
                  height: 1,
                  width: width,
                  backgroundColor: 'rgb(233,239,247)',
                  marginTop: 20
                }}
              />
              <Text
                style={[
                  modalErrorStyle.inpHeader,
                  { paddingTop: 29 }
                ]}
              >
                MESSAGE
              </Text>
              <View style={{
                borderWidth: 1,
                borderRadius: 3,
                borderColor: 'rgb(233,238,241)',
                marginTop: 4
                }}>
                <TextInput
                  multiline={true}
                  numberOfLines={4}
                  style={[modalErrorStyle.textInp, {textAlignVertical: 'top'}]}
                  value={this.state.connectMessage}
                  onChangeText={this._handleTextChange}
                  maxLength={200}
                />
                <Text
                  style={modalErrorStyle.inpDesc}
                >
                  Character limit 200
                </Text>
              </View>
              <TouchableOpacity
                style={{
                  width: 126,
                  backgroundColor: 'rgb(52,135,230)',
                  alignSelf: 'flex-end',
                  borderRadius: 3,
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop: 15,
                  paddingVertical: 8
                }}
                onPress={() => this._confirmPressed()}
              >
                <Text style={modalErrorStyle.sendBtnTxt}>Send</Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              style={modalErrorStyle.crossButton}
              onPress={() => this.closeModal()}
            >
              <Icon
                style={modalErrorStyle.crossIcon}
                  name={'md-close'}
                />
            </TouchableOpacity>
          </KeyboardAwareScrollView>
        </Modal>
      </View>
    );
  }
}

const mapStateToProps = ({ errorModal, peopleInfo , userInfoAPI}, props) => {
  return {
    isShown: errorModal.isShown,
    title: errorModal.title,
    message: errorModal.message,
    dialogType: errorModal.dialogType,
    closable: errorModal.closable,
    onConfirm: errorModal.onConfirm,
    onCancel: errorModal.onCancel,
    peopleInfo: peopleInfo.peopleInfo[props.peopleId],
    userInfo: userInfoAPI.userInfo
  };
}

const mapDispatchToProps = dispatch => ({
  dialogHides: bindActionCreators(dialogHides, dispatch),
  peopleApi: bindActionCreators(PeopleApi, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(MSAModalConnect);

// Example
// <MSAModalError
// onPress={() => this._handleErrorPress.bind(this)}
// type='Refresh'
// error='Sign up'
// onClose={() => this.setState({ showModal: false})}
// showModal = {this.state.showModal}
// errorDescription = {'There was a problem with the signup.'}
// />
