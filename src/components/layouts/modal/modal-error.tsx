import React, { Component } from 'react';
import { View, TouchableOpacity, Text, Modal } from 'react-native';
import { Icon } from 'native-base';

import { modalErrorStyle } from './modal-style';

import { dialogHides } from '../../../store/actions/ErrorModal.js'

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

type Props = {
  onPress?: Function,
  type?: string,
  isShown?: boolean,
  title?: string,
  message?: string,
  dialogType?: string,
  closable?: boolean,
  dialogHides?: any,
  instructionMessage?: string,
  button?: string
};

type state = {
  modalVisible
}

class MSAModalError extends Component<Props, state> {
  constructor(props) {
    super(props);
    // this._onPressButton = this._onPressButton.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  // _onPressButton() {
  //   if (this.props.onPress) this.props.onPress();
  // }

  closeModal() {
    console.log('why not')
    this.props.dialogHides();
    // this.props.onClose();
  }

  render() {
    console.log(this.props.isShown, this.props.title, this.props.message, this.props.dialogType, this.props.closable, 'Modals' );
    if (this.props.isShown)
      return <View>
          <Modal
            visible={this.props.isShown}
            animationType={'slide'}
            onRequestClose={() => this.closeModal()}
          >
            <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, paddingHorizontal: 50 }}>
              <Text style={modalErrorStyle.title}>{this.props.title}</Text>
              <Text style={modalErrorStyle.errorDescription}>{this.props.message}</Text>
              <Text style={modalErrorStyle.errorInstruction}>{this.props.instructionMessage ? this.props.instructionMessage : ''}</Text>
              <TouchableOpacity style={modalErrorStyle.button} onPress={() => this.closeModal()}>
                <Text style={modalErrorStyle.buttonText}>{this.props.button ? this.props.button : this.props.dialogType}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={modalErrorStyle.crossButton} onPress={() => this.closeModal()}>
                {/* <Image
                source={require('../../../../assets/app_icons/Icons/Close-dark.png')}
                style={modalErrorStyle.crossIcon}
                /> */}
                <Icon
                style={modalErrorStyle.crossIcon}
                  name={'md-close'}
                />
              </TouchableOpacity>
            </View>
          </Modal>
        </View>;
    else {
      return <View />;
    }
  }
}

const mapStateToProps = ({ errorModal }) => {
  return {
    isShown: errorModal.isShown,
    title: errorModal.title,
    message: errorModal.message,
    dialogType: errorModal.dialogType,
    closable: errorModal.closable,
    onConfirm: errorModal.onConfirm,
    onCancel: errorModal.onCancel,
    instructionMessage: errorModal.instructionMessage,
    button: errorModal.button
  };
}

const mapDispatchToProps = dispatch => ({
  dialogHides: bindActionCreators(dialogHides, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(MSAModalError);

// Example
// <MSAModalError
// onPress={() => this._handleErrorPress.bind(this)}
// type='Refresh'
// error='Sign up'
// onClose={() => this.setState({ showModal: false})}
// showModal = {this.state.showModal}
// errorDescription = {'There was a problem with the signup.'}
// />