import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  Dimensions,
  TouchableOpacity,
  Animated,
  Platform,
  Alert
} from 'react-native';
const { width, height } = Dimensions.get('window');
import profileStyles from '../../profile/profile-styles';
import { get } from 'lodash';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Mailer from 'react-native-mail';

import { getProfileDetails } from '../../../store/actions/Profile.js';
import {
  DrawerActions,
  LoginApi,
  DialogModal,
  InviteActions
} from '../../../store/actions';

const LOGOUT_MSG = 'You will stop receiving notifications and new updates';

type IPeopleFilterProps = {
  isOpen: boolean;
  getProfileDetails: Function;
  userInfo: {
    name: string;
  };
  navigation: any;
  userId;
  drawerActions;
  loginApi;
  dialogModal;
  drawerOpen;
  inviteActions;
};
type State = {
  movFac: Animated.Value;
  open: boolean;
};
class Drawer extends Component<IPeopleFilterProps, State> {
  constructor(props) {
    super(props);
    this.state = {
      movFac: new Animated.Value(100),
      open: false
    };
  }

  // componentDidMount() {
  //   console.log(this.props.userId, '!!@@#')
  //   this.props.getProfileDetails(this.props.userId);
  // }

  componentWillReceiveProps(nextProp) {
    if (nextProp.drawerOpen !== this.state.open) {
      this.setState({ open: nextProp.drawerOpen });
      Animated.timing(
        this.state.movFac, // The value to drive
        {
          toValue: nextProp.drawerOpen ? 0 : 100,
          duration: 500
        }
      ).start(); // Start the animation
    }
    if (nextProp.userId !== this.props.userId && nextProp.userId) {
      this.props.getProfileDetails(nextProp.userId);
    }
  }
  closeDrawer = () => {
    this.props.drawerActions.drawerClosed();
  };

  onProfilePressed = () => {
    this.props.drawerActions.drawerClosed();
    this.props.navigation.navigate('MyProfile');
  };

  onSettingsPressed = () => {
    this.props.drawerActions.drawerClosed();
    this.props.navigation.navigate('SettingsMain');
  };

  onInvitePressed = () => {
    this.props.drawerActions.drawerClosed();
    this.props.navigation.navigate('Invite');
  };

  onLogoutPressed = () => {
    this.props.drawerActions.drawerClosed();
    this.props.dialogModal.dialogShows(
      'Logout',
      LOGOUT_MSG,
      'action',
      true,
      () => this.props.loginApi.logout()
    );
  };

  onProfilePicPressed = image => {
    let images = [];
    images.push({ originalUrl: image });
    this.props.dialogModal.dialogShows(
      'images',
      undefined,
      'image',
      true,
      undefined,
      undefined,
      undefined,
      undefined,
      images
    );
    this.props.drawerActions.drawerClosed();
  };

  render() {
    const mover = this.state.movFac.interpolate({
      inputRange: [0, 100],
      outputRange: [0, -width]
    });
    const colorer = this.state.movFac.interpolate({
      inputRange: [0, 5, 100],
      outputRange: [
        'rgba(75,106,143,0.2)',
        'rgba(75,106,143,0)',
        'rgba(75,106,143,0)'
      ]
    });
    let userImage = get(this.props.userInfo, 'profilePicture', undefined)
      ? {
        uri: get(this.props.userInfo, 'profilePicture', undefined)
      }
      : require('../../../../assets/images/user_dummy_image.png');
    return (
      <Animated.View
        style={{
          flexDirection: 'row',
          transform: [{ translateX: mover }],
          position: 'absolute'
        }}
      >
        <View
          style={{
            height: height,
            width: width - 60,
            justifyContent: 'flex-start',
            paddingTop: 26,
            backgroundColor: 'white',
            zIndex: 4,
            elevation: 4,
            shadowOffset: { width: 5, height: 0 },
            shadowOpacity: 0.1
          }}
        >
          <TouchableOpacity onPress={this.onProfilePressed}>
            <TouchableOpacity
              onPress={this.onProfilePressed}
            // onPress={() =>
            //   this.onProfilePicPressed(
            //     get(this.props.userInfo, 'profilePicture', undefined)
            //   )
            // }
            >
              <Image
                source={userImage}
                style={{
                  height: 63,
                  width: 63,
                  marginHorizontal: 20,
                  borderRadius: 31.5
                }}
              />
            </TouchableOpacity>
            <View style={{ marginTop: 8 }}>
              <Text style={[profileStyles.nameText, { marginHorizontal: 20 }]}>
                {get(this.props.userInfo, 'firstName', 'NA') +
                  ' ' +
                  get(this.props.userInfo, 'lastName', '')}
              </Text>
            </View>
          </TouchableOpacity>
          <View style={[profileStyles.statsContainer, { top: 15 }]}>
            <TouchableOpacity
              style={profileStyles.statOneContainer}
              onPress={() => {
                this.props.drawerActions.drawerClosed();
                this.props.navigation.navigate('PeopleFollowerList', {
                  userType: 'loggedInUser',
                  title: 'Followers'
                });
              }}
            >
              <Text style={profileStyles.statNmbrText}>
                {get(this.props.userInfo, 'followers', []).length}
              </Text>
              <Text style={profileStyles.statNameText}>Followers</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={profileStyles.statOneContainer}
              onPress={() => {
                this.props.drawerActions.drawerClosed();
                this.props.navigation.navigate('PeopleFollowerList', {
                  userType: 'loggedInUser',
                  title: 'Following'
                });
              }}
            >
              <Text style={profileStyles.statNmbrText}>
                {get(this.props.userInfo, 'following', []).length}
              </Text>
              <Text style={profileStyles.statNameText}>Following</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[profileStyles.statOneContainer, { flex: 0.7 }]}
              onPress={() => {
                this.props.drawerActions.drawerClosed();
                this.props.navigation.navigate('PeopleFollowerList', {
                  userType: 'loggedInUser',
                  title: 'Connections'
                });
              }}
            >
              <Text style={profileStyles.statNmbrText}>
                {get(this.props.userInfo, 'connections', []).length}
              </Text>
              <Text style={profileStyles.statNameText}>Connections</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={{ flexDirection: 'row', top: 40 }}
            onPress={() => this.onProfilePressed()}
          >
            <Image
              source={require('../../../../assets/profileDefault.png')}
              style={{ marginHorizontal: 20 }}
            />
            <Text style={[profileStyles.asheetItem, { marginHorizontal: 0 }]}>
              Profile
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.onSettingsPressed()}
            style={{ flexDirection: 'row', top: 60 }}
          >
            <Image
              source={require('../../../../assets/settingsDefault.png')}
              style={{ marginHorizontal: 20 }}
            />
            <Text style={[profileStyles.asheetItem, { marginHorizontal: 0 }]}>
              Settings
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{ flexDirection: 'row', top: 80 }}
            onPress={this.onInvitePressed}
          >
            <Image
              source={require('../../../../assets/inviteDefault.png')}
              style={{ marginHorizontal: 20 }}
            />
            <Text style={[profileStyles.asheetItem, { marginHorizontal: 0 }]}>
              Invite a friend
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              position: 'absolute',
              top: Platform.OS === 'ios' ? height - 141 : height - 161,
              right: 0,
              left: 0,
              paddingVertical: 10
            }}
            onPress={() =>
              Mailer.mail(
                {
                  subject: 'Feedback',
                  recipients: ['support@uple.co'],
                  body: '',
                  isHTML: true
                },
                (error, event) => {
                  if (event && event !== 'cancelled' && event !== 'sent') {
                    Alert.alert(
                      error,
                      event,
                      [
                        {
                          text: 'Ok',
                          onPress: () => console.log('OK: Email Error Response')
                        }
                      ],
                      { cancelable: true }
                    );
                  }
                }
              )
            }
          >
            <Text style={[profileStyles.asheetItem, { marginHorizontal: 20 }]}>
              Send Feedback
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              position: 'absolute',
              top: Platform.OS === 'ios' ? height - 99 : height - 119,
              right: 0,
              left: 0,
              paddingVertical: 10
            }}
            onPress={() => this.onLogoutPressed()}
          >
            <Text style={[profileStyles.asheetItem, { marginHorizontal: 20 }]}>
              Logout
            </Text>
          </TouchableOpacity>
        </View>
        <Animated.View
          style={{ height: height, width: 60, backgroundColor: colorer }}
        >
          <TouchableOpacity
            onPress={() => this.closeDrawer()}
            style={{ height: height, width: 60 }}
          />
        </Animated.View>
      </Animated.View>
    );
  }
}

const mapStateToProps = state => {
  return {
    userInfo: state.userInfoAPI.userInfo,
    userId: state.userInfoAPI.userId,
    drawerOpen: state.drawerReducer.isOpened
  };
};

const mapDispatchToProps = dispatch => ({
  getProfileDetails: bindActionCreators(getProfileDetails, dispatch),
  drawerActions: bindActionCreators(DrawerActions, dispatch),
  loginApi: bindActionCreators(LoginApi, dispatch),
  dialogModal: bindActionCreators(DialogModal, dispatch),
  inviteActions: bindActionCreators(InviteActions, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Drawer);
