import { StyleSheet } from 'react-native';
import { style } from '../../../styles/variables';

export const buttonStyle = StyleSheet.create({
  button: {
    // height: 45
    width: '100%',
    backgroundColor: 'rgb(53,135,230)',
    borderRadius: 3,
    marginVertical: 10,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 12
  },
  textStyle: {
    color: 'white',
    fontFamily: style.font.family.rubikMedium,
    fontSize: 14,
    lineHeight: 17
  },
  imageStyle: {
    tintColor: style.color.primary,
    marginLeft: 5
  }
});
