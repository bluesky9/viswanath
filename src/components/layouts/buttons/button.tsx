import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Image } from 'react-native';

import { buttonStyle } from './button-style';
import { style } from '../../../styles/variables';

type Props = {
  onPress?: Function;
  type: string;
  label: string;
  disabled?: boolean;
  bordered?: boolean;
  containerWidth?: string;
  containerHeight?: string;
  customButtonStyle?: object;
  textSize?: number;
  textLineHeight?: number;
  textFamily?: string;
  enableShadow?: boolean;
  enableImage?: boolean;
  imageColor?: string;
};

export default class MSAButton extends Component<Props> {
  buttonStyles: Array<Object>;
  textStyles: Array<Object>;
  imageStyles: Array<Object>;
  constructor(props) {
    super(props);

    this.state = { enabled: true };

    this._defineStyles = this._defineStyles.bind(this);

    this._defineStyles(props);
    this._onPressButton = this._onPressButton.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this._defineStyles(nextProps);
  }

  _defineStyles(props) {
    this.buttonStyles = [buttonStyle.button];
    this.textStyles = [buttonStyle.textStyle];
    this.imageStyles = [buttonStyle.imageStyle];
    if (props.bordered) {
      this.buttonStyles.push({
        borderWidth: 1,
        borderColor: style.color.primary
      });
    }
    if (props.type === 'primary') {
      this.buttonStyles.push({ backgroundColor: style.color.primary });
    }
    if (props.type === 'inverted') {
      this.buttonStyles.push({ backgroundColor: style.color.transparent });
      this.textStyles.push({ color: style.color.primary });
    }
    if ( props.type === 'invertedGrey') {
      this.buttonStyles.push({ backgroundColor: style.color.transparent, borderColor: '#757B86' });
      this.textStyles.push({ color: '#757B86' });
    }
    if ( props.type === 'grey') {
      this.buttonStyles.push({ backgroundColor: '#757B86', borderColor: '#757B86'  });
      this.textStyles.push({ color: style.color.white });
    }
    if ( props.type === 'bloodRed') {
      this.buttonStyles.push({ backgroundColor: style.color.bloodRed, borderWidth: 0 });
      this.textStyles.push({ color: style.color.white });
    }
    if (props.containerWidth) {
      this.buttonStyles.push({ width: props.containerWidth });
    }
    if (props.containerHeight) {
      this.buttonStyles.push({ height: props.containerHeight });
    }
    if (props.customButtonStyle) {
      this.buttonStyles.push(props.customButtonStyle);
    }
    if (props.textSize) {
      this.textStyles.push({ fontSize: props.textSize });
    }
    if (props.textLineHeight) {
      this.textStyles.push({ lineHeight: props.textLineHeight });
    }
    if (props.textFamily) {
      this.textStyles.push({ fontFamily: props.textFamily });
    }
    if (props.enableShadow) {
      this.buttonStyles.push({
        shadowColor: style.color.primary,
        shadowOffset: { height: 5, width: 0 },
        shadowOpacity: 0.2,
        elevation: 3
      });
    }
    if (props.imageColor) {
      this.imageStyles.push({ tintColor: props.imageColor })
    }
  }

  _onPressButton() {
    if (this.props.onPress) this.props.onPress();
  }

  render() {
    return (
      <TouchableOpacity
        style={{ flexDirection: 'row' }}
        disabled={this.props.disabled}
        onPress={this._onPressButton}
      >
        <View style={[this.buttonStyles]}>
          <Text style={this.textStyles}>{this.props.label}</Text>
          {this.props.enableImage &&
            <Image
              source={require('../../../../assets/app_icons/Icons/arrowhead-down-default.png')}
              style={this.imageStyles}
            />
          }
        </View>
      </TouchableOpacity>
    );
  }
}

// Example
// <MSAButton onPress={this.handlePress} type='inverted' bordered={true} label={'Button'} containerWidth={'40%'}/>
