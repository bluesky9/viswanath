import { StyleSheet } from 'react-native';
import { style } from '../../../styles/variables';

export const loaderStyle = StyleSheet.create({
  loaderHolder : {
    width: '100%',
    height: '100%',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.3)',
    position: 'absolute'
  },
  loaderText : {
    fontFamily: style.font.family.rubikRegular,
    fontSize: style.font.size.small,
    color: style.color.primary,
    textAlign: 'center'
  }

});
