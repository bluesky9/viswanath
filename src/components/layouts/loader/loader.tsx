import React, { Component } from 'react';
import { View, ActivityIndicator } from 'react-native';

import { loaderStyle } from './loader-style';
import { style } from '../../../styles/variables';

import { connect } from 'react-redux';

type Props = {
  isShown?: boolean;
}

class MSALoader extends Component<Props> {

  constructor(props) {
    super(props);
  }

  shouldComponentUpdate(nextProps) {
    return this.props.isShown !== nextProps.isShown;
  }
  render() {
    if (this.props.isShown) {
      return (
        <View style={loaderStyle.loaderHolder}>
          <ActivityIndicator animating={this.props.isShown} size='large' color={style.color.lightGrey}/>
        </View>
      );
    } else {
      return <View />;
    }
  }
}

function mapStateToProps( { loaderApi } ) {
  return {
    isShown: loaderApi.isShown
  };
}

export default connect(mapStateToProps)(MSALoader);
