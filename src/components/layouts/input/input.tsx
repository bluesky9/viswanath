import React, { Component } from 'react';
import { View, TextInput, Text, Platform } from 'react-native';
import { style } from '../../../styles/variables';

type IPeopleFilterProps = {
  placeholder: string;
  changeText?: Function;
  error?: string;
  secure?: boolean;
  style?: Object;
  onFocusLost?: Function;
  keyboard?: any;
  returnKey?: any;
  onSubmit?: Function;
  inputRef?: any;
  maxChar?: any;
};
type State = {
  focussed: boolean;
};
export default class Drawer extends Component<IPeopleFilterProps, State> {
  constructor(props) {
    super(props);
    this.state = {
      focussed: false
    };
  }

  _handleOnBlur = () => {
    if (this.props.onFocusLost)
      this.props.onFocusLost()
    this.setState({
      focussed: false
    })
  }

  render() {
    return (
      <View
        style={[
          {
            width: '100%',
            alignSelf: 'center',
            justifyContent: 'flex-start',
            alignItems: 'center'
          },
          this.props.style ? this.props.style : undefined
        ]}
      >
        <TextInput
          ref={this.props.inputRef}
          onChangeText={text => this.props.changeText(text)}
          autoCapitalize={'none'}
          underlineColorAndroid={'rgba(0,0,0,0)'}
          autoCorrect={false}
          secureTextEntry={this.props.secure ? this.props.secure : false}
          onFocus={() => this.setState({ focussed: true })}
          onBlur={this._handleOnBlur}
          placeholder={this.props.placeholder}
          placeholderTextColor={'rgb(153,153,153)'}
          keyboardType={this.props.keyboard ? this.props.keyboard : 'default'}
          returnKeyType={this.props.returnKey}
          onSubmitEditing={() => { if (this.props.onSubmit) this.props.onSubmit() }}
          maxLength={this.props.maxChar}
          style={{
            width: '100%',
            borderWidth: 1,
            borderRadius: 3,
            paddingVertical: 12,
            paddingHorizontal: 12,
            fontSize: style.font.size.normal,
            lineHeight: style.font.lineHeight.normal.normal,
            fontFamily: style.font.family.rubikRegular,
            borderColor: this.state.focussed ? style.color.primary : 'rgb(233,238,241)',
            height: Platform.OS === 'ios' ? undefined : 42
          }}
        />
        <Text
          style={{
            height: this.props.error && this.props.error !== '' ? 18 : 0,
            fontSize: 10,
            fontFamily: style.font.family.rubikRegular,
            color:
              this.props.error === 'Weak Password' ||
                this.props.error === 'Improper Password'
                ? 'rgb(204,20,51)'
                : this.props.error === 'Fair Password'
                  ? 'orange'
                  : this.props.error === 'Strong Password'
                    ? 'green'
                    : 'rgb(204,20,51)',
            alignSelf: 'flex-start',
            top: 5,
            left: 5
          }}
        >
          {this.props.error ? this.props.error : ''}
        </Text>
      </View>
    );
  }
}
