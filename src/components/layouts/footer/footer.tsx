import React, { Component } from 'react';
import {
  View,
  Dimensions,
  TouchableOpacity,
  Platform
} from 'react-native';
import { Icon } from 'native-base';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { getNotificationsUnreadCount } from '../../../store/actions/Notifications.js';

const { width, height } = Dimensions.get('window');

type IPeopleFilterProps = {
  index: number;
  pressCall: Function;
  visible: boolean;
  footerState: any;
  actions: any;
};
type State = {
  unreadNoti: number;
};
class Footer extends Component<IPeopleFilterProps, State> {
  constructor(props) {
    super(props);
    this.state = {
      unreadNoti: 0
    };
  }
  componentWillReceiveProps(next) {
    if (next.footerState.notificationState.unreadCount !== undefined) {
      this.setState({
        unreadNoti: next.footerState.notificationState.unreadCount.general
      });
    }
  }
  render() {
    return (
      <View
        style={{
          flexDirection: 'row',
          position: 'absolute',
          height: 54,
          width: width,
          top: this.props.visible
            ? Platform.OS === 'ios' ? height - 54 : height - 74
            : height,
          borderTopWidth: 1,
          borderColor: 'rgb(233,238,241)',
          backgroundColor: 'white',
          zIndex: 10,
          elevation: 10,
          shadowOpacity: 0.2,
          shadowOffset: { width: 0, height: 2 }
        }}
      >
        <TouchableOpacity
          onPress={() => this.props.pressCall(0)}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          {this.props.index === 0 ? (
            <Icon name='md-paper' style={{ color: 'rgb(204, 20, 51)', fontSize: 28 }} />
          ) : (
            <Icon name='md-paper' style={{ color: 'rgb(207, 208, 220)', fontSize: 28 }} />
          )}
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.props.pressCall(1)}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          {this.props.index === 1 ? (
            <Icon name='md-contacts' style={{ color: 'rgb(204, 20, 51)', fontSize: 28 }} />
          ) : (
            <Icon name='md-contacts' style={{ color: 'rgb(207, 208, 220)', fontSize: 28 }} />
          )}
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.props.pressCall(2)}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          {this.props.index === 2 ? (
            <Icon name='md-mail' style={{ color: 'rgb(204, 20, 51)', fontSize: 28 }} />
          ) : (
            <Icon name='md-mail' style={{ color: 'rgb(207, 208, 220)', fontSize: 28 }} />
          )}
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.props.pressCall(3)}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          {this.props.index === 3 ? (
            <Icon name='ios-notifications-outline' style={{ color: 'rgb(204, 20, 51)', fontSize: 28 }} />
          ) : this.state.unreadNoti > 0 ? (
              <Icon name='ios-notifications' style={{ color: 'rgb(207, 208, 220)', fontSize: 28 }} />
            )
            :
            (
              <Icon name='ios-notifications-outline' style={{ color: 'rgb(207, 208, 220)', fontSize: 28 }} />
            )}
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.props.pressCall(4)}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          {this.props.index === 4 ? (
            <Icon name='md-school' style={{ color: 'rgb(204, 20, 51)', fontSize: 28 }} />
          ) : (
            <Icon name='md-school' style={{ color: 'rgb(207, 208, 220)', fontSize: 28 }} />
          )}
        </TouchableOpacity>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    footerState: state
  };
};
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      getNotificationsUnreadCount: getNotificationsUnreadCount
    },
    dispatch
  )
});

export default connect(mapStateToProps, mapDispatchToProps)(Footer);
