import React, { Component } from 'react';
import { View, StatusBar, TouchableOpacity, Image, Text } from 'react-native';
import { Icon } from 'native-base';

import { headerStyle } from './header-style';
import { style } from '../../../styles/variables';

type Props = {
  title: string;
  onPress?: Function;
  leftIcon?: string;
  onLeftIconPress?: Function;
  rightIcon?: string;
  onRightIconPress?: Function;
  rightText?: string;
  leftText?: string;
  onRightTextPress?: Function;
  disableRightText?: boolean;
  rightTextColor?: string;
  disableLeftIcon?: boolean;
};

export default class MSAHeader extends Component<Props> {
  constructor(props) {
    super(props);

    this._onPressButton = this._onPressButton.bind(this);
    this._onLeftIconPress = this._onLeftIconPress.bind(this);
    this._onRightIconPress = this._onRightIconPress.bind(this);
    this._onRightTextPress = this._onRightTextPress.bind(this);
  }

  _onLeftIconPress() {
    this.props.onLeftIconPress();
  }

  _onRightIconPress() {
    this.props.onRightIconPress();
  }

  _onPressButton() {
    if (this.props.onPress) this.props.onPress();
  }

  _onRightTextPress() {
    this.props.onRightTextPress();
  }

  render() {
    return (
      <View style={{ backgroundColor: style.color.white }}>
        <View style={{ height: style.header.height }}>
          <StatusBar />
        </View>
        <View style={headerStyle.headerContainer}>
          <View style={headerStyle.iconContainer}>
            {this.props.leftIcon === 'add' ? (
              <TouchableOpacity
                onPress={() => this._onLeftIconPress()}
                style={{ paddingLeft: 15 }}
              >
                <Image
                  source={require('../../../../assets/app_icons/Icons/Add-grey.png')}
                  style={{ height: 20, width: 20 }}
                />
              </TouchableOpacity>
            ) : this.props.leftText ? (
              <TouchableOpacity
                onPress={() => this._onPressButton()}
                style={{ paddingLeft: 15 }}
              >
                <Text style={headerStyle.leftText}>{this.props.leftText}</Text>
              </TouchableOpacity>
            ) : !this.props.disableLeftIcon && (
              <TouchableOpacity
                onPress={() => this._onPressButton()}
                style={{ paddingLeft: 15 }}
              >
                <Icon
                  style={headerStyle.backIcon}
                  name={'md-arrow-back'}
                />
              </TouchableOpacity>
            )}
          </View>
          <View style={headerStyle.titleContainer}>
            <Text style={headerStyle.title} numberOfLines={1}>{this.props.title}</Text>
          </View>
          {this.props.rightIcon ? (
            <View style={headerStyle.iconContainer}>
              <TouchableOpacity
                onPress={() => this._onRightIconPress()}
                style={{ alignItems: 'flex-end', paddingRight: 15, paddingLeft: 10, paddingVertical: 7 }}
              >
                {this.props.rightIcon === 'filter' ? (
                  <Image
                    source={require('../../../../assets/app_icons/Icons/filters.png')}
                  />
                ) : (
                  <Image
                    source={require('../../../../assets/app_icons/Icons/bento-menu.png')}
                  />
                )}
              </TouchableOpacity>
            </View>
          ) : (
            <View style={{ flex: 0.8 }}>
              {this.props.rightText && (
                <TouchableOpacity
                  onPress={() => this._onRightTextPress()}
                  style={{ alignItems: 'flex-end', paddingRight: 15 }}
                  disabled={this.props.disableRightText}
                >
                  <Text style={[headerStyle.rightText, { color: this.props.rightTextColor ? this.props.rightTextColor : style.color.primary }]}>
                    {this.props.rightText}
                  </Text>
                </TouchableOpacity>
              )}
            </View>
          )}
        </View>
      </View>
    );
  }
}

// Example
// <MSAHeader title={'New Password'} onPress={this.handleBackPress.bind(this)}/>
