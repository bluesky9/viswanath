import { StyleSheet } from 'react-native';
import { style } from '../../../styles/variables';

export const headerStyle = StyleSheet.create({
  headerContainer: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    borderBottomWidth: 2,
    borderBottomColor: style.color.lightGrey,
    paddingVertical: 17
  },
  iconContainer: {
    alignSelf: 'center',
    flex: 0.8
  },
  titleContainer: {
    flex: 2.0,
    alignItems: 'center'
  },
  title: {
    color: style.color.bluishGrey,
    fontFamily: style.font.family.rubikRegular,
    letterSpacing: 0.2,
    fontSize: style.font.size.large,
    lineHeight: style.font.lineHeight.normal.large
  },
  rightText: {
    fontSize: 14,
    lineHeight: 17,
    letterSpacing: 0.5,
    fontFamily: style.font.family.rubikMedium,
    color: style.color.primary
  },
  leftText: {
    fontSize: 14,
    lineHeight: 17,
    letterSpacing: 0.5,
    fontFamily: style.font.family.rubikMedium,
    color: style.color.bluishGrey
  },
  backIcon: {
    fontSize: 30,
    color: 'rgb(197, 207, 219)'
  }
});
