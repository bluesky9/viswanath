export const constants = {
  USERTYPES: {
    SIGULAR: {
      CURRENT_STUDENT : 'Current Student',
      UNIVERSITY_STAFF : 'Administrative Staff',
      PROSPECTIVE_STUDENT : 'Future Student',
      ALUMNI : 'Alumnus',
      PROFESSOR: 'Faculty'
    },
    PLURAL: {
      CURRENT_STUDENT : 'Current Students',
      UNIVERSITY_STAFF : 'Administrative Staff',
      PROSPECTIVE_STUDENT : 'Future Students',
      ALUMNI : 'Alumni',
      PROFESSOR: 'Faculty'
    },
    CONTEXT: {
      CURRENT_STUDENT : 'Currently Studying at',
      UNIVERSITY_STAFF : 'Administrative Staff at',
      PROSPECTIVE_STUDENT : 'Future Student at',
      ALUMNI : 'Alumnus of',
      PROFESSOR: 'Faculty at'
    }
  }
}