import {NavigationActions} from 'react-navigation';
import { get, last } from 'lodash';

const customNavigationAction = NavigationActions as any;

let config = {
  navigator: undefined
};

export function setNavigator(nav) {
  if (nav) {
    config.navigator = nav;
  }
}
export function getLastRouteName() {
  const lastRouteName = get( last( get( config, 'navigator.state.nav.routes', [] ) ), 'routeName');
  return lastRouteName;
}

export function reset(routeName, params) {
  let action = NavigationActions.reset({ index: 0, actions: [ NavigationActions.navigate({routeName, params}) ] })
  config.navigator.dispatch(action);
}
export function navigate(routeName, params) {
  if (config.navigator && routeName) {
    let action = NavigationActions.navigate({routeName, params});
    config.navigator.dispatch(action);
  }
}
export function goBack() {
  if (config.navigator) {
    let action = NavigationActions.back({});
    config.navigator.dispatch(action);
  }
}

export function replace(route, params) {
  // Replace Messaging routes
  console.log(config.navigator.state.nav.routes[config.navigator.state.nav.routes.length - 1].key)
  if (config.navigator) {
    let action = customNavigationAction.replace(
      {
        key: config.navigator.state.nav.routes[config.navigator.state.nav.routes.length - 1].key,
        routeName: route,
        params
      });
    config.navigator.dispatch(action);
  }
}