// Common Styles for App

import { Dimensions, StyleSheet, Platform } from 'react-native';

// const { StatusBarManager } = NativeModules;

// fade structure is not correct yet

// rgba is crude but working for basic rgb colors
// better tor refer the semantic things ('primaryColor' etc) to another color, not sure how todo that and still return a const.
// for now copy pasted

export const style = {
  device: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
  },
  header: {
    height: Platform.OS === 'ios' ? 20 : 0 // Android, statusBar is outside App component by default, no overlay needed
  },
  font: {
    size: {
      humongous: 48,
      huger: 28,
      huge: 23,
      hugish: 22,
      larger: 18,
      large: 16,
      largish: 15,
      normal: 14,
      small: 12,
      smaller: 11,
      tiny: 9
    },
    lineHeight: {
      tight: {
        humongous: 48,
        huger: 28,
        huge: 23,
        hugish: 22,
        larger: 20,
        large: 16,
        normal: 14,
        small: 12,
        smaller: 11
      },
      normal: {
        huge: 28,
        hugish: 27,
        larger: 22,
        large: 19,
        normal: 17,
        small: 15,
        smaller: 13
      },
      wide: {
        huge: 34,
        hugish: 32,
        larger: 27,
        large: 24,
        normal: 21,
        small: 18,
        smaller: 15
      }
    },
    family: {
      ptSerif: 'PTSerif-Regular',
      ptSerifBold: 'PTSerif-Bold',
      rubikMedium: 'Rubik-Medium',
      rubikRegular: 'Rubik-Regular',
      rubikLight: 'Rubik-Light',
      helveticaNeue: 'helveticaNeue'
    }
  },
  icon: {
    size: {
      gargantuan: 120,
      humongous: 100,
      huge: 80,
      larger: 60,
      large: 40,
      normal: 30,
      small: 27,
      smaller: 22,
      tab: 44
    }
  },
  color: {
    transparent: 'transparent',
    none: 'transparent',

    // primary: '#3587e6', // special         // rgb 53 135 230
    primary: '#2F7CE2',
    nero: '#3b3e44', // rgb 59 62 68
    skyBlue: '#69aaf5',
    dustyGrey: '#999999', // rgb 153 153 153
    grey: '#808691', // rgb 128 134 145
    bluishGrey: '#35475b', // rgb 53 71 91
    lighterGrey: '#EEF1E9',
    lightGrey: '#e9eff7', // rgb 233 239 247
    coal: '#53575e', //  rgb 83 87 94
    bloodRed: '#8E2929', // rgb 204 20 51
    white: '#FFFFFF',
    cream: '#E9EEF1', // rgb 233 238 241
    whiteCream: '#FAFBFC' // rgb 250 251 252

    // nero: '#1A1A1A',
    // secondary: '#1A1A1A', // special
    // crimson: '#E30138',
    // warning: '#E30138',

    // green: '#95C11F',
    // highlight: '#95C11F', // special

    // grey: '#9D9D9C',
    // soft: '#9D9D9C', // special

    // inactive: 'rgba(0,0,0,0.1)',

    // yellow: '#FCEA10',

    // darkerGrey: '#3C3C3B',
    // darkGrey: '#808080',
    // black: '#000000',
    // white: '#FFFFFF',
    // lightGrey: '#B3B3B3',
    // lighterGrey: '#DADADA',
    // offWhite: '#EDEDED',

    // orange: '#E28764',
    // seagull: '#6FABC0',
    // bermuda: '#67CCC0',
    // blueHaze: '#BDC6D8',
    // purple: '#9587BC',
    // shakespeare: '#5EA3BA',
    // doveGrey: '#757575',
    // cabaret: '#E64576',
    // oil: '#323231',
    // gulf: '#3EB5B3',
    // vanilla: '#CFB99C',
    // silver: '#D2C7C3',
    // pearl: '#E9DFBC',
    // colonialWhite: '#FCF8D3',
    // albescentWhite: '#E0D9CE',
    // chestnut: '#DBACAC',
    // perwinkle: '#C1D0E0',
    // ash: '#C8C1B4',
    // glacier: '#80ABC0',
    // hopbush: '#BE6E92',
    // mulberry: '#C14780',
    // blueYonder: '#7686B3',
    // aqua: '#A5E2D5',
    // fade: {
    //   light: { from: '#f0ede8', to: '#FFFFFF' },
    //   veryLight: { from: '#b1b1b1', to: '#dadada' }
    // }
  },
  border: {
    width: {
      lighter: StyleSheet.hairlineWidth, // lighter:0.125,
      light: StyleSheet.hairlineWidth, // light:0.25,
      normal: StyleSheet.hairlineWidth * 2, // normal:0.5,
      heavy: 1, // heavy:1.0,
      heavier: 2, // heavier:1.5,
      heaviest: 3 // heaviest:3,
    }
  },
  alpha: {
    darker: 0.9,
    dark: 0.85,
    normal: 0.75,
    light: 0.66,
    lighter: 0.5,
    lightest: 0.33,
    full: 0,
    none: 1
  },
  padding: {
    huge: 39, // used to add extra padding to the bottom, as offset to the bottom menu bar (which should be 39...)
    larger: 19,
    large: 14.5,
    normal: 10,
    small: 4.5,
    smallish: 3.5,
    smaller: 2.25
  },
  form: {
    field: {
      height: 36
    }
  },
  animation: {
    duration: {
      short: 300,
      normal: 700,
      long: 1200
    },
    fadeToGreyRatio: 0.05
  }
};
