const emailRegx = /^[\w._-]+[+]?[\w._-]+@[\w.-]+\.[a-zA-Z]{2,6}$/;

const weakPass = /^(?=.*?[A-Za-z])(?=.*?[0-9]).{8,}$/; // Minimum eight characters, at least one letter and one number

const fairPass = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$/; // Minimum eight characters, at least one letter and one number

// strongPass Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character
const strongPass = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/;

export function urlify(text) {
  let urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
  let urlFromText = '';
  text.replace(urlRegex, function(url) {
      urlFromText = url
  })
  return urlFromText;
}

export default function validate(type: string, text: string): string {
  if (type === 'Email') {
    if (emailRegx.test(text)) {
      return '';
    }
    return 'Improper Email';
  }
  if (type === 'Password') {
    if (text && text.length < 8) {
      return 'Minimum 8 Charachter required'
    }
    if (weakPass.test(text)) {
      if (fairPass.test(text)) {
        if (strongPass.test(text)) {
          return 'Strong Password';
        }
        if (!strongPass.test(text)) {
          return 'Fair Password';
        }
      } else if (!fairPass.test(text)) {
        return 'Weak Password';
      }
    }
    return 'Password must be a combination of characters and numbers';
  }
  return '';
}
