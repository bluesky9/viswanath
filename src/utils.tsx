import moment from 'moment';

function customTemplate() {
    let format = 's[s]';
    if (this.duration.asSeconds() >= 0 && this.duration.asSeconds() < 60) {
      format = 's[s]';
    } else if (this.duration.asSeconds() >= 60 && this.duration.asSeconds() < 3600) {
      format = 'm[m]';
    } else if (this.duration.asSeconds() >= 3600 && this.duration.asSeconds() < 24 * 60 * 60) {
      format = 'h[h]';
    } else if (this.duration.asSeconds() >= 24 * 60 * 60 && this.duration.asSeconds() < 24 * 60 * 60 * 7) {
      format = 'd[d]';
    } else {
      format = 'w[w]';
    }
    return format;
  }

function customConversationTemplate(timeStamp, duration) {
    let format = 'HH:mm';
    if (duration.asSeconds() >= 0 && duration.asSeconds() < 24 * 60 * 60) {
      format = moment(timeStamp).format('HH:mm a')
    } else if (duration.asSeconds() >= 24 * 60 * 60 && duration.asSeconds() < 24 * 60 * 60 * 2) {
      format = 'Yesterday';
    } else if (duration.asSeconds() >= 24 * 60 * 60 * 2 && duration.asSeconds() < 24 * 60 * 60 * 7) {
      format = moment(timeStamp).format('ddd');
    } else {
      format = moment(timeStamp).format('MM/DD/YY');
    }
    return format;
}

export function transformForPosts(timeStamp) {
    let m: any = moment.duration(moment(Date.now()).diff(timeStamp, 'seconds'), 'seconds');
    m = m.format(customTemplate, {
      trim: false
    })
    return m;
}

export function transformForConversations(timestamp) {
    let m: any = moment.duration(moment(Date.now()).diff(timestamp, 'seconds'), 'seconds');
    return customConversationTemplate(timestamp, m)
}

export function transformDate(date) {
  if (typeof date === 'string') {
    let newText = date.split(' ');
    let endMonth = parseInt(moment().month(newText[0]).format('M'), 10)
    let endYear = parseInt(newText[1], 10)
    return endYear + '-' + ((endMonth > 10) ? endMonth : '0' + endMonth) + '-01'
  } else {
    return '2018-12-12'
  }
}